package com.github.theresajayne.dmjava.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Colors;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.utils.viewport.Viewport;

public class FadeWidget extends Widget {

    private Texture texture;

    FadeWidget(boolean fadeIn, float duration, final Runnable callback) {
        SequenceAction sequence = new SequenceAction();
        if (fadeIn) {
            sequence.addAction(Actions.alpha(1f));
            sequence.addAction(Actions.alpha(0f, duration, Interpolation.pow2Out));
        } else {
            sequence.addAction(Actions.alpha(0f));
            sequence.addAction(Actions.alpha(1f, duration, Interpolation.pow2In));
        }
        sequence.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    Gdx.app.postRunnable(callback);
                }
            }
        }));
        sequence.addAction(Actions.removeActor());
        addAction(sequence);
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        // Manage texture
        if (stage != null) {
            Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
            pixmap.setColor(Color.WHITE);
            pixmap.drawPixel(0, 0);
            texture = new Texture(pixmap);
            pixmap.dispose();
        } else {
            if (texture != null) {
                texture.dispose();
                texture = null;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        batch.setColor(getColor());
        Viewport viewport = getStage().getViewport();
        batch.draw(texture, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());
    }

    public static class Builder {
        private final Color color = Colors.get("DIM_COLOR") != null ? Colors.get("DIM_COLOR") : new Color(0x000000ff);
        private float duration = 1.0f;
        private boolean fadeIn = true;
        private Runnable callback;

        public Builder duration(float duration) {
            this.duration = duration;
            return this;
        }

        public Builder color(Color color) {
            this.color.set(color);
            return this;
        }

        public Builder fadeIn() {
            this.fadeIn = true;
            return this;
        }

        public Builder fadeOut() {
            this.fadeIn = false;
            return this;
        }

        public Builder callback(Runnable callback) {
            this.callback = callback;
            return this;
        }

        public void show(Stage stage) {
            FadeWidget fadeWidget = new FadeWidget(fadeIn, duration, callback);
            fadeWidget.setColor(color);
            stage.addActor(fadeWidget);
        }
    }
}