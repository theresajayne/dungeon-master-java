package com.github.theresajayne.dmjava.entities;

class WeaponClick implements ActionListener {
    WeaponClick() {
    }

    public void actionPerformed(ActionEvent var1) {
        dmnew.this.weaponready = Integer.parseInt(var1.getActionCommand());
        dmnew.this.weaponsheet.update();
    }
}
