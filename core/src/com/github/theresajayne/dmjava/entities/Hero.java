package com.github.theresajayne.dmjava.entities;

import java.util.ArrayList;

class Hero extends JComponent {
    String name;
    String lastname;
    String picname;
    Image pic;
    Item weapon;
    Item head;
    Item torso;
    Item legs;
    Item feet;
    Item hand;
    Item neck;
    Item pouch1;
    Item pouch2;
    Item[] pack;
    Item[] quiver;
    int heronumber;
    int subsquare;
    int number;
    int maxmana;
    int maxhealth;
    int maxstamina;
    int mana;
    int health;
    int stamina;
    int food;
    int water;
    int strength;
    int vitality;
    int dexterity;
    int intelligence;
    int wisdom;
    int defense;
    int magicresist;
    int flevel;
    int nlevel;
    int plevel;
    int wlevel;
    int fxp;
    int nxp;
    int pxp;
    int wxp;
    int strengthboost;
    int intelligenceboost;
    int wisdomboost;
    int dexterityboost;
    int vitalityboost;
    int defenseboost;
    int magicresistboost;
    float maxload;
    float load;
    boolean isleader;
    boolean isdead;
    boolean splready;
    boolean wepready;
    boolean ispoisoned;
    boolean silenced;
    boolean hurthead;
    boolean hurttorso;
    boolean hurtlegs;
    boolean hurtfeet;
    boolean hurthand;
    boolean hurtweapon;
    int silencecount;
    int hit;
    int poison;
    int spellcount;
    int weaponcount;
    int timecounter;
    int hurtcounter;
    int hitcounter;
    int poisoncounter;
    int walkcounter;
    int kuswordcount;
    int rosbowcount;
    String currentspell;

    public Hero(String var2) {
        this.weapon = dmnew.fistfoot;
        this.head = null;
        this.torso = null;
        this.legs = null;
        this.feet = null;
        this.hand = null;
        this.neck = null;
        this.pouch1 = null;
        this.pouch2 = null;
        this.pack = new Item[16];
        this.quiver = new Item[6];
        this.fxp = 0;
        this.nxp = 0;
        this.pxp = 0;
        this.wxp = 0;
        this.isleader = false;
        this.isdead = false;
        this.splready = true;
        this.wepready = true;
        this.ispoisoned = false;
        this.silenced = false;
        this.hurthead = false;
        this.hurttorso = false;
        this.hurtlegs = false;
        this.hurtfeet = false;
        this.hurthand = false;
        this.hurtweapon = false;
        this.silencecount = 0;
        this.poison = 0;
        this.spellcount = 0;
        this.weaponcount = 0;
        this.timecounter = 0;
        this.hurtcounter = 0;
        this.hitcounter = 0;
        this.poisoncounter = 0;
        this.walkcounter = 0;
        this.kuswordcount = 0;
        this.rosbowcount = 0;
        this.currentspell = "";
        this.setPreferredSize(new Dimension(100, 124));
        this.setBackground(Color.black);
        this.picname = var2;
        this.pic = dmnew.tk.getImage(var2);
    }

    public Hero(String var2, String var3, String var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15, int var16, int var17, int var18) {
        this.weapon = dmnew.fistfoot;
        this.head = null;
        this.torso = null;
        this.legs = null;
        this.feet = null;
        this.hand = null;
        this.neck = null;
        this.pouch1 = null;
        this.pouch2 = null;
        this.pack = new Item[16];
        this.quiver = new Item[6];
        this.fxp = 0;
        this.nxp = 0;
        this.pxp = 0;
        this.wxp = 0;
        this.isleader = false;
        this.isdead = false;
        this.splready = true;
        this.wepready = true;
        this.ispoisoned = false;
        this.silenced = false;
        this.hurthead = false;
        this.hurttorso = false;
        this.hurtlegs = false;
        this.hurtfeet = false;
        this.hurthand = false;
        this.hurtweapon = false;
        this.silencecount = 0;
        this.poison = 0;
        this.spellcount = 0;
        this.weaponcount = 0;
        this.timecounter = 0;
        this.hurtcounter = 0;
        this.hitcounter = 0;
        this.poisoncounter = 0;
        this.walkcounter = 0;
        this.kuswordcount = 0;
        this.rosbowcount = 0;
        this.currentspell = "";
        this.setPreferredSize(new Dimension(100, 124));
        this.setBackground(Color.black);
        this.picname = var2;
        this.pic = dmnew.tk.getImage(this.picname);
        this.name = var3;
        this.lastname = var4;
        this.flevel = var5;
        this.nlevel = var6;
        this.wlevel = var7;
        this.plevel = var8;
        this.maxhealth = var9;
        this.health = var9;
        this.maxstamina = var10;
        this.stamina = var10;
        this.maxmana = var11;
        this.mana = var11;
        this.strength = var12;
        this.dexterity = var13;
        this.vitality = var14;
        this.intelligence = var15;
        this.wisdom = var16;
        this.defense = var17;
        this.magicresist = var18;
        this.setMaxLoad();
        this.load = 0.0F;
        this.food = 1000;
        this.water = 1000;
    }

    public void setMaxLoad() {
        this.maxload = (float)(this.strength * 4 / 5);
        if (this.stamina < this.maxstamina / 5) {
            this.maxload = this.maxload * 2.0F / 3.0F;
        } else if (this.stamina < this.maxstamina / 3) {
            this.maxload = this.maxload * 4.0F / 5.0F;
        }

    }

    public void setLoad() {
        if (this.head != null) {
            this.load += this.head.weight;
        }

        if (this.neck != null) {
            this.load += this.neck.weight;
        }

        if (this.torso != null) {
            this.load += this.torso.weight;
        }

        if (this.legs != null) {
            this.load += this.legs.weight;
        }

        if (this.feet != null) {
            this.load += this.feet.weight;
        }

        if (this.hand != null) {
            this.load += this.hand.weight;
        }

        if (this.weapon != null) {
            this.load += this.weapon.weight;
        }

        if (this.pouch1 != null) {
            this.load += this.pouch1.weight;
        }

        if (this.pouch2 != null) {
            this.load += this.pouch2.weight;
        }

        for(int var1 = 0; var1 < 6; ++var1) {
            if (this.quiver[var1] != null) {
                this.load += this.quiver[var1].weight;
            }
        }

        for(int var2 = 0; var2 < 16; ++var2) {
            if (this.pack[var2] != null) {
                this.load += this.pack[var2].weight;
            }
        }

    }

    public void setDefense() {
        if (this.weapon != null && (this.weapon.type == 0 || this.weapon.type == 1)) {
            this.weapon.equipEffect(this);
        }

        if (this.head != null) {
            this.head.equipEffect(this);
        }

        if (this.neck != null) {
            this.neck.equipEffect(this);
        }

        if (this.torso != null) {
            this.torso.equipEffect(this);
        }

        if (this.hand != null) {
            this.hand.equipEffect(this);
        }

        if (this.legs != null) {
            this.legs.equipEffect(this);
        }

        if (this.feet != null) {
            this.feet.equipEffect(this);
        }

    }

    public void doCompass() {
        if (this.weapon.number == 8) {
            dmnew.this.compass.add(this.weapon);
        }

        if (this.hand != null && this.hand.number == 8) {
            dmnew.this.compass.add(this.hand);
        }

        for(int var1 = 0; var1 < 16; ++var1) {
            if (this.pack[var1] != null) {
                if (this.pack[var1].number == 8) {
                    dmnew.this.compass.add(this.pack[var1]);
                } else if (this.pack[var1].number == 5) {
                    for(int var2 = 0; var2 < 6; ++var2) {
                        Item var3 = ((Chest)this.pack[var1]).itemAt(var2);
                        if (var3 != null && var3.number == 8) {
                            dmnew.this.compass.add(var3);
                        }
                    }
                }
            }
        }

    }

    public void eatdrink() {
        if (dmnew.inhand.type == 7) {
            this.food += dmnew.inhand.foodvalue;
            if (this.food > 1000) {
                this.food = 1000;
            }

            dmnew.iteminhand = false;
            dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
            dmnew.inhand = null;
        } else if (dmnew.inhand.number != 72 && (dmnew.inhand.number != 73 || ((Waterskin)dmnew.inhand).drinks <= 0)) {
            if (!dmnew.inhand.ispotion) {
                return;
            }

            if (!this.usePotion(dmnew.inhand)) {
                return;
            }

            dmnew.inhand = new Item(7);
            dmnew.hero[dmnew.leader].load -= 0.2F;
        } else {
            this.water += dmnew.inhand.foodvalue;
            if (this.water > 1000) {
                this.water = 1000;
            }

            if (dmnew.inhand.number == 72) {
                dmnew.inhand = new Item(7);
                dmnew.hero[dmnew.leader].load -= 0.2F;
            } else if (dmnew.inhand.number == 73) {
                --((Waterskin)dmnew.inhand).drinks;
                if (((Waterskin)dmnew.inhand).drinks == 0) {
                    ((Waterskin)dmnew.inhand).swapPics();
                }

                dmnew.inhand.weight -= 0.2F;
                dmnew.hero[dmnew.leader].load -= 0.2F;
            }
        }

        dmnew.playSound("gulp.wav", -1, -1);
    }

    public void timePass() {
        if (!this.isdead) {
            byte var1 = 1;
            if (dmnew.this.sleeping) {
                var1 = 2;
            }

            for(int var2 = 0; var2 < var1; ++var2) {
                if (this.hurtcounter > 0) {
                    --this.hurtcounter;
                    this.repaint();
                }

                if (!this.wepready && this.weaponcount >= -1) {
                    --this.weaponcount;
                    if (this.hitcounter > 0) {
                        --this.hitcounter;
                        if (this.hitcounter == 0) {
                            dmnew.this.weaponsheet.update();
                        }
                    }

                    if (this.weaponcount <= 0) {
                        this.wepready = true;
                        if (this.hitcounter > 0) {
                            this.hitcounter = 0;
                        }

                        dmnew.this.weaponsheet.update();
                    }
                }

                if (this.silencecount > 0) {
                    --this.silencecount;
                    if (this.weapon.number == 215) {
                        --this.silencecount;
                    }

                    if (this.silencecount <= 0) {
                        this.silencecount = 0;
                        this.silenced = false;
                    }
                }

                if (this.kuswordcount > 0) {
                    --this.kuswordcount;
                    if (this.kuswordcount == 0) {
                        this.weapon = dmnew.fistfoot;
                        this.repaint();
                        dmnew.this.weaponsheet.update();
                    }
                } else if (this.rosbowcount > 0) {
                    --this.rosbowcount;
                    if (this.rosbowcount == 0) {
                        this.weapon = dmnew.fistfoot;
                        this.repaint();
                        dmnew.this.weaponsheet.update();
                    }
                }
            }

            if (this.ispoisoned) {
                ++this.poisoncounter;
                this.poisoncounter += dmnew.sleeper;
                if (this.poisoncounter > 30) {
                    this.poisoncounter = 0;
                    if (this.poison > 15) {
                        this.poison = 15;
                    }

                    this.damage(this.poison, 2);
                    this.repaint();
                    if (dmnew.sheet && this.equals(dmnew.herosheet.hero) && !dmnew.this.sleeping && !dmnew.this.viewing) {
                        dmnew.herosheet.repaint();
                    }
                }
            }

            ++this.timecounter;
            this.timecounter += dmnew.sleeper;
            if (this.timecounter > 250) {
                this.timecounter = 0;
                if (!dmnew.this.sleeping && this.weapon.number == 215) {
                    this.vitalize(-this.maxstamina / 20);
                    if (!dmnew.this.sleeping && dmnew.numheroes > 1 && dmnew.randGen.nextInt(60) == 0) {
                        int var3;
                        for(var3 = dmnew.randGen.nextInt(dmnew.numheroes); dmnew.hero[var3].equals(this); var3 %= dmnew.numheroes) {
                            ++var3;
                        }

                        dmnew.hero[var3].damage(dmnew.hero[var3].maxhealth, 4);
                        dmnew.message.setMessage("Stormbringer feeds...", 5);
                        this.maxhealth += dmnew.hero[var3].maxhealth / 10;
                        this.maxstamina += dmnew.hero[var3].maxstamina / 10;
                        this.maxmana += dmnew.hero[var3].maxmana / 10;
                        this.strength += dmnew.hero[var3].strength / 10;
                        this.vitality += dmnew.hero[var3].vitality / 10;
                        this.intelligence += dmnew.hero[var3].intelligence / 10;
                        this.health = this.maxhealth;
                        this.stamina = this.maxstamina;
                        this.mana = this.maxmana;
                        this.setMaxLoad();
                        this.repaint();
                    }
                }

                boolean var4 = this.hurtweapon || this.hurthand || this.hurthead || this.hurttorso || this.hurtlegs || this.hurtfeet;
                this.food -= 8;
                this.water -= 6;
                if (!var4) {
                    this.heal(this.vitality / 8 + 1);
                } else {
                    this.heal(this.vitality / 12 + 1);
                }

                this.stamcheck();
                if (!var4) {
                    this.vitalize(this.vitality / 8 + 1);
                } else {
                    this.vitalize(this.vitality / 16 + 1);
                }

                if (!this.hurthead) {
                    this.energize((this.intelligence + this.wisdom) / 16 + 1);
                } else {
                    this.energize((this.intelligence + this.wisdom) / 24 + 1);
                }

                if (dmnew.randGen.nextInt(100) + 20 < this.vitality || dmnew.randGen.nextInt(10) == 0) {
                    --this.poison;
                }

                if (this.poison <= 0) {
                    this.ispoisoned = false;
                }

                if (this.strengthboost > 0) {
                    --this.strengthboost;
                    --this.strength;
                } else if (this.strengthboost < 0) {
                    ++this.strengthboost;
                    ++this.strength;
                }

                if (this.dexterityboost > 0) {
                    --this.dexterityboost;
                    --this.dexterity;
                } else if (this.dexterityboost < 0) {
                    ++this.dexterityboost;
                    ++this.dexterity;
                }

                if (this.vitalityboost > 0) {
                    --this.vitalityboost;
                    --this.vitality;
                } else if (this.vitalityboost < 0) {
                    ++this.vitalityboost;
                    ++this.vitality;
                }

                if (this.intelligenceboost > 0) {
                    --this.intelligenceboost;
                    --this.intelligence;
                } else if (this.intelligenceboost < 0) {
                    ++this.intelligenceboost;
                    ++this.intelligence;
                }

                if (this.wisdomboost > 0) {
                    --this.wisdomboost;
                    --this.wisdom;
                } else if (this.wisdomboost < 0) {
                    ++this.wisdomboost;
                    ++this.wisdom;
                }

                if (this.defenseboost > 0) {
                    --this.defenseboost;
                    --this.defense;
                } else if (this.defenseboost < 0) {
                    ++this.defenseboost;
                    ++this.defense;
                }

                if (this.magicresistboost > 0) {
                    --this.magicresistboost;
                    --this.magicresist;
                } else if (this.magicresistboost < 0) {
                    ++this.magicresistboost;
                    ++this.magicresist;
                }

                this.setMaxLoad();
                this.repaint();
                if (dmnew.sheet && this.equals(dmnew.herosheet.hero) && !dmnew.this.sleeping && !dmnew.this.viewing) {
                    dmnew.herosheet.repaint();
                }
            }

            if (dmnew.sleeper > 0 || this.timecounter % 25 == 0) {
                this.stamcheck();
            }
        }

    }

    public void stamcheck() {
        if (this.food < 50) {
            this.vitalize(-this.maxstamina / 25 - 1);
            if (this.food < 11) {
                this.food = 10;
            }
        } else if (this.food < 100) {
            this.vitalize(-this.maxstamina / 50 - 1);
        }

        if (this.water < 75) {
            this.vitalize(-this.maxstamina / 25 - 1);
            if (this.water < 11) {
                this.water = 10;
            }
        } else if (this.water < 100) {
            this.vitalize(-this.maxstamina / 50 - 1);
        }

        if (this.load > this.maxload * 2.0F) {
            this.vitalize(-this.maxstamina / 50 - 1);
        }

        int var1 = (int)((float)this.stamina / (float)this.maxstamina * 100.0F);
        if (var1 < 20) {
            int var2 = (int)((float)this.maxhealth / 10.0F) + dmnew.randGen.nextInt(8);
            this.damage(var2, 2);
            if (dmnew.sheet && this.equals(dmnew.herosheet.hero) && !dmnew.this.sleeping && !dmnew.this.viewing) {
                dmnew.herosheet.repaint();
            }
        }

        this.repaint();
    }

    public void useweapon(int var1) {
        int var2;
        int var3;
        boolean var4;
        if (this.weapon.function[var1][0].equals("Shoot")) {
            if (this.hand != null && this.hand.projtype == this.weapon.projtype && (double)this.hand.weight <= 1.0D) {
                if (this.weapon.name.toLowerCase().indexOf("bow") != -1) {
                    dmnew.playSound("bow.wav", -1, -1);
                } else {
                    dmnew.playSound("swing.wav", -1, -1);
                }

                this.hand.shotpow = dmnew.randGen.nextInt() % 10 + this.weapon.power[var1] * this.strength / 24 + this.weapon.power[var1] * this.nlevel / 4;
                if (this.hand.shotpow <= 0) {
                    this.hand.shotpow = dmnew.randGen.nextInt(4) + 1;
                }

                var2 = this.subsquare;
                if (var2 == 2) {
                    var2 = 1;
                } else if (var2 == 3) {
                    var2 = 0;
                }

                this.wepThrow(this.hand, var2);
                this.hand = null;
                this.repaint();
                var3 = 1;

                for(var4 = false; var3 < 6 && !var4; ++var3) {
                    if (this.quiver[var3] != null && this.quiver[var3].projtype == this.weapon.projtype) {
                        this.hand = this.quiver[var3];
                        this.quiver[var3] = null;
                        var4 = true;
                    }
                }

                if (!var4 && this.quiver[0] != null) {
                    this.hand = this.quiver[0];
                    this.quiver[0] = null;
                    if (this.hand.number == 9) {
                        ((Torch)this.hand).setPic();
                        dmnew.updateDark();
                    }

                    if (this.hand.projtype != this.weapon.projtype) {
                        Item var5 = this.weapon;
                        this.weapon = this.hand;
                        this.hand = var5;
                        this.hand.unEquipEffect(this);
                        this.weapon.equipEffect(this);
                    }
                }

                this.weaponcount = this.weapon.speed[var1];
                if (this.dexterity < 40) {
                    ++this.weaponcount;
                    if (this.dexterity < 30) {
                        this.weaponcount += 2;
                        if (this.dexterity < 20) {
                            this.weaponcount += 2;
                            if (this.dexterity < 10) {
                                this.weaponcount += 4;
                            }
                        }
                    }
                } else if (this.dexterity > 50) {
                    --this.weaponcount;
                    if (this.dexterity > 70) {
                        --this.weaponcount;
                        if (this.dexterity > 90) {
                            --this.weaponcount;
                        }
                    }
                }

                if (this.hurthand || this.hurtweapon) {
                    this.weaponcount += 4;
                }

                if (this.weaponcount < 1) {
                    this.weaponcount = 1;
                }

                if (this.stamina < this.maxstamina / 5 || this.load > this.maxload) {
                    this.weaponcount += 4;
                }

                this.gainxp(this.weapon.function[var1][1].charAt(0), 1);
                this.vitalize(-dmnew.randGen.nextInt((int)this.weapon.weight / 2 + this.weapon.power[var1] / 3 + 2));
                this.wepready = false;
                this.repaint();
            } else {
                this.hitcounter = 2;
                dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                dmnew.this.weaponsheet.hitlabel.setText("Need Ammo");
                this.weaponcount = 2;
            }
        } else if (this.weapon.function[var1][0].equals("Magic Shot")) {
            dmnew.playSound("bow.wav", -1, -1);
            Spell var6 = new Spell(this.weapon.power[0]);
            var6.power += dmnew.randGen.nextInt((this.nlevel + 1) * 5);
            var3 = this.subsquare;
            if (var3 == 2) {
                var3 = 1;
            } else if (var3 == 3) {
                var3 = 0;
            }

            dmnew.this.new Projectile(var6, 50, dmnew.facing, var3);
            this.weaponcount = this.weapon.speed[var1];
            if (this.dexterity < 40) {
                ++this.weaponcount;
                if (this.dexterity < 30) {
                    this.weaponcount += 2;
                    if (this.dexterity < 20) {
                        this.weaponcount += 2;
                        if (this.dexterity < 10) {
                            this.weaponcount += 4;
                        }
                    }
                }
            } else if (this.dexterity > 50) {
                --this.weaponcount;
                if (this.dexterity > 70) {
                    --this.weaponcount;
                    if (this.dexterity > 90) {
                        --this.weaponcount;
                    }
                }
            }

            if (this.hurthand || this.hurtweapon) {
                this.weaponcount += 4;
            }

            if (this.weaponcount < 1) {
                this.weaponcount = 1;
            }

            if (this.stamina < this.maxstamina / 5 || this.load > this.maxload) {
                this.weaponcount += 4;
            }

            this.gainxp(this.weapon.function[var1][1].charAt(0), 1);
            this.vitalize(-dmnew.randGen.nextInt((int)this.weapon.weight / 2 + this.weapon.power[var1] / 3 + 2));
            this.wepready = false;
            this.repaint();
        } else if (this.weapon.function[var1][0].equals("Drink")) {
            if (this.usePotion(this.weapon)) {
                dmnew.playSound("gulp.wav", -1, -1);
                this.weaponcount = this.weapon.speed[var1];
                this.wepready = false;
                this.weapon = new Item(7);
                this.load -= 0.2F;
                this.repaint();
                if (dmnew.sheet && dmnew.herosheet.hero.heronumber == this.heronumber) {
                    dmnew.herosheet.repaint();
                }

            }
        } else {
            this.weaponcount = this.weapon.speed[var1];
            if (this.dexterity < 40) {
                ++this.weaponcount;
                if (this.dexterity < 30) {
                    this.weaponcount += 2;
                    if (this.dexterity < 20) {
                        this.weaponcount += 2;
                        if (this.dexterity < 10) {
                            this.weaponcount += 4;
                        }
                    }
                }
            } else if (this.dexterity > 50) {
                --this.weaponcount;
                if (this.dexterity > 70) {
                    --this.weaponcount;
                    if (this.dexterity > 90) {
                        --this.weaponcount;
                    }
                }
            }

            if (this.hurtweapon) {
                this.weaponcount += 4;
            }

            if (this.weaponcount < 1) {
                this.weaponcount = 1;
            }

            if (this.stamina < this.maxstamina / 5 || this.load > this.maxload) {
                this.weaponcount += 4;
            }

            if (!this.weapon.function[var1][0].equals("Climb Down") && !this.weapon.function[var1][0].equals("Climb Up")) {
                this.gainxp(this.weapon.function[var1][1].charAt(0), 1);
            }

            this.vitalize(-dmnew.randGen.nextInt((int)this.weapon.weight / 2 + this.weapon.power[var1] / 3 + 2));
            this.wepready = false;
            if (this.weapon.function[var1][0].equals("Throw")) {
                dmnew.playSound("swing.wav", -1, -1);
                this.weapon.shotpow = this.strength / 10 + dmnew.randGen.nextInt(4);
                var2 = this.subsquare;
                if (var2 == 2) {
                    var2 = 1;
                } else if (var2 == 3) {
                    var2 = 0;
                }

                this.wepThrow(this.weapon, var2);
                this.weapon = dmnew.fistfoot;
                this.weapon.unEquipEffect(this);
                this.repaint();
                var3 = 1;

                for(var4 = false; var3 < 6 && !var4; ++var3) {
                    if (this.quiver[var3] != null) {
                        this.weapon = this.quiver[var3];
                        this.quiver[var3] = null;
                        var4 = true;
                    }
                }

                if (!var4 && this.quiver[0] != null) {
                    this.weapon = this.quiver[0];
                    this.quiver[0] = null;
                    if (this.weapon.number == 9) {
                        ((Torch)this.weapon).setPic();
                        dmnew.updateDark();
                    }

                    this.weapon.equipEffect(this);
                }
            } else if (!this.checkmagic(var1)) {
                dmnew.playSound("swing.wav", -1, -1);
                if (!this.checkmon(var1)) {
                    this.checkdoor(var1);
                }
            }

            this.repaint();
        }
    }

    public boolean checkmagic(int var1) {
        int var5;
        byte var21;
        dmnew.Monster var22;
        byte var26;
        if (this.weapon.function[var1][0].equals("Blow Horn")) {
            var26 = 0;
            var21 = 0;
            if (dmnew.facing == 0) {
                var21 = 1;
            } else if (dmnew.facing == 1) {
                var26 = 1;
            } else if (dmnew.facing == 2) {
                var21 = -1;
            } else {
                var26 = -1;
            }

            for(var5 = 0; var5 < 6; ++var5) {
                var22 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var26) + "," + (dmnew.partyy - var21) + "," + var5);
                if (var22 != null && dmnew.randGen.nextInt(11) < var22.fearresist) {
                    var22.runcounter += this.weapon.power[var1];
                    var22.wasfrightened = true;
                }

                if (var5 == 3) {
                    ++var5;
                }
            }

            return true;
        } else if (this.weapon.function[var1][0].equals("Climb Down")) {
            var26 = 0;
            var21 = 0;
            if (dmnew.facing == 0) {
                var21 = 1;
            } else if (dmnew.facing == 1) {
                var26 = 1;
            } else if (dmnew.facing == 2) {
                var21 = -1;
            } else {
                var26 = -1;
            }

            if (dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21].mapchar != 'p' || !((Pit)dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21]).isOpen || dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21].hasMons || dmnew.DungeonMap[dmnew.level + 1][dmnew.partyx - var26][dmnew.partyy - var21].hasMons || dmnew.DungeonMap[dmnew.level + 1][dmnew.partyx - var26][dmnew.partyy - var21].mapchar == 'p' && (dmnew.DungeonMap[dmnew.level + 1][dmnew.partyx - var26][dmnew.partyy - var21].mapchar != 'p' || ((Pit)dmnew.DungeonMap[dmnew.level + 1][dmnew.partyx - var26][dmnew.partyy - var21]).isOpen)) {
                dmnew.message.setMessage("Can't Climb Down.", 4);
                dmnew.this.weaponsheet.hitlabel.setText("Can't Climb");
                dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                this.hitcounter = 2;
            } else {
                dmnew.climbing = true;
                dmnew.this.dmove.partyMove(3);
                this.gainxp('n', 1);
            }

            return true;
        } else {
            int var4;
            if (this.weapon.function[var1][0].equals("Climb Up")) {
                var26 = 0;
                var21 = 0;
                if (dmnew.facing == 0) {
                    var21 = 1;
                } else if (dmnew.facing == 1) {
                    var26 = 1;
                } else if (dmnew.facing == 2) {
                    var21 = -1;
                } else {
                    var26 = -1;
                }

                if (dmnew.level > 0 && dmnew.DungeonMap[dmnew.level - 1][dmnew.partyx][dmnew.partyy].mapchar == 'p' && ((Pit)dmnew.DungeonMap[dmnew.level - 1][dmnew.partyx][dmnew.partyy]).isOpen && dmnew.DungeonMap[dmnew.level - 1][dmnew.partyx - var26][dmnew.partyy - var21].isPassable && !dmnew.DungeonMap[dmnew.level - 1][dmnew.partyx - var26][dmnew.partyy - var21].hasMons) {
                    --dmnew.level;
                    dmnew.this.dmove.partyMove(3);

                    for(var4 = 0; var4 < dmnew.numheroes; ++var4) {
                        if (!dmnew.hero[var4].isdead) {
                            dmnew.hero[var4].energize((int)(-dmnew.hero[var4].load));
                        }
                    }

                    this.gainxp('n', 1);
                } else {
                    dmnew.message.setMessage("Can't Climb Up.", 4);
                    dmnew.this.weaponsheet.hitlabel.setText("Can't Climb");
                    dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                    this.hitcounter = 2;
                }

                return true;
            } else if (this.weapon.function[var1][0].equals("Detect Illusion")) {
                var26 = 0;
                var21 = 0;
                if (dmnew.facing == 0) {
                    var21 = 1;
                } else if (dmnew.facing == 1) {
                    var26 = 1;
                } else if (dmnew.facing == 2) {
                    var21 = -1;
                } else {
                    var26 = -1;
                }

                if (dmnew.randGen.nextInt(16 - this.nlevel) < 5) {
                    if (dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21].mapchar != '2' && (dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21].mapchar != 'p' || !((Pit)dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21]).isIllusionary)) {
                        dmnew.message.setMessage(this.name + " senses there is no illusion.", this.heronumber);
                    } else {
                        dmnew.message.setMessage(this.name + " senses an illusion.", this.heronumber);
                        if (dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21].mapchar == 'p') {
                            ((Pit)dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21]).isIllusionary = false;
                        } else {
                            Floor var28 = new Floor();
                            MapObject var24 = dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21];
                            var28.numProjs = var24.numProjs;
                            var28.hasMons = var24.hasMons;
                            var28.hasItems = var24.hasItems;
                            var28.hasCloud = var24.hasCloud;
                            if (var28.hasItems) {
                                while(!var24.mapItems.isEmpty()) {
                                    var28.mapItems.add(var24.mapItems.remove(0));
                                }
                            }

                            dmnew.DungeonMap[dmnew.level][dmnew.partyx - var26][dmnew.partyy - var21] = var28;
                        }

                        dmnew.needredraw = true;
                    }

                    this.gainxp('n', 1);
                } else {
                    dmnew.message.setMessage(this.name + " fails to sense anything.", 4);
                }

                return true;
            } else if (this.weapon.charges[var1] == 0) {
                return false;
            } else {
                int var25;
                if (this.weapon.function[var1][0].equals("Heal")) {
                    this.heal(this.maxhealth * this.weapon.power[var1] / 100 + 5);
                    this.repaint();
                } else {
                    int var2;
                    Spell var3;
                    if (this.weapon.function[var1][0].equals("Fireball")) {
                        var2 = this.subsquare;
                        if (var2 == 2) {
                            var2 = 1;
                        } else if (var2 == 3) {
                            var2 = 0;
                        }

                        try {
                            var3 = new Spell(this.weapon.power[var1] + "44");

                            for(var4 = var3.gain - 1; var4 >= 0; --var4) {
                                var3.powers[var4] += dmnew.randGen.nextInt() % 10 + var4 * this.wlevel;
                                if (this.wlevel == 15) {
                                    var3.powers[var4] += dmnew.randGen.nextInt(20);
                                }

                                if (var3.powers[var4] < 1) {
                                    var3.powers[var4] = dmnew.randGen.nextInt(4) + 1;
                                }
                            }

                            var3.power = var3.powers[var3.gain - 1];
                            dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                        } catch (Exception var20) {
                            ;
                        }
                    } else if (this.weapon.function[var1][0].equals("Dispell")) {
                        var2 = this.subsquare;
                        if (var2 == 2) {
                            var2 = 1;
                        } else if (var2 == 3) {
                            var2 = 0;
                        }

                        try {
                            var3 = new Spell(this.weapon.power[var1] + "52");

                            for(var4 = var3.gain - 1; var4 >= 0; --var4) {
                                var3.powers[var4] += dmnew.randGen.nextInt() % 10 + var4 * this.wlevel;
                                if (this.wlevel == 15) {
                                    var3.powers[var4] += dmnew.randGen.nextInt(20);
                                }

                                if (var3.powers[var4] < 1) {
                                    var3.powers[var4] = dmnew.randGen.nextInt(4) + 1;
                                }
                            }

                            var3.power = var3.powers[var3.gain - 1];
                            dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                        } catch (Exception var19) {
                            ;
                        }
                    } else if (this.weapon.function[var1][0].equals("Bolt")) {
                        var2 = this.subsquare;
                        if (var2 == 2) {
                            var2 = 1;
                        } else if (var2 == 3) {
                            var2 = 0;
                        }

                        try {
                            var3 = new Spell(this.weapon.power[var1] + "335");

                            for(var4 = var3.gain - 1; var4 >= 0; --var4) {
                                var3.powers[var4] += dmnew.randGen.nextInt() % 10 + var4 * this.wlevel;
                                if (this.wlevel == 15) {
                                    var3.powers[var4] += dmnew.randGen.nextInt(20);
                                }

                                if (var3.powers[var4] < 1) {
                                    var3.powers[var4] = dmnew.randGen.nextInt(4) + 1;
                                }
                            }

                            var3.power = var3.powers[var3.gain - 1];
                            dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                        } catch (Exception var18) {
                            ;
                        }
                    } else if (this.weapon.function[var1][0].equals("Venom")) {
                        var2 = this.subsquare;
                        if (var2 == 2) {
                            var2 = 1;
                        } else if (var2 == 3) {
                            var2 = 0;
                        }

                        try {
                            var3 = new Spell(this.weapon.power[var1] + "51");

                            for(var4 = var3.gain - 1; var4 >= 0; --var4) {
                                var3.powers[var4] += dmnew.randGen.nextInt() % 10 + var4 * this.wlevel;
                                if (this.wlevel == 15) {
                                    var3.powers[var4] += dmnew.randGen.nextInt(20);
                                }

                                if (var3.powers[var4] < 1) {
                                    var3.powers[var4] = dmnew.randGen.nextInt(4) + 1;
                                }
                            }

                            var3.power = var3.powers[var3.gain - 1];
                            dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                        } catch (Exception var17) {
                            ;
                        }
                    } else if (this.weapon.function[var1][0].equals("Ven Cloud")) {
                        var2 = this.subsquare;
                        if (var2 == 2) {
                            var2 = 1;
                        } else if (var2 == 3) {
                            var2 = 0;
                        }

                        try {
                            var3 = new Spell(this.weapon.power[var1] + "31");
                            dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                        } catch (Exception var14) {
                            ;
                        }
                    } else if (this.weapon.function[var1][0].equals("Shield")) {
                        for(var2 = 0; var2 < dmnew.numheroes; ++var2) {
                            if (dmnew.hero[var2].defenseboost < this.weapon.power[var1]) {
                                dmnew.hero[var2].defense -= dmnew.hero[var2].defenseboost;
                                dmnew.hero[var2].defenseboost = this.weapon.power[var1];
                                dmnew.hero[var2].defense += this.weapon.power[var1];
                                dmnew.hero[var2].repaint();
                            }
                        }
                    } else if (this.weapon.function[var1][0].equals("SpellShield")) {
                        for(var2 = 0; var2 < dmnew.numheroes; ++var2) {
                            if (dmnew.hero[var2].magicresistboost < this.weapon.power[var1]) {
                                dmnew.hero[var2].magicresist -= dmnew.hero[var2].magicresistboost;
                                dmnew.hero[var2].magicresistboost = this.weapon.power[var1];
                                dmnew.hero[var2].magicresist += this.weapon.power[var1];
                                dmnew.hero[var2].repaint();
                            }
                        }
                    } else if (this.weapon.function[var1][0].equals("Freeze")) {
                        var26 = 0;
                        var21 = 0;
                        if (dmnew.facing == 0) {
                            var21 = 1;
                        } else if (dmnew.facing == 1) {
                            var26 = 1;
                        } else if (dmnew.facing == 2) {
                            var21 = -1;
                        } else {
                            var26 = -1;
                        }

                        for(var5 = 0; var5 < 6; ++var5) {
                            var22 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var26) + "," + (dmnew.partyy - var21) + "," + var5);
                            if (var22 != null && var22.number != 26) {
                                var22.timecounter = -this.weapon.power[var1];
                            }

                            if (var5 == 3) {
                                ++var5;
                            }
                        }
                    } else if (this.weapon.function[var1][0].equals("Freeze Life")) {
                        dmnew.freezelife += this.weapon.power[var1];
                    } else if (this.weapon.function[var1][0].equals("Sight")) {
                        dmnew.magicvision += this.weapon.power[var1];
                        dmnew.needredraw = true;
                    } else if (this.weapon.function[var1][0].equals("Anti-Ven")) {
                        this.poison -= this.weapon.power[var1];
                        if (this.poison <= 0) {
                            this.ispoisoned = false;
                            this.poison = 0;
                        }

                        this.repaint();
                    } else if (this.weapon.function[var1][0].equals("Light")) {
                        dmnew.darkcounter = 0;
                        dmnew.magictorch += this.weapon.power[var1];
                        if (dmnew.magictorch > 285) {
                            dmnew.magictorch = 285;
                        }

                        if (dmnew.darkfactor + this.weapon.power[var1] > 255) {
                            dmnew.darkfactor = 255;
                        } else {
                            dmnew.darkfactor += this.weapon.power[var1];
                        }

                        dmnew.needredraw = true;
                    } else if (!this.weapon.function[var1][0].equals("Frighten") && !this.weapon.function[var1][0].equals("Calm")) {
                        if (this.weapon.function[var1][0].equals("Arc Bolt")) {
                            var2 = this.subsquare;
                            if (var2 == 2) {
                                var2 = 1;
                            } else if (var2 == 3) {
                                var2 = 0;
                            }

                            try {
                                var3 = new Spell(this.weapon.power[var1] + "642");

                                for(var4 = var3.gain - 1; var4 >= 0; --var4) {
                                    var3.powers[var4] += dmnew.randGen.nextInt() % 10 + var4 * this.wlevel;
                                    if (this.wlevel == 15) {
                                        var3.powers[var4] += dmnew.randGen.nextInt(20);
                                    }

                                    if (var3.powers[var4] < 1) {
                                        var3.powers[var4] = dmnew.randGen.nextInt(4) + 1;
                                    }
                                }

                                var3.power = var3.powers[var3.gain - 1];
                                dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                            } catch (Exception var16) {
                                ;
                            }
                        } else if (this.weapon.function[var1][0].equals("Slow")) {
                            var2 = this.subsquare;
                            if (var2 == 2) {
                                var2 = 1;
                            } else if (var2 == 3) {
                                var2 = 0;
                            }

                            try {
                                var3 = new Spell(this.weapon.power[var1] + "362");
                                dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                            } catch (Exception var13) {
                                ;
                            }
                        } else if (this.weapon.function[var1][0].equals("Slowfall")) {
                            dmnew.floatcounter += this.weapon.power[var1];
                            if (!dmnew.climbing) {
                                dmnew.message.setMessage("Slowfall active.", 4);
                            }

                            dmnew.climbing = true;
                        } else if (this.weapon.function[var1][0].equals("Silence")) {
                            var2 = this.subsquare;
                            if (var2 == 2) {
                                var2 = 1;
                            } else if (var2 == 3) {
                                var2 = 0;
                            }

                            try {
                                var3 = new Spell(this.weapon.power[var1] + "523");
                                dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                            } catch (Exception var12) {
                                ;
                            }
                        } else if (this.weapon.function[var1][0].equals("Weakness")) {
                            var2 = this.subsquare;
                            if (var2 == 2) {
                                var2 = 1;
                            } else if (var2 == 3) {
                                var2 = 0;
                            }

                            try {
                                var3 = new Spell(this.weapon.power[var1] + "461");
                                dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                            } catch (Exception var11) {
                                ;
                            }
                        } else if (this.weapon.function[var1][0].equals("Feeble Mind")) {
                            var2 = this.subsquare;
                            if (var2 == 2) {
                                var2 = 1;
                            } else if (var2 == 3) {
                                var2 = 0;
                            }

                            try {
                                var3 = new Spell(this.weapon.power[var1] + "363");
                                dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                            } catch (Exception var10) {
                                ;
                            }
                        } else if (this.weapon.function[var1][0].equals("Strip Defenses")) {
                            var2 = this.subsquare;
                            if (var2 == 2) {
                                var2 = 1;
                            } else if (var2 == 3) {
                                var2 = 0;
                            }

                            try {
                                var3 = new Spell(this.weapon.power[var1] + "664");
                                dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                            } catch (Exception var9) {
                                ;
                            }
                        } else {
                            int var6;
                            if (this.weapon.function[var1][0].equals("Drain Life")) {
                                var2 = dmnew.partyx;
                                var25 = dmnew.partyy;
                                if (dmnew.facing == 0) {
                                    --var25;
                                } else if (dmnew.facing == 1) {
                                    --var2;
                                } else if (dmnew.facing == 2) {
                                    ++var25;
                                } else {
                                    ++var2;
                                }

                                var22 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + var2 + "," + var25 + "," + 5);

                                for(var5 = 3; var22 == null && var5 >= 0; --var5) {
                                    var22 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + var2 + "," + var25 + "," + (var5 - dmnew.facing + 4) % 4);
                                }

                                if (var22 == null) {
                                    return true;
                                }

                                var6 = var22.damage(this.weapon.power[var1], 7);
                                if (var6 >= 0) {
                                    this.heal(var6);
                                    dmnew.this.weaponsheet.hitlabel.setText("" + var6);
                                    if (var6 < 50) {
                                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon3);
                                    } else if (var6 < 100) {
                                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon2);
                                    } else {
                                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon);
                                    }

                                    this.hitcounter = 2;
                                } else {
                                    this.damage(var6, 1);
                                }
                            } else if (this.weapon.function[var1][0].equals("Invoke")) {
                                var2 = this.subsquare;
                                if (var2 == 2) {
                                    var2 = 1;
                                } else if (var2 == 3) {
                                    var2 = 0;
                                }

                                String[] var27 = new String[]{"44", "52", "335", "31", "51", "44"};
                                var4 = dmnew.randGen.nextInt(this.wlevel / 2) + this.wlevel / 2 + 1;
                                if (var4 > 6) {
                                    var4 = 6;
                                }

                                try {
                                    Spell var23 = new Spell(var4 + var27[dmnew.randGen.nextInt(6)]);
                                    var23.power += dmnew.randGen.nextInt(20) + 20;

                                    for(var6 = var23.gain - 2; var6 >= 0; --var6) {
                                        var23.powers[var6] += dmnew.randGen.nextInt(20) + 20;
                                    }

                                    dmnew.this.new Projectile(var23, var23.dist, dmnew.facing, var2);
                                    this.energize(-dmnew.randGen.nextInt(10) - 15);
                                } catch (Exception var15) {
                                    var15.printStackTrace();
                                }
                            } else if (this.weapon.function[var1][0].equals("Fuse")) {
                                var2 = dmnew.partyx;
                                var25 = dmnew.partyy;
                                if (dmnew.facing == 0) {
                                    --var25;
                                } else if (dmnew.facing == 1) {
                                    --var2;
                                } else if (dmnew.facing == 2) {
                                    ++var25;
                                } else {
                                    ++var2;
                                }

                                var4 = this.subsquare;
                                if (var4 == 2) {
                                    var4 = 1;
                                } else if (var4 == 3) {
                                    var4 = 0;
                                }

                                dmnew.this.new Projectile(new Spell(), 1, dmnew.facing, var4);
                            } else if (this.weapon.function[var1][0].equals("Fluxcage")) {
                                var2 = dmnew.partyx;
                                var25 = dmnew.partyy;
                                if (dmnew.facing == 0) {
                                    --var25;
                                } else if (dmnew.facing == 1) {
                                    --var2;
                                } else if (dmnew.facing == 2) {
                                    ++var25;
                                } else {
                                    ++var2;
                                }

                                if (!(dmnew.DungeonMap[dmnew.level][var2][var25] instanceof Wall)) {
                                    dmnew.this.new FluxCage(dmnew.level, var2, var25);
                                    dmnew.needredraw = true;
                                }
                            } else if (this.weapon.function[var1][0].equals("Ruiner")) {
                                var2 = this.subsquare;
                                if (var2 == 2) {
                                    var2 = 1;
                                } else if (var2 == 3) {
                                    var2 = 0;
                                }

                                try {
                                    var3 = new Spell(this.weapon.power[var1] + "461");
                                    dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                                    var3 = new Spell(this.weapon.power[var1] + "363");
                                    dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                                    var3 = new Spell(this.weapon.power[var1] + "362");
                                    dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                                    var3 = new Spell(this.weapon.power[var1] + "664");
                                    dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                                    var3 = new Spell(this.weapon.power[var1] + "523");
                                    dmnew.this.new Projectile(var3, var3.dist, dmnew.facing, var2);
                                } catch (Exception var8) {
                                    ;
                                }
                            } else if (this.weapon.function[var1][0].equals("True Sight")) {
                                dmnew.dispell += this.weapon.power[var1];
                                if (dmnew.dispell == this.weapon.power[var1]) {
                                    dmnew.needredraw = true;
                                }
                            }
                        }
                    } else {
                        var26 = 0;
                        var21 = 0;
                        if (dmnew.facing == 0) {
                            var21 = 1;
                        } else if (dmnew.facing == 1) {
                            var26 = 1;
                        } else if (dmnew.facing == 2) {
                            var21 = -1;
                        } else {
                            var26 = -1;
                        }

                        for(var5 = 0; var5 < 6; ++var5) {
                            var22 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var26) + "," + (dmnew.partyy - var21) + "," + var5);
                            if (var22 != null && dmnew.randGen.nextInt(11) < var22.fearresist) {
                                var22.runcounter += this.weapon.power[var1];
                                var22.wasfrightened = true;
                            }

                            if (var5 == 3) {
                                ++var5;
                            }
                        }
                    }
                }

                if (this.weapon.charges[var1] == -1) {
                    return true;
                } else {
                    --this.weapon.charges[var1];
                    if (this.weapon.charges[var1] < 1) {
                        --this.weapon.functions;
                        if (var1 == 1 && this.weapon.functions == 2) {
                            this.weapon.function[1][0] = this.weapon.function[2][0];
                            this.weapon.function[1][1] = this.weapon.function[2][1];
                            this.weapon.power[1] = this.weapon.power[2];
                            this.weapon.charges[1] = this.weapon.charges[2];
                        } else if (var1 == 0) {
                            if (this.weapon.functions <= 0) {
                                this.load -= this.weapon.weight;
                                this.weapon = dmnew.fistfoot;
                                this.repaint();
                                return true;
                            }

                            this.weapon.function[0][0] = this.weapon.function[1][0];
                            this.weapon.function[0][1] = this.weapon.function[1][1];
                            this.weapon.power[0] = this.weapon.power[1];
                            this.weapon.charges[0] = this.weapon.charges[1];
                        }

                        if (this.weapon.upic != null) {
                            boolean var29 = false;

                            for(var25 = 0; var25 < this.weapon.functions; ++var25) {
                                if (this.weapon.charges[var25] != 0) {
                                    var29 = true;
                                }
                            }

                            if (!var29) {
                                this.weapon.pic.flush();
                                if (this.weapon.temppic != null) {
                                    this.weapon.temppic.flush();
                                }

                                this.weapon.pic = this.weapon.upic;
                                this.weapon.temppic = this.weapon.upic;
                                this.weapon.picstring = this.weapon.usedupstring;
                                if (this.weapon.epic != null) {
                                    this.weapon.epic.flush();
                                    this.weapon.epic = this.weapon.upic;
                                    this.weapon.equipstring = this.weapon.usedupstring;
                                }

                                this.repaint();
                            }
                        }
                    }

                    return true;
                }
            }
        }
    }

    public boolean checkmon(int var1) {
        byte var2 = 0;
        byte var3 = 0;
        if (dmnew.facing == 0) {
            var3 = 1;
        } else if (dmnew.facing == 1) {
            var2 = 1;
        } else if (dmnew.facing == 2) {
            var3 = -1;
        } else {
            var2 = -1;
        }

        if (this.subsquare == 2 && dmnew.heroatsub[1] != -1 || this.subsquare == 3 && dmnew.heroatsub[0] != -1) {
            if (!dmnew.DungeonMap[dmnew.level][dmnew.partyx - var2][dmnew.partyy - var3].hasMons) {
                return false;
            } else {
                dmnew.message.setMessage(this.name + " can't reach.", 4);
                dmnew.this.weaponsheet.hitlabel.setText("Can't Reach");
                dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                this.hitcounter = 2;
                return true;
            }
        } else {
            dmnew.Monster var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var2) + "," + (dmnew.partyy - var3) + "," + 5);
            int var6;
            int var8;
            if (var4 == null) {
                boolean[] var5 = new boolean[4];
                var6 = dmnew.randGen.nextInt(2) + 2;
                int var7 = (var6 - dmnew.facing + 4) % 4;
                var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var2) + "," + (dmnew.partyy - var3) + "," + var7);
                var5[var7] = var4 != null && (var4.isImmaterial && !this.weapon.hitsImmaterial || !var4.isImmaterial && this.weapon.function[var1][0].equals("Disrupt"));
                if (var4 == null || var5[var7]) {
                    byte var12;
                    if (var6 == 2) {
                        var12 = 3;
                    } else {
                        var12 = 2;
                    }

                    var7 = (var12 - dmnew.facing + 4) % 4;
                    var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var2) + "," + (dmnew.partyy - var3) + "," + var7);
                    var5[var7] = var4 != null && (var4.isImmaterial && !this.weapon.hitsImmaterial || !var4.isImmaterial && this.weapon.function[var1][0].equals("Disrupt"));
                    if (var4 == null || var5[var7]) {
                        var6 = dmnew.randGen.nextInt(2);
                        var7 = (var6 - dmnew.facing + 4) % 4;
                        var8 = (3 - var6 - dmnew.facing + 4) % 4;
                        var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var2) + "," + (dmnew.partyy - var3) + "," + var7);
                        var5[var7] = var4 != null && (var4.isImmaterial && !this.weapon.hitsImmaterial || !var4.isImmaterial && this.weapon.function[var1][0].equals("Disrupt"));
                        if (var4 == null || var5[var8] || var5[var7]) {
                            if (var6 == 0) {
                                var12 = 1;
                            } else {
                                var12 = 0;
                            }

                            var7 = (var12 - dmnew.facing + 4) % 4;
                            var8 = (3 - var12 - dmnew.facing + 4) % 4;
                            var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + (dmnew.partyx - var2) + "," + (dmnew.partyy - var3) + "," + var7);
                            if (var5[var8]) {
                                var4 = null;
                            } else if (var4 != null && (var4.isImmaterial && !this.weapon.hitsImmaterial || !var4.isImmaterial && this.weapon.function[var1][0].equals("Disrupt"))) {
                                var4 = null;
                            }
                        }
                    }
                }
            } else if (var4.isImmaterial && !this.weapon.hitsImmaterial || !var4.isImmaterial && this.weapon.function[var1][0].equals("Disrupt")) {
                var4 = null;
            }

            if (var4 == null) {
                return false;
            } else {
                this.hitcounter = 2;
                if (dmnew.randGen.nextInt(20 + this.nlevel) == 0 && this.nlevel < 15) {
                    dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                    dmnew.this.weaponsheet.hitlabel.setText("Critical Miss");
                    this.weaponcount += 4;
                    dmnew.message.setMessage(this.name + ": Critical Miss", this.heronumber);
                    return true;
                } else {
                    int var11 = this.dexterity - var4.speed;
                    if (this.stamina < this.maxstamina / 4) {
                        var11 -= 5;
                    }

                    if (this.load > this.maxload) {
                        var11 -= 5;
                    }

                    if (this.hurtweapon) {
                        var11 -= 10;
                    }

                    var6 = 0;
                    if (var4.subsquare != 5 && (var4.subsquare + dmnew.facing) % 4 < 2) {
                        var11 -= 10;
                        ++var6;
                    }

                    boolean var13;
                    if (var11 > 40) {
                        var13 = dmnew.randGen.nextInt(8) != 0;
                    } else if (var11 > 30) {
                        var13 = dmnew.randGen.nextInt(7) != 0;
                    } else if (var11 > 20) {
                        var13 = dmnew.randGen.nextInt(6) != 0;
                    } else if (var11 > 10) {
                        var13 = dmnew.randGen.nextInt(5) != 0;
                    } else if (var11 > 0) {
                        var13 = dmnew.randGen.nextInt(4) != 0;
                    } else if (var11 > -10) {
                        var13 = dmnew.randGen.nextInt(3) != 0;
                    } else if (var11 > -20) {
                        var13 = dmnew.randGen.nextInt(2) != 0;
                    } else if (var11 > -30) {
                        var13 = dmnew.randGen.nextInt(3) == 0;
                    } else {
                        var13 = dmnew.randGen.nextInt(4) == 0;
                    }

                    if (var13) {
                        var8 = dmnew.randGen.nextInt() % 10 + this.weapon.power[var1] * this.strength / 12;
                        if (var6 > 0) {
                            var8 = var8 * 2 / 3;
                        }

                        if (this.nlevel > 0 && this.weapon.function[var1][1].equals("n")) {
                            var8 += (dmnew.randGen.nextInt(this.nlevel) + 1) * 3;
                        }

                        if (this.hurtweapon) {
                            var8 /= 2;
                        }

                        if (var8 < 1) {
                            var8 = dmnew.randGen.nextInt(4);
                        }

                        if (this.weapon.function[var1][0].equals("Parry")) {
                            var4.parry = this.heronumber;
                        } else if (this.dexterity > 50 && dmnew.randGen.nextInt(20 - 2 * this.nlevel / 3) == 0) {
                            var8 = 3 * var8 / 2;
                            dmnew.message.setMessage(this.name + ": Critical Hit", this.heronumber);
                        }

                        this.gainxp(this.weapon.function[var1][1].charAt(0), 1);
                        if (var4.hurtitem != 0 && this.weapon.number != var4.hurtitem && this.weapon.number != 215 && (var4.hurtitem != 248 || this.weapon.number != 249)) {
                            dmnew.message.setMessage(this.name + "'s Weapon Has No Effect.", 4);
                            return true;
                        }

                        int var9 = 0;
                        if (this.weapon.number == 206 && (var4.number == 3 || var4.number == 21 || var4.number == 23 || var4.defense > 79)) {
                            var9 = var4.defense;
                            var4.defense /= 2;
                        }

                        if (this.weapon.number != 215) {
                            var8 = var4.damage(var8, 0);
                        } else {
                            var8 = var4.damage(var8, 4);
                        }

                        if (var8 < 50) {
                            dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon3);
                        } else if (var8 < 100) {
                            dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon2);
                        } else {
                            dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon);
                        }

                        dmnew.this.weaponsheet.hitlabel.setText("" + var8);
                        if (var9 > 0) {
                            var4.defense = var9;
                        }

                        if (this.weapon.poisonous > 0 && dmnew.randGen.nextBoolean()) {
                            var4.ispoisoned = true;
                            var4.poisonpow += this.weapon.poisonous;
                        }

                        if (this.weapon.function[var1][0].equals("Stun") && var4.number != 26 && var4.health < var4.maxhealth / 3 && dmnew.randGen.nextBoolean()) {
                            var4.timecounter = -this.weapon.power[var1];
                        }

                        if (this.weapon.number == 215 && var8 > 0) {
                            this.heal(var8);
                            this.vitalize(var8);
                            this.energize(var8);
                            if (var4.isdying && dmnew.numheroes > 1 && dmnew.randGen.nextInt(50) == 0) {
                                int var10;
                                for(var10 = dmnew.randGen.nextInt(dmnew.numheroes); dmnew.hero[var10].equals(this); var10 %= dmnew.numheroes) {
                                    ++var10;
                                }

                                dmnew.hero[var10].damage(dmnew.hero[var10].maxhealth, 4);
                                dmnew.message.setMessage("Stormbringer feeds...", 5);
                                this.maxhealth += dmnew.hero[var10].maxhealth / 10;
                                this.maxstamina += dmnew.hero[var10].maxstamina / 10;
                                this.maxmana += dmnew.hero[var10].maxmana / 10;
                                this.strength += dmnew.hero[var10].strength / 10;
                                this.vitality += dmnew.hero[var10].vitality / 10;
                                this.intelligence += dmnew.hero[var10].intelligence / 10;
                                this.health = this.maxhealth;
                                this.stamina = this.maxstamina;
                                this.mana = this.maxmana;
                                this.setMaxLoad();
                                this.repaint();
                            }
                        }
                    } else {
                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                        dmnew.this.weaponsheet.hitlabel.setText("Miss");
                    }

                    return true;
                }
            }
        }
    }

    public void checkdoor(int var1) {
        if (this.weapon.number <= 235 && this.weapon.number != 9 && (this.weapon.function[var1][0].equals("Swing") || this.weapon.function[var1][0].equals("Chop") || this.weapon.function[var1][0].equals("Melee") || this.weapon.function[var1][0].equals("Bash") || this.weapon.function[var1][0].equals("Crush") || this.weapon.function[var1][0].equals("Berzerk"))) {
            if (this.subsquare == 2 && dmnew.heroatsub[1] != -1 || this.subsquare == 3 && dmnew.heroatsub[0] != -1) {
                dmnew.message.setMessage(this.name + " can't reach.", 4);
                dmnew.this.weaponsheet.hitlabel.setText("Can't Reach");
                dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.missicon);
                this.hitcounter = 2;
            } else {
                byte var2 = 0;
                byte var3 = 0;
                if (dmnew.facing == 0) {
                    var3 = 1;
                } else if (dmnew.facing == 1) {
                    var2 = 1;
                } else if (dmnew.facing == 2) {
                    var3 = -1;
                } else {
                    var2 = -1;
                }

                if (dmnew.DungeonMap[dmnew.level][dmnew.partyx - var2][dmnew.partyy - var3].mapchar == 'd') {
                    int var4 = dmnew.randGen.nextInt() % 10 + this.weapon.power[var1] * this.strength / 12;
                    ((Door)dmnew.DungeonMap[dmnew.level][dmnew.partyx - var2][dmnew.partyy - var3]).breakDoor(var4, true, true);
                    dmnew.this.weaponsheet.hitlabel.setText("" + var4);
                    if (var4 < 50) {
                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon3);
                    } else if (var4 < 100) {
                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon2);
                    } else {
                        dmnew.this.weaponsheet.hitlabel.setIcon(dmnew.this.weaponsheet.hiticon);
                    }

                    this.hitcounter = 2;
                }

            }
        }
    }

    public void gainxp(char var1, int var2) {
        switch(var1) {
            case 'n':
                if (this.nlevel < 15) {
                    this.nxp += var2;
                    if (this.nxp > this.nlevel * 30 + 50) {
                        this.levelgain('n');
                    }
                }
                break;
            case 'p':
                if (this.plevel < 15) {
                    this.pxp += var2;
                    if (this.neck != null && this.neck.number == 92) {
                        ++this.pxp;
                    }

                    if (this.pxp > this.plevel * 30 + 50) {
                        this.levelgain('p');
                    }
                }
                break;
            case 'w':
                if (this.wlevel < 15) {
                    this.wxp += var2;
                    if (this.wxp > this.wlevel * 30 + 50) {
                        this.levelgain('w');
                    }
                }
                break;
            default:
                if (this.flevel < 15) {
                    this.fxp += var2;
                    if (this.fxp > this.flevel * 30 + 50) {
                        this.levelgain('f');
                    }
                }
        }

    }

    public void levelgain(char var1) {
        int var2;
        switch(var1) {
            case 'f':
                ++this.flevel;
                this.fxp = 0;
                var2 = dmnew.randGen.nextInt(3) + 2;
                this.strengthboost -= var2;
                var2 = dmnew.randGen.nextInt(2) + 1;
                this.dexterityboost -= var2;
                var2 = dmnew.randGen.nextInt(3) + 1;
                this.vitalityboost -= var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 4;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxhealth += var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 4;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxstamina += var2;
                this.stamina += var2;
                this.setMaxLoad();
                dmnew.message.setMessage(this.name + " gains a fighter level!", this.heronumber);
                break;
            case 'n':
                ++this.nlevel;
                this.nxp = 0;
                var2 = dmnew.randGen.nextInt(3) + 1;
                this.strengthboost -= var2;
                var2 = dmnew.randGen.nextInt(3) + 2;
                this.dexterityboost -= var2;
                var2 = dmnew.randGen.nextInt(3) + 1;
                this.vitalityboost -= var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 5;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxhealth += var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 5;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxstamina += var2;
                this.stamina += var2;
                this.setMaxLoad();
                dmnew.message.setMessage(this.name + " gains a ninja level!", this.heronumber);
                break;
            case 'p':
                ++this.plevel;
                this.pxp = 0;
                var2 = dmnew.randGen.nextInt(3) + 2;
                this.wisdomboost -= var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 7;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxhealth += var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 7;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxstamina += var2;
                this.stamina += var2;
                var2 = 9 - this.plevel;
                if (var2 < 4) {
                    var2 = 4;
                }

                var2 = dmnew.randGen.nextInt(5) + this.wisdom / var2;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxmana += var2;
                dmnew.message.setMessage(this.name + " gains a priest level!", this.heronumber);
                break;
            case 'w':
                ++this.wlevel;
                this.wxp = 0;
                var2 = dmnew.randGen.nextInt(3) + 2;
                this.intelligenceboost -= var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 7;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxhealth += var2;
                var2 = dmnew.randGen.nextInt() % 5 + this.vitality / 7;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxstamina += var2;
                this.stamina += var2;
                var2 = 9 - this.wlevel;
                if (var2 < 4) {
                    var2 = 4;
                }

                var2 = dmnew.randGen.nextInt(5) + this.intelligence / var2;
                if (var2 < 1) {
                    var2 = 1;
                }

                this.maxmana += var2;
                dmnew.message.setMessage(this.name + " gains a wizard level!", this.heronumber);
        }

    }

    public void wepThrow(Item var1, int var2) {
        int var3 = (this.strength + 5) / 10 - (int)(var1.weight / 2.0F) + dmnew.randGen.nextInt() % 2 + var1.shotpow / 3 + this.nlevel / 2;
        if (var3 < 2) {
            var3 = 2;
        }

        if (var3 < this.nlevel) {
            var3 = this.nlevel;
        }

        if (this.nlevel > 0) {
            var1.shotpow += dmnew.randGen.nextInt(this.nlevel) + this.nlevel;
        }

        if (var1.isbomb) {
            try {
                Spell var5 = new Spell(var1.bombnum);
                var5.power = var1.potionpow + dmnew.randGen.nextInt() % 10;
                dmnew.this.new Projectile(var5, var3, dmnew.facing, var2);
            } catch (Exception var6) {
                dmnew.this.new Projectile(var1, var3, dmnew.facing, var2);
            }
        } else {
            dmnew.this.new Projectile(var1, var3, dmnew.facing, var2);
        }

        this.load -= var1.weight;
    }

    public int castSpell() {
        Spell var1;
        try {
            var1 = new Spell(this.currentspell);
        } catch (Exception var7) {
            return 0;
        }

        this.weaponcount = var1.gain * 6 + 2;
        if (this.hurthead) {
            this.weaponcount += 4;
        }

        for(int var2 = 0; var2 < dmnew.numheroes; ++var2) {
            if (this.equals(dmnew.hero[var2])) {
                this.wepready = false;
                if (dmnew.this.weaponready == var2) {
                    dmnew.this.weaponsheet.update();
                }
                break;
            }
        }

        if (this.silenced) {
            return 5;
        } else {
            int var3;
            if (this.hurthead) {
                if (var1.clsgain == 'w') {
                    if (this.wlevel < 15) {
                        var3 = dmnew.randGen.nextInt(3);
                    } else {
                        var3 = dmnew.randGen.nextInt(5);
                    }
                } else if (this.plevel < 15) {
                    var3 = dmnew.randGen.nextInt(3);
                } else {
                    var3 = dmnew.randGen.nextInt(5);
                }

                if (var3 == 0) {
                    return 5;
                }
            }

            var3 = var1.gain * this.currentspell.length();
            int var4;
            if (var1.clsgain == 'w') {
                var4 = dmnew.randGen.nextInt((this.wlevel + 1) * 2) + this.wlevel + 1;
                if (this.wlevel > 8) {
                    var4 = 24;
                } else if (this.wlevel > 6) {
                    var4 += 4;
                } else if (this.wlevel > 4) {
                    ++var4;
                }
            } else {
                var4 = dmnew.randGen.nextInt((this.plevel + 1) * 2) + this.plevel + 1;
                if (this.plevel > 8) {
                    var4 = 24;
                } else if (this.plevel > 6) {
                    var4 += 4;
                } else if (this.plevel > 4) {
                    ++var4;
                }
            }

            if (var4 < var3) {
                this.gainxp(var1.clsgain, var1.gain);
                if (var1.clsgain == 'w') {
                    dmnew.this.spellclass = "wizard";
                } else {
                    dmnew.this.spellclass = "priest";
                }

                return 3;
            } else {
                int var5;
                if (var1.clsgain == 'w' && var1.type != 2 && var1.number != 461 && var1.number != 363 && var1.number != 362 && var1.number != 664 && var1.number != 523) {
                    for(var5 = var1.gain - 1; var5 >= 0; --var5) {
                        var1.powers[var5] += dmnew.randGen.nextInt() % 10 + var5 * this.intelligence / 8;
                        if (this.wlevel == 15) {
                            var1.powers[var5] += dmnew.randGen.nextInt(this.intelligence / 8);
                        }

                        if (var1.powers[var5] < 1) {
                            var1.powers[var5] = dmnew.randGen.nextInt(4) + 1;
                        }
                    }

                    var1.power = var1.powers[var1.gain - 1];
                } else if (var1.type != 2 && (var1.type != 0 || var1.number == 1 || var1.number == 2 || var1.number == 655)) {
                    var1.power += dmnew.randGen.nextInt() % 10 + var1.gain * this.wisdom / 8;
                    if (this.plevel == 15) {
                        var1.power += dmnew.randGen.nextInt(this.wisdom / 8);
                    }

                    if (var1.power < 1) {
                        var1.power = dmnew.randGen.nextInt(4) + 1;
                    }

                    if (var1.power < 4 && (var1.number == 1 || var1.number == 2 || var1.number == 655)) {
                        var1.power = dmnew.randGen.nextInt(4) + 4;
                    }
                }

                if (var1.gain == 6 && var1.number == 4 && this.hand != null && this.weapon != null && (this.hand.number == 83 && !this.hand.bound[0] && this.weapon.number == 282 || this.weapon.number == 83 && !this.weapon.bound[0] && this.hand.number == 282)) {
                    dmnew.message.setMessage("Fire has been bound.", 4);
                    if (this.hand.number == 282) {
                        this.load -= this.hand.weight;
                        this.hand = null;
                        this.weapon.bound[0] = true;
                        if (this.weapon.bound[1] && this.weapon.bound[2] && this.weapon.bound[3]) {
                            this.load -= this.weapon.weight;
                            this.weapon = new Item(248);
                            this.load += this.weapon.weight;
                            dmnew.message.setMessage("The Firestaff is Complete.", 4);
                        }
                    } else {
                        this.load -= this.weapon.weight;
                        this.weapon = dmnew.fistfoot;
                        this.hand.bound[0] = true;
                        if (this.hand.bound[1] && this.hand.bound[2] && this.hand.bound[3]) {
                            this.load -= this.hand.weight;
                            this.hand = new Item(248);
                            this.load += this.hand.weight;
                            dmnew.message.setMessage("The Firestaff is Complete.", 4);
                        }
                    }

                    this.repaint();
                    if (dmnew.sheet) {
                        dmnew.herosheet.repaint();
                    }

                    return 1;
                } else if (var1.gain == 6 && var1.number == 2 && this.hand != null && this.weapon != null && (this.hand.number == 83 && !this.hand.bound[1] && this.weapon.number == 283 || this.weapon.number == 83 && !this.weapon.bound[1] && this.hand.number == 283)) {
                    dmnew.message.setMessage("Water has been bound.", 4);
                    if (this.hand.number == 283) {
                        this.load -= this.hand.weight;
                        this.hand = null;
                        this.weapon.bound[1] = true;
                        if (this.weapon.bound[0] && this.weapon.bound[2] && this.weapon.bound[3]) {
                            this.load -= this.weapon.weight;
                            this.weapon = new Item(248);
                            this.load += this.weapon.weight;
                            dmnew.message.setMessage("The Firestaff is Complete.", 4);
                        }
                    } else {
                        this.load -= this.weapon.weight;
                        this.weapon = dmnew.fistfoot;
                        this.hand.bound[1] = true;
                        if (this.hand.bound[0] && this.hand.bound[2] && this.hand.bound[3]) {
                            this.load -= this.hand.weight;
                            this.hand = new Item(248);
                            this.load += this.hand.weight;
                            dmnew.message.setMessage("The Firestaff is Complete.", 4);
                        }
                    }

                    this.repaint();
                    if (dmnew.sheet) {
                        dmnew.herosheet.repaint();
                    }

                    return 1;
                } else if (var1.gain != 6 || var1.number != 1 || this.hand == null || this.weapon == null || (this.hand.number != 83 || this.hand.bound[2] || this.weapon.number != 284) && (this.weapon.number != 83 || this.weapon.bound[2] || this.hand.number != 284)) {
                    if (var1.gain != 6 || var1.number != 3 || this.hand == null || this.weapon == null || (this.hand.number != 83 || this.hand.bound[3] || this.weapon.number != 285) && (this.weapon.number != 83 || this.weapon.bound[3] || this.hand.number != 285)) {
                        if (var1.number == 3) {
                            return 0;
                        } else {
                            if (var1.type == 0) {
                                if (this.hand != null && this.hand.number == 7) {
                                    this.load -= this.hand.weight;
                                    this.hand = new Item(var1.potionnum, var1.power, var1.gain);
                                    this.load += this.hand.weight;
                                } else {
                                    if (this.weapon.number != 7) {
                                        return 2;
                                    }

                                    this.load -= this.weapon.weight;
                                    this.weapon = new Item(var1.potionnum, var1.power, var1.gain);
                                    this.load += this.weapon.weight;
                                }

                                dmnew.this.hupdate();
                            } else if (var1.type == 1) {
                                var5 = this.subsquare;
                                if (var5 == 2) {
                                    var5 = 1;
                                } else if (var5 == 3) {
                                    var5 = 0;
                                }

                                dmnew.this.new Projectile(var1, var1.dist, dmnew.facing, var5);
                            } else if (var1.type == 2) {
                                var5 = this.specialSpell(var1);
                                if (var5 == 1) {
                                    this.gainxp(var1.clsgain, var1.gain - 1 + this.currentspell.length());
                                }

                                return var5;
                            }

                            this.gainxp(var1.clsgain, var1.gain - 1 + this.currentspell.length());
                            return 1;
                        }
                    } else {
                        dmnew.message.setMessage("Wind has been bound.", 4);
                        if (this.hand.number == 285) {
                            this.load -= this.hand.weight;
                            this.hand = null;
                            this.weapon.bound[3] = true;
                            if (this.weapon.bound[0] && this.weapon.bound[1] && this.weapon.bound[2]) {
                                this.load -= this.weapon.weight;
                                this.weapon = new Item(248);
                                this.load += this.weapon.weight;
                                dmnew.message.setMessage("The Firestaff is Complete.", 4);
                            }
                        } else {
                            this.load -= this.weapon.weight;
                            this.weapon = dmnew.fistfoot;
                            this.hand.bound[3] = true;
                            if (this.hand.bound[0] && this.hand.bound[1] && this.hand.bound[2]) {
                                this.load -= this.hand.weight;
                                this.hand = new Item(248);
                                this.load += this.hand.weight;
                                dmnew.message.setMessage("The Firestaff is Complete.", 4);
                            }
                        }

                        this.repaint();
                        if (dmnew.sheet) {
                            dmnew.herosheet.repaint();
                        }

                        return 1;
                    }
                } else {
                    dmnew.message.setMessage("Earth has been bound.", 4);
                    if (this.hand.number == 284) {
                        this.load -= this.hand.weight;
                        this.hand = null;
                        this.weapon.bound[2] = true;
                        if (this.weapon.bound[0] && this.weapon.bound[1] && this.weapon.bound[3]) {
                            this.load -= this.weapon.weight;
                            this.weapon = new Item(248);
                            this.load += this.weapon.weight;
                            dmnew.message.setMessage("The Firestaff is Complete.", 4);
                        }
                    } else {
                        this.load -= this.weapon.weight;
                        this.weapon = dmnew.fistfoot;
                        this.hand.bound[2] = true;
                        if (this.hand.bound[0] && this.hand.bound[1] && this.hand.bound[3]) {
                            this.load -= this.hand.weight;
                            this.hand = new Item(248);
                            this.load += this.hand.weight;
                            dmnew.message.setMessage("The Firestaff is Complete.", 4);
                        }
                    }

                    this.repaint();
                    if (dmnew.sheet) {
                        dmnew.herosheet.repaint();
                    }

                    return 1;
                }
            }
        }
    }

    public int specialSpell(Spell var1) {
        if (var1.number == 325) {
            dmnew.magicvision += var1.power;
            if (dmnew.magicvision == var1.power) {
                dmnew.needredraw = true;
            }
        } else if (var1.number == 4) {
            dmnew.magictorch += var1.power;
            if (dmnew.magictorch > 285) {
                dmnew.magictorch = 285;
            }

            if (dmnew.darkfactor + var1.power > 255) {
                dmnew.darkfactor = 255;
            } else {
                dmnew.darkfactor += var1.power;
            }

            dmnew.needredraw = true;
        } else {
            int var2;
            if (var1.number == 14) {
                if (this.plevel == 15) {
                    var1.power += 10;
                }

                for(var2 = 0; var2 < dmnew.numheroes; ++var2) {
                    if (dmnew.hero[var2].defenseboost < var1.power) {
                        dmnew.hero[var2].defense -= dmnew.hero[var2].defenseboost;
                        dmnew.hero[var2].defenseboost = var1.power;
                        dmnew.hero[var2].defense += var1.power;
                        dmnew.hero[var2].repaint();
                    }
                }
            } else if (var1.number == 64) {
                if (this.plevel == 15) {
                    var1.power += 10;
                }

                for(var2 = 0; var2 < dmnew.numheroes; ++var2) {
                    if (dmnew.hero[var2].magicresistboost < var1.power) {
                        dmnew.hero[var2].magicresist -= dmnew.hero[var2].magicresistboost;
                        dmnew.hero[var2].magicresistboost = var1.power;
                        dmnew.hero[var2].magicresist += var1.power;
                        dmnew.hero[var2].repaint();
                    }
                }
            } else if (var1.number == 344) {
                dmnew.floatcounter += var1.power;
                if (!dmnew.climbing) {
                    dmnew.message.setMessage("Slowfall active.", 4);
                }

                dmnew.climbing = true;
            } else if (var1.number == 121) {
                if (this.weapon.number == 219 && this.kuswordcount < var1.power) {
                    this.kuswordcount = var1.power;
                    this.weapon.power[0] = var1.gain * 2 + 6;
                    return 1;
                }

                if (this.weapon != dmnew.fistfoot) {
                    return 4;
                }

                this.weapon = new Item(219);
                this.weapon.power[0] = var1.gain * 2 + 6;
                this.kuswordcount = var1.power;
                this.repaint();
            } else if (var1.number == 122) {
                if (this.weapon.number == 261 && this.rosbowcount < var1.power) {
                    this.rosbowcount = var1.power;
                    this.weapon.power[0] = var1.gain;
                    return 1;
                }

                if (this.weapon != dmnew.fistfoot) {
                    return 4;
                }

                this.weapon = new Item(261);
                this.weapon.power[0] = var1.gain;
                this.rosbowcount = var1.power;
                this.repaint();
            } else if (var1.number == 322) {
                if (this.plevel < 15) {
                    dmnew.this.spellclass = "priest";
                    return 3;
                }

                dmnew.dispell += var1.power;
                if (dmnew.dispell == var1.power) {
                    dmnew.needredraw = true;
                }
            } else if (var1.number == 666) {
                var2 = dmnew.partyx;
                int var3 = dmnew.partyy;
                if (dmnew.facing == 0) {
                    --var3;
                } else if (dmnew.facing == 1) {
                    --var2;
                } else if (dmnew.facing == 2) {
                    ++var3;
                } else {
                    ++var2;
                }

                dmnew.Monster var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + var2 + "," + var3 + "," + 5);

                for(int var5 = 3; var4 == null && var5 >= 0; --var5) {
                    var4 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + var2 + "," + var3 + "," + (var5 - dmnew.facing + 4) % 4);
                }

                if (var4 == null) {
                    return 4;
                }

                int var6 = var4.damage(var1.power, 7);
                if (var6 >= 0) {
                    this.heal(var6);
                } else {
                    this.damage(var6, 1);
                }
            } else if (var1.number == 635) {
                if (this.weapon.name.equals("Fist/Foot")) {
                    this.weapon = new Item(80);
                    this.repaint();
                } else if (this.hand == null) {
                    this.hand = new Item(80);
                    this.repaint();
                } else {
                    if (!dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].canHoldItems) {
                        return 4;
                    }

                    dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].addItem(new Item(80));
                    dmnew.needredraw = true;
                }
            }
        }

        return 1;
    }

    public boolean usePotion(Item var1) {
        switch(var1.number) {
            case 10:
                this.heal(var1.potionpow);
                int var2 = dmnew.randGen.nextInt(var1.potioncastpow) + var1.potioncastpow / 3;
                if (var2 == 0 && var1.potioncastpow > 4) {
                    var2 += var1.potioncastpow - 4;
                }

                if (var2 > 0 && (this.hurthead || this.hurttorso || this.hurtlegs || this.hurtfeet || this.hurthand || this.hurtweapon)) {
                    ArrayList var3 = new ArrayList(6);
                    var3.add(new Integer(0));
                    var3.add(new Integer(1));
                    var3.add(new Integer(2));
                    var3.add(new Integer(3));
                    var3.add(new Integer(4));
                    var3.add(new Integer(5));
                    int var4 = 0;
                    int var5 = dmnew.randGen.nextInt(6);

                    while(var2 > 0 && var4 < 6) {
                        switch((Integer)var3.remove(var5)) {
                            case 0:
                                if (this.hurthead) {
                                    this.hurthead = false;
                                    --var2;
                                }
                                break;
                            case 1:
                                if (this.hurttorso) {
                                    this.hurttorso = false;
                                    --var2;
                                }
                                break;
                            case 2:
                                if (this.hurtlegs) {
                                    this.hurtlegs = false;
                                    --var2;
                                }
                                break;
                            case 3:
                                if (this.hurtfeet) {
                                    this.hurtfeet = false;
                                    --var2;
                                }
                                break;
                            case 4:
                                if (this.hurthand) {
                                    this.hurthand = false;
                                    --var2;
                                }
                                break;
                            case 5:
                                if (this.hurtweapon) {
                                    this.hurtweapon = false;
                                    --var2;
                                }
                        }

                        ++var4;
                        if (var4 < 6) {
                            var5 = (var5 + 1) % (6 - var4);
                        }
                    }
                }
                break;
            case 11:
                this.vitalize(var1.potionpow);
                break;
            case 12:
                this.mana += var1.potionpow;
                break;
            case 13:
                if (this.strength > 150) {
                    return false;
                }

                this.strengthboost += var1.potionpow;
                this.strength += var1.potionpow;
                this.setMaxLoad();
                break;
            case 14:
                if (this.dexterity > 150) {
                    return false;
                }

                this.dexterityboost += var1.potionpow;
                this.dexterity += var1.potionpow;
                break;
            case 15:
                if (this.vitality > 150) {
                    return false;
                }

                this.vitalityboost += var1.potionpow;
                this.vitality += var1.potionpow;
                break;
            case 16:
                if (this.intelligence > 150) {
                    return false;
                }

                this.intelligenceboost += var1.potionpow;
                this.intelligence += var1.potionpow;
                break;
            case 17:
                if (this.wisdom > 150) {
                    return false;
                }

                this.wisdomboost += var1.potionpow;
                this.wisdom += var1.potionpow;
                break;
            case 18:
                if (this.defenseboost < var1.potionpow) {
                    this.defense -= this.defenseboost;
                    this.defenseboost = var1.potionpow;
                    this.defense += var1.potionpow;
                }
                break;
            case 19:
                if (this.magicresistboost < var1.potionpow) {
                    this.magicresist -= this.magicresistboost;
                    this.magicresistboost += var1.potionpow;
                    this.magicresist += var1.potionpow;
                }
                break;
            case 20:
                this.poison -= var1.potionpow;
                if (this.poison < 1) {
                    this.ispoisoned = false;
                    this.poison = 0;
                }
                break;
            case 21:
            case 22:
            case 23:
            default:
                return false;
            case 24:
                this.silencecount -= var1.potionpow;
                if (this.silencecount < 1) {
                    this.silenced = false;
                    this.silencecount = 0;
                }
        }

        return true;
    }

    public int damage(int var1, int var2) {
        if (!this.isdead) {
            if (dmnew.this.sleeping && var2 != 2) {
                dmnew.needredraw = true;
                dmnew.this.sleeping = false;
                dmnew.sleeper = 0;
                dmnew.sheet = false;
                dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
                dmnew.this.spellsheet.setVisible(true);
                dmnew.this.weaponsheet.setVisible(true);
                dmnew.this.weaponsheet.update();
            }

            boolean var3 = this.hurtweapon || this.hurthand || this.hurthead || this.hurttorso || this.hurtlegs || this.hurtfeet;
            int var4;
            if (var2 != 0 && var2 != 5) {
                if (var2 != 1 && var2 != 7) {
                    if (var2 == 3 && this.head != null) {
                        var1 -= var1 * this.head.defense / 100;
                    }
                } else {
                    var4 = this.magicresist;
                    if (var3) {
                        if (this.hurthand && this.hand != null && this.hand.type == 1 && this.hand.magicresist > 0) {
                            this.magicresist -= this.hand.magicresist;
                        }

                        if (this.hurtweapon && this.weapon != null && this.weapon.type == 1 && this.weapon.magicresist > 0) {
                            this.magicresist -= this.weapon.magicresist;
                        }

                        if (this.magicresist < 0) {
                            this.magicresist = 0;
                        } else {
                            this.magicresist = this.magicresist * 2 / 3;
                        }
                    }

                    var1 -= var1 * this.magicresist / 100;
                    this.magicresist = var4;
                }
            } else {
                var4 = this.defense;
                if (var3) {
                    if (this.hurthand && this.hand != null && this.hand.type == 1 && this.hand.defense > 0) {
                        this.defense -= this.hand.defense;
                    }

                    if (this.hurtweapon && this.weapon != null && this.weapon.type == 1 && this.weapon.defense > 0) {
                        this.defense -= this.weapon.defense;
                    }

                    if (this.defense < 0) {
                        this.defense = 0;
                    } else {
                        this.defense = this.defense * 2 / 3;
                    }
                }

                var1 -= var1 * this.defense / 100;
                this.defense = var4;
            }

            if (var1 < 1) {
                var1 = dmnew.randGen.nextInt(4) + 1;
            }

            this.health -= var1;
            this.hit = var1;
            int var5;
            if (this.health < 1) {
                this.isdead = true;
                this.repaint();
                this.removeMouseListener(dmnew.this.hclick);
                this.health = 0;
                this.currentspell = "";
                this.ispoisoned = false;
                this.poison = 0;
                if (!dmnew.this.sleeping && dmnew.sheet && this.equals(dmnew.herosheet.hero)) {
                    dmnew.herosheet.skipchestscroll = false;
                    dmnew.this.viewing = false;
                    if (dmnew.inhand.number == 4) {
                        dmnew.inhand.pic = dmnew.inhand.temppic;
                        if (!this.isleader) {
                            dmnew.this.changeCursor();
                        }
                    }

                    dmnew.sheet = false;
                    dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
                }

                if (this.weapon.number == 215) {
                    var2 = 4;
                }

                this.dropAllItems(var2);
                this.load = 0.0F;
                dmnew.updateDark();
                dmnew.this.formation.mouseExited((MouseEvent)null);
                dmnew.heroatsub[this.subsquare] = -1;
                dmnew.this.formation.addNewHero();
                boolean var8 = false;
                if (this.isleader) {
                    this.isleader = false;

                    for(var5 = 0; var5 < dmnew.numheroes; ++var5) {
                        if (var5 != this.heronumber && !dmnew.hero[var5].isdead) {
                            dmnew.hero[var5].isleader = true;
                            dmnew.leader = var5;
                            if (dmnew.sheet && dmnew.herosheet.hero.heronumber == var5) {
                                dmnew.herosheet.repaint();
                            }
                            break;
                        }
                    }
                }

                if (dmnew.leader == this.heronumber) {
                    dmnew.playSound("scream.wav", -1, -1);
                    dmnew.iteminhand = false;
                    dmnew.this.changeCursor();
                    dmnew.alldead = true;
                    dmnew.gameover = true;
                    if (dmnew.this.sleeping) {
                        dmnew.this.sleeping = false;
                        dmnew.sleeper = 0;
                        dmnew.sheet = false;
                        dmnew.needredraw = true;
                        dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
                        dmnew.this.spellsheet.setVisible(true);
                        dmnew.this.weaponsheet.setVisible(true);
                        dmnew.this.weaponsheet.update();
                    }
                } else {
                    dmnew.this.spellsheet.casterButton[this.heronumber].setEnabled(false);
                    if (dmnew.this.spellready == this.heronumber) {
                        dmnew.this.spellsheet.casterButton[this.heronumber].setText("");
                        dmnew.this.spellsheet.casterButton[this.heronumber].setSelected(false);
                        dmnew.this.spellsheet.casterButton[this.heronumber].setPreferredSize(new Dimension(16, 20));
                        dmnew.this.spellsheet.casterButton[this.heronumber].setMaximumSize(new Dimension(16, 20));
                        dmnew.this.spellsheet.casterButton[dmnew.leader].setSelected(true);
                        dmnew.this.spellready = dmnew.leader;
                        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setPreferredSize(new Dimension(95, 20));
                        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setMaximumSize(new Dimension(95, 20));
                        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setText(dmnew.hero[dmnew.this.spellready].name);
                    }

                    dmnew.this.weaponsheet.weaponButton[this.heronumber].setDisabledIcon((Icon)null);
                    dmnew.this.weaponsheet.weaponButton[this.heronumber].setEnabled(false);
                    if (dmnew.this.weaponready == this.heronumber) {
                        dmnew.this.weaponsheet.weaponButton[this.heronumber].setSelected(false);
                        dmnew.this.weaponsheet.weaponButton[dmnew.leader].setSelected(true);
                        dmnew.this.weaponready = dmnew.leader;
                    }

                    dmnew.this.weaponsheet.update();
                    dmnew.this.spellsheet.update();
                    this.strength -= this.strengthboost;
                    this.strengthboost = 0;
                    this.vitality -= this.vitalityboost;
                    this.vitalityboost = 0;
                    this.dexterity -= this.dexterityboost;
                    this.dexterityboost = 0;
                    this.intelligence -= this.intelligenceboost;
                    this.intelligenceboost = 0;
                    this.wisdom -= this.wisdomboost;
                    this.wisdomboost = 0;
                    this.defense -= this.defenseboost;
                    this.defenseboost = 0;
                    this.magicresist -= this.magicresistboost;
                    this.magicresistboost = 0;
                    this.hurtweapon = false;
                    this.hurthand = false;
                    this.hurthead = false;
                    this.hurttorso = false;
                    this.hurtlegs = false;
                    this.hurtfeet = false;
                }
            } else {
                this.hurtcounter = 3;
                this.paint(this.getGraphics());
                if (dmnew.sheet && !dmnew.this.viewing && !dmnew.this.sleeping) {
                    dmnew.herosheet.repaint();
                }

                if (var2 == 0) {
                    this.gainxp('f', 1);
                }

                if ((var2 == 0 || var2 == 1 || var2 == 5) && (var1 > 100 || var1 > this.maxhealth / 4 && var1 > 40) && (!this.hurtweapon || !this.hurthand || !this.hurthead || !this.hurttorso || !this.hurtlegs || !this.hurtfeet) && dmnew.randGen.nextInt(10) > 0) {
                    ArrayList var9 = new ArrayList(6);
                    var9.add(new Integer(0));
                    var9.add(new Integer(1));
                    var9.add(new Integer(2));
                    var9.add(new Integer(3));
                    var9.add(new Integer(4));
                    var9.add(new Integer(5));
                    var5 = 6;

                    for(boolean var7 = false; !var7 && var5 > 0; --var5) {
                        int var6 = dmnew.randGen.nextInt(var5);
                        switch((Integer)var9.remove(var6)) {
                            case 0:
                                if (!this.hurthead) {
                                    this.hurthead = true;
                                    var7 = true;
                                }
                                break;
                            case 1:
                                if (!this.hurttorso) {
                                    this.hurttorso = true;
                                    var7 = true;
                                }
                                break;
                            case 2:
                                if (!this.hurtlegs) {
                                    this.hurtlegs = true;
                                    var7 = true;
                                }
                                break;
                            case 3:
                                if (!this.hurtfeet) {
                                    this.hurtfeet = true;
                                    var7 = true;
                                }
                                break;
                            case 4:
                                if (!this.hurthand) {
                                    this.hurthand = true;
                                    var7 = true;
                                }
                                break;
                            case 5:
                                if (!this.hurtweapon) {
                                    this.hurtweapon = true;
                                    var7 = true;
                                }
                        }
                    }
                }
            }

            return var1;
        } else {
            return 0;
        }
    }

    private void dropAllItems(int var1) {
        MapObject var2 = dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy];
        var2.tryFloorSwitch(2);

        for(int var3 = 0; var3 < 16; ++var3) {
            if (this.pack[var3] != null) {
                this.pack[var3].subsquare = (this.subsquare - dmnew.facing + 4) % 4;
                if (!var2.tryTeleport(this.pack[var3])) {
                    var2.addItem(this.pack[var3]);
                }

                this.pack[var3] = null;
            }
        }

        if (this.pouch2 != null) {
            this.pouch2.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.pouch2)) {
                var2.addItem(this.pouch2);
            }

            this.pouch2 = null;
        }

        if (this.pouch1 != null) {
            this.pouch1.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.pouch1)) {
                var2.addItem(this.pouch1);
            }

            this.pouch1 = null;
        }

        for(int var4 = 0; var4 < 6; ++var4) {
            if (this.quiver[var4] != null) {
                this.quiver[var4].subsquare = (this.subsquare - dmnew.facing + 4) % 4;
                if (!var2.tryTeleport(this.quiver[var4])) {
                    var2.addItem(this.quiver[var4]);
                }

                this.quiver[var4] = null;
            }
        }

        if (this.feet != null) {
            this.feet.unEquipEffect(this);
            this.feet.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.feet)) {
                var2.addItem(this.feet);
            }

            this.feet = null;
        }

        if (this.legs != null) {
            this.legs.unEquipEffect(this);
            this.legs.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.legs)) {
                var2.addItem(this.legs);
            }

            this.legs = null;
        }

        if (this.torso != null) {
            this.torso.unEquipEffect(this);
            this.torso.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.torso)) {
                var2.addItem(this.torso);
            }

            this.torso = null;
        }

        if (this.neck != null) {
            this.neck.unEquipEffect(this);
            if (this.neck.number == 89) {
                dmnew.leveldark -= 60;
            }

            this.neck.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.neck)) {
                var2.addItem(this.neck);
            }

            this.neck = null;
        }

        if (this.head != null) {
            this.head.unEquipEffect(this);
            this.head.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.head)) {
                var2.addItem(this.head);
            }

            this.head = null;
        }

        if (this.hand != null) {
            if (this.hand.number == 9) {
                ((Torch)this.hand).putOut();
            }

            if (this.hand.type == 1) {
                this.hand.unEquipEffect(this);
            }

            this.hand.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(this.hand)) {
                var2.addItem(this.hand);
            }

            this.hand = null;
        }

        if (!this.weapon.name.equals("Fist/Foot")) {
            if (this.weapon.number == 9) {
                ((Torch)this.weapon).putOut();
            }

            if (this.weapon.type == 0 || this.weapon.type == 1) {
                this.weapon.unEquipEffect(this);
            }

            if (this.weapon.number != 219 && this.weapon.number != 261) {
                this.weapon.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
                if (!var2.tryTeleport(this.weapon)) {
                    var2.addItem(this.weapon);
                }
            }

            this.weapon = dmnew.fistfoot;
        }

        if (this.isleader && dmnew.iteminhand) {
            dmnew.iteminhand = false;
            dmnew.inhand.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
            if (!var2.tryTeleport(dmnew.inhand)) {
                var2.addItem(dmnew.inhand);
            }

            dmnew.this.changeCursor();
        }

        Item var5;
        if (var1 != 4) {
            var5 = new Item(this.name, this.heronumber);
        } else {
            var5 = new Item(75);
            var5.name = this.name + " Bones";
        }

        var5.subsquare = (this.subsquare - dmnew.facing + 4) % 4;
        if (!var2.tryTeleport(var5)) {
            var2.addItem(var5);
        }

        dmnew.needredraw = true;
    }

    public void heal(int var1) {
        this.health += var1;
        if (this.health > this.maxhealth) {
            this.health = this.maxhealth;
        }

    }

    public void vitalize(int var1) {
        this.stamina += var1;
        if (this.stamina > this.maxstamina) {
            this.stamina = this.maxstamina;
        } else if (this.stamina <= 0) {
            this.stamina = 1;
        }

        this.setMaxLoad();
        if (dmnew.sheet && dmnew.herosheet.hero.heronumber == this.heronumber) {
            dmnew.herosheet.repaint();
        }

    }

    public void energize(int var1) {
        this.mana += var1;
        if (this.mana > this.maxmana) {
            this.mana = this.maxmana;
        } else if (this.mana < 0) {
            this.mana = 0;
        }

    }

    public void paint(Graphics var1) {
        if (!this.isdead) {
            var1.setFont(new Font("TimesRoman", 1, 14));
            var1.setColor(new Color(100, 100, 100));
            var1.fillRect(0, 0, this.getSize().width, 84);
            var1.setColor(new Color(150, 150, 150));
            var1.fillRect(0, 0, this.getSize().width, 18);
            if (this.hurtcounter > 0) {
                var1.setColor(Color.red);
                var1.fillRect(5, 20, 60, 60);
                var1.setColor(new Color(30, 30, 30));
                var1.drawString("< " + this.hit + " >", 17, 50);
                var1.setColor(Color.yellow);
                var1.drawString("< " + this.hit + " >", 14, 47);
            } else {
                var1.drawImage(this.pic, 5, 20, this);
            }

            int var2 = (int)((float)this.health / (float)this.maxhealth * 60.0F);
            var1.setColor(new Color(20, 20, 20));
            var1.fillRect(72, 82 - var2, 5, var2);
            var1.setColor(new Color(0, 150, 0));
            var1.fillRect(70, 80 - var2, 5, var2);
            var2 = (int)((float)this.stamina / (float)this.maxstamina * 60.0F);
            var1.setColor(new Color(20, 20, 20));
            var1.fillRect(82, 82 - var2, 5, var2);
            var1.setColor(new Color(250, 200, 0));
            var1.fillRect(80, 80 - var2, 5, var2);
            var2 = (int)((float)this.mana / (float)this.maxmana * 60.0F);
            var1.setColor(new Color(20, 20, 20));
            var1.fillRect(92, 82 - var2, 5, var2);
            var1.setColor(new Color(100, 0, 240));
            var1.fillRect(90, 80 - var2, 5, var2);
            var1.setColor(new Color(30, 30, 30));
            var1.drawString(this.name, 7, 14);
            if (this.isleader) {
                var1.setColor(new Color(240, 220, 0));
            } else {
                var1.setColor(Color.white);
            }

            var1.drawString(this.name, 5, 12);
            if (this.heronumber == 0) {
                var1.setColor(new Color(0, 216, 0));
            } else if (this.heronumber == 1) {
                var1.setColor(new Color(236, 217, 0));
            } else if (this.heronumber == 2) {
                var1.setColor(Color.red);
            } else {
                var1.setColor(Color.blue);
            }

            var1.fillRect(this.getSize().width - 10, 0, 10, 10);
            var1.setColor(new Color(60, 60, 60));
            var1.fillRect(12, 89, 32, 32);
            var1.fillRect(56, 89, 32, 32);
            var1.setColor(new Color(150, 150, 150));
            var1.drawRect(10, 87, 35, 35);
            var1.drawRect(11, 88, 33, 33);
            var1.drawRect(54, 87, 35, 35);
            var1.drawRect(55, 88, 33, 33);
            if (this.hand != null) {
                var1.drawImage(this.hand.pic, 12, 89, this);
            } else {
                var1.drawImage(dmnew.fistfoot.pic, 44, 89, 12, 121, 0, 0, 32, 32, this);
            }

            Graphics2D var3;
            if (this.hurthand) {
                if (this.hand == null) {
                    var1.drawImage(dmnew.hurthand, 12, 89, this);
                }

                var3 = (Graphics2D)var1;
                var3.setColor(Color.red);
                var3.setStroke(new BasicStroke(2.0F));
                var3.drawRect(11, 88, 34, 34);
            }

            var1.drawImage(this.weapon.pic, 56, 89, this);
            if (this.hurtweapon) {
                if (this.weapon == dmnew.fistfoot) {
                    var1.drawImage(dmnew.hurtweapon, 56, 89, this);
                }

                var3 = (Graphics2D)var1;
                var3.setColor(Color.red);
                var3.setStroke(new BasicStroke(2.0F));
                var3.drawRect(55, 88, 34, 34);
            }

            if (dmnew.sheet && dmnew.herosheet.hero.equals(this)) {
                var1.setColor(Color.yellow);
                var1.drawRect(0, 0, this.getSize().width - 1, this.getSize().height - 1);
            } else if (this.ispoisoned) {
                var1.setColor(new Color(0, 150, 0));
                var1.drawRect(0, 0, this.getSize().width - 1, this.getSize().height - 1);
            } else if (this.magicresistboost > 0 && this.defenseboost > 0) {
                var1.setColor(new Color(150, 0, 150));
                var1.drawRect(0, 0, this.getSize().width - 1, this.getSize().height - 1);
            } else if (this.magicresistboost > 0) {
                var1.setColor(Color.red);
                var1.drawRect(0, 0, this.getSize().width - 1, this.getSize().height - 1);
            } else if (this.defenseboost > 0) {
                var1.setColor(Color.blue);
                var1.drawRect(0, 0, this.getSize().width - 1, this.getSize().height - 1);
            }
        } else {
            var1.setColor(Color.black);
            var1.fillRect(0, 0, this.getSize().width, this.getSize().height);
            var1.drawImage(dmnew.deadheropic, 15, 20, this);
        }

    }
}

