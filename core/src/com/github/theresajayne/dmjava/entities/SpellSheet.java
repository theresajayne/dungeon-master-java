package com.github.theresajayne.dmjava.entities;

class SpellSheet extends JPanel {
    ImageIcon[] spellsymbol = new ImageIcon[24];
    ImageIcon casticon;
    JButton[] spellButton = new JButton[6];
    JButton backButton = new JButton();
    JButton toCastButton = new JButton();
    JToggleButton[] casterButton = new JToggleButton[4];
    dmnew.SpellCasterClick spcastclick;
    ButtonGroup casterGroup = new ButtonGroup();
    Box uppan;
    Box midpan;
    Box dwnpan;
    BufferedImage castsymbs;
    BufferedImage bufsrc;
    Graphics2D castg;

    public SpellSheet() {
        Image var2 = dmnew.tk.createImage("spell.gif");
        dmnew.ImageTracker.addImage(var2, 5);

        try {
            dmnew.ImageTracker.waitForID(5);
        } catch (InterruptedException var10) {
            ;
        }

        this.bufsrc = new BufferedImage(var2.getWidth((ImageObserver)null), var2.getHeight((ImageObserver)null), 2);
        this.bufsrc.createGraphics().drawImage(var2, 0, 0, (ImageObserver)null);
        int var3 = 0;

        for(int var4 = 0; var4 < 4; ++var4) {
            for(int var5 = 0; var5 < 6; ++var5) {
                this.spellsymbol[var3] = new ImageIcon(this.bufsrc.getSubimage(var5 * 12, var4 * 12, 12, 12));
                ++var3;
            }
        }

        dmnew.SpellClick var11 = dmnew.this.new SpellClick();
        dmnew.SpellSymbolClick var6 = dmnew.this.new SpellSymbolClick();
        this.spcastclick = dmnew.this.new SpellCasterClick();
        CompoundBorder var7 = BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder());
        this.setBorder(var7);
        this.setLayout(new GridLayout(3, 1, 0, 0));
        this.setPreferredSize(new Dimension(160, 80));
        this.setMaximumSize(new Dimension(160, 80));
        this.uppan = new Box(0);
        this.midpan = new Box(0);
        this.dwnpan = new Box(0);
        this.uppan.add(Box.createHorizontalGlue());
        this.midpan.add(Box.createHorizontalGlue());
        this.dwnpan.add(Box.createHorizontalGlue());

        for(int var8 = 0; var8 < 4; ++var8) {
            this.casterButton[var8] = new JToggleButton();
            this.casterButton[var8].setPreferredSize(new Dimension(16, 20));
            this.casterButton[var8].setMaximumSize(new Dimension(16, 20));
            this.casterButton[var8].setActionCommand("" + var8);
            this.casterButton[var8].addActionListener(this.spcastclick);
            this.casterGroup.add(this.casterButton[var8]);
            this.uppan.add(this.casterButton[var8]);
        }

        this.casterButton[dmnew.this.spellready].setSelected(true);
        this.casterButton[dmnew.this.spellready].setPreferredSize(new Dimension(95, 20));
        this.casterButton[dmnew.this.spellready].setMaximumSize(new Dimension(95, 20));
        this.casterButton[dmnew.this.spellready].setText(dmnew.hero[dmnew.this.spellready].name);
        this.backButton.setIcon(new ImageIcon(this.createImage(new FilteredImageSource(var2.getSource(), new CropImageFilter(0, 48, 17, 14)))));
        this.backButton.setPreferredSize(new Dimension(20, 20));
        this.backButton.setMaximumSize(new Dimension(20, 20));
        this.backButton.setActionCommand("undo");
        this.backButton.addActionListener(var11);
        this.toCastButton.setPreferredSize(new Dimension(100, 20));
        this.toCastButton.setMaximumSize(new Dimension(100, 20));
        this.toCastButton.addActionListener(var11);

        for(int var9 = 0; var9 < 6; ++var9) {
            this.spellButton[var9] = new JButton();
            this.spellButton[var9].setPreferredSize(new Dimension(20, 20));
            this.spellButton[var9].setMaximumSize(new Dimension(20, 20));
            this.spellButton[var9].setActionCommand("" + var9);
            this.spellButton[var9].addActionListener(var6);
            this.midpan.add(this.spellButton[var9]);
        }

        this.castsymbs = new BufferedImage(70, 12, 2);
        this.castg = this.castsymbs.createGraphics();
        this.casticon = new ImageIcon(this.castsymbs);
        this.toCastButton.setIcon(this.casticon);
        this.dwnpan.add(this.toCastButton);
        this.dwnpan.add(this.backButton);
        this.uppan.add(Box.createHorizontalGlue());
        this.midpan.add(Box.createHorizontalGlue());
        this.dwnpan.add(Box.createHorizontalGlue());
        this.add(this.uppan);
        this.add(this.midpan);
        this.add(this.dwnpan);
        this.setCursor(new Cursor(12));
    }

    public void update() {
        for(int var1 = 1; var1 < 4; ++var1) {
            if (dmnew.numheroes <= var1) {
                this.casterButton[var1].setEnabled(false);
            }
        }

        int var2 = dmnew.hero[dmnew.this.spellready].currentspell.length();

        for(int var3 = 0; var3 < 6; ++var3) {
            if (var2 < 4) {
                this.spellButton[var3].setIcon(this.spellsymbol[var3 + 6 * var2]);
            } else {
                this.spellButton[var3].setIcon(this.spellsymbol[var3]);
            }
        }

        if (var2 > 0) {
            this.castg.setColor(new Color(80, 80, 140));
            this.castg.fillRect(0, 0, 70, 12);

            for(int var4 = 0; var4 < var2; ++var4) {
                this.castg.drawImage(this.spellsymbol[Integer.parseInt(dmnew.hero[dmnew.this.spellready].currentspell.substring(var4, var4 + 1)) + var4 * 6 - 1].getImage(), var4 * 15 + 6, 0, (ImageObserver)null);
            }

            this.casticon.setImage(this.castsymbs);
        } else {
            this.castsymbs.flush();
            this.castsymbs = new BufferedImage(70, 12, 2);
            this.castg = this.castsymbs.createGraphics();
            this.casticon.setImage(this.castsymbs);
        }

        ((BoxLayout)this.uppan.getLayout()).invalidateLayout(this.uppan);
        this.uppan.validate();
        this.repaint();
    }
}
