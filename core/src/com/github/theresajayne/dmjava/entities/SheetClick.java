package com.github.theresajayne.dmjava.entities;

class SheetClick extends MouseAdapter {
    int x;
    int y;

    SheetClick() {
    }

    public void mousePressed(MouseEvent var1) {
        this.x = var1.getX();
        this.y = var1.getY();
        if (dmnew.this.sleeping) {
            dmnew.needredraw = true;
            dmnew.this.sleeping = false;
            dmnew.sleeper = 0;
            dmnew.this.spellsheet.setVisible(true);
            dmnew.this.weaponsheet.setVisible(true);
            if (dmnew.herosheet.hero.isdead) {
                dmnew.herosheet.setHero(dmnew.hero[dmnew.leader]);
            } else {
                dmnew.herosheet.repaint();
            }
        } else if ((this.x <= 420 || this.x >= 440 || this.y >= 25 || this.y <= 5) && !SwingUtilities.isRightMouseButton(var1)) {
            Item var4;
            if (this.x > 124 && this.x < 156 && this.y < 136 && this.y > 104) {
                if (dmnew.herosheet.hero.weapon.number == 215) {
                    dmnew.message.setMessage("Stormbringer resists...", 4);
                    return;
                }

                if (dmnew.herosheet.hero.weapon.number == 219 || dmnew.herosheet.hero.weapon.number == 261) {
                    return;
                }

                if (dmnew.iteminhand) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                }

                if (dmnew.herosheet.hero.weapon != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.weapon.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.weapon.weight;
                }

                if (!dmnew.iteminhand && !dmnew.herosheet.hero.weapon.name.equals("Fist/Foot")) {
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.weapon;
                    dmnew.herosheet.hero.weapon = dmnew.fistfoot;
                    if (dmnew.inhand.number == 9) {
                        ((Torch)dmnew.inhand).putOut();
                        dmnew.updateDark();
                    }

                    if (dmnew.inhand.type == 0 || dmnew.inhand.type == 1 || dmnew.inhand.number == 4) {
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    }

                    dmnew.herosheet.repaint();
                    dmnew.herosheet.hero.repaint();
                    dmnew.this.weaponsheet.update();
                } else if (dmnew.iteminhand) {
                    if (dmnew.herosheet.hero.weapon.name.equals("Fist/Foot")) {
                        dmnew.herosheet.hero.weapon = dmnew.inhand;
                        dmnew.iteminhand = false;
                        if (dmnew.herosheet.hero.weapon.number == 9) {
                            ((Torch)dmnew.herosheet.hero.weapon).setPic();
                            dmnew.updateDark();
                        }

                        if (dmnew.inhand.type == 0 || dmnew.inhand.type == 1 || dmnew.inhand.number == 4) {
                            dmnew.herosheet.hero.weapon.equipEffect(dmnew.herosheet.hero);
                        }
                    } else {
                        var4 = dmnew.herosheet.hero.weapon;
                        dmnew.herosheet.hero.weapon = dmnew.inhand;
                        dmnew.inhand = var4;
                        if (dmnew.inhand.number == 9) {
                            ((Torch)dmnew.inhand).putOut();
                            dmnew.updateDark();
                        }

                        if (dmnew.herosheet.hero.weapon.number == 9) {
                            ((Torch)dmnew.herosheet.hero.weapon).setPic();
                            dmnew.updateDark();
                        }

                        if (dmnew.inhand.type == 0 || dmnew.inhand.type == 1 || dmnew.inhand.number == 4) {
                            dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        }

                        if (dmnew.herosheet.hero.weapon.type == 0 || dmnew.herosheet.hero.weapon.type == 1 || dmnew.herosheet.hero.weapon.number == 4) {
                            dmnew.herosheet.hero.weapon.equipEffect(dmnew.herosheet.hero);
                        }
                    }

                    dmnew.herosheet.repaint();
                    dmnew.herosheet.hero.repaint();
                    dmnew.this.weaponsheet.update();
                }
            } else if (this.x > 12 && this.x < 44 && this.y < 136 && this.y > 104) {
                if (dmnew.iteminhand) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                }

                if (dmnew.herosheet.hero.hand != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.hand.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.hand.weight;
                }

                if (!dmnew.iteminhand && dmnew.herosheet.hero.hand != null) {
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.hand;
                    dmnew.herosheet.hero.hand = null;
                    if (dmnew.inhand.number == 9) {
                        ((Torch)dmnew.inhand).putOut();
                        dmnew.updateDark();
                    }

                    if (dmnew.inhand.type == 1) {
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    }

                    dmnew.herosheet.repaint();
                    dmnew.herosheet.hero.repaint();
                } else if (dmnew.iteminhand) {
                    if (dmnew.herosheet.hero.hand != null) {
                        var4 = dmnew.herosheet.hero.hand;
                        dmnew.herosheet.hero.hand = dmnew.inhand;
                        dmnew.inhand = var4;
                        if (dmnew.inhand.number == 9) {
                            ((Torch)dmnew.inhand).putOut();
                            dmnew.updateDark();
                        }

                        if (dmnew.herosheet.hero.hand.number == 9) {
                            ((Torch)dmnew.herosheet.hero.hand).setPic();
                            dmnew.updateDark();
                        }

                        if (dmnew.inhand.type == 1) {
                            dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        }

                        if (dmnew.herosheet.hero.hand.type == 1) {
                            dmnew.herosheet.hero.hand.equipEffect(dmnew.herosheet.hero);
                        }
                    } else {
                        dmnew.herosheet.hero.hand = dmnew.inhand;
                        dmnew.iteminhand = false;
                        if (dmnew.herosheet.hero.hand.number == 9) {
                            ((Torch)dmnew.herosheet.hero.hand).setPic();
                            dmnew.updateDark();
                        }

                        if (dmnew.inhand.type == 1) {
                            dmnew.herosheet.hero.hand.equipEffect(dmnew.herosheet.hero);
                        }
                    }

                    dmnew.herosheet.repaint();
                    dmnew.herosheet.hero.repaint();
                }
            } else if (this.x > 68 && this.x < 100 && this.y < 84 && this.y > 52) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.head != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.head.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.head.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.head;
                    dmnew.herosheet.hero.head = null;
                    dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.type == 2) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.head != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.head.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.head.weight;
                        var4 = dmnew.herosheet.hero.head;
                        dmnew.herosheet.hero.head = dmnew.inhand;
                        dmnew.inhand = var4;
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        dmnew.herosheet.hero.head.equipEffect(dmnew.herosheet.hero);
                    } else {
                        dmnew.herosheet.hero.head = dmnew.inhand;
                        dmnew.iteminhand = false;
                        dmnew.herosheet.hero.head.equipEffect(dmnew.herosheet.hero);
                    }

                    dmnew.herosheet.repaint();
                }
            } else if (this.x > 12 && this.x < 44 && this.y < 98 && this.y > 66) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.neck != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.neck.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.neck.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.neck;
                    dmnew.herosheet.hero.neck = null;
                    dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    if (dmnew.inhand.number == 89) {
                        dmnew.leveldark -= 60;
                        dmnew.updateDark();
                    }

                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.type == 4) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.neck != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.neck.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.neck.weight;
                        var4 = dmnew.herosheet.hero.neck;
                        dmnew.herosheet.hero.neck = dmnew.inhand;
                        dmnew.inhand = var4;
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        dmnew.herosheet.hero.neck.equipEffect(dmnew.herosheet.hero);
                        if (dmnew.inhand.number == 89) {
                            dmnew.leveldark -= 60;
                            dmnew.updateDark();
                        } else if (dmnew.herosheet.hero.neck.number == 89) {
                            dmnew.leveldark += 60;
                            dmnew.updateDark();
                        }
                    } else {
                        dmnew.herosheet.hero.neck = dmnew.inhand;
                        dmnew.iteminhand = false;
                        dmnew.herosheet.hero.neck.equipEffect(dmnew.herosheet.hero);
                        if (dmnew.inhand.number == 89) {
                            dmnew.leveldark += 60;
                            dmnew.updateDark();
                        }
                    }

                    dmnew.herosheet.repaint();
                }
            } else if (this.x > 68 && this.x < 100 && this.y < 124 && this.y > 92) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.torso != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.torso.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.torso.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.torso;
                    dmnew.herosheet.hero.torso = null;
                    dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.type == 3) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.torso != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.torso.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.torso.weight;
                        var4 = dmnew.herosheet.hero.torso;
                        dmnew.herosheet.hero.torso = dmnew.inhand;
                        dmnew.inhand = var4;
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        dmnew.herosheet.hero.torso.equipEffect(dmnew.herosheet.hero);
                    } else {
                        dmnew.herosheet.hero.torso = dmnew.inhand;
                        dmnew.iteminhand = false;
                        dmnew.herosheet.hero.torso.equipEffect(dmnew.herosheet.hero);
                    }

                    dmnew.herosheet.repaint();
                }
            } else if (this.x > 68 && this.x < 100 && this.y < 164 && this.y > 132) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.legs != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.legs.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.legs.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.legs;
                    dmnew.herosheet.hero.legs = null;
                    dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.type == 5) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.legs != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.legs.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.legs.weight;
                        var4 = dmnew.herosheet.hero.legs;
                        dmnew.herosheet.hero.legs = dmnew.inhand;
                        dmnew.inhand = var4;
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        dmnew.herosheet.hero.legs.equipEffect(dmnew.herosheet.hero);
                    } else {
                        dmnew.herosheet.hero.legs = dmnew.inhand;
                        dmnew.iteminhand = false;
                        dmnew.herosheet.hero.legs.equipEffect(dmnew.herosheet.hero);
                    }

                    dmnew.herosheet.repaint();
                }
            } else if (this.x > 68 && this.x < 100 && this.y < 204 && this.y > 172) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.feet != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.feet.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.feet.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.feet;
                    dmnew.herosheet.hero.feet = null;
                    dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.type == 6) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.feet != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.feet.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.feet.weight;
                        var4 = dmnew.herosheet.hero.feet;
                        dmnew.herosheet.hero.feet = dmnew.inhand;
                        dmnew.inhand = var4;
                        dmnew.inhand.unEquipEffect(dmnew.herosheet.hero);
                        dmnew.herosheet.hero.feet.equipEffect(dmnew.herosheet.hero);
                    } else {
                        dmnew.herosheet.hero.feet = dmnew.inhand;
                        dmnew.iteminhand = false;
                        dmnew.herosheet.hero.feet.equipEffect(dmnew.herosheet.hero);
                    }

                    dmnew.herosheet.repaint();
                }
            } else if (this.x > 12 && this.x < 44 && this.y < 199 && this.y > 167) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.pouch1 != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.pouch1.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.pouch1.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.pouch1;
                    dmnew.herosheet.hero.pouch1 = null;
                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.size < 2) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.pouch1 != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.pouch1.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.pouch1.weight;
                        var4 = dmnew.herosheet.hero.pouch1;
                        dmnew.herosheet.hero.pouch1 = dmnew.inhand;
                        dmnew.inhand = var4;
                    } else {
                        dmnew.herosheet.hero.pouch1 = dmnew.inhand;
                        dmnew.iteminhand = false;
                    }

                    dmnew.herosheet.repaint();
                }
            } else if (this.x > 12 && this.x < 44 && this.y < 233 && this.y > 201) {
                if (!dmnew.iteminhand && dmnew.herosheet.hero.pouch2 != null) {
                    dmnew.herosheet.hero.load -= dmnew.herosheet.hero.pouch2.weight;
                    dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.pouch2.weight;
                    dmnew.iteminhand = true;
                    dmnew.inhand = dmnew.herosheet.hero.pouch2;
                    dmnew.herosheet.hero.pouch2 = null;
                    dmnew.herosheet.repaint();
                } else if (dmnew.iteminhand && dmnew.inhand.size < 2) {
                    dmnew.herosheet.hero.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    if (dmnew.herosheet.hero.pouch2 != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.pouch2.weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.pouch2.weight;
                        var4 = dmnew.herosheet.hero.pouch2;
                        dmnew.herosheet.hero.pouch2 = dmnew.inhand;
                        dmnew.inhand = var4;
                    } else {
                        dmnew.herosheet.hero.pouch2 = dmnew.inhand;
                        dmnew.iteminhand = false;
                    }

                    dmnew.herosheet.repaint();
                }
            } else {
                byte var2;
                Item var3;
                if (this.x > 166 && this.x < 436 && this.y < 98 && this.y > 32) {
                    var2 = 0;
                    if (this.y > 65) {
                        var2 = 8;
                    }

                    if (dmnew.iteminhand) {
                        dmnew.herosheet.hero.load += dmnew.inhand.weight;
                        dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    }

                    if (dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2] != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2].weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2].weight;
                    }

                    if (!dmnew.iteminhand && dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2] != null) {
                        dmnew.iteminhand = true;
                        dmnew.inhand = dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2];
                        dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2] = null;
                        dmnew.herosheet.repaint();
                    } else if (dmnew.iteminhand) {
                        if (dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2] != null) {
                            var3 = dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2];
                            dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2] = dmnew.inhand;
                            dmnew.inhand = var3;
                        } else {
                            dmnew.herosheet.hero.pack[(this.x - 166) / 34 + var2] = dmnew.inhand;
                            dmnew.iteminhand = false;
                        }

                        dmnew.herosheet.repaint();
                    }
                } else if (this.x > 124 && this.x < 190 && this.y < 267 && this.y > 167) {
                    var2 = 0;
                    if (this.y > 234) {
                        var2 = 4;
                    } else if (this.y > 200) {
                        var2 = 2;
                    }

                    if (!dmnew.iteminhand && dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2] != null) {
                        dmnew.herosheet.hero.load -= dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2].weight;
                        dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2].weight;
                        dmnew.iteminhand = true;
                        dmnew.inhand = dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2];
                        dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2] = null;
                        dmnew.herosheet.repaint();
                    } else if (dmnew.iteminhand && dmnew.inhand.type == 0 && (dmnew.inhand.projtype > 0 && dmnew.inhand.size < 2 || dmnew.inhand.size < 4 && (this.x - 124) / 34 + var2 == 0)) {
                        dmnew.herosheet.hero.load += dmnew.inhand.weight;
                        dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                        if (dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2] != null) {
                            dmnew.herosheet.hero.load -= dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2].weight;
                            dmnew.hero[dmnew.leader].load += dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2].weight;
                            var3 = dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2];
                            dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2] = dmnew.inhand;
                            dmnew.inhand = var3;
                        } else {
                            dmnew.herosheet.hero.quiver[(this.x - 124) / 34 + var2] = dmnew.inhand;
                            dmnew.iteminhand = false;
                        }

                        dmnew.herosheet.repaint();
                    }
                } else if (this.x > 227 && this.x < 431 && this.y > 206 && this.y < 271 && dmnew.herosheet.hero.weapon != null && dmnew.herosheet.hero.weapon.number == 5) {
                    var2 = 0;
                    if (this.y > 238) {
                        var2 = 6;
                    }

                    if (!dmnew.iteminhand && ((Chest)dmnew.herosheet.hero.weapon).contents[(this.x - 227) / 34 + var2] != null) {
                        dmnew.herosheet.hero.load -= ((Chest)dmnew.herosheet.hero.weapon).contents[(this.x - 227) / 34 + var2].weight;
                        dmnew.hero[dmnew.leader].load += ((Chest)dmnew.herosheet.hero.weapon).contents[(this.x - 227) / 34 + var2].weight;
                        dmnew.iteminhand = true;
                        dmnew.inhand = ((Chest)dmnew.herosheet.hero.weapon).getItem((this.x - 227) / 34 + var2);
                        dmnew.herosheet.repaint();
                    } else if (dmnew.iteminhand) {
                        dmnew.herosheet.hero.load += dmnew.inhand.weight;
                        dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                        if (((Chest)dmnew.herosheet.hero.weapon).contents[(this.x - 227) / 34 + var2] != null) {
                            dmnew.herosheet.hero.load -= ((Chest)dmnew.herosheet.hero.weapon).contents[(this.x - 227) / 34 + var2].weight;
                            dmnew.hero[dmnew.leader].load += ((Chest)dmnew.herosheet.hero.weapon).contents[(this.x - 227) / 34 + var2].weight;
                            var3 = ((Chest)dmnew.herosheet.hero.weapon).getItem((this.x - 227) / 34 + var2);
                            ((Chest)dmnew.herosheet.hero.weapon).putItem((this.x - 227) / 34 + var2, dmnew.inhand);
                            dmnew.inhand = var3;
                        } else {
                            ((Chest)dmnew.herosheet.hero.weapon).putItem((this.x - 227) / 34 + var2, dmnew.inhand);
                            dmnew.iteminhand = false;
                        }

                        dmnew.herosheet.repaint();
                    }
                } else if (this.x > 20 && this.x < 57 && this.y < 58 && this.y > 24) {
                    dmnew.herosheet.skipchestscroll = true;
                    dmnew.herosheet.paint(dmnew.herosheet.offg);
                    dmnew.this.viewing = true;
                    if (!dmnew.iteminhand) {
                        dmnew.herosheet.stats = true;
                    } else if (dmnew.inhand.number == 4) {
                        dmnew.inhand.temppic = dmnew.inhand.pic;
                        dmnew.inhand.pic = dmnew.inhand.epic;
                        dmnew.this.changeCursor();
                    }

                    dmnew.herosheet.repaint();
                } else if (this.x > 109 && this.x < 146 && this.y < 58 && this.y > 24) {
                    if (dmnew.iteminhand && (dmnew.inhand.type == 7 || dmnew.inhand.ispotion || dmnew.inhand.number == 72 || dmnew.inhand.number == 73)) {
                        dmnew.herosheet.hero.eatdrink();
                        dmnew.herosheet.hero.repaint();
                        dmnew.herosheet.repaint();
                    }
                } else if (this.x > 260 && this.x < 295 && this.y < 24 && this.y > 5) {
                    dmnew.this.sleeping = true;
                    dmnew.sleeper = 50;
                    dmnew.this.spellsheet.setVisible(false);
                    dmnew.this.weaponsheet.setVisible(false);
                    dmnew.herosheet.repaint();
                } else if (this.x > 347 && this.x < 366 && this.y < 24 && this.y > 5 && dmnew.actionqueue.isEmpty()) {
                    dmnew.actionqueue.add("o");
                }
            }
        } else {
            dmnew.sheet = false;
            dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
            dmnew.this.hupdate();
        }

        dmnew.this.changeCursor();
    }

    public void mouseReleased(MouseEvent var1) {
        if (dmnew.this.viewing) {
            dmnew.herosheet.skipchestscroll = false;
            dmnew.herosheet.stats = false;
            dmnew.this.viewing = false;
            if (dmnew.iteminhand && dmnew.inhand.number == 4) {
                dmnew.inhand.pic = dmnew.inhand.temppic;
                dmnew.this.changeCursor();
            }

            dmnew.herosheet.paint(dmnew.herosheet.getGraphics());
        }

    }
}
