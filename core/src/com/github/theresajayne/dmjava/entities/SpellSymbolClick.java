package com.github.theresajayne.dmjava.entities;

class SpellSymbolClick implements ActionListener {
    SpellSymbolClick() {
    }

    public void actionPerformed(ActionEvent var1) {
        if (dmnew.hero[dmnew.this.spellready].currentspell.length() != 4) {
            int var2 = Integer.parseInt(var1.getActionCommand());
            int var3 = var2 + 1;
            if (dmnew.hero[dmnew.this.spellready].currentspell.length() > 0) {
                var3 = dmnew.SYMBOLCOST[var2 + 6 * (dmnew.hero[dmnew.this.spellready].currentspell.length() - 1)][Integer.parseInt(dmnew.hero[dmnew.this.spellready].currentspell.substring(0, 1)) - 1];
            }

            if (dmnew.hero[dmnew.this.spellready].mana >= var3) {
                dmnew.hero[dmnew.this.spellready].mana -= var3;
                dmnew.hero[dmnew.this.spellready].currentspell = dmnew.hero[dmnew.this.spellready].currentspell + "" + (var2 + 1);
                dmnew.this.spellsheet.update();
                dmnew.hero[dmnew.this.spellready].repaint();
                if (dmnew.sheet && dmnew.herosheet.hero.equals(dmnew.hero[dmnew.this.spellready])) {
                    dmnew.herosheet.repaint();
                }
            }

        }
    }
}
