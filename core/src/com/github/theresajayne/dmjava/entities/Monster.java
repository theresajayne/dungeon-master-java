package com.github.theresajayne.dmjava.entities;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

class Monster {
    int number;
    String name;
    int power;
    int defense;
    int magicresist;
    int speed;
    int manapower;
    int movespeed;
    int attackspeed;
    int mana;
    int maxmana = 0;
    int health;
    int maxhealth;
    int fearresist = 5;
    boolean wasfrightened = false;
    int facing = 0;
    int subsquare = 0;
    int currentai = 0;
    int defaultai = 1;
    int newai;
    int powerboost;
    int defenseboost;
    int magicresistboost;
    int speedboost;
    int manapowerboost;
    int movespeedboost;
    int attackspeedboost;
    int parry = -1;
    Image towardspic;
    Image awaypic;
    Image rightpic;
    Image leftpic;
    Image attackpic;
    Image castpic;
    boolean hasmagic = false;
    boolean hasheal = false;
    boolean hasdrain = false;
    boolean silenced = false;
    int silencecount = 0;
    int castpower;
    int minproj = 0;
    boolean ignoremons = true;
    int numspells;
    String[] knownspells;
    int poison = 0;
    boolean isImmaterial = false;
    boolean isflying = false;
    boolean canusestairs = true;
    int timecounter = 0;
    int movecounter = 0;
    int deathcounter = 0;
    int randomcounter = 0;
    int runcounter = 0;
    int x;
    int y;
    int xdist = 0;
    int ydist = 0;
    int level;
    boolean isattacking = false;
    boolean iscasting = false;
    boolean isdying = false;
    boolean mirrored = false;
    boolean hurt = false;
    boolean wasstuck;
    int moveattack = 0;
    int hurttest = 0;
    boolean waitattack = false;
    boolean ispoisoned;
    int poisonpow;
    int poisoncounter = 0;
    int olddir = -1;
    int castsub = 0;
    boolean breakdoor = false;
    ArrayList carrying = new ArrayList();
    int ammo = 0;
    boolean useammo = false;
    static final int RANDOM = 0;
    static final int GOTOWARDS = 1;
    static final int STAYBACK = 2;
    static final int RUN = 3;
    static final int GUARD = 4;
    boolean HITANDRUN = false;
    boolean gamewin = false;
    String endanim;
    String endsound;
    String picstring;
    String soundstring;
    int hurtitem;
    int needitem;
    int needhandneck;
    int pickup;

    public Monster(int var2, int var3, int var4, int var5, String var6, String var7, String var8, boolean var9, boolean var10, boolean var11) {
        this.number = var2;
        this.x = var3;
        this.y = var4;
        this.level = var5;
        this.name = var6;
        this.picstring = var7;
        this.soundstring = var8;
        this.canusestairs = var9;
        this.isflying = var10;
        this.ignoremons = var11;
        File var12 = new File("Monsters" + File.separator + var7 + "-toward.gif");
        if (var12.exists()) {
            this.towardspic = dmnew.tk.getImage(var12.getPath());
        } else {
            this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "screamer-toward.gif");
        }

        var12 = new File("Monsters" + File.separator + var7 + "-away.gif");
        if (var12.exists()) {
            this.awaypic = dmnew.tk.getImage(var12.getPath());
        } else {
            this.awaypic = this.towardspic;
        }

        var12 = new File("Monsters" + File.separator + var7 + "-right.gif");
        if (var12.exists()) {
            this.rightpic = dmnew.tk.getImage(var12.getPath());
        } else {
            this.rightpic = this.towardspic;
        }

        var12 = new File("Monsters" + File.separator + var7 + "-left.gif");
        if (var12.exists()) {
            this.leftpic = dmnew.tk.getImage(var12.getPath());
        } else {
            this.leftpic = this.towardspic;
        }

        var12 = new File("Monsters" + File.separator + var7 + "-attack.gif");
        if (var12.exists()) {
            this.attackpic = dmnew.tk.getImage(var12.getPath());
        } else {
            this.attackpic = this.towardspic;
        }

        var12 = new File("Monsters" + File.separator + var7 + "-cast.gif");
        if (var12.exists()) {
            this.castpic = dmnew.tk.getImage(var12.getPath());
        } else {
            this.castpic = this.attackpic;
        }

        dmnew.ImageTracker.addImage(this.towardspic, 2);
        dmnew.ImageTracker.addImage(this.awaypic, 2);
        dmnew.ImageTracker.addImage(this.rightpic, 2);
        dmnew.ImageTracker.addImage(this.leftpic, 2);
        dmnew.ImageTracker.addImage(this.attackpic, 2);
        dmnew.ImageTracker.addImage(this.castpic, 2);

        try {
            dmnew.ImageTracker.waitForID(2);
        } catch (InterruptedException var14) {
            ;
        }

        dmnew.ImageTracker.removeImage(this.towardspic, 2);
        dmnew.ImageTracker.removeImage(this.awaypic, 2);
        dmnew.ImageTracker.removeImage(this.rightpic, 2);
        dmnew.ImageTracker.removeImage(this.leftpic, 2);
        dmnew.ImageTracker.removeImage(this.attackpic, 2);
        dmnew.ImageTracker.removeImage(this.castpic, 2);
    }

    public Monster(int var2, int var3, int var4, int var5) {
        this.number = var2;
        this.x = var3;
        this.y = var4;
        this.level = var5;
        switch(this.number) {
            case 0:
                this.name = "Mummy";
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "mummy-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "mummy-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "mummy-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "mummy-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "mummy-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "mummy.wav";
                break;
            case 1:
                this.name = "Screamer";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "screamer.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "screamer-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "spider.wav";
                break;
            case 2:
                this.name = "Giggler";
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "giggler-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "giggler-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "giggler-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "giggler-left.gif");
                this.attackpic = this.towardspic;
                this.castpic = this.attackpic;
                this.soundstring = "giggler.wav";
                break;
            case 3:
                this.name = "Rock Pile";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "rockpile.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "rockpile-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "rockmon.wav";
                break;
            case 4:
                this.name = "Slime";
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "slime.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "slime-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "slime.wav";
                break;
            case 5:
                this.name = "Wing Eye";
                this.isflying = true;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "wingeye.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "wingeye-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 6:
                this.name = "Ghost";
                this.isflying = true;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "ghost.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "ghost-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "mummy.wav";
                break;
            case 7:
                this.name = "Muncher";
                this.isflying = true;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "muncher-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "muncher-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "muncher-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "muncher-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "muncher-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 8:
                this.name = "Skeleton";
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "skeleton-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "skeleton-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "skeleton-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "skeleton-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "skeleton-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "swing.wav";
                break;
            case 9:
                this.name = "Worm";
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "worm-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "worm-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "worm-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "worm-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "worm-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "worm.wav";
                break;
            case 10:
                this.name = "Fire Elemental";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "fireel.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "fireel-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 11:
                this.name = "Water Elemental";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "waterel.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "waterel-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "waterel.wav";
                break;
            case 12:
                this.name = "Goblin";
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "goblin-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "goblin-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "goblin-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "goblin-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "goblin-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 13:
                this.name = "Giant Rat";
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "rat-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "rat-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "rat-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "rat-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "rat-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "roar.wav";
                break;
            case 14:
                this.name = "Ant Man";
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "antman-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "antman-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "antman-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "antman-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "antman-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "swing.wav";
                break;
            case 15:
                this.name = "Beholder";
                this.isflying = true;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "beholder.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "beholder-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 16:
                this.name = "Couatyl";
                this.isflying = true;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "couatyl-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "couatyl-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "couatyl-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "couatyl-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "couatyl-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "couatyl.wav";
                break;
            case 17:
                this.name = "Fader";
                this.isflying = true;
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "fader.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = this.towardspic;
                this.castpic = this.attackpic;
                this.soundstring = "rockmon.wav";
                break;
            case 18:
                this.name = "Tentacle Beast";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "tentaclemon.gif");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "tentaclemon-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "slime.wav";
                break;
            case 19:
                this.name = "Scorpion";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "scorpion-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "scorpion-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "scorpion-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "scorpion-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "scorpion-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "scorpion.wav";
                break;
            case 20:
                this.name = "Demon";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "demon-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "demon-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "demon-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "demon-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "demon-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 21:
                this.name = "Deth Knight";
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "dethknight-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "dethknight-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "dethknight-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "dethknight-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "dethknight-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "swing.wav";
                break;
            case 22:
                this.name = "Spider";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "spider-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "spider-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "spider-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "spider-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "spider-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "spider.wav";
                break;
            case 23:
                this.name = "Stone Golem";
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "golem-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "golem-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "golem-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "golem-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "golem-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "swing.wav";
                break;
            case 24:
                this.name = "Sorcerer";
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "sorcerer-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "sorcerer-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "sorcerer-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "sorcerer-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "sorcerer-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 25:
                this.name = "Dragon";
                this.canusestairs = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "dragon-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "dragon-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "dragon-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "dragon-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "dragon-attack.gif");
                this.castpic = this.attackpic;
                this.soundstring = "roar.wav";
                break;
            case 26:
                this.name = "Lord Chaos";
                this.isflying = true;
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "chaos-toward.gif");
                this.awaypic = dmnew.tk.getImage("Monsters" + File.separator + "chaos-away.gif");
                this.rightpic = dmnew.tk.getImage("Monsters" + File.separator + "chaos-right.gif");
                this.leftpic = dmnew.tk.getImage("Monsters" + File.separator + "chaos-left.gif");
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "chaos-attack.gif");
                this.castpic = this.attackpic;
                break;
            case 27:
                this.name = "Demon Lord";
                this.ignoremons = false;
                this.towardspic = dmnew.tk.getImage("Monsters" + File.separator + "demonlord.png");
                this.awaypic = this.towardspic;
                this.rightpic = this.towardspic;
                this.leftpic = this.towardspic;
                this.attackpic = dmnew.tk.getImage("Monsters" + File.separator + "demonlord-attack.png");
                this.castpic = this.attackpic;
                this.soundstring = "demonlord.wav";
        }

        dmnew.ImageTracker.addImage(this.towardspic, 2);
        dmnew.ImageTracker.addImage(this.awaypic, 2);
        dmnew.ImageTracker.addImage(this.rightpic, 2);
        dmnew.ImageTracker.addImage(this.leftpic, 2);
        dmnew.ImageTracker.addImage(this.attackpic, 2);
        dmnew.ImageTracker.addImage(this.castpic, 2);

        try {
            dmnew.ImageTracker.waitForID(2);
        } catch (InterruptedException var7) {
            ;
        }

        dmnew.ImageTracker.removeImage(this.towardspic, 2);
        dmnew.ImageTracker.removeImage(this.awaypic, 2);
        dmnew.ImageTracker.removeImage(this.rightpic, 2);
        dmnew.ImageTracker.removeImage(this.leftpic, 2);
        dmnew.ImageTracker.removeImage(this.attackpic, 2);
        dmnew.ImageTracker.removeImage(this.castpic, 2);
    }

    public void timePass() {
        if (this.moveattack == dmnew.moncycle) {
            this.moveattack = 0;
        } else {
            if (this.moveattack != 0) {
                this.moveattack = 0;
            }

            if (this.movespeedboost > 0) {
                --this.movespeedboost;
                --this.movespeed;
            }

            if (this.isdying) {
                ++this.deathcounter;
                if (this.deathcounter > 1) {
                    dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                    int var1 = 0;
                    boolean var2 = false;

                    while(var1 < 6 && !var2) {
                        if (dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + var1) != null) {
                            var2 = true;
                        }

                        if (var1 == 3) {
                            var1 = 5;
                        } else {
                            ++var1;
                        }
                    }

                    dmnew.DungeonMap[this.level][this.x][this.y].hasMons = var2;
                    if (this.level == dmnew.level) {
                        dmnew.needredraw = true;
                    }
                }

            } else if (this.isattacking) {
                if (!this.breakdoor) {
                    this.hitHero();
                }

                this.isattacking = false;
                if (this.level == dmnew.level) {
                    dmnew.needredraw = true;
                }

            } else if (this.iscasting) {
                this.iscasting = false;
                if (this.level == dmnew.level) {
                    dmnew.needredraw = true;
                }

            } else {
                this.breakdoor = false;
                if (dmnew.freezelife <= 0 || this.number == 26 || this.number == 27) {
                    if (this.timecounter < 0) {
                        ++this.timecounter;
                        this.timecounter += dmnew.sleeper;
                    } else {
                        ++this.movecounter;
                        this.movecounter += dmnew.sleeper;
                        if (this.movecounter > this.movespeed) {
                            if (this.wasfrightened) {
                                this.wasfrightened = false;
                                this.currentai = 3;
                            }

                            if (this.number == 26 && dmnew.fluxchanging && dmnew.fluxcages.get(this.level + "," + this.x + "," + this.y) != null) {
                                this.runcounter = 1;
                                this.currentai = 3;
                            }

                            this.doAI();
                            if (this.defaultai == 2 && this.currentai == 1) {
                                if (!this.wasstuck && (this.ammo > 0 || this.hasmagic && !this.silenced && this.mana >= this.minproj)) {
                                    this.currentai = 2;
                                }

                                this.wasstuck = false;
                            }
                        } else if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5 && dmnew.randGen.nextInt(5) == 0) {
                            this.mirrored = !this.mirrored;
                            dmnew.needredraw = true;
                        }

                        ++this.timecounter;
                        this.timecounter += dmnew.sleeper;
                        if (this.timecounter > 210) {
                            this.timecounter = 0;
                            this.heal(this.maxhealth / 20 + 2);
                            this.energize(this.maxmana / 20 + 2);
                            if (this.ispoisoned) {
                                if (!this.isImmaterial && this.number != 8) {
                                    if (this.poisonpow > 0) {
                                        if (this.poisonpow > 15) {
                                            this.poisonpow = 15;
                                        } else if (dmnew.randGen.nextBoolean()) {
                                            --this.poisonpow;
                                        }
                                    } else {
                                        this.ispoisoned = false;
                                    }
                                } else {
                                    this.ispoisoned = false;
                                    this.poisonpow = 0;
                                }
                            }

                            if (this.silencecount > 0) {
                                --this.silencecount;
                                if (this.silencecount == 0) {
                                    this.silenced = false;
                                }
                            }

                            if (this.powerboost < 0) {
                                ++this.powerboost;
                                ++this.power;
                            }

                            if (this.defenseboost < 0) {
                                ++this.defenseboost;
                                ++this.defense;
                            }

                            if (this.magicresistboost < 0) {
                                ++this.magicresistboost;
                                ++this.magicresist;
                            }

                            if (this.speedboost < 0) {
                                ++this.speedboost;
                                ++this.speed;
                            }

                            if (this.manapowerboost < 0) {
                                ++this.manapowerboost;
                                ++this.manapower;
                            }
                        }

                        if (this.ispoisoned) {
                            ++this.poisoncounter;
                            this.poisoncounter += dmnew.sleeper;
                            if (this.poisoncounter > 30) {
                                this.poisoncounter = 0;
                                this.damage(this.poisonpow, 2);
                            }
                        }

                    }
                }
            }
        }
    }

    public void hitHero() {
        byte var3;
        byte var4;
        byte var5;
        byte var6;
        if (this.facing == 0) {
            var3 = 2;
            var4 = 3;
            var5 = 0;
            var6 = 1;
        } else if (this.facing == 2) {
            var3 = 0;
            var4 = 1;
            var5 = 2;
            var6 = 3;
        } else if (this.facing == 3) {
            var3 = 0;
            var4 = 3;
            var5 = 1;
            var6 = 2;
        } else {
            var3 = 1;
            var4 = 2;
            var5 = 0;
            var6 = 3;
        }

        int var1;
        int var2;
        if (dmnew.randGen.nextBoolean()) {
            var1 = (var3 + dmnew.facing) % 4;
            var2 = (var4 + dmnew.facing) % 4;
        } else {
            var1 = (var4 + dmnew.facing) % 4;
            var2 = (var3 + dmnew.facing) % 4;
        }

        if (dmnew.heroatsub[var1] == -1) {
            var1 = var2;
            if (dmnew.heroatsub[var2] == -1) {
                if (dmnew.randGen.nextBoolean()) {
                    var1 = (var5 + dmnew.facing) % 4;
                    var2 = (var6 + dmnew.facing) % 4;
                } else {
                    var1 = (var6 + dmnew.facing) % 4;
                    var2 = (var5 + dmnew.facing) % 4;
                }

                if (dmnew.heroatsub[var1] == -1) {
                    var1 = var2;
                }
            }
        }

        var1 = dmnew.heroatsub[var1];
        if (var1 != -1) {
            int var9;
            if (this.name.equals("Giggler")) {
                boolean var12 = false;
                if (!dmnew.hero[var1].weapon.name.equals("Fist/Foot") && dmnew.hero[var1].weapon.number != 219 && dmnew.hero[var1].weapon.number != 261) {
                    this.carrying.add(dmnew.hero[var1].weapon);
                    dmnew.hero[var1].load -= dmnew.hero[var1].weapon.weight;
                    if (dmnew.hero[var1].weapon.type == 0 || dmnew.hero[var1].weapon.type == 1) {
                        dmnew.hero[var1].weapon.unEquipEffect(dmnew.hero[var1]);
                    }

                    if (dmnew.hero[var1].weapon.number == 9) {
                        ((Torch)dmnew.hero[var1].weapon).putOut();
                    }

                    dmnew.hero[var1].weapon = dmnew.fistfoot;
                    var12 = true;
                    dmnew.hero[var1].repaint();
                    dmnew.this.weaponsheet.update();
                    dmnew.updateDark();
                } else if (dmnew.hero[var1].hand != null) {
                    this.carrying.add(dmnew.hero[var1].hand);
                    dmnew.hero[var1].load -= dmnew.hero[var1].hand.weight;
                    if (dmnew.hero[var1].hand.type == 1) {
                        dmnew.hero[var1].hand.unEquipEffect(dmnew.hero[var1]);
                    } else if (dmnew.hero[var1].hand.number == 9) {
                        ((Torch)dmnew.hero[var1].hand).putOut();
                    }

                    dmnew.hero[var1].hand = null;
                    var12 = true;
                    dmnew.hero[var1].repaint();
                    dmnew.updateDark();
                } else if (!var12) {
                    ArrayList var13 = new ArrayList(5);
                    var13.add(new Integer(0));
                    var13.add(new Integer(1));
                    var13.add(new Integer(2));
                    var13.add(new Integer(3));
                    var13.add(new Integer(4));
                    var13.add(new Integer(5));
                    var13.add(new Integer(6));
                    var13.add(new Integer(7));
                    var13.add(new Integer(8));

                    label254:
                    while(true) {
                        label245:
                        while(true) {
                            if (var12 || var13.isEmpty()) {
                                break label254;
                            }

                            var9 = (Integer)var13.remove(0);
                            int var10;
                            int var11;
                            switch(var9) {
                                case 0:
                                    if (dmnew.hero[var1].head != null) {
                                        this.carrying.add(dmnew.hero[var1].head);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].head.weight;
                                        dmnew.hero[var1].head.unEquipEffect(dmnew.hero[var1]);
                                        dmnew.hero[var1].head = null;
                                        var12 = true;
                                    }
                                    break;
                                case 1:
                                    if (dmnew.hero[var1].neck != null) {
                                        if (dmnew.hero[var1].neck.number == 89) {
                                            dmnew.leveldark -= 60;
                                        }

                                        this.carrying.add(dmnew.hero[var1].neck);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].neck.weight;
                                        dmnew.hero[var1].neck.unEquipEffect(dmnew.hero[var1]);
                                        dmnew.hero[var1].neck = null;
                                        var12 = true;
                                        dmnew.updateDark();
                                    }
                                    break;
                                case 2:
                                    if (dmnew.hero[var1].torso != null) {
                                        this.carrying.add(dmnew.hero[var1].torso);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].torso.weight;
                                        dmnew.hero[var1].torso.unEquipEffect(dmnew.hero[var1]);
                                        dmnew.hero[var1].torso = null;
                                        var12 = true;
                                    }
                                    break;
                                case 3:
                                    if (dmnew.hero[var1].legs != null) {
                                        this.carrying.add(dmnew.hero[var1].legs);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].legs.weight;
                                        dmnew.hero[var1].legs.unEquipEffect(dmnew.hero[var1]);
                                        dmnew.hero[var1].legs = null;
                                        var12 = true;
                                    }
                                    break;
                                case 4:
                                    if (dmnew.hero[var1].feet != null) {
                                        this.carrying.add(dmnew.hero[var1].feet);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].feet.weight;
                                        dmnew.hero[var1].feet.unEquipEffect(dmnew.hero[var1]);
                                        dmnew.hero[var1].feet = null;
                                        var12 = true;
                                    }
                                    break;
                                case 5:
                                    if (dmnew.hero[var1].pouch1 != null) {
                                        this.carrying.add(dmnew.hero[var1].pouch1);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].pouch1.weight;
                                        dmnew.hero[var1].pouch1 = null;
                                        var12 = true;
                                    }
                                    break;
                                case 6:
                                    if (dmnew.hero[var1].pouch2 != null) {
                                        this.carrying.add(dmnew.hero[var1].pouch2);
                                        dmnew.hero[var1].load -= dmnew.hero[var1].pouch2.weight;
                                        dmnew.hero[var1].pouch2 = null;
                                        var12 = true;
                                    }
                                    break;
                                case 7:
                                    var10 = dmnew.randGen.nextInt(6);
                                    var11 = 0;

                                    while(true) {
                                        if (var12 || var11 >= 6) {
                                            continue label245;
                                        }

                                        if (dmnew.hero[var1].quiver[var10] != null) {
                                            this.carrying.add(dmnew.hero[var1].quiver[var10]);
                                            dmnew.hero[var1].load -= dmnew.hero[var1].quiver[var10].weight;
                                            dmnew.hero[var1].quiver[var10] = null;
                                            var12 = true;
                                        } else {
                                            var10 = (var10 + 1) % 6;
                                            ++var11;
                                        }
                                    }
                                case 8:
                                    var10 = dmnew.randGen.nextInt(16);
                                    var11 = 0;

                                    while(!var12 && var11 < 16) {
                                        if (dmnew.hero[var1].pack[var10] != null) {
                                            this.carrying.add(dmnew.hero[var1].pack[var10]);
                                            dmnew.hero[var1].load -= dmnew.hero[var1].pack[var10].weight;
                                            dmnew.hero[var1].pack[var10] = null;
                                            var12 = true;
                                        } else {
                                            var10 = (var10 + 1) % 16;
                                            ++var11;
                                        }
                                    }
                            }
                        }
                    }
                }

                if (dmnew.sheet && var12 && dmnew.herosheet.hero.equals(dmnew.hero[var1])) {
                    dmnew.herosheet.repaint();
                }

                if (this.HITANDRUN && (!var12 || dmnew.randGen.nextInt(32) != 0)) {
                    this.runcounter = 40 + dmnew.randGen.nextInt(10);
                    this.currentai = 3;
                }

            } else if (!dmnew.this.sleeping && dmnew.randGen.nextInt(20) == 0) {
                this.movecounter -= 4;
                dmnew.message.setMessage(this.name + ": Critical Miss", 5);
            } else {
                int var7 = this.speed - dmnew.hero[var1].dexterity;
                if (dmnew.hero[var1].stamina < dmnew.hero[var1].maxstamina / 4) {
                    var7 += 5;
                }

                if (dmnew.hero[var1].load > dmnew.hero[var1].maxload) {
                    var7 += 5;
                }

                if (dmnew.hero[var1].hurttorso || dmnew.hero[var1].hurtlegs || dmnew.hero[var1].hurtfeet) {
                    var7 += 10;
                }

                if (this.parry == var1) {
                    var7 -= 15;
                }

                this.parry = -1;
                boolean var8;
                if (dmnew.this.sleeping) {
                    var8 = true;
                } else if (var7 > 40) {
                    var8 = dmnew.randGen.nextInt(8) != 0;
                } else if (var7 > 30) {
                    var8 = dmnew.randGen.nextInt(7) != 0;
                } else if (var7 > 20) {
                    var8 = dmnew.randGen.nextInt(6) != 0;
                } else if (var7 > 10) {
                    var8 = dmnew.randGen.nextInt(5) != 0;
                } else if (var7 > 0) {
                    var8 = dmnew.randGen.nextInt(4) != 0;
                } else if (var7 > -10) {
                    var8 = dmnew.randGen.nextInt(3) != 0;
                } else if (var7 > -20) {
                    var8 = dmnew.randGen.nextInt(2) != 0;
                } else if (var7 > -30) {
                    var8 = dmnew.randGen.nextInt(3) == 0;
                } else {
                    var8 = dmnew.randGen.nextInt(4) == 0;
                }

                if (var8) {
                    dmnew.playSound("oof.wav", -1, -1);
                    var9 = this.power + dmnew.randGen.nextInt(this.power / 4 + 10);
                    if (this.speed > 50 && (dmnew.randGen.nextInt(20) == 0 || dmnew.this.sleeping && dmnew.randGen.nextInt(5) == 0)) {
                        var9 = 3 * var9 / 2;
                        dmnew.message.setMessage(this.name + ": Critical Hit", 5);
                    }

                    if (this.poison > 0 && Math.abs(dmnew.randGen.nextInt()) % 10 > 4) {
                        dmnew.hero[var1].poison += this.poison;
                        dmnew.hero[var1].ispoisoned = true;
                    }

                    dmnew.hero[var1].damage(var9, 0);
                    dmnew.this.hupdate();
                    if (this.HITANDRUN) {
                        this.runcounter = 12 + Math.abs(dmnew.randGen.nextInt()) % 8;
                        this.currentai = 3;
                    }
                }

            }
        }
    }

    public void doAI() {
        this.xdist = this.x - dmnew.partyx;
        if (this.xdist < 0) {
            this.xdist *= -1;
        }

        this.ydist = this.y - dmnew.partyy;
        if (this.ydist < 0) {
            this.ydist *= -1;
        }

        if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5 && (this.number == 16 || this.number == 5)) {
            dmnew.playSound("flap.wav", this.x, this.y);
        }

        boolean var1 = false;
        if (this.level == dmnew.level && (this.xdist == 0 && this.ydist == 1 || this.ydist == 0 && this.xdist == 1)) {
            if (this.subsquare != 5) {
                if (this.x > dmnew.partyx) {
                    if (this.subsquare != 0 && this.subsquare != 3) {
                        if (this.currentai == 1) {
                            if (this.subsquare == 1 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 0) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 0;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 0, this);
                            } else if (this.subsquare == 2 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 3) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 3;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 3, this);
                            }

                            this.facing = 1;
                        }
                    } else {
                        var1 = true;
                    }
                } else if (this.x < dmnew.partyx) {
                    if (this.subsquare != 1 && this.subsquare != 2) {
                        if (this.currentai == 1) {
                            if (this.subsquare == 0 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 1) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 1;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 1, this);
                            } else if (this.subsquare == 3 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 2) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 2;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 2, this);
                            }

                            this.facing = 3;
                        }
                    } else {
                        var1 = true;
                    }
                } else if (this.y > dmnew.partyy) {
                    if (this.subsquare != 0 && this.subsquare != 1) {
                        if (this.currentai == 1) {
                            if (this.subsquare == 2 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 1) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 1;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 1, this);
                            } else if (this.subsquare == 3 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 0) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 0;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 0, this);
                            }

                            this.facing = 0;
                        }
                    } else {
                        var1 = true;
                    }
                } else if (this.y < dmnew.partyy) {
                    if (this.subsquare != 2 && this.subsquare != 3) {
                        if (this.currentai == 1) {
                            if (this.subsquare == 0 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 3) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 3;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 3, this);
                            } else if (this.subsquare == 1 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0 && dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 2) == null) {
                                var1 = true;
                                this.moveattack = dmnew.moncycle;
                                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                                this.subsquare = 2;
                                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + 2, this);
                            }

                            this.facing = 2;
                        }
                    } else {
                        var1 = true;
                    }
                }
            } else {
                var1 = true;
            }
        }

        this.newai = this.currentai;
        if (this.level != dmnew.level && this.currentai != 4 && this.currentai != 0) {
            this.currentai = 0;
        }

        while(true) {
            do {
                do {
                    do {
                        do {
                            label1564: {
                                if (this.movecounter <= this.movespeed) {
                                    return;
                                }

                                int var2;
                                int var3;
                                switch(this.currentai) {
                                    case 0:
                                        if (var1) {
                                            this.randomcounter = 0;
                                            this.newai = this.defaultai;
                                            this.doAttack();
                                            break label1564;
                                        }

                                        if (this.randomcounter > 0) {
                                            --this.randomcounter;
                                            if (this.randomcounter == 0) {
                                                this.newai = this.defaultai;
                                            }
                                        } else if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                                            this.newai = this.defaultai;
                                        }

                                        if ((this.number == 26 || this.number == 24 && (!this.silenced || dmnew.randGen.nextBoolean())) && dmnew.randGen.nextInt(20) == 0 && this.teleport()) {
                                            break label1564;
                                        }

                                        var2 = 0;

                                        for(var3 = dmnew.randGen.nextInt(4); !this.canMove(var3) && var2 < 4; var3 = (var3 + 1) % 4) {
                                            ++var2;
                                        }

                                        if (var2 < 4) {
                                            this.monMove(var3);
                                        } else {
                                            this.movecounter = 0;
                                        }
                                        break label1564;
                                    case 1:
                                        if (this.health < this.maxhealth / 5 && this.hasheal && !this.silenced && this.mana > 59) {
                                            this.useHealMagic();
                                        } else if (this.fearresist > 0 && this.hurttest == 0 && this.health < this.maxhealth / 5) {
                                            if (dmnew.randGen.nextInt(10) < this.fearresist || this.health < 40 && this.maxhealth < 400 && dmnew.randGen.nextBoolean()) {
                                                this.hurt = true;
                                                this.newai = 3;
                                                break label1564;
                                            }

                                            this.hurttest = 12 - this.fearresist;
                                        } else if (this.hurttest > 0) {
                                            --this.hurttest;
                                        }

                                        if (var1) {
                                            this.doAttack();
                                            break label1564;
                                        }

                                        if ((this.ammo > 0 || this.hasmagic && !this.silenced && this.mana >= this.minproj) && dmnew.randGen.nextBoolean() && (this.x == dmnew.partyx && this.ydist > 1 || this.y == dmnew.partyy && this.xdist > 1)) {
                                            if (this.x < dmnew.partyx && this.canDoProj(3)) {
                                                this.doProjAttack(3);
                                            } else if (this.x > dmnew.partyx && this.canDoProj(1)) {
                                                this.doProjAttack(1);
                                            } else if (this.y > dmnew.partyy && this.canDoProj(0)) {
                                                this.doProjAttack(0);
                                            } else if (this.y < dmnew.partyy && this.canDoProj(2)) {
                                                this.doProjAttack(2);
                                            }

                                            if (this.movecounter < this.movespeed) {
                                                break label1564;
                                            }
                                        }

                                        if (dmnew.randGen.nextInt() % 10 > 5) {
                                            if (this.attackspeed > 0) {
                                                this.movecounter = this.attackspeed;
                                            } else {
                                                this.movecounter = 0;
                                            }
                                            break label1564;
                                        }

                                        if (this.xdist == 1 && this.ydist == 1 && !this.HITANDRUN && dmnew.randGen.nextBoolean()) {
                                            this.movecounter = this.movespeed;
                                            this.waitattack = true;
                                            break label1564;
                                        }

                                        if (dmnew.randGen.nextBoolean()) {
                                            if (this.x < dmnew.partyx && this.canMove(3)) {
                                                this.monMove(3);
                                                break label1564;
                                            }

                                            if (this.x > dmnew.partyx && this.canMove(1)) {
                                                this.monMove(1);
                                                break label1564;
                                            }

                                            if (this.y < dmnew.partyy && this.canMove(2)) {
                                                this.monMove(2);
                                                break label1564;
                                            }

                                            if (this.y > dmnew.partyy && this.canMove(0)) {
                                                this.monMove(0);
                                                break label1564;
                                            }

                                            this.randomcounter = 3;
                                            this.newai = 0;
                                            break label1564;
                                        }

                                        if (this.y < dmnew.partyy && this.canMove(2)) {
                                            this.monMove(2);
                                            break label1564;
                                        }

                                        if (this.y > dmnew.partyy && this.canMove(0)) {
                                            this.monMove(0);
                                            break label1564;
                                        }

                                        if (this.x < dmnew.partyx && this.canMove(3)) {
                                            this.monMove(3);
                                            break label1564;
                                        }

                                        if (this.x > dmnew.partyx && this.canMove(1)) {
                                            this.monMove(1);
                                            break label1564;
                                        }

                                        this.randomcounter = 3;
                                        this.newai = 0;
                                        break label1564;
                                    case 2:
                                        if (this.health < this.maxhealth / 5 && this.hasheal && !this.silenced && this.mana > 59) {
                                            this.useHealMagic();
                                        } else if (this.fearresist > 0 && this.hurttest == 0 && this.health < this.maxhealth / 5) {
                                            if (dmnew.randGen.nextInt(10) < this.fearresist || this.health < 40 && this.maxhealth < 400 && dmnew.randGen.nextBoolean()) {
                                                this.hurt = true;
                                                this.newai = 3;
                                                break label1564;
                                            }

                                            this.hurttest = 12 - this.fearresist;
                                        } else if (this.hurttest > 0) {
                                            --this.hurttest;
                                        }

                                        if (this.ammo == 0 && (!this.hasmagic || this.mana < this.minproj || this.silenced)) {
                                            if (dmnew.randGen.nextBoolean()) {
                                                this.newai = 1;
                                            } else {
                                                this.runcounter = 5;
                                                this.newai = 3;
                                            }
                                            break label1564;
                                        }

                                        if (this.x == dmnew.partyx) {
                                            if ((this.ydist > 1 || !var1 && dmnew.randGen.nextInt(3) != 0) && dmnew.randGen.nextInt() % 10 < 6) {
                                                if (this.y > dmnew.partyy && this.canDoProj(0)) {
                                                    this.doProjAttack(0);
                                                    break label1564;
                                                }

                                                if (this.y < dmnew.partyy && this.canDoProj(2)) {
                                                    this.doProjAttack(2);
                                                    break label1564;
                                                }

                                                if ((this.subsquare == 0 || this.subsquare == 3) && this.canMove(3)) {
                                                    this.monMove(3);
                                                    this.movecounter = this.attackspeed;
                                                    break label1564;
                                                }

                                                if ((this.subsquare == 1 || this.subsquare == 2) && this.canMove(1)) {
                                                    this.monMove(1);
                                                    this.movecounter = this.attackspeed;
                                                    break label1564;
                                                }

                                                this.randomcounter = 3;
                                                this.newai = 0;
                                                break label1564;
                                            }

                                            if (this.y > dmnew.partyy && this.canMove(2)) {
                                                this.monMove(2);
                                                break label1564;
                                            }

                                            if (this.y < dmnew.partyy && this.canMove(0)) {
                                                this.monMove(0);
                                                break label1564;
                                            }

                                            if (dmnew.randGen.nextInt() % 10 > 2) {
                                                if (dmnew.randGen.nextBoolean() && this.canMove(3)) {
                                                    this.monMove(3);
                                                    break label1564;
                                                }

                                                if (this.canMove(1)) {
                                                    this.monMove(1);
                                                } else if (this.canMove(3)) {
                                                    this.monMove(3);
                                                } else {
                                                    this.newai = 1;
                                                    this.wasstuck = true;
                                                }
                                                break label1564;
                                            }

                                            this.newai = 1;
                                            this.wasstuck = true;
                                            break label1564;
                                        }

                                        if (this.y == dmnew.partyy) {
                                            if ((this.xdist > 1 || !var1 && dmnew.randGen.nextInt(3) != 0) && dmnew.randGen.nextInt() % 10 < 6) {
                                                if (this.x > dmnew.partyx && this.canDoProj(1)) {
                                                    this.doProjAttack(1);
                                                    break label1564;
                                                }

                                                if (this.x < dmnew.partyx && this.canDoProj(3)) {
                                                    this.doProjAttack(3);
                                                    break label1564;
                                                }

                                                if ((this.subsquare == 0 || this.subsquare == 1) && this.canMove(2)) {
                                                    this.monMove(2);
                                                    this.movecounter = this.attackspeed;
                                                    break label1564;
                                                }

                                                if ((this.subsquare == 2 || this.subsquare == 3) && this.canMove(0)) {
                                                    this.monMove(0);
                                                    this.movecounter = this.attackspeed;
                                                    break label1564;
                                                }

                                                this.randomcounter = 3;
                                                this.newai = 0;
                                                break label1564;
                                            }

                                            if (this.x > dmnew.partyx && this.canMove(3)) {
                                                this.monMove(3);
                                                break label1564;
                                            }

                                            if (this.x < dmnew.partyx && this.canMove(1)) {
                                                this.monMove(1);
                                                break label1564;
                                            }

                                            if (dmnew.randGen.nextInt() % 10 > 2) {
                                                if (dmnew.randGen.nextBoolean() && this.canMove(0)) {
                                                    this.monMove(0);
                                                    break label1564;
                                                }

                                                if (this.canMove(2)) {
                                                    this.monMove(2);
                                                } else if (this.canMove(0)) {
                                                    this.monMove(0);
                                                } else {
                                                    this.newai = 1;
                                                    this.wasstuck = true;
                                                }
                                                break label1564;
                                            }

                                            this.newai = 1;
                                            this.wasstuck = true;
                                            break label1564;
                                        }

                                        if (this.xdist != this.ydist) {
                                            if (this.xdist < this.ydist) {
                                                if (this.x < dmnew.partyx && this.canMove(3)) {
                                                    this.monMove(3);
                                                    break label1564;
                                                }

                                                if (this.x > dmnew.partyx && this.canMove(1)) {
                                                    this.monMove(1);
                                                    break label1564;
                                                }

                                                if (this.y < dmnew.partyy && this.canMove(2)) {
                                                    this.monMove(2);
                                                    break label1564;
                                                }

                                                if (this.y > dmnew.partyy && this.canMove(0)) {
                                                    this.monMove(0);
                                                    break label1564;
                                                }

                                                this.randomcounter = 3;
                                                this.newai = 0;
                                                break label1564;
                                            }

                                            if (this.y < dmnew.partyy && this.canMove(2)) {
                                                this.monMove(2);
                                                break label1564;
                                            }

                                            if (this.y > dmnew.partyy && this.canMove(0)) {
                                                this.monMove(0);
                                                break label1564;
                                            }

                                            if (this.x < dmnew.partyx && this.canMove(3)) {
                                                this.monMove(3);
                                                break label1564;
                                            }

                                            if (this.x > dmnew.partyx && this.canMove(1)) {
                                                this.monMove(1);
                                                break label1564;
                                            }

                                            this.randomcounter = 3;
                                            this.newai = 0;
                                            break label1564;
                                        }

                                        var2 = 0;

                                        for(var3 = dmnew.randGen.nextInt(4); !this.canMove(var3) && var2 < 4; var3 = (var3 + 1) % 4) {
                                            ++var2;
                                        }

                                        if (var2 < 4) {
                                            this.monMove(var3);
                                        } else {
                                            this.movecounter = 0;
                                        }
                                        break label1564;
                                    case 3:
                                        if ((this.number == 24 && (!this.silenced || dmnew.randGen.nextBoolean()) || this.number == 26 && this.wasstuck) && this.teleport()) {
                                            this.wasstuck = false;
                                            this.movecounter = 0;
                                        } else if (this.wasstuck && var1) {
                                            this.wasstuck = false;
                                            this.doAttack();
                                        } else if (var1 && dmnew.randGen.nextBoolean()) {
                                            this.waitattack = true;
                                            this.doAttack();
                                        } else if (dmnew.randGen.nextBoolean()) {
                                            if (this.x == dmnew.partyx && dmnew.randGen.nextBoolean()) {
                                                if (dmnew.randGen.nextBoolean() && this.canMove(3)) {
                                                    this.monMove(3);
                                                } else if (this.canMove(1)) {
                                                    this.monMove(1);
                                                } else if (this.canMove(3)) {
                                                    this.monMove(3);
                                                } else if (this.y > dmnew.partyy) {
                                                    if ((!this.wasstuck || this.olddir == -1 || this.olddir != 0) && this.canMove(2)) {
                                                        this.monMove(2);
                                                    }
                                                } else if (this.y < dmnew.partyy) {
                                                    if ((!this.wasstuck || this.olddir == -1 || this.olddir != 2) && this.canMove(0)) {
                                                        this.monMove(0);
                                                    }
                                                } else {
                                                    this.wasstuck = true;
                                                    this.randomcounter = 3;
                                                    this.newai = 0;
                                                }
                                            } else if (this.x > dmnew.partyx) {
                                                if ((!this.wasstuck || this.olddir == -1 || this.olddir != 1) && this.canMove(3)) {
                                                    this.monMove(3);
                                                }
                                            } else if (this.x < dmnew.partyx) {
                                                if ((!this.wasstuck || this.olddir == -1 || this.olddir != 3) && this.canMove(1)) {
                                                    this.monMove(1);
                                                }
                                            } else if (this.y > dmnew.partyy) {
                                                if ((!this.wasstuck || this.olddir == -1 || this.olddir != 0) && this.canMove(2)) {
                                                    this.monMove(2);
                                                }
                                            } else if (this.y < dmnew.partyy && (!this.wasstuck || this.olddir == -1 || this.olddir != 2) && this.canMove(0)) {
                                                this.monMove(0);
                                            }

                                            if (this.movecounter > this.movespeed) {
                                                this.wasstuck = true;
                                                var2 = 0;

                                                for(var3 = dmnew.randGen.nextInt(4); var2 < 4 && (this.olddir != -1 && var3 == (this.olddir + 2) % 4 || var3 == 0 && this.y > dmnew.partyy || var3 == 2 && this.y < dmnew.partyy || var3 == 3 && this.x < dmnew.partyx || var3 == 1 && this.x > dmnew.partyx || !this.canMove(var3)); var3 = (var3 + 1) % 4) {
                                                    ++var2;
                                                }

                                                if (var2 < 4 && dmnew.randGen.nextInt(15) > 0) {
                                                    this.olddir = var3;
                                                    this.monMove(var3);
                                                } else if (var1) {
                                                    this.doAttack();
                                                } else if ((this.ammo > 0 || this.hasmagic && !this.silenced && this.mana >= this.minproj) && (this.x == dmnew.partyx && this.ydist > 1 || this.y == dmnew.partyy && this.xdist > 1)) {
                                                    if (this.x < dmnew.partyx && this.canDoProj(3)) {
                                                        this.doProjAttack(3);
                                                    } else if (this.x > dmnew.partyx && this.canDoProj(1)) {
                                                        this.doProjAttack(1);
                                                    } else if (this.y > dmnew.partyy && this.canDoProj(0)) {
                                                        this.doProjAttack(0);
                                                    } else if (this.y < dmnew.partyy && this.canDoProj(2)) {
                                                        this.doProjAttack(2);
                                                    } else {
                                                        this.movecounter = 0;
                                                    }
                                                } else if (dmnew.randGen.nextBoolean()) {
                                                    this.olddir = -1;
                                                    this.newai = 1;
                                                } else if (this.x > dmnew.partyx && this.canMove(1)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 1;
                                                    this.monMove(1);
                                                } else if (this.x < dmnew.partyx && this.canMove(3)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 3;
                                                    this.monMove(3);
                                                } else if (this.y > dmnew.partyy && this.canMove(0)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 0;
                                                    this.monMove(0);
                                                } else if (this.y < dmnew.partyy && this.canMove(2)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 2;
                                                    this.monMove(2);
                                                } else {
                                                    this.movecounter = 0;
                                                }
                                            }
                                        } else {
                                            if (this.y == dmnew.partyy && dmnew.randGen.nextBoolean()) {
                                                if (dmnew.randGen.nextBoolean() && this.canMove(0)) {
                                                    this.monMove(0);
                                                } else if (this.canMove(2)) {
                                                    this.monMove(2);
                                                } else if (this.canMove(0)) {
                                                    this.monMove(0);
                                                } else if (this.x > dmnew.partyx) {
                                                    if ((!this.wasstuck || this.olddir == -1 || this.olddir != 1) && this.canMove(3)) {
                                                        this.monMove(3);
                                                    }
                                                } else if (this.x < dmnew.partyx) {
                                                    if ((!this.wasstuck || this.olddir == -1 || this.olddir != 3) && this.canMove(1)) {
                                                        this.monMove(1);
                                                    }
                                                } else {
                                                    this.wasstuck = true;
                                                    this.randomcounter = 3;
                                                    this.newai = 0;
                                                }
                                            } else if (this.y > dmnew.partyy) {
                                                if ((!this.wasstuck || this.olddir != -1 || this.olddir != 0) && this.canMove(2)) {
                                                    this.monMove(2);
                                                }
                                            } else if (this.y < dmnew.partyy) {
                                                if ((!this.wasstuck || this.olddir != -1 || this.olddir != 2) && this.canMove(0)) {
                                                    this.monMove(0);
                                                }
                                            } else if (this.x > dmnew.partyx) {
                                                if ((!this.wasstuck || this.olddir != -1 || this.olddir != 1) && this.canMove(3)) {
                                                    this.monMove(3);
                                                }
                                            } else if (this.x < dmnew.partyx && (!this.wasstuck || this.olddir != -1 || this.olddir != 3) && this.canMove(1)) {
                                                this.monMove(1);
                                            }

                                            if (this.movecounter > this.movespeed) {
                                                this.wasstuck = true;
                                                var2 = 0;

                                                for(var3 = dmnew.randGen.nextInt(4); var2 < 4 && (this.olddir != -1 && var3 == (this.olddir + 2) % 4 || var3 == 0 && this.y > dmnew.partyy || var3 == 2 && this.y < dmnew.partyy || var3 == 3 && this.x < dmnew.partyx || var3 == 1 && this.x > dmnew.partyx || !this.canMove(var3)); var3 = (var3 + 1) % 4) {
                                                    ++var2;
                                                }

                                                if (var2 < 4 && dmnew.randGen.nextInt(15) > 0) {
                                                    this.olddir = var3;
                                                    this.monMove(var3);
                                                } else if (var1) {
                                                    this.doAttack();
                                                } else if ((this.ammo > 0 || this.hasmagic && !this.silenced && this.mana >= this.minproj) && (this.x == dmnew.partyx && this.ydist > 1 || this.y == dmnew.partyy && this.xdist > 1)) {
                                                    if (this.x < dmnew.partyx && this.canDoProj(3)) {
                                                        this.doProjAttack(3);
                                                    } else if (this.x > dmnew.partyx && this.canDoProj(1)) {
                                                        this.doProjAttack(1);
                                                    } else if (this.y > dmnew.partyy && this.canDoProj(0)) {
                                                        this.doProjAttack(0);
                                                    } else if (this.y < dmnew.partyy && this.canDoProj(2)) {
                                                        this.doProjAttack(2);
                                                    } else {
                                                        this.movecounter = 0;
                                                    }
                                                } else if (dmnew.randGen.nextBoolean()) {
                                                    this.olddir = -1;
                                                    this.newai = 1;
                                                } else if (this.x > dmnew.partyx && this.canMove(1)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 1;
                                                    this.monMove(1);
                                                } else if (this.x < dmnew.partyx && this.canMove(3)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 3;
                                                    this.monMove(3);
                                                } else if (this.y > dmnew.partyy && this.canMove(0)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 0;
                                                    this.monMove(0);
                                                } else if (this.y < dmnew.partyy && this.canMove(2)) {
                                                    this.wasstuck = true;
                                                    this.olddir = 2;
                                                    this.monMove(2);
                                                } else {
                                                    this.movecounter = 0;
                                                }
                                            }
                                        }

                                        if (this.hurt && this.hasheal && !this.silenced && this.mana > 59 && dmnew.randGen.nextBoolean()) {
                                            this.useHealMagic();
                                        } else if (this.hurt && (double)this.health > 0.2D * (double)this.maxhealth) {
                                            this.hurt = false;
                                            if (this.runcounter == 0) {
                                                this.olddir = -1;
                                                this.newai = this.defaultai;
                                            }
                                        }

                                        if (!this.hurt && this.runcounter > 0) {
                                            --this.runcounter;
                                            if (this.runcounter == 0) {
                                                this.olddir = -1;
                                                this.newai = this.defaultai;
                                                if (this.newai == 4) {
                                                    this.newai = 1;
                                                    this.defaultai = 1;
                                                }
                                            }
                                        }
                                        break label1564;
                                    case 4:
                                }

                                if (this.hasheal && !this.silenced && (double)this.health < 0.2D * (double)this.maxhealth && this.mana > 59) {
                                    this.useHealMagic();
                                } else if (this.level != dmnew.level) {
                                    this.movecounter = 0;
                                } else if (var1) {
                                    this.doAttack();
                                } else if (this.x != dmnew.partyx || this.ydist <= 1 || this.ammo <= 0 && (!this.hasmagic || this.silenced || this.mana < this.minproj)) {
                                    if (this.y != dmnew.partyy || this.xdist <= 1 || this.ammo <= 0 && (!this.hasmagic || this.silenced || this.mana < this.minproj)) {
                                        if (this.xdist == 0 && this.ydist == 1) {
                                            if (this.y > dmnew.partyy && this.canMove(0)) {
                                                this.monMove(0);
                                            } else if (this.y < dmnew.partyy && this.canMove(2)) {
                                                this.monMove(2);
                                            } else {
                                                this.movecounter = 0;
                                            }
                                        } else if (this.ydist == 0 && this.xdist == 1) {
                                            if (this.x > dmnew.partyx && this.canMove(1)) {
                                                this.monMove(1);
                                            } else if (this.x < dmnew.partyx && this.canMove(3)) {
                                                this.monMove(3);
                                            } else {
                                                this.movecounter = 0;
                                            }
                                        } else {
                                            this.movecounter = 0;
                                        }
                                    } else if (this.x < dmnew.partyx && this.canDoProj(3)) {
                                        this.doProjAttack(3);
                                    } else if (this.x > dmnew.partyx && this.canDoProj(1)) {
                                        this.doProjAttack(1);
                                    } else if ((this.subsquare == 0 || this.subsquare == 1) && this.canMove(2)) {
                                        this.monMove(2);
                                    } else if ((this.subsquare == 2 || this.subsquare == 3) && this.canMove(0)) {
                                        this.monMove(0);
                                    } else {
                                        this.movecounter = 0;
                                    }
                                } else if (this.y > dmnew.partyy && this.canDoProj(0)) {
                                    this.doProjAttack(0);
                                } else if (this.y < dmnew.partyy && this.canDoProj(2)) {
                                    this.doProjAttack(2);
                                } else if ((this.subsquare == 0 || this.subsquare == 3) && this.canMove(3)) {
                                    this.monMove(3);
                                } else if ((this.subsquare == 1 || this.subsquare == 2) && this.canMove(1)) {
                                    this.monMove(1);
                                } else {
                                    this.movecounter = 0;
                                }

                                if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                                    if (this.xdist != 0 && this.xdist > this.ydist) {
                                        if (dmnew.partyx > this.x) {
                                            this.facing = 3;
                                        } else {
                                            this.facing = 1;
                                        }
                                    } else if (dmnew.partyy > this.y) {
                                        this.facing = 2;
                                    } else {
                                        this.facing = 0;
                                    }

                                    if (this.useammo && this.ammo == 0) {
                                        this.defaultai = 1;
                                        this.newai = 1;
                                    }
                                }
                            }

                            this.currentai = this.newai;
                        } while(this.currentai == 4);
                    } while(this.xdist <= 4);
                } while(this.ydist <= 4);
            } while(this.currentai == 0 && this.randomcounter <= 0);

            this.randomcounter = 0;
            this.currentai = 0;
        }
    }

    public boolean canMove(int var1) {
        int var2 = this.x;
        int var3 = this.y;
        byte var4 = 5;
        if (var1 != 0 && var1 != 2) {
            if (this.subsquare == 0) {
                var4 = 1;
            } else if (this.subsquare == 1) {
                var4 = 0;
            } else if (this.subsquare == 2) {
                var4 = 3;
            } else if (this.subsquare == 3) {
                var4 = 2;
            }

            if (var1 != 3 || var4 != 0 && var4 != 3 && var4 != 5) {
                if (var1 == 1 && (var4 == 1 || var4 == 2 || var4 == 5)) {
                    --var2;
                }
            } else {
                ++var2;
            }
        } else {
            if (this.subsquare == 0) {
                var4 = 3;
            } else if (this.subsquare == 3) {
                var4 = 0;
            } else if (this.subsquare == 1) {
                var4 = 2;
            } else if (this.subsquare == 2) {
                var4 = 1;
            }

            if (var1 == 0 && (var4 == 3 || var4 == 2 || var4 == 5)) {
                --var3;
            } else if (var1 == 2 && (var4 == 0 || var4 == 1 || var4 == 5)) {
                ++var3;
            }
        }

        if (var2 >= 0 && var3 >= 0 && var2 < dmnew.mapwidth && var3 < dmnew.mapheight) {
            boolean var5 = true;
            if ((dmnew.DungeonMap[this.level][var2][var3].isPassable || this.isImmaterial) && !dmnew.DungeonMap[this.level][var2][var3].hasParty && dmnew.DungeonMap[this.level][var2][var3].numProjs <= 0 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs <= 0) {
                if (!dmnew.DungeonMap[this.level][var2][var3].hasMons || var4 != 5 && dmnew.dmmons.get(this.level + "," + var2 + "," + var3 + "," + var4) == null && dmnew.dmmons.get(this.level + "," + var2 + "," + var3 + "," + 5) == null) {
                    if (var2 >= 0 && var3 >= 0 && var2 != dmnew.mapwidth && var3 != dmnew.mapheight) {
                        if (!dmnew.DungeonMap[this.level][var2][var3].canPassMons) {
                            var5 = false;
                        } else if (this.isImmaterial && !dmnew.DungeonMap[this.level][var2][var3].canPassImmaterial) {
                            var5 = false;
                        } else if (!this.isImmaterial && !this.ignoremons && dmnew.DungeonMap[this.level][var2][var3].hasCloud && dmnew.randGen.nextBoolean()) {
                            var5 = false;
                        } else if (this.isflying || !(dmnew.DungeonMap[this.level][var2][var3] instanceof FulYaPit) && (!(dmnew.DungeonMap[this.level][var2][var3] instanceof Pit) || !((Pit)dmnew.DungeonMap[this.level][var2][var3]).isOpen && (this.ignoremons || !((Pit)dmnew.DungeonMap[this.level][var2][var3]).isActive))) {
                            if (!this.canusestairs && dmnew.DungeonMap[this.level][var2][var3] instanceof Stairs) {
                                var5 = false;
                            } else if (this.number == 26 && dmnew.fluxchanging && dmnew.fluxcages.get(this.level + "," + var2 + "," + var3) != null) {
                                var5 = false;
                            }
                        } else {
                            var5 = false;
                        }
                    } else {
                        var5 = false;
                    }
                } else {
                    var5 = false;
                }
            } else {
                var5 = false;
            }

            if (!var5 && dmnew.DungeonMap[this.level][var2][var3] instanceof Door && !((Door)dmnew.DungeonMap[this.level][var2][var3]).isOpen) {
                if (!this.ignoremons && ((Door)dmnew.DungeonMap[this.level][var2][var3]).opentype == 1) {
                    ((Door)dmnew.DungeonMap[this.level][var2][var3]).activate();
                    if (!dmnew.DungeonMap[this.level][var2][var3].hasMons && !dmnew.DungeonMap[this.level][var2][var3].hasParty && dmnew.DungeonMap[this.level][var2][var3].numProjs == 0 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0) {
                        var5 = true;
                    }
                } else if (!this.breakdoor && (this.number == 8 || this.number == 13 || this.number == 20 || this.number == 21 || this.number == 23 || this.number == 25) && ((Door)dmnew.DungeonMap[this.level][var2][var3]).isBreakable && ((Door)dmnew.DungeonMap[this.level][var2][var3]).pictype == 0) {
                    this.breakdoor = true;
                    if (this.level == dmnew.level) {
                        this.facing = var1;
                        this.isattacking = true;
                        dmnew.needredraw = true;
                    }

                    ((Door)dmnew.DungeonMap[this.level][var2][var3]).breakDoor(this.power + dmnew.randGen.nextInt(this.power / 4 + 10), true, false);
                    if (((Door)dmnew.DungeonMap[this.level][var2][var3]).isBroken && !dmnew.DungeonMap[this.level][var2][var3].hasMons && !dmnew.DungeonMap[this.level][var2][var3].hasParty && dmnew.DungeonMap[this.level][var2][var3].numProjs == 0 && dmnew.DungeonMap[this.level][this.x][this.y].numProjs == 0) {
                        var5 = true;
                    }
                }
            }

            return var5;
        } else {
            return false;
        }
    }

    public void monMove(int var1) {
        this.parry = -1;
        int var2 = this.x;
        int var3 = this.y;
        byte var4 = 5;
        if (var1 != 0 && var1 != 2) {
            if (this.subsquare == 0) {
                var4 = 1;
            } else if (this.subsquare == 1) {
                var4 = 0;
            } else if (this.subsquare == 2) {
                var4 = 3;
            } else if (this.subsquare == 3) {
                var4 = 2;
            }

            if (var1 != 3 || var4 != 0 && var4 != 3 && var4 != 5) {
                if (var1 == 1 && (var4 == 1 || var4 == 2 || var4 == 5)) {
                    --var2;
                }
            } else {
                ++var2;
            }
        } else {
            if (this.subsquare == 0) {
                var4 = 3;
            } else if (this.subsquare == 3) {
                var4 = 0;
            } else if (this.subsquare == 1) {
                var4 = 2;
            } else if (this.subsquare == 2) {
                var4 = 1;
            }

            if (var1 == 0 && (var4 == 3 || var4 == 2 || var4 == 5)) {
                --var3;
            } else if (var1 == 2 && (var4 == 0 || var4 == 1 || var4 == 5)) {
                ++var3;
            }
        }

        this.facing = var1;
        this.movecounter = 0;
        if (!this.breakdoor) {
            if (this.isImmaterial || dmnew.DungeonMap[this.level][var2][var3].mapchar != 'd' || dmnew.DungeonMap[this.level][var2][var3].isPassable) {
                boolean var5 = false;
                dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                if (this.x == var2 && this.y == var3) {
                    if (dmnew.DungeonMap[this.level][this.x][this.y].mapchar == '>') {
                        var5 = true;
                    }
                } else {
                    var5 = true;
                    boolean var7 = false;
                    boolean var8 = false;
                    int var9 = 0;

                    while(var9 < 6) {
                        dmnew.Monster var6 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + var9);
                        if (var6 != null) {
                            var7 = true;
                            if (!var6.isImmaterial && !var6.isflying) {
                                var8 = true;
                            }
                        }

                        if (var9 == 3) {
                            var9 = 5;
                        } else {
                            ++var9;
                        }
                    }

                    dmnew.DungeonMap[this.level][this.x][this.y].hasMons = var7;
                    if (!this.isflying && !this.isImmaterial && !var8) {
                        dmnew.dmmons.put(this.level + "," + var2 + "," + var3 + "," + var4, this);
                        boolean var10 = dmnew.DungeonMap[this.level][var2][var3].hasMons;
                        dmnew.DungeonMap[this.level][var2][var3].hasMons = true;
                        dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(5);
                        dmnew.dmmons.remove(this.level + "," + var2 + "," + var3 + "," + var4);
                        dmnew.DungeonMap[this.level][var2][var3].hasMons = var10;
                    }
                }

                this.subsquare = var4;
                if ((this.x != var2 || this.y != var3) && !this.isflying && !this.isImmaterial) {
                    dmnew.DungeonMap[this.level][var2][var3].tryFloorSwitch(4);
                }

                this.x = var2;
                this.y = var3;
                dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + this.subsquare, this);
                dmnew.DungeonMap[this.level][this.x][this.y].hasMons = true;
                this.xdist = this.x - dmnew.partyx;
                if (this.xdist < 0) {
                    this.xdist *= -1;
                }

                this.ydist = this.y - dmnew.partyy;
                if (this.ydist < 0) {
                    this.ydist *= -1;
                }

                if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                    dmnew.needredraw = true;
                }

                if (var5) {
                    dmnew.DungeonMap[this.level][this.x][this.y].tryTeleport(this);
                    this.xdist = this.x - dmnew.partyx;
                    if (this.xdist < 0) {
                        this.xdist *= -1;
                    }

                    this.ydist = this.y - dmnew.partyy;
                    if (this.ydist < 0) {
                        this.ydist *= -1;
                    }
                }

                if (this.pickup != 0 && dmnew.DungeonMap[this.level][this.x][this.y].hasItems && (this.pickup == 4 || this.pickup == 2 && dmnew.randGen.nextBoolean() || this.pickup == 1 && dmnew.randGen.nextInt(4) == 0 || this.pickup == 3 && dmnew.randGen.nextInt(4) != 0)) {
                    Item var11;
                    if (this.subsquare < 4) {
                        var11 = dmnew.DungeonMap[this.level][this.x][this.y].pickUpItem(this.subsquare);
                    } else {
                        var11 = (Item)dmnew.DungeonMap[this.level][this.x][this.y].mapItems.remove(dmnew.DungeonMap[this.level][this.x][this.y].mapItems.size() - 1);
                        if (dmnew.DungeonMap[this.level][this.x][this.y].mapItems.isEmpty()) {
                            dmnew.DungeonMap[this.level][this.x][this.y].hasItems = false;
                        }
                    }

                    if (var11 != null) {
                        this.carrying.add(var11);
                        if (this.useammo && (var11.number > 220 && (var11.hasthrowpic || var11.number == 266) || var11.projtype > 0 && (double)var11.weight <= 1.0D)) {
                            ++this.ammo;
                        }

                        dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(3);
                    }
                }

                if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                    dmnew.needredraw = true;
                    if (this.attackspeed > 0 && (this.xdist == 0 && this.ydist == 1 || this.ydist == 0 && this.xdist == 1)) {
                        this.movecounter = this.attackspeed;
                    }
                }

                if (this.movespeed - this.movecounter < 2) {
                    this.moveattack = dmnew.moncycle;
                }

            }
        }
    }

    public void doAttack() {
        boolean var1 = false;
        if (dmnew.partyx > this.x && this.facing != 3) {
            this.facing = 3;
            var1 = true;
        } else if (dmnew.partyx < this.x && this.facing != 1) {
            this.facing = 1;
            var1 = true;
        } else if (dmnew.partyy > this.y && this.facing != 2) {
            this.facing = 2;
            var1 = true;
        } else if (dmnew.partyy < this.y && this.facing != 0) {
            this.facing = 0;
            var1 = true;
        } else if (dmnew.alldead) {
            var1 = true;
        }

        if (this.waitattack && var1 && !dmnew.alldead) {
            var1 = false;
            this.waitattack = false;
        }

        if (!var1) {
            if (this.hasmagic && !this.silenced && this.mana >= this.minproj && dmnew.randGen.nextInt() % 10 > 0) {
                if (this.x < dmnew.partyx && this.canDoProj(3)) {
                    this.doProjAttack(3);
                    return;
                }

                if (this.x > dmnew.partyx && this.canDoProj(1)) {
                    this.doProjAttack(1);
                    return;
                }

                if (this.y > dmnew.partyy && this.canDoProj(0)) {
                    this.doProjAttack(0);
                    return;
                }

                if (this.y < dmnew.partyy && this.canDoProj(2)) {
                    this.doProjAttack(2);
                    return;
                }
            } else if (this.hasdrain && !this.silenced && this.mana >= this.minproj && this.health < this.maxhealth) {
                byte var4;
                byte var5;
                byte var6;
                byte var7;
                if (this.facing == 0) {
                    var4 = 2;
                    var5 = 3;
                    var6 = 0;
                    var7 = 1;
                } else if (this.facing == 2) {
                    var4 = 0;
                    var5 = 1;
                    var6 = 2;
                    var7 = 3;
                } else if (this.facing == 3) {
                    var4 = 0;
                    var5 = 3;
                    var6 = 1;
                    var7 = 2;
                } else {
                    var4 = 1;
                    var5 = 2;
                    var6 = 0;
                    var7 = 3;
                }

                int var2;
                int var3;
                if (dmnew.randGen.nextBoolean()) {
                    var2 = (var4 + dmnew.facing) % 4;
                    var3 = (var5 + dmnew.facing) % 4;
                } else {
                    var2 = (var5 + dmnew.facing) % 4;
                    var3 = (var4 + dmnew.facing) % 4;
                }

                if (dmnew.heroatsub[var2] == -1) {
                    var2 = var3;
                    if (dmnew.heroatsub[var3] == -1) {
                        if (dmnew.randGen.nextBoolean()) {
                            var2 = (var6 + dmnew.facing) % 4;
                            var3 = (var7 + dmnew.facing) % 4;
                        } else {
                            var2 = (var7 + dmnew.facing) % 4;
                            var3 = (var6 + dmnew.facing) % 4;
                        }

                        if (dmnew.heroatsub[var2] == -1) {
                            var2 = var3;
                        }
                    }
                }

                var2 = dmnew.heroatsub[var2];
                int var9 = this.castpower + 1;

                int var8;
                do {
                    --var9;
                    var8 = var9;

                    for(int var10 = 0; var10 < 3; ++var10) {
                        var8 += var9 * 6 * var10;
                    }
                } while(var8 > this.mana && var9 >= 0);

                try {
                    Spell var13 = new Spell(var9 + "666");
                    var13.powers[var13.gain - 1] += dmnew.randGen.nextInt() % 10 + (var13.gain - 1) * this.manapower / 8;
                    var13.power = var13.powers[var13.gain - 1];
                    if (var13.power < 1) {
                        var13.power = dmnew.randGen.nextInt(4) + 1;
                    }

                    int var11 = dmnew.hero[var2].damage(var13.power, 7);
                    if (var11 > 0) {
                        this.heal(var11);
                        dmnew.message.setMessage(this.name + " drains " + var11 + " life from " + dmnew.hero[var2].name, 5);
                    } else if (var11 != 0) {
                        this.damage(var11, 1);
                    }

                    if (this.soundstring != null) {
                        dmnew.playSound(this.soundstring, this.x, this.y);
                    }

                    this.iscasting = true;
                    this.movecounter = this.attackspeed;
                    dmnew.needredraw = true;
                    dmnew.playSound("oof.wav", -1, -1);
                    return;
                } catch (Exception var12) {
                    ;
                }
            }

            if (this.soundstring != null) {
                dmnew.playSound(this.soundstring, this.x, this.y);
            }

            this.isattacking = true;
            this.movecounter = this.attackspeed;
        } else {
            this.movecounter = this.movespeed - 2;
            if (this.movecounter < 0) {
                this.movecounter = 0;
            }
        }

        dmnew.needredraw = true;
    }

    public boolean canDoProj(int var1) {
        boolean var3 = false;
        boolean var4 = true;
        boolean var5 = true;
        boolean var6 = false;
        boolean var7 = true;
        boolean var8 = true;
        int var2;
        dmnew.Monster var9;
        byte var10;
        byte var11;
        switch(var1) {
            case 0:
                if (this.hasmagic && this.mana >= this.minproj) {
                    if (this.ydist > 4 && this.ydist * 2 > this.castpower * 5 + 3) {
                        return false;
                    }
                } else if (this.ydist > 4 && this.ydist * 2 > (this.power + 5) / 8 + 1) {
                    return false;
                }

                var2 = this.y;
                this.castsub = 0;
                var10 = 0;
                var11 = 3;
                if (this.subsquare > 0 && this.subsquare < 3) {
                    var10 = 1;
                    var11 = 2;
                }

                if (dmnew.heroatsub[(var10 + dmnew.facing) % 4] == -1 && dmnew.heroatsub[(var11 + dmnew.facing) % 4] == -1) {
                    var5 = false;
                    var7 = false;
                }

                if (this.subsquare == 5) {
                    var8 = dmnew.heroatsub[(1 + dmnew.facing) % 4] != -1 || dmnew.heroatsub[(2 + dmnew.facing) % 4] != -1;
                    if (var8 && (!var5 || dmnew.randGen.nextBoolean())) {
                        var10 = 1;
                        var11 = 2;
                        var5 = true;
                        this.castsub = 1;
                    }
                }

                if (!var5) {
                    return false;
                }

                if (this.subsquare == 0 || this.subsquare == 1 || this.subsquare == 5 || this.ignoremons) {
                    --var2;
                }

                for(; var5 && var2 > dmnew.partyy; --var2) {
                    if (!dmnew.DungeonMap[this.level][this.x][var2].canPassProjs) {
                        var5 = false;
                    } else if (!this.ignoremons) {
                        var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + var2 + "," + 5);
                        if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.y || 5 != this.subsquare)) {
                            var5 = false;
                        } else {
                            var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + var2 + "," + var10);
                            if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.y || var10 != this.subsquare)) {
                                var5 = false;
                            } else {
                                var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + var2 + "," + var11);
                                if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.y || var11 != this.subsquare)) {
                                    var5 = false;
                                }
                            }
                        }

                        if (!var5 && this.subsquare == 5 && !var6) {
                            if (this.castsub == 0 && var8) {
                                var5 = true;
                                this.castsub = 1;
                                var10 = 1;
                                var11 = 2;
                            } else if (var7) {
                                var5 = true;
                                this.castsub = 0;
                                var10 = 0;
                                var11 = 3;
                            }

                            var6 = true;
                            var2 = this.y;
                        }
                    }
                }

                return var5;
            case 1:
                if (this.hasmagic && this.mana >= this.minproj) {
                    if (this.xdist > 4 && this.xdist * 2 > this.castpower * 5 + 3) {
                        return false;
                    }
                } else if (this.xdist > 4 && this.xdist * 2 > (this.power + 5) / 8 + 1) {
                    return false;
                }

                var2 = this.x;
                this.castsub = 0;
                var10 = 0;
                var11 = 1;
                if (this.subsquare > 1 && this.subsquare != 5) {
                    var10 = 2;
                    var11 = 3;
                }

                if (dmnew.heroatsub[(var10 + dmnew.facing) % 4] == -1 && dmnew.heroatsub[(var11 + dmnew.facing) % 4] == -1) {
                    var5 = false;
                    var7 = false;
                }

                if (this.subsquare == 5) {
                    var8 = dmnew.heroatsub[(2 + dmnew.facing) % 4] != -1 || dmnew.heroatsub[(3 + dmnew.facing) % 4] != -1;
                    if (var8 && (!var5 || dmnew.randGen.nextBoolean())) {
                        var5 = true;
                        this.castsub = 3;
                        var10 = 2;
                        var11 = 3;
                    }
                }

                if (!var5) {
                    return false;
                }

                if (this.subsquare == 0 || this.subsquare == 3 || this.subsquare == 5 || this.ignoremons) {
                    --var2;
                }

                for(; var5 && var2 > dmnew.partyx; --var2) {
                    if (!dmnew.DungeonMap[this.level][var2][this.y].canPassProjs) {
                        var5 = false;
                    } else if (!this.ignoremons) {
                        var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + var2 + "," + this.y + "," + 5);
                        if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.x || 5 != this.subsquare)) {
                            var5 = false;
                        } else {
                            var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + var2 + "," + this.y + "," + var10);
                            if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.x || var10 != this.subsquare)) {
                                var5 = false;
                            } else {
                                var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + var2 + "," + this.y + "," + var11);
                                if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.x || var11 != this.subsquare)) {
                                    var5 = false;
                                }
                            }
                        }

                        if (!var5 && this.subsquare == 5 && !var6) {
                            if (this.castsub == 0 && var8) {
                                var5 = true;
                                this.castsub = 3;
                                var10 = 2;
                                var11 = 3;
                            } else if (var7) {
                                var5 = true;
                                this.castsub = 0;
                                var10 = 0;
                                var11 = 1;
                            }

                            var6 = true;
                            var2 = this.x;
                        }
                    }
                }

                return var5;
            case 2:
                if (this.hasmagic && this.mana >= this.minproj) {
                    if (this.ydist > 4 && this.ydist * 2 > this.castpower * 5 + 3) {
                        return false;
                    }
                } else if (this.ydist > 4 && this.ydist * 2 > (this.power + 5) / 8 + 1) {
                    return false;
                }

                var2 = this.y;
                this.castsub = 3;
                var10 = 0;
                var11 = 3;
                if (this.subsquare > 0 && this.subsquare < 3) {
                    var10 = 1;
                    var11 = 2;
                }

                if (dmnew.heroatsub[(var10 + dmnew.facing) % 4] == -1 && dmnew.heroatsub[(var11 + dmnew.facing) % 4] == -1) {
                    var5 = false;
                    var7 = false;
                }

                if (this.subsquare == 5) {
                    var8 = dmnew.heroatsub[(1 + dmnew.facing) % 4] != -1 || dmnew.heroatsub[(2 + dmnew.facing) % 4] != -1;
                    if (var8 && (!var5 || dmnew.randGen.nextBoolean())) {
                        var10 = 1;
                        var11 = 2;
                        var5 = true;
                        this.castsub = 2;
                    }
                }

                if (!var5) {
                    return false;
                }

                if (this.subsquare == 2 || this.subsquare == 3 || this.subsquare == 5 || this.ignoremons) {
                    ++var2;
                }

                for(; var5 && var2 < dmnew.partyy; ++var2) {
                    if (!dmnew.DungeonMap[this.level][this.x][var2].canPassProjs) {
                        var5 = false;
                    } else if (!this.ignoremons) {
                        var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + var2 + "," + 5);
                        if (var9 == null || var9.isdying || var9.isImmaterial || var2 == this.y && 5 == this.subsquare) {
                            var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + var2 + "," + var10);
                            if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.y || var10 != this.subsquare)) {
                                var5 = false;
                            } else {
                                var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + var2 + "," + var11);
                                if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.y || var11 != this.subsquare)) {
                                    var5 = false;
                                }
                            }
                        } else {
                            var5 = false;
                        }

                        if (!var5 && this.subsquare == 5 && !var6) {
                            if (this.castsub == 3 && var8) {
                                var5 = true;
                                this.castsub = 2;
                                var10 = 1;
                                var11 = 2;
                            } else if (var7) {
                                var5 = true;
                                this.castsub = 3;
                                var10 = 0;
                                var11 = 3;
                            }

                            var6 = true;
                            var2 = this.y;
                        }
                    }
                }

                return var5;
            case 3:
                if (this.hasmagic && this.mana >= this.minproj) {
                    if (this.xdist > 4 && this.xdist * 2 > this.castpower * 5 + 3) {
                        return false;
                    }
                } else if (this.xdist > 4 && this.xdist * 2 > (this.power + 5) / 8 + 1) {
                    return false;
                }

                var2 = this.x;
                this.castsub = 1;
                var10 = 0;
                var11 = 1;
                if (this.subsquare > 1 && this.subsquare != 5) {
                    var10 = 2;
                    var11 = 3;
                }

                if (dmnew.heroatsub[(var10 + dmnew.facing) % 4] == -1 && dmnew.heroatsub[(var11 + dmnew.facing) % 4] == -1) {
                    var5 = false;
                    var7 = false;
                }

                if (this.subsquare == 5) {
                    var8 = dmnew.heroatsub[(2 + dmnew.facing) % 4] != -1 || dmnew.heroatsub[(3 + dmnew.facing) % 4] != -1;
                    if (var8 && (!var5 || dmnew.randGen.nextBoolean())) {
                        var5 = true;
                        this.castsub = 2;
                        var10 = 2;
                        var11 = 3;
                    }
                }

                if (!var5) {
                    return false;
                }

                if (this.subsquare == 1 || this.subsquare == 2 || this.subsquare == 5 || this.ignoremons) {
                    ++var2;
                }

                for(; var5 && var2 < dmnew.partyx; ++var2) {
                    if (!dmnew.DungeonMap[this.level][var2][this.y].canPassProjs) {
                        var5 = false;
                    } else if (!this.ignoremons) {
                        var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + var2 + "," + this.y + "," + 5);
                        if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.x || 5 != this.subsquare)) {
                            var5 = false;
                        } else {
                            var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + var2 + "," + this.y + "," + var10);
                            if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.x || var10 != this.subsquare)) {
                                var5 = false;
                            } else {
                                var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + var2 + "," + this.y + "," + var11);
                                if (var9 != null && !var9.isdying && !var9.isImmaterial && (var2 != this.x || var11 != this.subsquare)) {
                                    var5 = false;
                                }
                            }
                        }

                        if (!var5 && this.subsquare == 5 && !var6) {
                            if (this.castsub == 1 && var8) {
                                var5 = true;
                                this.castsub = 2;
                                var10 = 2;
                                var11 = 3;
                            } else if (var7) {
                                var5 = true;
                                this.castsub = 1;
                                var10 = 0;
                                var11 = 1;
                            }

                            var6 = true;
                            var2 = this.x;
                        }
                    }
                }
        }

        return var5;
    }

    public void doProjAttack(int var1) {
        int var3 = (this.subsquare + dmnew.facing) % 4;
        if (this.subsquare == 5) {
            var3 = (this.castsub + dmnew.facing) % 4;
        }

        int var5;
        int var7;
        Spell var8;
        if (this.hasmagic && !this.silenced && this.mana >= this.minproj && (this.ammo == 0 || dmnew.randGen.nextBoolean())) {
            boolean var12 = false;
            var5 = 0;
            int var13 = 0;
            var7 = dmnew.randGen.nextInt(this.numspells);

            while(!var12) {
                var13 = this.castpower;

                do {
                    var5 = var13;

                    for(int var14 = 0; var14 < this.knownspells[var7].length(); ++var14) {
                        var5 += dmnew.SYMBOLCOST[Integer.parseInt(this.knownspells[var7].substring(var14, var14 + 1)) - 1 + var14 * 6][var13 - 1];
                    }

                    --var13;
                    if (this.number == 25) {
                        var5 = 0;
                    }
                } while(var5 > this.mana && var13 > 0);

                if (var5 <= this.mana) {
                    var12 = true;
                } else {
                    var7 = (var7 + 1) % this.numspells;
                }
            }

            if (this.soundstring != null) {
                dmnew.playSound(this.soundstring, this.x, this.y);
            }

            try {
                var8 = new Spell(var13 + 1 + this.knownspells[var7]);
                if (var8.number != 461 && var8.number != 363 && var8.number != 362 && var8.number != 664 && var8.number != 523) {
                    for(int var9 = var8.gain - 1; var9 >= 0; --var9) {
                        var8.powers[var9] += dmnew.randGen.nextInt() % 10 + var9 * this.manapower / 8;
                        if (var8.powers[var9] < 1) {
                            var8.powers[var9] = dmnew.randGen.nextInt(4) + 1;
                        }
                    }

                    var8.power = var8.powers[var8.gain - 1];
                }

                dmnew.this.new Projectile(var8, this.x, this.y, var8.dist, var1, var3);
                this.mana -= var5;
            } catch (Exception var11) {
                var11.printStackTrace();
                this.movecounter = this.attackspeed;
            }
        } else {
            if (this.soundstring != null) {
                dmnew.playSound(this.soundstring, this.x, this.y);
            }

            Item var4 = null;
            var5 = 0;
            boolean var6 = false;

            while(true) {
                while(!var6) {
                    var4 = (Item)this.carrying.get(var5);
                    if (var4.type == 0 && (var4.number > 220 && (var4.hasthrowpic || var4.number == 266) || var4.projtype > 0 && (double)var4.weight <= 1.0D)) {
                        this.carrying.remove(var5);
                        var6 = true;
                    } else {
                        ++var5;
                    }
                }

                --this.ammo;
                var4.shotpow = this.castpower;
                var7 = (this.power + 5) / 5 - (int)(var4.weight / 2.0F) + dmnew.randGen.nextInt() % 2;
                if (var7 < 8) {
                    var7 = 8;
                }

                if (var4.isbomb) {
                    try {
                        var8 = new Spell(var4.bombnum);
                        var8.power = var4.potionpow + dmnew.randGen.nextInt() % 10;
                        dmnew.this.new Projectile(var8, this.x, this.y, var7, var1, var3);
                    } catch (Exception var10) {
                        dmnew.this.new Projectile(var4, this.x, this.y, var7, var1, var3);
                    }
                } else {
                    dmnew.this.new Projectile(var4, this.x, this.y, var7, var1, var3);
                }
                break;
            }
        }

        if (dmnew.partyx > this.x && this.facing != 3) {
            this.facing = 3;
        } else if (dmnew.partyx < this.x && this.facing != 1) {
            this.facing = 1;
        } else if (dmnew.partyy > this.y && this.facing != 2) {
            this.facing = 2;
        } else if (dmnew.partyy < this.y && this.facing != 0) {
            this.facing = 0;
        }

        this.iscasting = true;
        dmnew.needredraw = true;
        this.movecounter = this.attackspeed;
    }

    public void useHealMagic() {
        int var1 = this.mana / 60;
        if (var1 > this.castpower) {
            var1 = this.castpower;
        }

        this.heal(var1 * (dmnew.randGen.nextInt(this.manapower / 2) + this.manapower / 2));
        this.energize(var1 * -60);
        this.iscasting = true;
        if (this.level == dmnew.level) {
            dmnew.needredraw = true;
        }

        this.movecounter = this.attackspeed;
    }

    public boolean teleport() {
        int var5 = 121;
        int var6 = dmnew.randGen.nextInt(4);
        int var1;
        int var2;
        byte var3;
        byte var4;
        if (var6 == 0) {
            var1 = this.x - 5;
            var2 = this.y - 5;
            var3 = 1;
            var4 = 1;
        } else if (var6 == 2) {
            var1 = this.x + 5;
            var2 = this.y + 5;
            var3 = -1;
            var4 = -1;
        } else if (var6 == 1) {
            var1 = this.x - 5;
            var2 = this.y + 5;
            var3 = 1;
            var4 = -1;
        } else {
            var1 = this.x + 5;
            var2 = this.y - 5;
            var3 = -1;
            var4 = 1;
        }

        do {
            var1 += var3;
            if (var1 > this.x + 5) {
                var1 = this.x - 5;
                var2 += var4;
            } else if (var1 < this.x - 5) {
                var1 = this.x + 5;
                var2 += var4;
            }

            --var5;
        } while(var5 > 0 && (var1 < 0 || var2 < 0 || var1 >= dmnew.mapwidth || var2 >= dmnew.mapheight || !dmnew.DungeonMap[this.level][var1][var2].isPassable || dmnew.DungeonMap[this.level][var1][var2].hasMons || dmnew.DungeonMap[this.level][var1][var2].hasParty || !dmnew.DungeonMap[this.level][var1][var2].canPassMons || dmnew.DungeonMap[this.level][var1][var2].mapchar == '>' || var1 == this.x && var2 == this.y || this.number == 26 && dmnew.fluxcages.get(this.level + "," + var1 + "," + var2) != null));

        if (var5 != 0) {
            dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
            boolean var8 = false;
            boolean var9 = false;
            int var10 = 0;

            while(var10 < 6) {
                dmnew.Monster var7 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + var10);
                if (var7 != null) {
                    var8 = true;
                    if (!var7.isImmaterial && !var7.isflying) {
                        var9 = true;
                    }
                }

                if (var10 == 3) {
                    var10 = 5;
                } else {
                    ++var10;
                }
            }

            dmnew.DungeonMap[this.level][this.x][this.y].hasMons = var8;
            if (!this.isflying && !this.isImmaterial && !var9) {
                dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(5);
            }

            if (!this.isflying && !this.isImmaterial) {
                dmnew.DungeonMap[this.level][var1][var2].tryFloorSwitch(4);
            }

            this.x = var1;
            this.y = var2;
            dmnew.dmmons.put(this.level + "," + this.x + "," + this.y + "," + this.subsquare, this);
            dmnew.DungeonMap[this.level][this.x][this.y].hasMons = true;
            this.xdist = this.x - dmnew.partyx;
            if (this.xdist < 0) {
                this.xdist *= -1;
            }

            this.ydist = this.y - dmnew.partyy;
            if (this.ydist < 0) {
                this.ydist *= -1;
            }

            if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                dmnew.needredraw = true;
            }

            dmnew.DungeonMap[this.level][this.x][this.y].tryTeleport(this);
            this.xdist = this.x - dmnew.partyx;
            if (this.xdist < 0) {
                this.xdist *= -1;
            }

            this.ydist = this.y - dmnew.partyy;
            if (this.ydist < 0) {
                this.ydist *= -1;
            }

            if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                dmnew.needredraw = true;
            }

            return true;
        } else {
            return false;
        }
    }

    public void heal(int var1) {
        this.health += var1;
        if (this.health > this.maxhealth) {
            this.health = this.maxhealth;
        }

    }

    public void energize(int var1) {
        this.mana += var1;
        if (this.mana > this.maxmana) {
            this.mana = this.maxmana;
        } else if (this.mana < 0) {
            this.mana = 0;
        }

    }

    public int damage(int var1, int var2) {
        if (this.isdying) {
            return 0;
        } else {
            boolean var3;
            if (this.needitem != 0 && var2 != 4 && var2 != 6) {
                var3 = false;
                int var4;
                if (this.needhandneck == 0) {
                    for(var4 = 0; var4 < dmnew.numheroes; ++var4) {
                        if (dmnew.hero[var4].weapon.number != this.needitem && (dmnew.hero[var4].hand == null || dmnew.hero[var4].hand.number != this.needitem)) {
                            if ((this.needitem != 248 || dmnew.hero[var4].weapon.number != 249) && (dmnew.hero[var4].hand == null || dmnew.hero[var4].hand.number != 249)) {
                                if (dmnew.hero[var4].weapon.number == 215) {
                                    var3 = true;
                                }
                            } else {
                                var3 = true;
                            }
                        } else {
                            var3 = true;
                        }
                    }
                } else {
                    for(var4 = 0; var4 < dmnew.numheroes; ++var4) {
                        if (dmnew.hero[var4].neck != null && dmnew.hero[var4].neck.number == this.needitem) {
                            var3 = true;
                        }
                    }

                    if (!var3) {
                        for(int var5 = 0; var5 < dmnew.numheroes; ++var5) {
                            if (dmnew.hero[var5].weapon.number == 215) {
                                var3 = true;
                            }
                        }
                    }
                }

                if (!var3) {
                    if (var2 != 2 && var2 != 3) {
                        dmnew.message.setMessage(this.name + " is unaffected.", 4);
                    }

                    return 0;
                }
            }

            if (this.hurtitem != 0 && var2 != 0 && var2 != 4 && var2 != 6 && var2 != 5) {
                if (var2 != 2 && var2 != 3) {
                    dmnew.message.setMessage(this.name + " is unaffected.", 4);
                }

                return 0;
            } else {
                if (var2 != 0 && var2 != 5 && var2 != 4) {
                    if (var2 == 1 || var2 == 7) {
                        var1 -= var1 * this.magicresist / 100;
                    }
                } else {
                    var1 -= var1 * this.defense / 100;
                }

                if (var1 < 1) {
                    var1 = dmnew.randGen.nextInt(4) + 1;
                }

                if (var2 == 7 && (this.isImmaterial || this.number == 8 || this.number == 21)) {
                    this.health += var1;
                    dmnew.message.setMessage(this.name + " gains some life.", 5);
                    return 0 - var1;
                } else {
                    this.health -= var1;
                    this.xdist = this.x - dmnew.partyx;
                    if (this.xdist < 0) {
                        this.xdist *= -1;
                    }

                    this.ydist = this.y - dmnew.partyy;
                    if (this.ydist < 0) {
                        this.ydist *= -1;
                    }

                    if (this.xdist < 5 && this.ydist < 5 && var2 != 2 && var2 != 3 && var2 != 6) {
                        dmnew.message.setMessage(this.name + " takes " + var1 + " damage.", 5);
                    }

                    Iterator var9;
                    if (this.health < 1) {
                        this.isdying = true;
                        if (this.gamewin) {
                            dmnew.nomovement = true;
                            this.isdying = false;
                            if (var2 == 6) {
                                dmnew.endanim = "fuseend.gif";
                                dmnew.endsound = "fuseend.wav";
                            } else {
                                dmnew.endanim = this.endanim;
                                dmnew.endsound = this.endsound;
                            }

                            dmnew.gameover = true;
                            return var1;
                        }

                        if (!this.carrying.isEmpty()) {
                            dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(2);
                        }

                        var3 = false;
                        var9 = this.carrying.iterator();

                        while(var9.hasNext()) {
                            Item var8 = (Item)var9.next();
                            if (var8.type != 0 || var8.weight > 4.0F) {
                                var3 = true;
                            }

                            if (this.subsquare != 5) {
                                var8.subsquare = this.subsquare;
                            } else {
                                var8.subsquare = dmnew.randGen.nextInt(4);
                            }

                            if (!dmnew.DungeonMap[this.level][this.x][this.y].tryTeleport(var8)) {
                                dmnew.DungeonMap[this.level][this.x][this.y].addItem(var8);
                            }
                        }

                        if (!this.carrying.isEmpty() && this.level == dmnew.level) {
                            if (var3) {
                                dmnew.playSound("thunk.wav", this.x, this.y);
                            } else {
                                dmnew.playSound("dink.wav", this.x, this.y);
                            }
                        }

                        if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
                            dmnew.needredraw = true;
                        } else {
                            dmnew.dmmons.remove(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                            boolean var6 = false;
                            if (this.subsquare != 5) {
                                for(int var7 = 0; var7 < 3; ++var7) {
                                    if (dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + var7) != null) {
                                        var6 = true;
                                    }
                                }
                            }

                            dmnew.DungeonMap[this.level][this.x][this.y].hasMons = var6;
                        }

                        if (!this.isflying && !this.isImmaterial) {
                            dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(5);
                        }
                    } else if (var2 != 3 || this.currentai == 3 || this.isImmaterial || !dmnew.randGen.nextBoolean() || this.health >= 80 && dmnew.randGen.nextInt(20) != 0) {
                        if (this.fearresist > 0 && this.currentai != 3 && !this.isImmaterial && this.health < 600 && dmnew.DungeonMap[this.level][this.x][this.y].hasCloud && dmnew.randGen.nextBoolean()) {
                            var3 = false;
                            if (dmnew.randGen.nextInt(10) < this.fearresist || this.fearresist > 0 && this.health < 40 && this.maxhealth < 400 && dmnew.randGen.nextBoolean()) {
                                var9 = dmnew.cloudstochange.iterator();

                                while(var9.hasNext()) {
                                    dmnew.PoisonCloud var10 = (dmnew.PoisonCloud)var9.next();
                                    if (var10.x == this.x && var10.y == this.y) {
                                        if (this.health < var10.stage * 100) {
                                            var3 = true;
                                        }
                                        break;
                                    }
                                }
                            }

                            if (var3) {
                                this.runcounter = 2;
                                this.currentai = 3;
                            }
                        }
                    } else {
                        this.runcounter = 2;
                        this.currentai = 3;
                    }

                    return var1;
                }
            }
        }
    }

    public void pitDeath() {
        if (!this.carrying.isEmpty()) {
            dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(2);
        }

        boolean var1 = false;
        Iterator var3 = this.carrying.iterator();

        while(var3.hasNext()) {
            Item var2 = (Item)var3.next();
            if (var2.type != 0 || var2.weight > 4.0F) {
                var1 = true;
            }

            var2.subsquare = this.subsquare;
            if (!dmnew.DungeonMap[this.level][this.x][this.y].tryTeleport(var2)) {
                dmnew.DungeonMap[this.level][this.x][this.y].addItem(var2);
            }
        }

        if (!this.carrying.isEmpty() && this.level == dmnew.level) {
            if (var1) {
                dmnew.playSound("thunk.wav", this.x, this.y);
            } else {
                dmnew.playSound("dink.wav", this.x, this.y);
            }
        }

        if (this.level == dmnew.level && this.xdist < 5 && this.ydist < 5) {
            dmnew.needredraw = true;
        }

        if (this.gamewin) {
            dmnew.nomovement = true;
            dmnew.endanim = this.endanim;
            dmnew.endsound = this.endsound;
            dmnew.gameover = true;
        }

    }
}
