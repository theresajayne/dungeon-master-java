package com.github.theresajayne.dmjava.entities;

class Formation extends JPanel implements MouseListener {
    Cursor cursor;
    ImageIcon[] heroicons = new ImageIcon[4];
    JLabel[] herolabels = new JLabel[4];
    boolean ischanging = false;
    int oldindex;

    public Formation() {
        this.setLayout(new GridLayout(2, 2));

        for(int var2 = 0; var2 < 4; ++var2) {
            this.heroicons[var2] = new ImageIcon("heroicon" + var2 + ".gif");
            this.herolabels[var2] = new JLabel();
            if (var2 < dmnew.numheroes) {
                this.herolabels[var2].setIcon(this.heroicons[var2]);
            }

            this.herolabels[var2].setPreferredSize(new Dimension(32, 24));
            this.herolabels[var2].setMinimumSize(new Dimension(32, 24));
            this.herolabels[var2].setMaximumSize(new Dimension(32, 24));
            this.add(this.herolabels[var2]);
        }

        this.setBackground(Color.black);
        this.setBorder(BorderFactory.createBevelBorder(1, new Color(60, 60, 80), new Color(20, 20, 40)));
        this.cursor = new Cursor(12);
        this.setCursor(this.cursor);
        this.addMouseListener(this);
    }

    public void addNewHero() {
        for(int var1 = 0; var1 < 2; ++var1) {
            if (dmnew.heroatsub[var1] != -1) {
                this.herolabels[var1].setIcon(this.heroicons[dmnew.heroatsub[var1]]);
            } else {
                this.herolabels[var1].setIcon((Icon)null);
            }
        }

        if (dmnew.heroatsub[2] != -1) {
            this.herolabels[3].setIcon(this.heroicons[dmnew.heroatsub[2]]);
        } else {
            this.herolabels[3].setIcon((Icon)null);
        }

        if (dmnew.heroatsub[3] != -1) {
            this.herolabels[2].setIcon(this.heroicons[dmnew.heroatsub[3]]);
        } else {
            this.herolabels[2].setIcon((Icon)null);
        }

        this.repaint();
    }

    public void mousePressed(MouseEvent var1) {
        int var3 = var1.getX();
        int var4 = var1.getY();
        byte var2;
        if (var4 < this.getSize().height / 2) {
            if (var3 < this.getSize().width / 2) {
                var2 = 0;
            } else {
                var2 = 1;
            }
        } else if (var3 < this.getSize().width / 2) {
            var2 = 3;
        } else {
            var2 = 2;
        }

        if (this.ischanging) {
            if (var2 == this.oldindex) {
                if (var2 < 2) {
                    this.herolabels[var2].setIcon(this.heroicons[dmnew.heroatsub[var2]]);
                } else if (var2 == 2) {
                    this.herolabels[3].setIcon(this.heroicons[dmnew.heroatsub[var2]]);
                } else {
                    this.herolabels[2].setIcon(this.heroicons[dmnew.heroatsub[var2]]);
                }
            } else if (dmnew.heroatsub[var2] != -1) {
                if (var2 < 2) {
                    this.herolabels[var2].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
                } else if (var2 == 2) {
                    this.herolabels[3].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
                } else {
                    this.herolabels[2].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
                }

                if (this.oldindex < 2) {
                    this.herolabels[this.oldindex].setIcon(this.heroicons[dmnew.heroatsub[var2]]);
                } else if (this.oldindex == 2) {
                    this.herolabels[3].setIcon(this.heroicons[dmnew.heroatsub[var2]]);
                } else {
                    this.herolabels[2].setIcon(this.heroicons[dmnew.heroatsub[var2]]);
                }

                int var5 = dmnew.heroatsub[var2];
                dmnew.heroatsub[var2] = dmnew.heroatsub[this.oldindex];
                dmnew.heroatsub[this.oldindex] = var5;
                dmnew.hero[dmnew.heroatsub[var2]].subsquare = var2;
                dmnew.hero[dmnew.heroatsub[this.oldindex]].subsquare = this.oldindex;
            } else {
                if (var2 < 2) {
                    this.herolabels[var2].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
                } else if (var2 == 2) {
                    this.herolabels[3].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
                } else {
                    this.herolabels[2].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
                }

                dmnew.heroatsub[var2] = dmnew.heroatsub[this.oldindex];
                dmnew.heroatsub[this.oldindex] = -1;
                dmnew.hero[dmnew.heroatsub[var2]].subsquare = var2;
            }

            this.ischanging = false;
            this.cursor = new Cursor(12);
            this.setCursor(this.cursor);
            this.repaint();
        } else if (dmnew.heroatsub[var2] != -1) {
            this.oldindex = var2;
            if (var2 < 2) {
                this.herolabels[var2].setIcon((Icon)null);
            } else if (var2 == 2) {
                this.herolabels[3].setIcon((Icon)null);
            } else {
                this.herolabels[2].setIcon((Icon)null);
            }

            this.cursor = dmnew.tk.createCustomCursor(this.heroicons[dmnew.heroatsub[var2]].getImage(), new Point(14, 14), "formc");
            this.setCursor(this.cursor);
            this.ischanging = true;
            this.repaint();
        }

    }

    public void mouseExited(MouseEvent var1) {
        if (this.ischanging) {
            this.ischanging = false;
            if (this.oldindex < 2) {
                this.herolabels[this.oldindex].setIcon(this.heroicons[dmnew.heroatsub[this.oldindex]]);
            } else if (this.oldindex == 2) {
                this.herolabels[3].setIcon(this.heroicons[dmnew.heroatsub[2]]);
            } else {
                this.herolabels[2].setIcon(this.heroicons[dmnew.heroatsub[3]]);
            }

            this.cursor = new Cursor(12);
            this.setCursor(this.cursor);
            this.repaint();
        }

    }

    public void mouseEntered(MouseEvent var1) {
    }

    public void mouseClicked(MouseEvent var1) {
    }

    public void mouseReleased(MouseEvent var1) {
    }
}
