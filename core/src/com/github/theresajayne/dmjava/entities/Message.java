package com.github.theresajayne.dmjava.entities;

class Message extends JComponent {
    Color[] messagecolor = new Color[4];
    String[] currentmessage = new String[]{" ", " ", " ", "Welcome"};
    int index = 0;
    int timecounter = 0;
    Color[] colors = new Color[6];

    public Message() {
        this.setPreferredSize(new Dimension(662, 70));
        this.setOpaque(false);
        this.colors[0] = Color.green;
        this.colors[1] = Color.yellow;
        this.colors[2] = Color.red;
        this.colors[3] = Color.blue;
        this.colors[4] = Color.white;
        this.colors[5] = Color.red;
        this.messagecolor[3] = Color.white;
    }

    public void setMessage(String var1, int var2) {
        this.currentmessage[0] = this.currentmessage[1];
        this.currentmessage[1] = this.currentmessage[2];
        this.currentmessage[2] = this.currentmessage[3];
        this.currentmessage[3] = var1;
        this.messagecolor[0] = this.messagecolor[1];
        this.messagecolor[1] = this.messagecolor[2];
        this.messagecolor[2] = this.messagecolor[3];
        this.messagecolor[3] = this.colors[var2];
        this.repaint();
    }

    public void clear() {
        this.currentmessage[0] = " ";
        this.currentmessage[1] = " ";
        this.currentmessage[2] = " ";
        this.currentmessage[3] = " ";
        this.repaint();
    }

    public void timePass() {
        if (!this.currentmessage[3].equals(" ")) {
            ++this.timecounter;
            if (this.timecounter > 150) {
                int var1;
                for(var1 = 0; this.currentmessage[var1].equals(" "); ++var1) {
                    ;
                }

                this.currentmessage[var1] = " ";
                this.timecounter = 0;
            }

            this.repaint();
        }
    }

    public void paint(Graphics var1) {
        var1.setFont(new Font("TimesRoman", 1, 12));
        var1.setColor(Color.black);
        var1.drawString(this.currentmessage[0], 2, 18);
        var1.drawString(this.currentmessage[1], 2, 34);
        var1.drawString(this.currentmessage[2], 2, 50);
        var1.drawString(this.currentmessage[3], 2, 66);
        var1.setColor(this.messagecolor[0]);
        var1.drawString(this.currentmessage[0], 0, 16);
        var1.setColor(this.messagecolor[1]);
        var1.drawString(this.currentmessage[1], 0, 32);
        var1.setColor(this.messagecolor[2]);
        var1.drawString(this.currentmessage[2], 0, 48);
        var1.setColor(this.messagecolor[3]);
        var1.drawString(this.currentmessage[3], 0, 64);
    }
}
