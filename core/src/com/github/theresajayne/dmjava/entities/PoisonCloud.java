package com.github.theresajayne.dmjava.entities;

import java.util.Iterator;

class PoisonCloud {
    int level;
    int x;
    int y;
    int stage;
    int stagecounter;

    public PoisonCloud(int var2, int var3, int var4, int var5) {
        if (dmnew.DungeonMap[var2][var3][var4].hasCloud) {
            Iterator var7 = dmnew.cloudstochange.iterator();

            while(var7.hasNext()) {
                dmnew.PoisonCloud var6 = (dmnew.PoisonCloud)var7.next();
                if (var6.x == var3 && var6.y == var4) {
                    var6.stagecounter = 0;
                    var6.stage += var5;
                    if (var6.stage > 6) {
                        var6.stage = 6;
                    }
                    break;
                }
            }
        } else {
            this.level = var2;
            this.x = var3;
            this.y = var4;
            this.stage = var5;
            this.stagecounter = 0;
            dmnew.cloudstochange.add(this);
            dmnew.DungeonMap[this.level][var3][var4].hasCloud = true;
            dmnew.cloudchanging = true;
        }

    }

    public boolean update() {
        if (dmnew.DungeonMap[this.level][this.x][this.y] instanceof Wall && dmnew.DungeonMap[this.level][this.x][this.y].mapchar != '>' && dmnew.DungeonMap[this.level][this.x][this.y].mapchar != '2') {
            dmnew.DungeonMap[this.level][this.x][this.y].hasCloud = false;
            return false;
        } else {
            ++this.stagecounter;
            boolean var1 = true;
            if (this.stagecounter > 6) {
                --this.stage;
                this.stagecounter = 0;
            }

            int var3;
            if (this.stage > 0) {
                if (dmnew.DungeonMap[this.level][this.x][this.y].hasMons) {
                    dmnew.Monster var2;
                    for(var3 = 0; var3 < 4; ++var3) {
                        var2 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + var3);
                        if (var2 != null && !var2.isImmaterial) {
                            var2.damage(4 * this.stage / 3, 2);
                        }
                    }

                    var2 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 5);
                    if (var2 != null && !var2.isImmaterial) {
                        var2.damage(4 * this.stage / 3, 2);
                    }
                } else if (dmnew.DungeonMap[this.level][this.x][this.y].hasParty) {
                    for(var3 = 0; var3 < dmnew.numheroes; ++var3) {
                        dmnew.hero[var3].damage(4 * this.stage / 3, 2);
                    }
                }
            } else {
                dmnew.DungeonMap[this.level][this.x][this.y].hasCloud = false;
                var1 = false;
            }

            if (!dmnew.needredraw && this.level == dmnew.level) {
                int var4 = this.x - dmnew.partyx;
                if (var4 < 0) {
                    var4 *= -1;
                }

                var3 = this.y - dmnew.partyy;
                if (var3 < 0) {
                    var3 *= -1;
                }

                if (var4 < 4 && var3 < 4) {
                    dmnew.needredraw = true;
                }
            }

            return var1;
        }
    }
}