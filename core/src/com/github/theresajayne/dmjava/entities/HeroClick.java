package com.github.theresajayne.dmjava.entities;

class HeroClick extends MouseAdapter {
    HeroClick() {
    }

    public void mousePressed(MouseEvent var1) {
        int var2 = var1.getX();
        int var3 = var1.getY();
        dmnew.Hero var4 = (dmnew.Hero)var1.getSource();
        if (dmnew.this.sleeping) {
            dmnew.needredraw = true;
            dmnew.this.sleeping = false;
            dmnew.sleeper = 0;
            dmnew.this.spellsheet.setVisible(true);
            dmnew.this.weaponsheet.setVisible(true);
            if (dmnew.herosheet.hero.isdead) {
                dmnew.herosheet.setHero(dmnew.hero[dmnew.leader]);
            } else {
                dmnew.herosheet.repaint();
            }
        } else if ((var3 <= 17 || var3 >= 83) && !SwingUtilities.isRightMouseButton(var1)) {
            if (var3 < 18) {
                if (dmnew.iteminhand) {
                    var4.load += dmnew.inhand.weight;
                    dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                }

                dmnew.hero[dmnew.leader].isleader = false;
                var4.isleader = true;
                if (dmnew.hero[0].isleader) {
                    dmnew.leader = 0;
                } else if (dmnew.hero[1].isleader) {
                    dmnew.leader = 1;
                } else if (dmnew.hero[2].isleader) {
                    dmnew.leader = 2;
                } else {
                    dmnew.leader = 3;
                }

                dmnew.this.hupdate();
                if (dmnew.sheet) {
                    dmnew.herosheet.repaint();
                }
            } else {
                Item var5;
                if (var2 > 55 && var2 < 89) {
                    if (var4.weapon.number == 215) {
                        dmnew.message.setMessage("Stormbringer resists...", 4);
                        return;
                    }

                    if (var4.weapon.number == 219 || var4.weapon.number == 261) {
                        return;
                    }

                    if (dmnew.iteminhand) {
                        var4.load += dmnew.inhand.weight;
                        dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    }

                    if (var4.weapon != null) {
                        var4.load -= var4.weapon.weight;
                        dmnew.hero[dmnew.leader].load += var4.weapon.weight;
                    }

                    if (!dmnew.iteminhand && !var4.weapon.name.equals("Fist/Foot")) {
                        dmnew.iteminhand = true;
                        dmnew.inhand = var4.weapon;
                        var4.weapon = dmnew.fistfoot;
                        if (dmnew.inhand.number == 9) {
                            ((Torch)dmnew.inhand).putOut();
                            dmnew.updateDark();
                        }

                        if (dmnew.inhand.type == 0 || dmnew.inhand.type == 1 || dmnew.inhand.number == 4) {
                            dmnew.inhand.unEquipEffect(var4);
                        }

                        var4.repaint();
                        if (dmnew.sheet) {
                            dmnew.herosheet.repaint();
                        }

                        dmnew.this.weaponsheet.update();
                        dmnew.this.changeCursor();
                    } else if (dmnew.iteminhand) {
                        if (var4.weapon.name.equals("Fist/Foot")) {
                            var4.weapon = dmnew.inhand;
                            dmnew.iteminhand = false;
                            if (var4.weapon.number == 9) {
                                ((Torch)var4.weapon).setPic();
                                dmnew.updateDark();
                            }

                            if (dmnew.inhand.type == 0 || dmnew.inhand.type == 1 || dmnew.inhand.number == 4) {
                                var4.weapon.equipEffect(var4);
                            }
                        } else {
                            var5 = var4.weapon;
                            var4.weapon = dmnew.inhand;
                            dmnew.inhand = var5;
                            if (dmnew.inhand.number == 9) {
                                ((Torch)dmnew.inhand).putOut();
                                dmnew.updateDark();
                            }

                            if (var4.weapon.number == 9) {
                                ((Torch)var4.weapon).setPic();
                                dmnew.updateDark();
                            }

                            if (dmnew.inhand.type == 0 || dmnew.inhand.type == 1 || dmnew.inhand.number == 4) {
                                dmnew.inhand.unEquipEffect(var4);
                            }

                            if (var4.weapon.type == 0 || var4.weapon.type == 1 || var4.weapon.number == 4) {
                                var4.weapon.equipEffect(var4);
                            }
                        }

                        var4.repaint();
                        if (dmnew.sheet) {
                            dmnew.herosheet.repaint();
                        }

                        dmnew.this.weaponsheet.update();
                        dmnew.this.changeCursor();
                    }
                } else if (var2 < 45 && var2 > 11) {
                    if (dmnew.iteminhand) {
                        var4.load += dmnew.inhand.weight;
                        dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                    }

                    if (var4.hand != null) {
                        var4.load -= var4.hand.weight;
                        dmnew.hero[dmnew.leader].load += var4.hand.weight;
                    }

                    if (!dmnew.iteminhand && var4.hand != null) {
                        dmnew.iteminhand = true;
                        dmnew.inhand = var4.hand;
                        var4.hand = null;
                        if (dmnew.inhand.number == 9) {
                            ((Torch)dmnew.inhand).putOut();
                            dmnew.updateDark();
                        }

                        if (dmnew.inhand.type == 1) {
                            dmnew.inhand.unEquipEffect(var4);
                        }

                        var4.repaint();
                        if (dmnew.sheet) {
                            dmnew.herosheet.repaint();
                        }

                        dmnew.this.changeCursor();
                    } else if (dmnew.iteminhand) {
                        if (var4.hand != null) {
                            var5 = var4.hand;
                            var4.hand = dmnew.inhand;
                            dmnew.inhand = var5;
                            if (dmnew.inhand.number == 9) {
                                ((Torch)dmnew.inhand).putOut();
                                dmnew.updateDark();
                            }

                            if (var4.hand.number == 9) {
                                ((Torch)var4.hand).setPic();
                                dmnew.updateDark();
                            }

                            if (dmnew.inhand.type == 1) {
                                dmnew.inhand.unEquipEffect(var4);
                            }

                            if (var4.hand.type == 1) {
                                var4.hand.equipEffect(var4);
                            }
                        } else {
                            var4.hand = dmnew.inhand;
                            dmnew.iteminhand = false;
                            if (var4.hand.number == 9) {
                                ((Torch)var4.hand).setPic();
                                dmnew.updateDark();
                            }

                            if (dmnew.inhand.type == 1) {
                                var4.hand.equipEffect(var4);
                            }
                        }

                        var4.repaint();
                        if (dmnew.sheet) {
                            dmnew.herosheet.repaint();
                        }

                        dmnew.this.changeCursor();
                    }
                }
            }
        } else if (!dmnew.sheet) {
            dmnew.herosheet.setHero((dmnew.Hero)var1.getSource());
            dmnew.sheet = true;
            dmnew.this.centerlay.show(dmnew.this.centerpanel, "herosheet");
        } else {
            dmnew.sheet = false;
            dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
            dmnew.this.hupdate();
            if (!dmnew.herosheet.hero.equals((dmnew.Hero)var1.getSource())) {
                ((dmnew.Hero)var1.getSource()).dispatchEvent(var1);
            }
        }

    }
}
