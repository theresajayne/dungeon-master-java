package com.github.theresajayne.dmjava.entities;


import com.badlogic.gdx.InputAdapter;

import java.util.Iterator;

class DungMove extends InputAdapter {
    DungMove() {
    }

    @Override
    public boolean keyDown(int keycode) {
        if (!dmnew.this.viewing && var1.getKeyCode() == 27) {
            dmnew.this.gamefrozen = !dmnew.this.gamefrozen;
            int var3;
            if (dmnew.this.gamefrozen) {
                dmnew.nomovement = true;
                dmnew.this.centerlay.show(dmnew.this.centerpanel, "freeze");
                if (dmnew.numheroes > 0) {
                    for(var3 = 0; var3 < dmnew.numheroes; ++var3) {
                        dmnew.hero[var3].removeMouseListener(dmnew.this.hclick);
                    }

                    dmnew.this.spellsheet.setVisible(false);
                    dmnew.this.weaponsheet.setVisible(false);
                }
            } else {
                dmnew.nomovement = false;
                if (dmnew.sheet) {
                    dmnew.this.centerlay.show(dmnew.this.centerpanel, "herosheet");
                } else {
                    dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
                }

                if (dmnew.numheroes > 0) {
                    if (!dmnew.this.sleeping) {
                        dmnew.this.spellsheet.setVisible(true);
                        dmnew.this.weaponsheet.setVisible(true);
                    }

                    for(var3 = 0; var3 < dmnew.numheroes; ++var3) {
                        if (!dmnew.hero[var3].isdead) {
                            dmnew.hero[var3].addMouseListener(dmnew.this.hclick);
                        }
                    }
                }
            }

        } else if (!dmnew.nomovement) {
            if (!dmnew.this.viewing) {
                MouseEvent var2;
                if (dmnew.numheroes > 0 && var1.getKeyCode() == 112) {
                    var2 = new MouseEvent(dmnew.hero[0], 501, 0L, 0, 20, 20, 1, false);
                    dmnew.hero[0].dispatchEvent(var2);
                } else if (dmnew.numheroes > 1 && var1.getKeyCode() == 113) {
                    var2 = new MouseEvent(dmnew.hero[1], 501, 0L, 0, 20, 20, 1, false);
                    dmnew.hero[1].dispatchEvent(var2);
                } else if (dmnew.numheroes > 2 && var1.getKeyCode() == 114) {
                    var2 = new MouseEvent(dmnew.hero[2], 501, 0L, 0, 20, 20, 1, false);
                    dmnew.hero[2].dispatchEvent(var2);
                } else if (dmnew.numheroes > 3 && var1.getKeyCode() == 115) {
                    var2 = new MouseEvent(dmnew.hero[3], 501, 0L, 0, 20, 20, 1, false);
                    dmnew.hero[3].dispatchEvent(var2);
                } else if (dmnew.numheroes > 0 && var1.getKeyCode() == 116) {
                    if (dmnew.actionqueue.isEmpty()) {
                        dmnew.actionqueue.add("s");
                    }
                } else if (var1.getKeyCode() == 117) {
                    if (dmnew.actionqueue.isEmpty()) {
                        dmnew.actionqueue.add("l");
                    }
                } else if (var1.getKeyCode() == 120 && dmnew.actionqueue.isEmpty()) {
                    dmnew.actionqueue.add("o");
                }
            }

            if (!dmnew.sheet) {
                if (var1.getKeyChar() != '7' && Character.toLowerCase(var1.getKeyChar()) != 'u') {
                    if (var1.getKeyChar() != '9' && Character.toLowerCase(var1.getKeyChar()) != 'o') {
                        if (var1.getKeyChar() != '8' && Character.toLowerCase(var1.getKeyChar()) != 'i') {
                            if (var1.getKeyChar() != '5' && var1.getKeyChar() != '2' && Character.toLowerCase(var1.getKeyChar()) != 'k') {
                                if (var1.getKeyChar() != '4' && Character.toLowerCase(var1.getKeyChar()) != 'j') {
                                    if ((var1.getKeyChar() == '6' || Character.toLowerCase(var1.getKeyChar()) == 'l') && dmnew.walkqueue.size() < 4) {
                                        dmnew.walkqueue.add(new Integer(0));
                                    }
                                } else if (dmnew.walkqueue.size() < 4) {
                                    dmnew.walkqueue.add(new Integer(2));
                                }
                            } else if (dmnew.walkqueue.size() < 4) {
                                dmnew.walkqueue.add(new Integer(1));
                            }
                        } else if (dmnew.walkqueue.size() < 4) {
                            dmnew.walkqueue.add(new Integer(3));
                        }
                    } else if (dmnew.walkqueue.size() < 4) {
                        dmnew.walkqueue.add(new Integer(-2));
                    }
                } else if (dmnew.walkqueue.size() < 4) {
                    dmnew.walkqueue.add(new Integer(-1));
                }
            }

        }
    }

    public void turnLeft() {
        if (dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy] instanceof Stairs) {
            dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].tryTeleport();
            dmnew.mirrorback = !dmnew.mirrorback;
            dmnew.walkqueue.clear();
        } else {
            dmnew.mirrorback = !dmnew.mirrorback;
            ++dmnew.facing;
            if (dmnew.facing > 3) {
                dmnew.facing = 0;
            }

            Iterator var2 = dmnew.dmprojs.iterator();

            while(var2.hasNext()) {
                dmnew.Projectile var1 = (dmnew.Projectile)var2.next();
                if (var1.it != null && var1.it.hasthrowpic) {
                    byte var3 = 2;
                    if (dmnew.facing == var1.direction) {
                        var3 = 0;
                    } else if ((dmnew.facing - var1.direction) % 2 == 0) {
                        var3 = 1;
                    } else if (dmnew.facing == (var1.direction + 1) % 4) {
                        var3 = 3;
                    }

                    var1.pic = var1.it.throwpic[var3];
                }
            }

            Pillar.swapmirror = !Pillar.swapmirror;
            dmnew.needredraw = true;
        }
    }

    public void turnRight() {
        if (dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy] instanceof Stairs) {
            dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].tryTeleport();
            dmnew.mirrorback = !dmnew.mirrorback;
            dmnew.walkqueue.clear();
        } else {
            dmnew.mirrorback = !dmnew.mirrorback;
            --dmnew.facing;
            if (dmnew.facing < 0) {
                dmnew.facing = 3;
            }

            Iterator var2 = dmnew.dmprojs.iterator();

            while(var2.hasNext()) {
                dmnew.Projectile var1 = (dmnew.Projectile)var2.next();
                if (var1.it != null && var1.it.hasthrowpic) {
                    byte var3 = 2;
                    if (dmnew.facing == var1.direction) {
                        var3 = 0;
                    } else if ((dmnew.facing - var1.direction) % 2 == 0) {
                        var3 = 1;
                    } else if (dmnew.facing == (var1.direction + 1) % 4) {
                        var3 = 3;
                    }

                    var1.pic = var1.it.throwpic[var3];
                }
            }

            Pillar.swapmirror = !Pillar.swapmirror;
            dmnew.needredraw = true;
        }
    }

    public void partyMove(int var1) {
        byte var2 = 0;
        byte var3 = 0;
        if (dmnew.facing == 0) {
            var3 = 1;
        } else if (dmnew.facing == 1) {
            var2 = 1;
        } else if (dmnew.facing == 2) {
            var3 = -1;
        } else {
            var2 = -1;
        }

        int var4;
        int var5;
        switch(var1) {
            case 1:
                var4 = dmnew.partyx + var2;
                var5 = dmnew.partyy + var3;
                break;
            case 2:
                var4 = dmnew.partyx - var3;
                var5 = dmnew.partyy + var2;
                break;
            case 3:
                var4 = dmnew.partyx - var2;
                var5 = dmnew.partyy - var3;
                break;
            default:
                var4 = dmnew.partyx + var3;
                var5 = dmnew.partyy - var2;
        }

        if (var4 >= 0 && var5 >= 0 && var4 < dmnew.mapwidth && var5 < dmnew.mapheight) {
            if (dmnew.DungeonMap[dmnew.level][var4][var5].isPassable && !dmnew.DungeonMap[dmnew.level][var4][var5].hasMons) {
                int var6;
                int var8;
                int var10;
                int var12;
                if (dmnew.numheroes > 0 && dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].numProjs > 0) {
                    var6 = dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].numProjs;
                    var8 = 0;
                    int var9 = 0;

                    for(var10 = -1; var8 < var6; ++var8) {
                        dmnew.Projectile var7;
                        do {
                            do {
                                var7 = (dmnew.Projectile)dmnew.dmprojs.get(var9);
                                ++var9;
                            } while(var7.level != dmnew.level);
                        } while(var7.x != dmnew.partyx || var7.y != dmnew.partyy);

                        if (!var7.justthrown && !var7.isending) {
                            int var11 = (var7.subsquare + dmnew.facing) % 4;
                            if (dmnew.heroatsub[var11] != -1) {
                                var10 = dmnew.heroatsub[var11];
                            } else {
                                switch(var1) {
                                    case 1:
                                        if (var11 == 2) {
                                            var10 = dmnew.heroatsub[1];
                                        } else if (var11 == 3) {
                                            var10 = dmnew.heroatsub[0];
                                        }
                                        break;
                                    case 2:
                                        if (var11 == 0) {
                                            var10 = dmnew.heroatsub[1];
                                        } else if (var11 == 3) {
                                            var10 = dmnew.heroatsub[2];
                                        }
                                        break;
                                    case 3:
                                        if (var11 == 0) {
                                            var10 = dmnew.heroatsub[3];
                                        } else if (var11 == 1) {
                                            var10 = dmnew.heroatsub[2];
                                        }
                                        break;
                                    default:
                                        if (var11 == 1) {
                                            var10 = dmnew.heroatsub[0];
                                        } else if (var11 == 2) {
                                            var10 = dmnew.heroatsub[3];
                                        }
                                }
                            }

                            if (var10 != -1) {
                                var12 = dmnew.heroatsub[var11];
                                dmnew.heroatsub[var11] = var10;
                                var7.projend();
                                if (var12 == -1 || !dmnew.hero[var12].isdead) {
                                    dmnew.heroatsub[var11] = var12;
                                }

                                if (dmnew.hero[var10].isdead) {
                                    dmnew.heroatsub[dmnew.hero[var10].subsquare] = -1;
                                }

                                dmnew.this.formation.addNewHero();
                                if (var7.it != null) {
                                    --var9;
                                    dmnew.dmprojs.remove(var9);
                                } else {
                                    var7.x = var4;
                                    var7.y = var5;
                                    --dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].numProjs;
                                    ++dmnew.DungeonMap[dmnew.level][var4][var5].numProjs;
                                }
                            }
                        }
                    }
                }

                dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].hasParty = false;
                if (dmnew.numheroes != 0) {
                    dmnew.DungeonMap[dmnew.level][var4][var5].hasParty = true;
                    dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].tryFloorSwitch(1);
                    dmnew.DungeonMap[dmnew.level][var4][var5].hasParty = false;
                }

                var6 = dmnew.level;
                int var17 = dmnew.partyy;
                var8 = dmnew.partyx;
                dmnew.partyy = var5;
                dmnew.partyx = var4;
                if (dmnew.numheroes != 0) {
                    dmnew.DungeonMap[dmnew.level][var4][var5].tryFloorSwitch(0);
                }

                dmnew.DungeonMap[dmnew.level][var4][var5].hasParty = true;
                boolean var18 = dmnew.mirrorback;
                dmnew.DungeonMap[dmnew.level][var4][var5].tryTeleport();
                if (dmnew.partyx == var8 && dmnew.partyy == var17 && dmnew.level == var6) {
                    if (dmnew.mirrorback != var18) {
                        dmnew.mirrorback = !dmnew.mirrorback;
                    }
                } else {
                    if (dmnew.mirrorback == var18) {
                        dmnew.mirrorback = !dmnew.mirrorback;
                    }

                    if (dmnew.numheroes > 0 && dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].numProjs > 0) {
                        var10 = dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].numProjs;
                        var12 = 0;
                        int var13 = 0;

                        for(int var14 = -1; var12 < var10; ++var12) {
                            dmnew.Projectile var19;
                            do {
                                do {
                                    var19 = (dmnew.Projectile)dmnew.dmprojs.get(var13);
                                    ++var13;
                                } while(var19.level != dmnew.level);
                            } while(var19.x != dmnew.partyx || var19.y != dmnew.partyy);

                            if (!var19.isending) {
                                int var15 = (var19.subsquare + dmnew.facing) % 4;
                                switch(var1) {
                                    case 1:
                                        if (var15 == 0) {
                                            var14 = dmnew.heroatsub[3];
                                        } else if (var15 == 1) {
                                            var14 = dmnew.heroatsub[2];
                                        }
                                        break;
                                    case 2:
                                        if (var15 == 1) {
                                            var14 = dmnew.heroatsub[0];
                                        } else if (var15 == 2) {
                                            var14 = dmnew.heroatsub[3];
                                        }
                                        break;
                                    case 3:
                                        if (var15 == 2) {
                                            var14 = dmnew.heroatsub[1];
                                        } else if (var15 == 3) {
                                            var14 = dmnew.heroatsub[0];
                                        }
                                        break;
                                    default:
                                        if (var15 == 0) {
                                            var14 = dmnew.heroatsub[1];
                                        } else if (var15 == 3) {
                                            var14 = dmnew.heroatsub[2];
                                        }
                                }

                                if (var14 != -1) {
                                    int var16 = dmnew.heroatsub[var15];
                                    dmnew.heroatsub[var15] = var14;
                                    var19.projend();
                                    if (var16 == -1 || !dmnew.hero[var16].isdead) {
                                        dmnew.heroatsub[var15] = var16;
                                    }

                                    if (dmnew.hero[var14].isdead) {
                                        dmnew.heroatsub[dmnew.hero[var14].subsquare] = -1;
                                    }

                                    dmnew.this.formation.addNewHero();
                                    if (var19.it != null) {
                                        --var13;
                                        dmnew.dmprojs.remove(var13);
                                    }
                                }
                            }
                        }
                    }
                }

                dmnew.needredraw = true;

                for(var10 = 0; var10 < dmnew.numheroes; ++var10) {
                    ++dmnew.hero[var10].walkcounter;
                    if (dmnew.hero[var10].load > dmnew.hero[var10].maxload * 3.0F / 4.0F) {
                        ++dmnew.hero[var10].walkcounter;
                    }

                    if (dmnew.hero[var10].load > dmnew.hero[var10].maxload) {
                        ++dmnew.hero[var10].walkcounter;
                    }

                    if (dmnew.hero[var10].walkcounter > dmnew.hero[var10].vitality / 4) {
                        dmnew.hero[var10].vitalize(-((int)dmnew.hero[var10].load) / 10 - 1);
                        dmnew.hero[var10].repaint();
                        dmnew.hero[var10].walkcounter = 0;
                    }
                }
            } else if (dmnew.numheroes != 0 && !dmnew.DungeonMap[dmnew.level][var4][var5].isPassable) {
                dmnew.playSound("bump.wav", var4, var5);
            }

        } else {
            if (dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy] instanceof Stairs) {
                dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy].tryTeleport();
            } else if (dmnew.numheroes != 0) {
                dmnew.playSound("bump.wav", var4, var5);
            }

        }
    }
}
