package com.github.theresajayne.dmjava.entities;

class DungView extends JComponent {
    BufferedImage offscreen;
    Graphics2D offg;
    Graphics2D offg2;

    public DungView() {
        this.setDoubleBuffered(false);
        this.setBackground(Color.black);
        this.setSize(448, 326);
    }

    public void paint(Graphics var1) {
        if (dmnew.this.needdrawdungeon || dmnew.nomovement || this.offscreen == null) {
            int var2;
            int var3;
            int var5;
            int var6;
            int var7;
            label141:
            switch(dmnew.facing) {
                case 0:
                    for(int var4 = 0; var4 < 4; ++var4) {
                        var3 = dmnew.partyy - 3 + var4;

                        for(var5 = 0; var5 < 5; ++var5) {
                            var2 = dmnew.partyx - 2 + var5;
                            if (var2 >= 0 && var3 >= 0 && var2 <= dmnew.mapwidth - 1 && var3 <= dmnew.mapheight - 1) {
                                dmnew.this.visibleg[var4][var5] = dmnew.DungeonMap[dmnew.level][var2][var3];
                            } else {
                                dmnew.this.visibleg[var4][var5] = dmnew.outWall;
                            }
                        }
                    }
                    break;
                case 1:
                    var6 = 0;

                    while(true) {
                        if (var6 >= 4) {
                            break label141;
                        }

                        var2 = dmnew.partyx - 3 + var6;

                        for(var7 = 0; var7 < 5; ++var7) {
                            var3 = dmnew.partyy + 2 - var7;
                            if (var2 >= 0 && var3 >= 0 && var2 <= dmnew.mapwidth - 1 && var3 <= dmnew.mapheight - 1) {
                                dmnew.this.visibleg[var6][var7] = dmnew.DungeonMap[dmnew.level][var2][var3];
                            } else {
                                dmnew.this.visibleg[var6][var7] = dmnew.outWall;
                            }
                        }

                        ++var6;
                    }
                case 2:
                    var5 = 0;

                    while(true) {
                        if (var5 >= 4) {
                            break label141;
                        }

                        var3 = dmnew.partyy + 3 - var5;

                        for(var6 = 0; var6 < 5; ++var6) {
                            var2 = dmnew.partyx + 2 - var6;
                            if (var2 >= 0 && var3 >= 0 && var2 <= dmnew.mapwidth - 1 && var3 <= dmnew.mapheight - 1) {
                                dmnew.this.visibleg[var5][var6] = dmnew.DungeonMap[dmnew.level][var2][var3];
                            } else {
                                dmnew.this.visibleg[var5][var6] = dmnew.outWall;
                            }
                        }

                        ++var5;
                    }
                case 3:
                    for(var7 = 0; var7 < 4; ++var7) {
                        var2 = dmnew.partyx + 3 - var7;

                        for(int var8 = 0; var8 < 5; ++var8) {
                            var3 = dmnew.partyy - 2 + var8;
                            if (var2 >= 0 && var3 >= 0 && var2 <= dmnew.mapwidth - 1 && var3 <= dmnew.mapheight - 1) {
                                dmnew.this.visibleg[var7][var8] = dmnew.DungeonMap[dmnew.level][var2][var3];
                            } else {
                                dmnew.this.visibleg[var7][var8] = dmnew.outWall;
                            }
                        }
                    }
            }

            if (!dmnew.mirrorback) {
                this.offg.drawImage(dmnew.back, 0, 0, (ImageObserver)null);
            } else {
                this.offg.drawImage(dmnew.back, 448, 0, 0, 326, 0, 0, 448, 326, (ImageObserver)null);
            }

            dmnew.this.visibleg[0][0].drawPic(3, 0, 0, 62, this.offg, this);
            dmnew.this.visibleg[0][1].drawPic(3, 1, 0, 62, this.offg, this);
            dmnew.this.visibleg[0][4].drawPic(3, 4, 416, 62, this.offg, this);
            dmnew.this.visibleg[0][3].drawPic(3, 3, 448, 62, this.offg, this);
            dmnew.this.visibleg[0][2].drawPic(3, 2, 148, 62, this.offg, this);
            dmnew.this.visibleg[1][1].drawPic(2, 1, 0, 50, this.offg, this);
            dmnew.this.visibleg[1][3].drawPic(2, 3, 448, 50, this.offg, this);
            dmnew.this.visibleg[1][2].drawPic(2, 2, 120, 50, this.offg, this);
            dmnew.this.visibleg[2][1].drawPic(1, 1, 0, 22, this.offg, this);
            dmnew.this.visibleg[2][3].drawPic(1, 3, 448, 22, this.offg, this);
            if (dmnew.magicvision > 0 && !(dmnew.this.visibleg[2][2] instanceof Floor) && !(dmnew.this.visibleg[2][2] instanceof Door) && !(dmnew.this.visibleg[2][2] instanceof Stairs)) {
                dmnew.this.visibleg[2][2].drawPic(1, 2, 64, 22, this.offg2, this);
            } else {
                dmnew.this.visibleg[2][2].drawPic(1, 2, 64, 22, this.offg, this);
            }

            dmnew.this.visibleg[3][1].drawPic(0, 1, 0, 0, this.offg, this);
            dmnew.this.visibleg[3][3].drawPic(0, 3, 448, 0, this.offg, this);
            dmnew.this.visibleg[3][2].drawPic(0, 2, 0, 0, this.offg, this);
            if (!dmnew.NODARK && dmnew.level > 0 && dmnew.darkfactor < 255) {
                this.offg.setColor(new Color(0, 0, 0, 255 - dmnew.darkfactor));
                this.offg.fillRect(0, 0, 448, 326);
            }

            dmnew.this.needdrawdungeon = false;
        }

        var1.drawImage(this.offscreen, 0, 0, (ImageObserver)null);
    }
}

