package com.github.theresajayne.dmjava.entities;

class SpellClick implements ActionListener {
    SpellClick() {
    }

    public void actionPerformed(ActionEvent var1) {
        if (var1.getActionCommand().equals("undo")) {
            if (dmnew.hero[dmnew.this.spellready].currentspell.length() > 0) {
                dmnew.hero[dmnew.this.spellready].currentspell = dmnew.hero[dmnew.this.spellready].currentspell.substring(0, dmnew.hero[dmnew.this.spellready].currentspell.length() - 1);
                if (dmnew.hero[dmnew.this.spellready].currentspell.length() > 0) {
                    dmnew.this.spellsheet.castsymbs.flush();
                    dmnew.this.spellsheet.castsymbs = new BufferedImage(70, 12, 2);
                    dmnew.this.spellsheet.castg = dmnew.this.spellsheet.castsymbs.createGraphics();
                }

                dmnew.this.spellsheet.update();
            }
        } else {
            boolean var2 = false;
            if (dmnew.hero[dmnew.this.spellready].currentspell.length() <= 1) {
                return;
            }

            int var3 = dmnew.hero[dmnew.this.spellready].castSpell();
            if (var3 == 0) {
                dmnew.message.setMessage(dmnew.hero[dmnew.this.spellready].name + " mumbles nonsense.", 4);
                dmnew.hero[dmnew.this.spellready].currentspell = "";
            } else if (var3 == 1) {
                dmnew.message.setMessage(dmnew.hero[dmnew.this.spellready].name + " casts a spell.", dmnew.this.spellready);
                dmnew.hero[dmnew.this.spellready].currentspell = "";
                if (dmnew.sheet && dmnew.herosheet.hero.equals(dmnew.hero[dmnew.this.spellready])) {
                    dmnew.herosheet.repaint();
                }
            } else if (var3 == 2) {
                dmnew.message.setMessage(dmnew.hero[dmnew.this.spellready].name + " needs an empty flask in hand.", 4);
            } else if (var3 == 3) {
                dmnew.message.setMessage(dmnew.hero[dmnew.this.spellready].name + " needs more practice to cast that " + dmnew.this.spellclass + " spell.", 4);
                dmnew.hero[dmnew.this.spellready].currentspell = "";
            } else if (var3 == 4) {
                dmnew.message.setMessage(dmnew.hero[dmnew.this.spellready].name + " can't cast that now.", 4);
            } else {
                dmnew.message.setMessage(dmnew.hero[dmnew.this.spellready].name + "'s spell fizzles.", 4);
                dmnew.hero[dmnew.this.spellready].currentspell = "";
            }

            dmnew.this.spellsheet.update();
            dmnew.this.weaponsheet.update();
        }

    }
}

