package com.github.theresajayne.dmjava.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapObject {
    protected Image[][] pic;
    protected Image blankpic;
    protected Map<String,Texture> tracker = new HashMap<>();
    protected char mapchar;
    protected String mapdir;
    protected static Image temppic;
    protected static DarknessFilter darkfilt = new DarknessFilter();
    private static final int PARTYSTEPPINGON = 0;
    private static final int PARTYSTEPPINGOFF = 1;
    private static final int PUTITEM = 2;
    private static final int TOOKITEM = 3;
    private static final int MONSTEPPINGON = 4;
    private static final int MONSTEPPINGOFF = 5;
    private int numProjs;
    private ArrayList mapItems;
    private boolean canHoldItems;
    private boolean isPassable;
    private boolean hasParty;
    private boolean canPassProjs;
    private boolean canPassMons;
    private boolean canPassImmaterial;
    private boolean hasMons;
    private boolean hasCloud;
    private boolean hasItems;
    private boolean drawItems;
    private boolean drawFurtherItems;

    public MapObject() {
        loadMedia("back",Gdx.files.internal("Maps/Back.gif"));
        loadMedia("doortrack",Gdx.files.internal("Maps/Doortrack.gif"));
        
        this.mapdir = "Maps" + File.separator;
        this.numProjs = 0;
        this.canPassMons = true;
        this.canPassImmaterial = true;

        this.mapItems = new ArrayList(4);
        this.pic = new Image[4][5];
        this.blankpic = new Image(new Texture(Gdx.files.internal("blank.gif")));
    }

    private void loadMedia(String name,FileHandle handle) {
        Texture tempTexture = new Texture(handle);
        tracker.put(name,tempTexture);
    }

    public void tryWallSwitch(int var1, int var2) {
    }

    public boolean tryWallSwitch(int var1, int var2, Item var3) {
        return true;
    }

    public void tryFloorSwitch(int var1) {
    }

    public void tryTeleport() {
    }

    public void tryTeleport(Monster var1) {
    }

    public boolean tryTeleport(Item var1) {
        return false;
    }

    public void tryTeleport(Projectile var1) {
    }

    public boolean changeState() {
        return false;
    }

    public void activate() {
    }

    public void deactivate() {
    }

    public void toggle() {
    }

    public void addItem(Item var1) {
        this.mapItems.add(var1);
        this.hasItems = true;
    }

    public Item pickUpItem(int var1) {
        Item var2 = null;
        int var3 = this.mapItems.size() - 1;
        int var4 = -1;
        int var5 = 0;

        boolean var6;
        for(var6 = false; var3 > -1 && (var5 < 2 || !var6); --var3) {
            Item var7 = (Item)this.mapItems.get(var3);
            if (!var6 && var7.subsquare == var1) {
                var6 = true;
                var2 = var7;
                var4 = var3;
            }

            ++var5;
        }

        if (var6 && var5 == 1) {
            this.hasItems = false;
        }

        if (var4 != -1) {
            this.mapItems.remove(var4);
        }

        return var2;
    }

    public void addSmoke(int var1, int var2) {
    }

    protected Image loadPic(String var1) {
        return dmnew.tk.getImage(this.mapdir + var1);
    }

    protected void setPics() {
    }

    public void drawPic(int var1, int var2, int var3, int var4, Graphics2D var5, ImageObserver var6) {
        if (var2 == 3) {
            var3 -= this.pic[var1][3].getWidth((ImageObserver)null);
        }

        var5.drawImage(this.pic[var1][var2], var3, var4, var6);
        if (var2 > 0 && var2 < 4) {
            this.drawContents(var5, 3 - var1, var2 - 1);
        }

    }

    public void drawContents(Graphics2D var1, int var2, int var3) {
        byte var4 = 0;
        short var5 = 0;
        int var14;
        int var15;
        if (dmnew.facing == 0) {
            var14 = dmnew.partyx - 1 + var3;
            var15 = dmnew.partyy - 3 + var2;
        } else if (dmnew.facing == 2) {
            var14 = dmnew.partyx + 1 - var3;
            var15 = dmnew.partyy + 3 - var2;
        } else if (dmnew.facing == 1) {
            var14 = dmnew.partyx - 3 + var2;
            var15 = dmnew.partyy + 1 - var3;
        } else {
            var14 = dmnew.partyx + 3 - var2;
            var15 = dmnew.partyy - 1 + var3;
        }

        if (dmnew.level > 0 && dmnew.DungeonMap[dmnew.level - 1][var14][var15].mapchar == 'p' && ((Pit)dmnew.DungeonMap[dmnew.level - 1][var14][var15]).isOpen && (this.hasParty || dmnew.dispell > 0 || var2 == 2 && var3 == 1 && dmnew.magicvision > 0 || !(this instanceof FakeWall))) {
            if (var2 == 0) {
                var4 = 64;
                if (var3 == 0) {
                    var5 = 9;
                } else if (var3 == 1) {
                    var5 = 161;
                } else {
                    var5 = 286;
                }
            } else if (var2 == 1) {
                var4 = 49;
                if (var3 == 0) {
                    var5 = 0;
                } else if (var3 == 1) {
                    var5 = 138;
                } else {
                    var5 = 308;
                }
            } else if (var2 == 2) {
                var4 = 21;
                if (var3 == 0) {
                    var5 = 0;
                } else if (var3 == 1) {
                    var5 = 87;
                } else {
                    var5 = 346;
                }
            } else {
                var4 = 0;
                if (var3 == 0) {
                    var5 = 0;
                } else if (var3 == 1) {
                    var5 = 51;
                } else {
                    var5 = 419;
                }
            }

            if (var3 == 1 && dmnew.mirrorback) {
                var1.drawImage(Pit.cpic[3 - var2][1], var5 + Pit.cpic[3 - var2][1].getWidth((ImageObserver)null), var4, var5, var4 + Pit.cpic[3 - var2][1].getHeight((ImageObserver)null), 0, 0, Pit.cpic[3 - var2][1].getWidth((ImageObserver)null), Pit.cpic[3 - var2][1].getHeight((ImageObserver)null), (ImageObserver)null);
            } else {
                var1.drawImage(Pit.cpic[3 - var2][var3], var5, var4, (ImageObserver)null);
            }
        }

        int var19 = this.mapItems.size();
        int var20 = 0;
        int var21 = 0;
        byte var22 = 0;
        byte var23 = 2;
        boolean var24 = false;
        int var25 = 0;
        if (var2 == 0) {
            darkfilt.setDarks(3);
        } else if (var2 == 1) {
            darkfilt.setDarks(2);
        } else if (var2 == 2) {
            darkfilt.setDarks(1);
        }

        float var11;
        float var12;
        float var13;
        int var45;
        int var46;
        for(int var26 = 0; var26 < 2; ++var26) {
            int var47 = var22;
            boolean var6;
            boolean var7;
            if (this.drawItems && var19 > 0 && var20 < var19) {
                while(var47 < var23) {
                    for(int var27 = 0; var27 < var19 && var20 < var19; ++var27) {
                        Item var16 = (Item)this.mapItems.get(var27);
                        if ((var16.subsquare + dmnew.facing) % 4 == var47) {
                            ++var20;
                            var6 = false;
                            var7 = false;
                            var11 = 0.0F;
                            var12 = (float)var16.dpic.getWidth((ImageObserver)null);
                            var13 = (float)var16.dpic.getHeight((ImageObserver)null);
                            if (var2 < 3 && (var2 > 0 || (var16.subsquare + dmnew.facing) % 4 > 1)) {
                                temppic = Item.darkpic[var16.number][var2];
                                if (temppic == null) {
                                    temppic = dmnew.tk.createImage(new FilteredImageSource(var16.dpic.getSource(), darkfilt));
                                    this.tracker.addImage(temppic, 0);

                                    try {
                                        this.tracker.waitForID(0);
                                    } catch (InterruptedException var44) {
                                        ;
                                    }

                                    this.tracker.removeImage(temppic);
                                    Item.darkpic[var16.number][var2] = temppic;
                                }
                            } else {
                                temppic = var16.dpic;
                            }

                            int var9;
                            int var10;
                            switch((var16.subsquare + dmnew.facing) % 4) {
                                case 0:
                                    if ((var3 != 0 || var2 == 1) && var2 > 0 && (dmnew.magicvision > 0 && var2 == 2 || this.drawFurtherItems)) {
                                        if (var2 == 3) {
                                            var4 = 120;
                                            if (var3 == 1) {
                                                var5 = -90;
                                            } else {
                                                var5 = 260;
                                            }
                                        } else if (var2 == 2) {
                                            var4 = 42;
                                            if (var3 == 1) {
                                                var5 = -56;
                                            } else {
                                                var5 = 165;
                                            }
                                        } else {
                                            var4 = -2;
                                            if (var3 == 0) {
                                                var5 = -200;
                                            } else if (var3 == 1) {
                                                var5 = -36;
                                            } else {
                                                var5 = 120;
                                            }
                                        }

                                        var11 = 2.8F - (float)var2 * 0.6F;
                                        var45 = (int)(var12 / var11);
                                        var46 = (int)(var13 / var11);
                                        var9 = (int)((float)var16.xoffset / var11);
                                        var10 = (int)((float)var16.yoffset / var11);
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2 + var9, 200 + var4 - var46 + var10, var45, var46, (ImageObserver)null);
                                    }
                                    break;
                                case 1:
                                    if (var3 == 2 && var2 != 1 || var2 <= 0 || (dmnew.magicvision <= 0 || var2 != 2) && !this.drawFurtherItems) {
                                        break;
                                    }

                                    if (var2 == 3) {
                                        var4 = 120;
                                        if (var3 == 1) {
                                            var5 = 90;
                                        } else {
                                            var5 = -260;
                                        }
                                    } else if (var2 == 2) {
                                        var4 = 42;
                                        if (var3 == 1) {
                                            var5 = 56;
                                        } else {
                                            var5 = -165;
                                        }
                                    } else {
                                        var4 = -2;
                                        if (var3 == 0) {
                                            var5 = -120;
                                        } else if (var3 == 1) {
                                            var5 = 36;
                                        } else {
                                            var5 = 200;
                                        }
                                    }

                                    var11 = 2.8F - (float)var2 * 0.6F;
                                    var45 = (int)(var12 / var11);
                                    var46 = (int)(var13 / var11);
                                    var9 = (int)((float)var16.xoffset / var11);
                                    var10 = (int)((float)var16.yoffset / var11);
                                    var1.drawImage(temppic, 224 + var5 - var45 / 2 + var9, 200 + var4 - var46 + var10, var45, var46, (ImageObserver)null);
                                    break;
                                case 2:
                                    if ((var3 != 2 || var2 < 2) && var2 != 3) {
                                        if (var2 == 2) {
                                            var4 = 76;
                                            if (var3 == 1) {
                                                var5 = 70;
                                            } else {
                                                var5 = -206;
                                            }
                                        } else if (var2 == 1) {
                                            var4 = 16;
                                            if (var3 == 0) {
                                                var5 = -135;
                                            } else if (var3 == 1) {
                                                var5 = 46;
                                            } else {
                                                var5 = 222;
                                            }
                                        } else if (var2 == 0) {
                                            var4 = -16;
                                            if (var3 == 0) {
                                                var5 = -106;
                                            } else if (var3 == 1) {
                                                var5 = 28;
                                            } else {
                                                var5 = 176;
                                            }
                                        }

                                        var11 = 2.5F - (float)var2 * 0.6F;
                                        var45 = (int)(var12 / var11);
                                        var46 = (int)(var13 / var11);
                                        var9 = (int)((float)var16.xoffset / var11);
                                        var10 = (int)((float)var16.yoffset / var11);
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2 + var9, 200 + var4 - var46 + var10, var45, var46, (ImageObserver)null);
                                    }
                                    break;
                                case 3:
                                    if ((var3 != 0 || var2 < 2) && var2 != 3) {
                                        if (var2 == 2) {
                                            var4 = 76;
                                            if (var3 == 1) {
                                                var5 = -70;
                                            } else {
                                                var5 = 206;
                                            }
                                        } else if (var2 == 1) {
                                            var4 = 16;
                                            if (var3 == 0) {
                                                var5 = -222;
                                            } else if (var3 == 1) {
                                                var5 = -46;
                                            } else {
                                                var5 = 135;
                                            }
                                        } else if (var2 == 0) {
                                            var4 = -16;
                                            if (var3 == 0) {
                                                var5 = -176;
                                            } else if (var3 == 1) {
                                                var5 = -28;
                                            } else {
                                                var5 = 106;
                                            }
                                        }

                                        var11 = 2.5F - (float)var2 * 0.6F;
                                        var45 = (int)(var12 / var11);
                                        var46 = (int)(var13 / var11);
                                        var9 = (int)((float)var16.xoffset / var11);
                                        var10 = (int)((float)var16.yoffset / var11);
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2 + var9, 200 + var4 - var46 + var10, var45, var46, (ImageObserver)null);
                                    }
                            }
                        }
                    }

                    ++var47;
                }
            }

            if (var23 == 4) {
                var23 = 6;
            }

            if (this.hasMons && var2 != 3) {
                if (var2 == 0) {
                    darkfilt.setDarks(2);
                } else if (var2 == 1) {
                    darkfilt.setDarks(1);
                }

                while(var25 < var23) {
                    if (var25 != 5) {
                        var47 = (var25 - dmnew.facing + 4) % 4;
                    } else {
                        var47 = 5;
                    }

                    Monster var17 = (Monster)dmnew.dmmons.get(dmnew.level + "," + var14 + "," + var15 + "," + var47);
                    if (var17 != null && (var17.isImmaterial || dmnew.dispell > 0 || var2 == 2 && var3 == 1 && dmnew.magicvision > 0 || !(this instanceof FakeWall))) {
                        int var18;
                        if (!var17.isattacking && !var17.isdying && !var17.iscasting) {
                            if (dmnew.facing == var17.facing) {
                                if (var2 < 2) {
                                    if (!var17.awaypic.equals(var17.towardspic)) {
                                        var18 = 1 + var2 * 4;
                                    } else {
                                        var18 = var2 * 4;
                                    }

                                    temppic = dmnew.mondarkpic[var17.number][var18];
                                    if (temppic == null) {
                                        temppic = dmnew.tk.createImage(new FilteredImageSource(var17.awaypic.getSource(), darkfilt));
                                        this.tracker.addImage(temppic, 0);

                                        try {
                                            this.tracker.waitForID(0);
                                        } catch (InterruptedException var43) {
                                            ;
                                        }

                                        this.tracker.removeImage(temppic);
                                        dmnew.mondarkpic[var17.number][var18] = temppic;
                                    }
                                } else {
                                    temppic = var17.awaypic;
                                }
                            } else if ((dmnew.facing - var17.facing) % 2 == 0) {
                                if (var2 < 2) {
                                    temppic = dmnew.mondarkpic[var17.number][var2 * 4];
                                    if (temppic == null) {
                                        temppic = dmnew.tk.createImage(new FilteredImageSource(var17.towardspic.getSource(), darkfilt));
                                        this.tracker.addImage(temppic, 0);

                                        try {
                                            this.tracker.waitForID(0);
                                        } catch (InterruptedException var42) {
                                            ;
                                        }

                                        this.tracker.removeImage(temppic);
                                        dmnew.mondarkpic[var17.number][var2 * 4] = temppic;
                                    }
                                } else {
                                    temppic = var17.towardspic;
                                }
                            } else if (dmnew.facing == (var17.facing + 1) % 4) {
                                if (var2 < 2) {
                                    if (!var17.rightpic.equals(var17.towardspic)) {
                                        var18 = 2 + var2 * 4;
                                    } else {
                                        var18 = var2 * 4;
                                    }

                                    temppic = dmnew.mondarkpic[var17.number][var18];
                                    if (temppic == null) {
                                        temppic = dmnew.tk.createImage(new FilteredImageSource(var17.rightpic.getSource(), darkfilt));
                                        this.tracker.addImage(temppic, 0);

                                        try {
                                            this.tracker.waitForID(0);
                                        } catch (InterruptedException var41) {
                                            ;
                                        }

                                        this.tracker.removeImage(temppic);
                                        dmnew.mondarkpic[var17.number][var18] = temppic;
                                    }
                                } else {
                                    temppic = var17.rightpic;
                                }

                                if (!var17.rightpic.equals(var17.towardspic)) {
                                    var17.mirrored = false;
                                }
                            } else {
                                if (var2 < 2) {
                                    if (!var17.leftpic.equals(var17.towardspic)) {
                                        var18 = 3 + var2 * 4;
                                    } else {
                                        var18 = var2 * 4;
                                    }

                                    temppic = dmnew.mondarkpic[var17.number][var18];
                                    if (temppic == null) {
                                        temppic = dmnew.tk.createImage(new FilteredImageSource(var17.leftpic.getSource(), darkfilt));
                                        this.tracker.addImage(temppic, 0);

                                        try {
                                            this.tracker.waitForID(0);
                                        } catch (InterruptedException var40) {
                                            ;
                                        }

                                        this.tracker.removeImage(temppic);
                                        dmnew.mondarkpic[var17.number][var18] = temppic;
                                    }
                                } else {
                                    temppic = var17.leftpic;
                                }

                                if (!var17.leftpic.equals(var17.towardspic)) {
                                    var17.mirrored = false;
                                }
                            }
                        } else if (var17.isdying) {
                            temppic = dmnew.mondeathpic;
                        } else if (var17.isattacking) {
                            if (var2 < 2) {
                                temppic = dmnew.mondarkpic[var17.number][8 + var2];
                                if (temppic == null) {
                                    temppic = dmnew.tk.createImage(new FilteredImageSource(var17.attackpic.getSource(), darkfilt));
                                    this.tracker.addImage(temppic, 0);

                                    try {
                                        this.tracker.waitForID(0);
                                    } catch (InterruptedException var39) {
                                        ;
                                    }

                                    this.tracker.removeImage(temppic);
                                    dmnew.mondarkpic[var17.number][8 + var2] = temppic;
                                }
                            } else {
                                temppic = var17.attackpic;
                            }
                        } else if (var2 < 2) {
                            if (!var17.castpic.equals(var17.attackpic)) {
                                var18 = 10 + var2;
                            } else {
                                var18 = 8 + var2;
                            }

                            temppic = dmnew.mondarkpic[var17.number][var18];
                            if (temppic == null) {
                                temppic = dmnew.tk.createImage(new FilteredImageSource(var17.castpic.getSource(), darkfilt));
                                this.tracker.addImage(temppic, 0);

                                try {
                                    this.tracker.waitForID(0);
                                } catch (InterruptedException var38) {
                                    ;
                                }

                                this.tracker.removeImage(temppic);
                                dmnew.mondarkpic[var17.number][var18] = temppic;
                            }
                        } else {
                            temppic = var17.castpic;
                        }

                        var12 = (float)temppic.getWidth((ImageObserver)null);
                        var13 = (float)temppic.getHeight((ImageObserver)null);
                        switch(var25) {
                            case 0:
                                if ((var3 != 0 || var2 == 1) && var2 > 0 && (dmnew.magicvision > 0 && var2 == 2 || this.drawFurtherItems)) {
                                    if (var2 == 2) {
                                        var4 = 42;
                                        if (var3 == 1) {
                                            var5 = -56;
                                        } else {
                                            var5 = 165;
                                        }
                                    } else {
                                        var4 = -2;
                                        if (var3 == 0) {
                                            var5 = -200;
                                        } else if (var3 == 1) {
                                            var5 = -36;
                                        } else {
                                            var5 = 120;
                                        }
                                    }

                                    var11 = 2.5F - (float)var2 * 0.6F;
                                    var45 = (int)(var12 / var11);
                                    var46 = (int)(var13 / var11);
                                    if (!dmnew.NOTRANS && var17.isImmaterial && var17.number != 10) {
                                        if (var17.mirrored) {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                        } else {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                        }
                                    } else if (var17.mirrored) {
                                        var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                    } else {
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                    }
                                }
                                break;
                            case 1:
                                if ((var3 != 2 || var2 == 1) && var2 > 0 && (dmnew.magicvision > 0 && var2 == 2 || this.drawFurtherItems)) {
                                    if (var2 == 2) {
                                        var4 = 42;
                                        if (var3 == 1) {
                                            var5 = 56;
                                        } else {
                                            var5 = -165;
                                        }
                                    } else {
                                        var4 = -2;
                                        if (var3 == 0) {
                                            var5 = -120;
                                        } else if (var3 == 1) {
                                            var5 = 36;
                                        } else {
                                            var5 = 200;
                                        }
                                    }

                                    var11 = 2.5F - (float)var2 * 0.6F;
                                    var45 = (int)(var12 / var11);
                                    var46 = (int)(var13 / var11);
                                    if (!dmnew.NOTRANS && var17.isImmaterial && var17.number != 10) {
                                        if (var17.mirrored) {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                        } else {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                        }
                                    } else if (var17.mirrored) {
                                        var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                    } else {
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                    }
                                }
                                break;
                            case 2:
                                if (var3 != 2 || var2 < 2) {
                                    if (var2 == 2) {
                                        var4 = 80;
                                        if (var3 == 1) {
                                            var5 = 70;
                                        } else {
                                            var5 = -206;
                                        }
                                    } else if (var2 == 1) {
                                        var4 = 16;
                                        if (var3 == 0) {
                                            var5 = -135;
                                        } else if (var3 == 1) {
                                            var5 = 46;
                                        } else {
                                            var5 = 222;
                                        }
                                    } else if (var2 == 0) {
                                        var4 = -16;
                                        if (var3 == 0) {
                                            var5 = -106;
                                        } else if (var3 == 1) {
                                            var5 = 28;
                                        } else {
                                            var5 = 176;
                                        }
                                    }

                                    var11 = 2.2F - (float)var2 * 0.6F;
                                    var45 = (int)(var12 / var11);
                                    var46 = (int)(var13 / var11);
                                    if (!dmnew.NOTRANS && var17.isImmaterial && var17.number != 10) {
                                        if (var17.mirrored) {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                        } else {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                        }
                                    } else if (var17.mirrored) {
                                        var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                    } else {
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                    }
                                }
                                break;
                            case 3:
                                if (var3 != 0 || var2 < 2) {
                                    if (var2 == 2) {
                                        var4 = 80;
                                        if (var3 == 1) {
                                            var5 = -70;
                                        } else {
                                            var5 = 206;
                                        }
                                    } else if (var2 == 1) {
                                        var4 = 16;
                                        if (var3 == 0) {
                                            var5 = -222;
                                        } else if (var3 == 1) {
                                            var5 = -46;
                                        } else {
                                            var5 = 135;
                                        }
                                    } else if (var2 == 0) {
                                        var4 = -16;
                                        if (var3 == 0) {
                                            var5 = -176;
                                        } else if (var3 == 1) {
                                            var5 = -28;
                                        } else {
                                            var5 = 106;
                                        }
                                    }

                                    var11 = 2.2F - (float)var2 * 0.6F;
                                    var45 = (int)(var12 / var11);
                                    var46 = (int)(var13 / var11);
                                    if (!dmnew.NOTRANS && var17.isImmaterial && var17.number != 10) {
                                        if (var17.mirrored) {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                        } else {
                                            dmnew.dview.offg2.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                        }
                                    } else if (var17.mirrored) {
                                        var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                    } else {
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                    }
                                }
                            case 4:
                            default:
                                break;
                            case 5:
                                if (var2 == 2) {
                                    var4 = 80;
                                    if (var3 == 0) {
                                        var5 = -256;
                                    } else if (var3 == 2) {
                                        var5 = 256;
                                    } else {
                                        var5 = 0;
                                    }
                                } else if (var2 == 1) {
                                    var4 = 16;
                                    if (var3 == 0) {
                                        var5 = -182;
                                    } else if (var3 == 2) {
                                        var5 = 182;
                                    } else {
                                        var5 = 0;
                                    }
                                } else if (var2 == 0) {
                                    var4 = -16;
                                    if (var3 == 0) {
                                        var5 = -136;
                                    } else if (var3 == 2) {
                                        var5 = 136;
                                    } else {
                                        var5 = 0;
                                    }
                                }

                                var11 = 2.2F - (float)var2 * 0.6F;
                                var45 = (int)(var12 / var11);
                                var46 = (int)(var13 / var11);
                                if (!dmnew.NOTRANS && var17.isImmaterial && var17.number != 10) {
                                    if (var17.mirrored) {
                                        dmnew.dview.offg2.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                    } else {
                                        dmnew.dview.offg2.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                    }
                                } else if (var17.mirrored) {
                                    var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46, 224 + var5 - var45 / 2, 200 + var4, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                } else {
                                    var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46, var45, var46, (ImageObserver)null);
                                }
                        }
                    }

                    if (var25 == 3) {
                        var25 = 5;
                    } else {
                        ++var25;
                    }
                }

                if (var2 == 0) {
                    darkfilt.setDarks(3);
                } else if (var2 == 1) {
                    darkfilt.setDarks(2);
                }
            }

            if (var23 == 6) {
                var23 = 4;
            }

            var47 = var22;
            int var28;
            if (this.numProjs > 0 && var21 < this.numProjs && (var2 != 3 || var3 == 1) && (this.hasParty || dmnew.dispell > 0 || var2 == 2 && var3 == 1 && dmnew.magicvision > 0 || !(this instanceof FakeWall))) {
                while(var47 < var23) {
                    int var8 = 0;

                    for(var28 = 0; var28 < this.numProjs && var21 < this.numProjs; ++var28) {
                        Projectile var48;
                        do {
                            do {
                                var48 = (Projectile)dmnew.dmprojs.get(var8);
                                ++var8;
                            } while(var48.level != dmnew.level);
                        } while(var48.x != var14 || var48.y != var15);

                        if ((var48.subsquare + dmnew.facing) % 4 == var47) {
                            ++var21;
                            temppic = null;
                            if (var48.isending && var48.sp != null) {
                                if (var2 == 3) {
                                    var1.drawImage(var48.sp.endpic0, 0, 0, (ImageObserver)null);
                                } else {
                                    temppic = var48.sp.endpic;
                                }
                            } else if ((var2 != 3 || var48.justthrown && var48.direction == dmnew.facing || !var48.justthrown && var47 < 2 && dmnew.heroatsub[var47] == -1 || !var48.justthrown && var48.direction != dmnew.facing && (var48.direction - dmnew.facing) % 2 == 0) && (var48.justthrown || var48.direction == dmnew.facing || (var48.direction - dmnew.facing) % 2 != 0 || dmnew.dmmons.get(dmnew.level + "," + var14 + "," + var15 + "," + 5) == null && dmnew.dmmons.get(dmnew.level + "," + var14 + "," + var15 + "," + var47) == null)) {
                                if (var2 < 3 && var48.it != null) {
                                    if (!var48.it.hasthrowpic) {
                                        temppic = Item.darkpic[var48.it.number][var2];
                                    }

                                    if (temppic == null) {
                                        temppic = dmnew.tk.createImage(new FilteredImageSource(var48.pic.getSource(), darkfilt));
                                        this.tracker.addImage(temppic, 0);

                                        try {
                                            this.tracker.waitForID(0);
                                        } catch (InterruptedException var37) {
                                            ;
                                        }

                                        this.tracker.removeImage(temppic);
                                        if (!var48.it.hasthrowpic) {
                                            Item.darkpic[var48.it.number][var2] = temppic;
                                        }
                                    }
                                } else {
                                    temppic = var48.pic;
                                }
                            }

                            if (temppic != null) {
                                var6 = false;
                                var7 = false;
                                var11 = 0.0F;
                                var12 = (float)temppic.getWidth((ImageObserver)null);
                                var13 = (float)temppic.getHeight((ImageObserver)null);
                                switch((var48.subsquare + dmnew.facing) % 4) {
                                    case 0:
                                        if ((var3 != 0 || var2 == 1) && var2 > 0 && (dmnew.magicvision > 0 && var2 == 2 || this.drawFurtherItems)) {
                                            if (var2 == 3) {
                                                var4 = -80;
                                                if (var3 == 1) {
                                                    var5 = -90;
                                                } else {
                                                    var5 = 260;
                                                }
                                            } else if (var2 == 2) {
                                                var4 = -88;
                                                if (var3 == 1) {
                                                    var5 = -56;
                                                } else {
                                                    var5 = 165;
                                                }
                                            } else {
                                                var4 = -92;
                                                if (var3 == 0) {
                                                    var5 = -200;
                                                } else if (var3 == 1) {
                                                    var5 = -36;
                                                } else {
                                                    var5 = 120;
                                                }
                                            }

                                            var11 = 2.8F - (float)var2 * 0.6F;
                                            if (var48.sp != null && var48.sp.type != 0) {
                                                var11 += (6.0F - (float)var48.sp.gain) / 5.0F;
                                            }

                                            var45 = (int)(var12 / var11);
                                            var46 = (int)(var13 / var11);
                                            if (var48.it == null || !var48.it.hasthrowpic || var3 != 0 && (var3 != 1 || (dmnew.facing - var48.direction) % 2 != 0)) {
                                                var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46 / 2, var45, var46, (ImageObserver)null);
                                            } else {
                                                var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46 / 2, 224 + var5 - var45 / 2, 200 + var4 + var46 / 2, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                            }
                                        }
                                        break;
                                    case 1:
                                        if (var3 == 2 && var2 != 1 || var2 <= 0 || (dmnew.magicvision <= 0 || var2 != 2) && !this.drawFurtherItems) {
                                            break;
                                        }

                                        if (var2 == 3) {
                                            var4 = -80;
                                            if (var3 == 1) {
                                                var5 = 90;
                                            } else {
                                                var5 = -260;
                                            }
                                        } else if (var2 == 2) {
                                            var4 = -88;
                                            if (var3 == 1) {
                                                var5 = 56;
                                            } else {
                                                var5 = -165;
                                            }
                                        } else {
                                            var4 = -92;
                                            if (var3 == 0) {
                                                var5 = -120;
                                            } else if (var3 == 1) {
                                                var5 = 36;
                                            } else {
                                                var5 = 200;
                                            }
                                        }

                                        var11 = 2.8F - (float)var2 * 0.6F;
                                        if (var48.sp != null && var48.sp.type != 0) {
                                            var11 += (6.0F - (float)var48.sp.gain) / 5.0F;
                                        }

                                        var45 = (int)(var12 / var11);
                                        var46 = (int)(var13 / var11);
                                        var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46 / 2, var45, var46, (ImageObserver)null);
                                        break;
                                    case 2:
                                        if ((var3 != 2 || var2 < 2) && var2 != 3) {
                                            if (var2 == 2) {
                                                var4 = -84;
                                                if (var3 == 1) {
                                                    var5 = 70;
                                                } else {
                                                    var5 = -206;
                                                }
                                            } else if (var2 == 1) {
                                                var4 = -90;
                                                if (var3 == 0) {
                                                    var5 = -135;
                                                } else if (var3 == 1) {
                                                    var5 = 46;
                                                } else {
                                                    var5 = 222;
                                                }
                                            } else if (var2 == 0) {
                                                var4 = -95;
                                                if (var3 == 0) {
                                                    var5 = -106;
                                                } else if (var3 == 1) {
                                                    var5 = 28;
                                                } else {
                                                    var5 = 176;
                                                }
                                            }

                                            var11 = 2.5F - (float)var2 * 0.6F;
                                            if (var48.sp != null && var48.sp.type != 0) {
                                                var11 += (6.0F - (float)var48.sp.gain) / 5.0F;
                                            }

                                            var45 = (int)(var12 / var11);
                                            var46 = (int)(var13 / var11);
                                            var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46 / 2, var45, var46, (ImageObserver)null);
                                        }
                                        break;
                                    case 3:
                                        if ((var3 != 0 || var2 < 2) && var2 != 3) {
                                            if (var2 == 2) {
                                                var4 = -84;
                                                if (var3 == 1) {
                                                    var5 = -70;
                                                } else {
                                                    var5 = 206;
                                                }
                                            } else if (var2 == 1) {
                                                var4 = -90;
                                                if (var3 == 0) {
                                                    var5 = -222;
                                                } else if (var3 == 1) {
                                                    var5 = -46;
                                                } else {
                                                    var5 = 135;
                                                }
                                            } else if (var2 == 0) {
                                                var4 = -95;
                                                if (var3 == 0) {
                                                    var5 = -176;
                                                } else if (var3 == 1) {
                                                    var5 = -28;
                                                } else {
                                                    var5 = 106;
                                                }
                                            }

                                            var11 = 2.5F - (float)var2 * 0.6F;
                                            if (var48.sp != null && var48.sp.type != 0) {
                                                var11 += (6.0F - (float)var48.sp.gain) / 5.0F;
                                            }

                                            var45 = (int)(var12 / var11);
                                            var46 = (int)(var13 / var11);
                                            if (var48.it == null || !var48.it.hasthrowpic || var3 != 0 && (var3 != 1 || (dmnew.facing - var48.direction) % 2 != 0)) {
                                                var1.drawImage(temppic, 224 + var5 - var45 / 2, 200 + var4 - var46 / 2, var45, var46, (ImageObserver)null);
                                            } else {
                                                var1.drawImage(temppic, 224 + var5 + var45 / 2, 200 + var4 - var46 / 2, 224 + var5 - var45 / 2, 200 + var4 + var46 / 2, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                                            }
                                        }
                                }
                            }
                        }
                    }

                    ++var47;
                }
            }

            if (var26 == 0 && var2 != 3 && this instanceof Door && (dmnew.facing == ((Door)this).side || (dmnew.facing + 2) % 4 == ((Door)this).side)) {
                var28 = 3 - var2;
                int var29 = var3 + 1;
                int var30 = ((Door)this).xc;
                int var31 = ((Door)this).yc;
                Graphics2D var32 = var1;
                if (var28 == 1 && var29 == 2 && dmnew.magicvision > 0 && ((Door)this).pictype != 8) {
                    var32 = dmnew.dview.offg2;
                }

                if (var29 == 2) {
                    var1.drawImage(((Door)this).openpic[var28 - 1], var30, var31, (ImageObserver)null);
                } else if (var28 == 1) {
                    if (var29 == 1) {
                        var1.drawImage(((Door)this).openpic[0], var30 - 197, var31, (ImageObserver)null);
                    } else {
                        var1.drawImage(((Door)this).openpic[0], var30 - 124, var31, (ImageObserver)null);
                    }
                } else if (var28 == 2) {
                    if (var29 == 1) {
                        var1.drawImage(((Door)this).openpic[1], var30 - 40, var31, (ImageObserver)null);
                    } else {
                        var1.drawImage(((Door)this).openpic[1], var30 - 168, var31, (ImageObserver)null);
                    }
                } else if (var28 == 3) {
                    if (var29 == 1) {
                        var1.drawImage(((Door)this).openpic[2], var30 + 18, var31, (ImageObserver)null);
                    } else {
                        var30 = var30 - ((Door)this).openpic[2].getWidth((ImageObserver)null) - 18;
                        var1.drawImage(((Door)this).openpic[2], var30, var31, (ImageObserver)null);
                    }
                }

                if (((Door)this).changecount != 0 || ((Door)this).isBroken) {
                    int var33 = this.pic[var28][2].getHeight((ImageObserver)null);
                    int var34 = this.pic[var28][2].getWidth((ImageObserver)null);
                    short var35 = 0;
                    byte var36 = 0;
                    if (var28 == 1) {
                        var36 = 18;
                        if (var29 == 2) {
                            var35 = 59;
                        } else if (var29 == 1) {
                            var35 = -138;
                        } else {
                            var35 = -64;
                        }
                    } else if (var28 == 2) {
                        var36 = 9;
                        if (var29 == 2) {
                            var35 = 37;
                        } else if (var29 == 1) {
                            var35 = -2;
                        } else {
                            var35 = 3;
                        }
                    } else if (var28 == 3) {
                        var36 = 6;
                        if (var29 == 2) {
                            var35 = 26;
                        } else if (var29 == 1) {
                            var35 = 45;
                        } else {
                            var35 = 25;
                        }
                    }

                    if (var28 != 3 && var28 != 1 && var29 == 3) {
                        var30 = var30 - this.pic[var28][3].getWidth((ImageObserver)null) - 1;
                    }

                    if (((Door)this).changecount == 0) {
                        var32.drawImage(((Door)this).brokenpic[var28 - 1], var30 + var35, var31 + var36, (ImageObserver)null);
                    } else if (((Door)this).changecount == 4) {
                        var32.drawImage(this.pic[var28][2], var30 + var35, var31 + var36, (ImageObserver)null);
                    } else if (((Door)this).changecount == 1) {
                        ((Door)this).offscreen = new BufferedImage(var34, var33, 2);
                        ((Door)this).offg = ((Door)this).offscreen.createGraphics();
                        ((Door)this).offg.drawImage(this.pic[var28][2], 0, 0, (ImageObserver)null);
                        var32.drawImage(((Door)this).offscreen.getSubimage(0, 3 * var33 / 4, var34, var33 / 4), var30 + var35, var31 + var36, (ImageObserver)null);
                    } else if (((Door)this).changecount == 2) {
                        ((Door)this).offscreen = new BufferedImage(var34, var33, 2);
                        ((Door)this).offg = ((Door)this).offscreen.createGraphics();
                        ((Door)this).offg.drawImage(this.pic[var28][2], 0, 0, (ImageObserver)null);
                        var32.drawImage(((Door)this).offscreen.getSubimage(0, var33 / 2, var34, var33 / 2), var30 + var35, var31 + var36, (ImageObserver)null);
                    } else if (((Door)this).changecount == 3) {
                        ((Door)this).offscreen = new BufferedImage(var34, var33, 2);
                        ((Door)this).offg = ((Door)this).offscreen.createGraphics();
                        ((Door)this).offg.drawImage(this.pic[var28][2], 0, 0, (ImageObserver)null);
                        var32.drawImage(((Door)this).offscreen.getSubimage(0, var33 / 4, var34, 3 * var33 / 4), var30 + var35, var31 + var36, (ImageObserver)null);
                    }
                }
            }

            var22 = 2;
            var23 = 4;
        }

        if (this.hasCloud && dmnew.cloudchanging && (var2 != 3 || var3 == 1) && (this.hasParty || dmnew.dispell > 0 || var2 == 2 && var3 == 1 && dmnew.magicvision > 0 || !(this instanceof FakeWall))) {
            int var52 = 0;
            if (var3 == 0) {
                var52 = -132 - 20 * var2 * var2;
            } else if (var3 == 2) {
                var52 = 132 + 20 * var2 * var2;
            }

            Iterator var50 = dmnew.cloudstochange.iterator();

            while(var50.hasNext()) {
                PoisonCloud var49 = (PoisonCloud)var50.next();
                if (var49.x == var14 && var49.y == var15) {
                    if (var2 == 3) {
                        if (var49.stagecounter >= 1 && var49.stagecounter != 4 && var49.stagecounter != 5) {
                            var1.drawImage(dmnew.cloudpicin, 448, 0, 0, 326, 0, 0, 448, 326, (ImageObserver)null);
                        } else {
                            var1.drawImage(dmnew.cloudpicin, 0, 0, (ImageObserver)null);
                        }
                    } else {
                        var12 = 272.0F;
                        var13 = 164.0F;
                        var11 = 3.5F - ((float)var2 + 0.5F) + 0.5F * (6.0F - (float)var49.stage);
                        var45 = (int)(var12 / var11);
                        var46 = (int)(var13 / var11);
                        if (var49.stagecounter >= 1 && var49.stagecounter != 4 && var49.stagecounter != 5) {
                            var1.drawImage(dmnew.cloudpic[2 - var2], 223 + var52 - var45 / 2 + var45, 120 + var2 * var2 - var46 / 2 - 4 + var2, 223 + var52 - var45 / 2, 120 + var2 * var2 - var46 / 2 - 4 + var2 + var46, 0, 0, (int)var12, (int)var13, (ImageObserver)null);
                        } else {
                            var1.drawImage(dmnew.cloudpic[2 - var2], 223 + var52 - var45 / 2, 120 + var2 * var2 - var46 / 2 - 4 + var2, var45, var46, (ImageObserver)null);
                        }
                    }
                    break;
                }
            }
        }

        if (dmnew.fluxchanging) {
            FluxCage var51 = (FluxCage)dmnew.fluxcages.get(dmnew.level + "," + var14 + "," + var15);
            if (var51 != null) {
                if (!var51.mirrored) {
                    var1.drawImage(dmnew.cagepic[3 - var2][var3], dmnew.cagex[3 - var2][var3], dmnew.cagey[3 - var2], (ImageObserver)null);
                } else if (var3 == 0) {
                    var1.drawImage(dmnew.cagepic[3 - var2][2], dmnew.cagex[3 - var2][0] + dmnew.cagepic[3 - var2][2].getWidth((ImageObserver)null), dmnew.cagey[3 - var2], dmnew.cagex[3 - var2][0], dmnew.cagey[3 - var2] + dmnew.cagepic[3 - var2][2].getHeight((ImageObserver)null), 0, 0, dmnew.cagepic[3 - var2][2].getWidth((ImageObserver)null), dmnew.cagepic[3 - var2][2].getHeight((ImageObserver)null), (ImageObserver)null);
                } else if (var3 == 2) {
                    var1.drawImage(dmnew.cagepic[3 - var2][0], dmnew.cagex[3 - var2][2] + dmnew.cagepic[3 - var2][0].getWidth((ImageObserver)null), dmnew.cagey[3 - var2], dmnew.cagex[3 - var2][2], dmnew.cagey[3 - var2] + dmnew.cagepic[3 - var2][0].getHeight((ImageObserver)null), 0, 0, dmnew.cagepic[3 - var2][0].getWidth((ImageObserver)null), dmnew.cagepic[3 - var2][0].getHeight((ImageObserver)null), (ImageObserver)null);
                } else {
                    var1.drawImage(dmnew.cagepic[3 - var2][var3], dmnew.cagex[3 - var2][var3] + dmnew.cagepic[3 - var2][var3].getWidth((ImageObserver)null), dmnew.cagey[3 - var2], dmnew.cagex[3 - var2][var3], dmnew.cagey[3 - var2] + dmnew.cagepic[3 - var2][var3].getHeight((ImageObserver)null), 0, 0, dmnew.cagepic[3 - var2][var3].getWidth((ImageObserver)null), dmnew.cagepic[3 - var2][var3].getHeight((ImageObserver)null), (ImageObserver)null);
                }
            }
        }

    }

    public String toString() {
        if (this.hasParty) {
            return "X";
        } else if (this.hasMons) {
            return "M";
        } else {
            return this.numProjs > 0 ? "P" : "" + this.mapchar;
        }
    }

    public void save(ObjectOutputStream var1) throws IOException {
        var1.writeChar(this.mapchar);
        var1.writeBoolean(this.canHoldItems);
        var1.writeBoolean(this.isPassable);
        var1.writeBoolean(this.canPassProjs);
        var1.writeBoolean(this.canPassMons);
        var1.writeBoolean(this.canPassImmaterial);
        var1.writeBoolean(this.drawItems);
        var1.writeBoolean(this.drawFurtherItems);
        var1.writeInt(this.numProjs);
        var1.writeBoolean(this.hasParty);
        var1.writeBoolean(this.hasMons);
        var1.writeBoolean(this.hasItems);
        if (this.hasItems) {
            var1.writeObject(this.mapItems);
        }

    }

    public void load(ObjectInputStream var1) throws IOException, ClassNotFoundException {
    }

}
