package com.github.theresajayne.dmjava.entities;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.io.*;
import java.util.HashMap;
import java.util.Random;

class Item implements Serializable {
    int type;
    int size = 0;
    float weight = 0.0F;
    int number;
    String name;
    Image pic;
    Image dpic;
    Image epic = null;
    Image upic = null;
    Image temppic;
    Image[] throwpic;
    String picstring = "";
    String dpicstring = "";
    String equipstring = "";
    String usedupstring = "";
    String throwpicstring = "";
    int throwpow = 1;
    int shotpow = 0;
    int projtype = 0;
    String[][] function;
    int[] power;
    int[] speed;
    int[] level;
    int defense = 0;
    int magicresist = 0;
    int foodvalue;
    int functions = 0;
    int[] charges = new int[]{0, 0, 0};
    int potionpow;
    int potioncastpow;
    String bombnum;
    boolean hasthrowpic = false;
    boolean ispotion = false;
    boolean isbomb = false;
    boolean hitsImmaterial = false;
    int poisonous = 0;
    String[] scroll;
    boolean[] bound;
    static Image[][] darkpic;
    static HashMap pics = new HashMap(350);
    static int maxitemnum = 1;
    static final long serialVersionUID = -5698111842003644130L;
    boolean haseffect = false;
    String[] effect;
    int fleveladded = 0;
    int nleveladded = 0;
    int wleveladded = 0;
    int pleveladded = 0;
    int subsquare = 0;
    int xoffset;
    int yoffset;
    static final int WEAPON = 0;
    static final int SHIELD = 1;
    static final int HEAD = 2;
    static final int TORSO = 3;
    static final int NECK = 4;
    static final int LEGS = 5;
    static final int FEET = 6;
    static final int FOOD = 7;
    static final int OTHER = 8;
    static final Random randGen = new Random();
    static final Toolkit tk = Toolkit.getDefaultToolkit();
    static final MediaTracker ImageTracker = new MediaTracker(new JPanel());

    public Item() {
        this.xoffset = randGen.nextInt() % 5;
        this.yoffset = randGen.nextInt() % 5;
    }

    public Item(int var1) {
        this.number = var1;
        this.xoffset = randGen.nextInt() % 5;
        this.yoffset = randGen.nextInt() % 5;
        this.subsquare = randGen.nextInt(4);
        switch(this.number) {
            case 6:
                this.type = 0;
                this.name = "Fist/Foot";
                this.picstring = "fistfoot.gif";
                this.dpicstring = "";
                this.weight = 0.0F;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Punch";
                this.function[0][1] = "n";
                this.function[1][0] = "Kick";
                this.function[1][1] = "n";
                this.function[2][0] = "Detect Illusion";
                this.function[2][1] = "n";
                this.power = new int[3];
                this.power[0] = 1;
                this.power[1] = 2;
                this.power[2] = 0;
                this.speed = new int[3];
                this.speed[0] = 3;
                this.speed[1] = 7;
                this.speed[2] = 25;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 9;
                break;
            case 7:
                this.type = 8;
                this.name = "Flask";
                this.picstring = "flask.gif";
                this.dpicstring = "dflask.gif";
                this.weight = 0.1F;
                this.size = 1;
                break;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 47:
            case 48:
            case 49:
            case 50:
            case 60:
            case 70:
            case 73:
            case 98:
            case 99:
            case 100:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 132:
            case 133:
            case 134:
            case 135:
            case 151:
            case 152:
            case 153:
            case 154:
            case 155:
            case 172:
            case 173:
            case 174:
            case 175:
            case 188:
            case 189:
            case 190:
            case 191:
            case 192:
            case 193:
            case 194:
            case 195:
            case 196:
            case 197:
            case 198:
            case 199:
            case 220:
            case 224:
            case 225:
            case 231:
            case 232:
            case 233:
            case 234:
            case 235:
            case 253:
            case 254:
            case 255:
            case 262:
            case 263:
            case 264:
            case 265:
            case 266:
            case 272:
            case 273:
            case 274:
            case 275:
            default:
                this.type = 0;
                this.name = "Rock";
                this.picstring = "rock.gif";
                this.dpicstring = "drock.gif";
                this.weight = 1.0F;
                this.projtype = 2;
                this.throwpow = 2;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Throw";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 2;
                this.speed = new int[1];
                this.speed[0] = 8;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 31:
                this.type = 8;
                this.name = "Iron Key";
                this.picstring = "iron_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 32:
                this.type = 8;
                this.name = "Key of B";
                this.picstring = "key_b.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 33:
                this.type = 8;
                this.name = "Square Key";
                this.picstring = "square_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 34:
                this.type = 8;
                this.name = "Solid Key";
                this.picstring = "solid_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.2F;
                break;
            case 35:
                this.type = 8;
                this.name = "Cross Key";
                this.picstring = "cross_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 36:
                this.type = 8;
                this.name = "Skeleton Key";
                this.picstring = "skeleton_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 37:
                this.type = 8;
                this.name = "Onyx Key";
                this.picstring = "onyx_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 38:
                this.type = 8;
                this.name = "Tourquoise Key";
                this.picstring = "tourquoise_key.gif";
                this.dpicstring = "dkey_iron.gif";
                this.weight = 0.1F;
                break;
            case 39:
                this.type = 8;
                this.name = "Gold Key";
                this.picstring = "gold_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 40:
                this.type = 8;
                this.name = "Master Key";
                this.picstring = "master_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 41:
                this.type = 8;
                this.name = "Ra Key";
                this.picstring = "ra_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 42:
                this.type = 8;
                this.name = "Winged Key";
                this.picstring = "winged_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 43:
                this.type = 8;
                this.name = "Emerald Key";
                this.picstring = "emerald_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 44:
                this.type = 8;
                this.name = "Ruby Key";
                this.picstring = "ruby_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 45:
                this.type = 8;
                this.name = "Sapphire Key";
                this.picstring = "sapphire_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 46:
                this.type = 8;
                this.name = "Topaz Key";
                this.picstring = "topaz_key.gif";
                this.dpicstring = "dkey_gold.gif";
                this.weight = 0.1F;
                break;
            case 51:
                this.type = 8;
                this.name = "Gold Coin";
                this.picstring = "coin_gold.gif";
                this.dpicstring = "dcoin_gold.gif";
                this.weight = 0.1F;
                break;
            case 52:
                this.type = 8;
                this.name = "Silver Coin";
                this.picstring = "coin_silver.gif";
                this.dpicstring = "dcoin_silver.gif";
                this.weight = 0.1F;
                break;
            case 53:
                this.type = 8;
                this.name = "Copper Coin";
                this.picstring = "coin_copper.gif";
                this.dpicstring = "dcoin_gold.gif";
                this.weight = 0.1F;
                break;
            case 54:
                this.type = 8;
                this.name = "Gor Coin";
                this.picstring = "coin_gor.gif";
                this.dpicstring = "dcoin_gold.gif";
                this.weight = 0.1F;
                break;
            case 55:
                this.type = 8;
                this.name = "Blue Gem";
                this.picstring = "gem_blue.gif";
                this.dpicstring = "dgem_blue.gif";
                this.weight = 0.2F;
                break;
            case 56:
                this.type = 8;
                this.name = "Green Gem";
                this.picstring = "gem_green.gif";
                this.dpicstring = "dgem_green.gif";
                this.weight = 0.2F;
                break;
            case 57:
                this.type = 8;
                this.name = "Orange Gem";
                this.picstring = "gem_orange.gif";
                this.dpicstring = "dgem_orange.gif";
                this.weight = 0.3F;
                break;
            case 58:
                this.type = 1;
                this.name = "Sar Crystal";
                this.picstring = "sar_crystal.gif";
                this.dpicstring = "dgem_blue.gif";
                this.weight = 0.5F;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,10";
                break;
            case 59:
                this.type = 1;
                this.name = "Ra Crystal";
                this.picstring = "ra_crystal.gif";
                this.dpicstring = "dgem_orange.gif";
                this.weight = 0.5F;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,10";
                break;
            case 61:
                this.type = 7;
                this.name = "Worm Round";
                this.picstring = "worm_round.gif";
                this.dpicstring = "dworm_round.gif";
                this.weight = 1.1F;
                this.size = 3;
                this.foodvalue = 150;
                break;
            case 62:
                this.type = 7;
                this.name = "Apple";
                this.picstring = "apple.gif";
                this.dpicstring = "dapple.gif";
                this.weight = 0.4F;
                this.foodvalue = 175;
                break;
            case 63:
                this.type = 7;
                this.name = "Corn";
                this.picstring = "corn.gif";
                this.dpicstring = "dcorn.gif";
                this.weight = 0.4F;
                this.size = 1;
                this.foodvalue = 200;
                break;
            case 64:
                this.type = 7;
                this.name = "Bread";
                this.picstring = "bread.gif";
                this.dpicstring = "dbread.gif";
                this.weight = 0.3F;
                this.size = 1;
                this.foodvalue = 250;
                break;
            case 65:
                this.type = 7;
                this.name = "Screamer Slice";
                this.picstring = "screamer_slice.gif";
                this.dpicstring = "dscreamer_slice.gif";
                this.weight = 0.5F;
                this.size = 3;
                this.foodvalue = 275;
                break;
            case 66:
                this.type = 7;
                this.name = "Cheese";
                this.picstring = "cheese.gif";
                this.dpicstring = "dcheese.gif";
                this.weight = 0.8F;
                this.size = 1;
                this.foodvalue = 300;
                break;
            case 67:
                this.type = 7;
                this.name = "Drumstick";
                this.picstring = "drumstick.gif";
                this.dpicstring = "ddrumstick.gif";
                this.weight = 0.4F;
                this.foodvalue = 450;
                break;
            case 68:
                this.type = 7;
                this.name = "Shank";
                this.picstring = "shank.gif";
                this.dpicstring = "dshank.gif";
                this.weight = 0.4F;
                this.size = 1;
                this.foodvalue = 550;
                break;
            case 69:
                this.type = 7;
                this.name = "Dragon Steak";
                this.picstring = "dragon_steak.gif";
                this.dpicstring = "dsteak.gif";
                this.weight = 0.6F;
                this.size = 1;
                this.foodvalue = 850;
                break;
            case 71:
                this.type = 8;
                this.name = "Lock Picks";
                this.picstring = "lock_picks.gif";
                this.dpicstring = "dlock_picks.gif";
                this.weight = 0.1F;
                break;
            case 72:
                this.type = 8;
                this.name = "Water Flask";
                this.picstring = "water_flask.gif";
                this.dpicstring = "dwater_flask.gif";
                this.weight = 0.3F;
                this.size = 1;
                this.foodvalue = 200;
                break;
            case 74:
                this.type = 8;
                this.name = "Ashes";
                this.picstring = "ashes.gif";
                this.dpicstring = "dashes.gif";
                this.weight = 0.4F;
                this.size = 1;
                break;
            case 75:
                this.type = 8;
                this.name = "Bones";
                this.picstring = "bones.gif";
                this.dpicstring = "dbones.gif";
                this.weight = 0.8F;
                this.size = 3;
                break;
            case 76:
                this.type = 8;
                this.name = "Boulder";
                this.picstring = "boulder.gif";
                this.dpicstring = "dboulder.gif";
                this.weight = 8.1F;
                this.size = 3;
                this.throwpow = 5;
                break;
            case 77:
                this.type = 8;
                this.name = "Magnifier";
                this.picstring = "magnifier.gif";
                this.dpicstring = "dmagnifier.gif";
                this.weight = 0.2F;
                break;
            case 78:
                this.type = 8;
                this.name = "Mirror of Dawn";
                this.picstring = "mirror_dawn.gif";
                this.dpicstring = "dmirror.gif";
                this.weight = 0.3F;
                this.size = 1;
                break;
            case 79:
                this.type = 8;
                this.name = "Corbamite";
                this.picstring = "corbamite.gif";
                this.dpicstring = "dcorbamite.gif";
                this.weight = 0.0F;
                break;
            case 80:
                this.type = 8;
                this.name = "Zo Kath Ra";
                this.picstring = "zo_kath_ra.gif";
                this.dpicstring = "zo_kath_ra.gif";
                this.weight = 0.0F;
                break;
            case 81:
                this.type = 0;
                this.name = "Rope";
                this.picstring = "rope.gif";
                this.dpicstring = "drope.gif";
                this.weight = 1.0F;
                this.size = 3;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Climb Down";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 0;
                this.speed = new int[1];
                this.speed[0] = 6;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 82:
                this.type = 8;
                this.name = "Corbum Ore";
                this.picstring = "corbum.gif";
                this.dpicstring = "corbum.gif";
                this.weight = 0.0F;
                break;
            case 83:
                this.type = 0;
                this.name = "Stick";
                this.picstring = "stick.gif";
                this.dpicstring = "dstick.gif";
                this.weight = 0.2F;
                this.size = 3;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.power = new int[1];
                this.power[0] = 2;
                this.speed = new int[1];
                this.speed[0] = 8;
                this.level = new int[1];
                this.level[0] = 0;
                this.bound = new boolean[4];
                break;
            case 84:
                this.type = 8;
                this.name = "Rabbit's Foot";
                this.picstring = "rabbit_foot.gif";
                this.dpicstring = "drabbit_foot.gif";
                this.weight = 0.1F;
                break;
            case 85:
                this.type = 0;
                this.name = "Grapple";
                this.picstring = "grapple.gif";
                this.dpicstring = "drope.gif";
                this.weight = 1.5F;
                this.size = 3;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Climb Down";
                this.function[0][1] = "n";
                this.function[1][0] = "Climb Up";
                this.function[1][1] = "n";
                this.power = new int[2];
                this.power[0] = 0;
                this.power[1] = 0;
                this.speed = new int[2];
                this.speed[0] = 6;
                this.speed[1] = 6;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 7;
                break;
            case 86:
                this.type = 4;
                this.name = "Cape";
                this.picstring = "cape.gif";
                this.dpicstring = "dbrown_clothes.gif";
                this.weight = 0.3F;
                this.size = 1;
                this.defense = 1;
                this.magicresist = 1;
                break;
            case 87:
                this.type = 4;
                this.name = "Cloak of Night";
                this.picstring = "cloak_night.gif";
                this.dpicstring = "dbrown_clothes.gif";
                this.weight = 0.4F;
                this.size = 1;
                this.defense = 3;
                this.magicresist = 5;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "dexterity,8";
                break;
            case 88:
                this.type = 4;
                this.name = "Choker";
                this.picstring = "choker.gif";
                this.dpicstring = "dchoker.gif";
                this.weight = 0.1F;
                this.size = 1;
                this.defense = 1;
                this.magicresist = 0;
                break;
            case 89:
                this.type = 4;
                this.name = "Illumlet";
                this.picstring = "illumlet.gif";
                this.dpicstring = "dnecklace.gif";
                this.equipstring = "illumlet-on.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 0;
                break;
            case 90:
                this.type = 4;
                this.name = "Moonstone";
                this.picstring = "moonstone.gif";
                this.dpicstring = "dmoonstone.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 1;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,10";
                break;
            case 91:
                this.type = 4;
                this.name = "Jewel Symal";
                this.picstring = "jewel_symal.gif";
                this.dpicstring = "dnecklace.gif";
                this.equipstring = "jewel_symal-on.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 10;
                break;
            case 92:
                this.type = 4;
                this.name = "Ekkhard Cross";
                this.picstring = "ekkhard_cross.gif";
                this.dpicstring = "dnecklace.gif";
                this.weight = 0.3F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 2;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "wisdom,4";
                break;
            case 93:
                this.type = 4;
                this.name = "Gem of Ages";
                this.picstring = "gem_ages.gif";
                this.dpicstring = "dnecklace.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 2;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "intelligence,4";
                break;
            case 94:
                this.type = 4;
                this.name = "Pendant Feral";
                this.picstring = "pendant_feral.gif";
                this.dpicstring = "dpendant.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 5;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "wlevel,1";
                break;
            case 95:
                this.type = 4;
                this.name = "The Hellion";
                this.picstring = "hellion.gif";
                this.dpicstring = "dpendant.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 2;
                this.haseffect = true;
                this.effect = new String[5];
                this.effect[0] = "strength,3";
                this.effect[1] = "vitality,3";
                this.effect[2] = "dexterity,3";
                this.effect[3] = "intelligence,3";
                this.effect[4] = "wisdom,3";
                break;
            case 96:
                this.type = 4;
                this.name = "Symbol of Ra";
                this.picstring = "ra_necklace.gif";
                this.dpicstring = "dnecklace.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 0;
                this.magicresist = 4;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "vitality,4";
                this.effect[1] = "intelligence,4";
                this.effect[2] = "wisdom,4";
                break;
            case 97:
                this.type = 4;
                this.name = "Symbol of Sar";
                this.picstring = "sar_necklace.gif";
                this.dpicstring = "dpendant.gif";
                this.weight = 0.2F;
                this.size = 1;
                this.defense = 2;
                this.magicresist = 2;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "strength,4";
                this.effect[1] = "intelligence,4";
                this.effect[2] = "wisdom,4";
                break;
            case 101:
                this.type = 1;
                this.name = "Hide Shield";
                this.picstring = "hide_shield.gif";
                this.dpicstring = "dsmall_shield.gif";
                this.weight = 1.0F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 2;
                break;
            case 102:
                this.type = 1;
                this.name = "Buckler";
                this.picstring = "buckler.gif";
                this.dpicstring = "dsmall_shield.gif";
                this.weight = 1.1F;
                this.size = 3;
                this.defense = 3;
                this.magicresist = 3;
                break;
            case 103:
                this.type = 1;
                this.name = "Small Shield";
                this.picstring = "small_shield.gif";
                this.dpicstring = "dsmall_shield.gif";
                this.weight = 2.1F;
                this.size = 3;
                this.defense = 5;
                this.magicresist = 5;
                break;
            case 104:
                this.type = 1;
                this.name = "Wooden Shield";
                this.picstring = "wooden_shield.gif";
                this.dpicstring = "dlarge_shield.gif";
                this.weight = 1.4F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 4;
                break;
            case 105:
                this.type = 1;
                this.name = "Large Shield";
                this.picstring = "large_shield.gif";
                this.dpicstring = "dlarge_shield.gif";
                this.weight = 3.4F;
                this.size = 3;
                this.defense = 7;
                this.magicresist = 8;
                break;
            case 106:
                this.type = 1;
                this.name = "Lyte Shield";
                this.picstring = "lyte_shield.gif";
                this.dpicstring = "dlarge_shield.gif";
                this.weight = 3.0F;
                this.size = 3;
                this.defense = 8;
                this.magicresist = 9;
                break;
            case 107:
                this.type = 1;
                this.name = "Sar Shield";
                this.picstring = "sar_shield.gif";
                this.dpicstring = "dlarge_shield.gif";
                this.weight = 5.0F;
                this.size = 3;
                this.defense = 10;
                this.magicresist = 10;
                break;
            case 108:
                this.type = 1;
                this.name = "Dragon Shield";
                this.picstring = "dragon_shield.gif";
                this.dpicstring = "dlarge_shield.gif";
                this.weight = 4.0F;
                this.size = 3;
                this.defense = 12;
                this.magicresist = 11;
                break;
            case 109:
                this.type = 1;
                this.name = "Ra Shield";
                this.picstring = "ra_shield.gif";
                this.dpicstring = "dlarge_shield.gif";
                this.weight = 3.4F;
                this.size = 3;
                this.defense = 10;
                this.magicresist = 12;
                break;
            case 116:
                this.type = 2;
                this.name = "Calista";
                this.picstring = "calista.gif";
                this.dpicstring = "dcrown.gif";
                this.weight = 0.4F;
                this.size = 1;
                this.defense = 1;
                this.magicresist = 8;
                break;
            case 117:
                this.type = 2;
                this.name = "Crown of Nerra";
                this.picstring = "crown_nerra.gif";
                this.dpicstring = "dcrown.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 4;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "wisdom,10";
                break;
            case 118:
                this.type = 2;
                this.name = "Berserker Helm";
                this.picstring = "berserker_helm.gif";
                this.dpicstring = "dsmall_helm.gif";
                this.weight = 1.1F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 2;
                break;
            case 119:
                this.type = 2;
                this.name = "Basinet";
                this.picstring = "basinet.gif";
                this.dpicstring = "dsmall_helm.gif";
                this.weight = 1.5F;
                this.size = 3;
                this.defense = 3;
                this.magicresist = 3;
                break;
            case 120:
                this.type = 2;
                this.name = "Helmet";
                this.picstring = "helmet.gif";
                this.dpicstring = "dsmall_helm.gif";
                this.weight = 1.4F;
                this.size = 3;
                this.defense = 4;
                this.magicresist = 4;
                break;
            case 121:
                this.type = 2;
                this.name = "Casque 'n Coif";
                this.picstring = "casque_n_coif.gif";
                this.dpicstring = "dsmall_helm.gif";
                this.weight = 1.6F;
                this.size = 3;
                this.defense = 5;
                this.magicresist = 5;
                break;
            case 122:
                this.type = 2;
                this.name = "Armet";
                this.picstring = "armet.gif";
                this.dpicstring = "dhelm.gif";
                this.weight = 1.9F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 6;
                break;
            case 123:
                this.type = 2;
                this.name = "Dexhelm";
                this.picstring = "dexhelm.gif";
                this.dpicstring = "dhelm.gif";
                this.weight = 1.4F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 6;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "dexterity,10";
                break;
            case 124:
                this.type = 2;
                this.name = "Lyte Helm";
                this.picstring = "lyte_helm.gif";
                this.dpicstring = "dhelm.gif";
                this.weight = 1.7F;
                this.size = 3;
                this.defense = 7;
                this.magicresist = 7;
                break;
            case 125:
                this.type = 2;
                this.name = "Sar Helm";
                this.picstring = "sar_helm.gif";
                this.dpicstring = "dhelm.gif";
                this.weight = 3.5F;
                this.size = 3;
                this.defense = 8;
                this.magicresist = 8;
                break;
            case 126:
                this.type = 2;
                this.name = "Dragon Helm";
                this.picstring = "dragon_helm.gif";
                this.dpicstring = "dhelm.gif";
                this.weight = 3.5F;
                this.size = 3;
                this.defense = 10;
                this.magicresist = 9;
                break;
            case 127:
                this.type = 2;
                this.name = "Ra Helm";
                this.picstring = "ra_helm.gif";
                this.dpicstring = "dhelm.gif";
                this.weight = 2.5F;
                this.size = 3;
                this.defense = 9;
                this.magicresist = 10;
                break;
            case 128:
                this.type = 2;
                this.name = "Sar Circlet";
                this.picstring = "sar_circlet.gif";
                this.dpicstring = "dcrown.gif";
                this.weight = 0.5F;
                this.size = 1;
                this.defense = 1;
                this.magicresist = 5;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "intelligence,8";
                this.effect[1] = "mana,15";
                break;
            case 129:
                this.type = 2;
                this.name = "Ra Circlet";
                this.picstring = "ra_circlet.gif";
                this.dpicstring = "dcrown.gif";
                this.weight = 0.3F;
                this.size = 1;
                this.defense = 1;
                this.magicresist = 5;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "intelligence,8";
                this.effect[1] = "mana,15";
                break;
            case 130:
                this.type = 2;
                this.name = "Executioner's Hood";
                this.picstring = "executioner_hood.gif";
                this.dpicstring = "dblack_clothes.gif";
                this.weight = 0.5F;
                this.size = 3;
                this.defense = 4;
                this.magicresist = 10;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "intelligence,10";
                this.effect[1] = "mana,20";
                break;
            case 131:
                this.type = 2;
                this.name = "Elven Circlet";
                this.picstring = "elven_circlet.gif";
                this.dpicstring = "dcrown.gif";
                this.equipstring = "elven_circlet-on.gif";
                this.weight = 0.1F;
                this.size = 1;
                this.defense = 4;
                this.magicresist = 10;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "wisdom,10";
                this.effect[1] = "intelligence,10";
                this.effect[2] = "mana,20";
                break;
            case 136:
                this.type = 3;
                this.name = "Halter";
                this.picstring = "halter.gif";
                this.dpicstring = "dhalter.gif";
                this.weight = 0.2F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 1;
                break;
            case 137:
                this.type = 3;
                this.name = "Robe";
                this.picstring = "robe_top.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.4F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 3;
                break;
            case 138:
                this.type = 3;
                this.name = "Fine Robe";
                this.picstring = "fine_robe_top.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.3F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 4;
                break;
            case 139:
                this.type = 3;
                this.name = "Ghi";
                this.picstring = "ghi.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.5F;
                this.size = 3;
                this.defense = 4;
                this.magicresist = 2;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "dexterity,4";
                break;
            case 140:
                this.type = 3;
                this.name = "Leather Jerkin";
                this.picstring = "leather_jerkin.gif";
                this.dpicstring = "dbrown_clothes.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 3;
                this.magicresist = 3;
                break;
            case 141:
                this.type = 3;
                this.name = "Elven Dublet";
                this.picstring = "elven_dublet.gif";
                this.dpicstring = "delven_clothes.gif";
                this.weight = 0.3F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 6;
                break;
            case 142:
                this.type = 3;
                this.name = "Mail Aketon";
                this.picstring = "mail_aketon.gif";
                this.dpicstring = "dmail.gif";
                this.weight = 6.5F;
                this.size = 3;
                this.defense = 8;
                this.magicresist = 8;
                break;
            case 143:
                this.type = 3;
                this.name = "Torso Plate";
                this.picstring = "torso_plate.gif";
                this.dpicstring = "dtorso_plate.gif";
                this.weight = 12.0F;
                this.size = 4;
                this.defense = 10;
                this.magicresist = 10;
                break;
            case 144:
                this.type = 3;
                this.name = "Mithril Aketon";
                this.picstring = "mithril_aketon.gif";
                this.dpicstring = "dmail.gif";
                this.weight = 4.2F;
                this.size = 3;
                this.defense = 12;
                this.magicresist = 12;
                break;
            case 145:
                this.type = 3;
                this.name = "Lyte Plate";
                this.picstring = "lyte_torso.gif";
                this.dpicstring = "dtorso_plate.gif";
                this.weight = 10.8F;
                this.size = 4;
                this.defense = 14;
                this.magicresist = 14;
                break;
            case 146:
                this.type = 3;
                this.name = "Sar Plate";
                this.picstring = "sar_torso.gif";
                this.dpicstring = "dtorso_plate.gif";
                this.weight = 14.1F;
                this.size = 4;
                this.defense = 16;
                this.magicresist = 16;
                break;
            case 147:
                this.type = 3;
                this.name = "Dragon Plate";
                this.picstring = "dragon_torso.gif";
                this.dpicstring = "dtorso_plate.gif";
                this.weight = 14.1F;
                this.size = 4;
                this.defense = 20;
                this.magicresist = 17;
                break;
            case 148:
                this.type = 3;
                this.name = "Ra Plate";
                this.picstring = "ra_torso.gif";
                this.dpicstring = "dtorso_plate.gif";
                this.weight = 12.1F;
                this.size = 4;
                this.defense = 17;
                this.magicresist = 20;
                break;
            case 149:
                this.type = 3;
                this.name = "Sar Robe";
                this.picstring = "sar_robe_top.gif";
                this.dpicstring = "dblack_clothes.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 5;
                this.magicresist = 12;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "intelligence,5";
                this.effect[1] = "wisdom,5";
                this.effect[2] = "mana,10";
                break;
            case 150:
                this.type = 3;
                this.name = "Ra Robe";
                this.picstring = "ra_robe_top.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 5;
                this.magicresist = 12;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "intelligence,5";
                this.effect[1] = "wisdom,5";
                this.effect[2] = "mana,10";
                break;
            case 156:
                this.type = 5;
                this.name = "Barbarian Hide";
                this.picstring = "barbarian_hide.gif";
                this.dpicstring = "dbrown_clothes.gif";
                this.weight = 0.3F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 0;
                break;
            case 157:
                this.type = 5;
                this.name = "Robe";
                this.picstring = "robe_bottom.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.4F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 1;
                break;
            case 158:
                this.type = 5;
                this.name = "Fine Robe";
                this.picstring = "fine_robe_bottom.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.3F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 2;
                break;
            case 159:
                this.type = 5;
                this.name = "Ghi Trousers";
                this.picstring = "ghi_trousers.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.5F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 1;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "dexterity,2";
                break;
            case 160:
                this.type = 5;
                this.name = "Leather Pants";
                this.picstring = "leather_pants.gif";
                this.dpicstring = "dbrown_clothes.gif";
                this.weight = 0.8F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 2;
                break;
            case 161:
                this.type = 5;
                this.name = "Elven Huke";
                this.picstring = "elven_huke.gif";
                this.dpicstring = "delven_clothes.gif";
                this.weight = 0.3F;
                this.size = 3;
                this.defense = 3;
                this.magicresist = 3;
                break;
            case 162:
                this.type = 5;
                this.name = "Leg Mail";
                this.picstring = "mail_leg.gif";
                this.dpicstring = "dmail.gif";
                this.weight = 5.3F;
                this.size = 3;
                this.defense = 4;
                this.magicresist = 4;
                break;
            case 163:
                this.type = 5;
                this.name = "Leg Plate";
                this.picstring = "leg_plate.gif";
                this.dpicstring = "dleg_plate.gif";
                this.weight = 8.0F;
                this.size = 3;
                this.defense = 5;
                this.magicresist = 5;
                break;
            case 164:
                this.type = 5;
                this.name = "Mithril Leg";
                this.picstring = "mithril_leg.gif";
                this.dpicstring = "dmail.gif";
                this.weight = 3.1F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 6;
                break;
            case 165:
                this.type = 5;
                this.name = "Lyte Poleyn";
                this.picstring = "lyte_poleyn.gif";
                this.dpicstring = "dleg_plate.gif";
                this.weight = 7.2F;
                this.size = 3;
                this.defense = 7;
                this.magicresist = 7;
                break;
            case 166:
                this.type = 5;
                this.name = "Sar Poleyn";
                this.picstring = "sar_poleyn.gif";
                this.dpicstring = "dleg_plate.gif";
                this.weight = 9.0F;
                this.size = 3;
                this.defense = 7;
                this.magicresist = 7;
                break;
            case 167:
                this.type = 5;
                this.name = "Dragon Poleyn";
                this.picstring = "dragon_poleyn.gif";
                this.dpicstring = "dleg_plate.gif";
                this.weight = 9.0F;
                this.size = 3;
                this.defense = 8;
                this.magicresist = 8;
                break;
            case 168:
                this.type = 5;
                this.name = "Ra Poleyn";
                this.picstring = "ra_poleyn.gif";
                this.dpicstring = "dleg_plate.gif";
                this.weight = 8.0F;
                this.size = 3;
                this.defense = 8;
                this.magicresist = 8;
                break;
            case 169:
                this.type = 5;
                this.name = "Powertowers";
                this.picstring = "powertowers.gif";
                this.dpicstring = "dleg_plate.gif";
                this.weight = 8.2F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 6;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "strength,10";
                break;
            case 170:
                this.type = 5;
                this.name = "Sar Robe";
                this.picstring = "sar_robe_bottom.gif";
                this.dpicstring = "dblack_clothes.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 4;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "intelligence,2";
                this.effect[1] = "wisdom,2";
                this.effect[2] = "mana,5";
                break;
            case 171:
                this.type = 5;
                this.name = "Ra Robe";
                this.picstring = "ra_robe_bottom.gif";
                this.dpicstring = "dwhite_clothes.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 4;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "intelligence,2";
                this.effect[1] = "wisdom,2";
                this.effect[2] = "mana,5";
                break;
            case 176:
                this.type = 6;
                this.name = "Sandals";
                this.picstring = "sandals.gif";
                this.dpicstring = "dsandals.gif";
                this.weight = 0.6F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 0;
                break;
            case 177:
                this.type = 6;
                this.name = "Leather Boots";
                this.picstring = "leather_boots.gif";
                this.dpicstring = "dleather_boots.gif";
                this.weight = 1.4F;
                this.size = 3;
                this.defense = 1;
                this.magicresist = 1;
                break;
            case 178:
                this.type = 6;
                this.name = "Black Boots";
                this.picstring = "black_boots.gif";
                this.dpicstring = "dblack_boots.gif";
                this.weight = 1.6F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 1;
                break;
            case 179:
                this.type = 6;
                this.name = "Elven Boots";
                this.picstring = "elven_boots.gif";
                this.dpicstring = "delven_boots.gif";
                this.weight = 0.4F;
                this.size = 3;
                this.defense = 2;
                this.magicresist = 3;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "strength,6";
                break;
            case 180:
                this.type = 6;
                this.name = "Mail Hosen";
                this.picstring = "mail_hosen.gif";
                this.dpicstring = "dmail.gif";
                this.weight = 1.6F;
                this.size = 3;
                this.defense = 3;
                this.magicresist = 3;
                break;
            case 181:
                this.type = 6;
                this.name = "Foot Plate";
                this.picstring = "foot_plate.gif";
                this.dpicstring = "dgreave.gif";
                this.weight = 2.8F;
                this.size = 3;
                this.defense = 4;
                this.magicresist = 3;
                break;
            case 182:
                this.type = 6;
                this.name = "Mithril Hosen";
                this.picstring = "mithril_hosen.gif";
                this.dpicstring = "dmail.gif";
                this.weight = 0.9F;
                this.size = 3;
                this.defense = 5;
                this.magicresist = 5;
                break;
            case 183:
                this.type = 6;
                this.name = "Lyte Greave";
                this.picstring = "lyte_greave.gif";
                this.dpicstring = "dgreave.gif";
                this.weight = 2.4F;
                this.size = 3;
                this.defense = 6;
                this.magicresist = 6;
                break;
            case 184:
                this.type = 6;
                this.name = "Sar Greave";
                this.picstring = "sar_greave.gif";
                this.dpicstring = "dgreave.gif";
                this.weight = 3.1F;
                this.size = 3;
                this.defense = 7;
                this.magicresist = 7;
                break;
            case 185:
                this.type = 6;
                this.name = "Dragon Greave";
                this.picstring = "dragon_greave.gif";
                this.dpicstring = "dgreave.gif";
                this.weight = 3.1F;
                this.size = 3;
                this.defense = 8;
                this.magicresist = 7;
                break;
            case 186:
                this.type = 6;
                this.name = "Ra Greave";
                this.picstring = "ra_greave.gif";
                this.dpicstring = "dgreave.gif";
                this.weight = 2.8F;
                this.size = 3;
                this.defense = 7;
                this.magicresist = 8;
                break;
            case 187:
                this.type = 6;
                this.name = "Boots of Speed";
                this.picstring = "boots_speed.gif";
                this.dpicstring = "dboots_speed.gif";
                this.weight = 0.3F;
                this.size = 3;
                this.defense = 3;
                this.magicresist = 4;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "dexterity,4";
                break;
            case 200:
                this.type = 0;
                this.name = "Falchion";
                this.picstring = "falchion.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.3F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "Chop";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 4;
                this.power[2] = 10;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 16;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 3;
                break;
            case 201:
                this.type = 0;
                this.name = "Sword";
                this.picstring = "sword.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.2F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[3][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "Chop";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 4;
                this.power[2] = 10;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 15;
                this.speed[2] = 15;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 3;
                break;
            case 202:
                this.type = 0;
                this.name = "Sabre";
                this.picstring = "sabre.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.5F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Slash";
                this.function[0][1] = "n";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "Melee";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 10;
                this.power[1] = 4;
                this.power[2] = 13;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 14;
                this.speed[2] = 18;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 4;
                break;
            case 203:
                this.type = 0;
                this.name = "Samurai Sword";
                this.picstring = "samurai_sword.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.6F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Slash";
                this.function[0][1] = "n";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "Melee";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 12;
                this.power[1] = 6;
                this.power[2] = 14;
                this.speed = new int[3];
                this.speed[0] = 10;
                this.speed[1] = 15;
                this.speed[2] = 17;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 4;
                break;
            case 204:
                this.type = 0;
                this.name = "Rapier";
                this.picstring = "rapier.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 2.6F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Jab";
                this.function[0][1] = "f";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "Thrust";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 5;
                this.power[1] = 4;
                this.power[2] = 15;
                this.speed = new int[3];
                this.speed[0] = 6;
                this.speed[1] = 14;
                this.speed[2] = 18;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 5;
                break;
            case 205:
                this.type = 0;
                this.name = "Delta";
                this.picstring = "delta.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.3F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Chop";
                this.function[0][1] = "f";
                this.function[1][0] = "Melee";
                this.function[1][1] = "f";
                this.function[2][0] = "Thrust";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 10;
                this.power[1] = 14;
                this.power[2] = 16;
                this.speed = new int[3];
                this.speed[0] = 15;
                this.speed[1] = 17;
                this.speed[2] = 19;
                this.level = new int[3];
                this.level[0] = 3;
                this.level[1] = 4;
                this.level[2] = 6;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,3";
                break;
            case 206:
                this.type = 0;
                this.name = "Diamond Edge";
                this.picstring = "diamond_edge.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.7F;
                this.size = 2;
                this.throwpow = 15;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Stab";
                this.function[0][1] = "f";
                this.function[1][0] = "Chop";
                this.function[1][1] = "f";
                this.function[2][0] = "Cleave";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 11;
                this.power[2] = 17;
                this.speed = new int[3];
                this.speed[0] = 7;
                this.speed[1] = 14;
                this.speed[2] = 18;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 4;
                this.level[2] = 7;
                break;
            case 207:
                this.type = 0;
                this.name = "The Inquisitor";
                this.picstring = "inquisitor.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.9F;
                this.size = 2;
                this.throwpow = 20;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Thrust";
                this.function[1][1] = "f";
                this.function[2][0] = "Berzerk";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 15;
                this.power[2] = 21;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 16;
                this.speed[2] = 24;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 5;
                this.level[2] = 8;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "mana,10";
                this.effect[1] = "health,25";
                break;
            case 208:
                this.type = 0;
                this.name = "Dragon Fang";
                this.picstring = "dragon_fang.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.4F;
                this.size = 2;
                this.throwpow = 25;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Cleave";
                this.function[1][1] = "f";
                this.function[2][0] = "Berzerk";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 17;
                this.power[2] = 20;
                this.speed = new int[3];
                this.speed[0] = 14;
                this.speed[1] = 18;
                this.speed[2] = 25;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 6;
                this.level[2] = 9;
                this.poisonous = 6;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,3";
                break;
            case 209:
                this.type = 0;
                this.name = "Vorpal Blade";
                this.picstring = "vorpal_blade.gif";
                this.dpicstring = "dvorpal_blade.gif";
                this.throwpicstring = "vorpal_blade";
                this.weight = 3.9F;
                this.size = 2;
                this.throwpow = 15;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Jab";
                this.function[0][1] = "f";
                this.function[1][0] = "Cleave";
                this.function[1][1] = "f";
                this.function[2][0] = "Disrupt";
                this.function[2][1] = "w";
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 17;
                this.power[2] = 20;
                this.speed = new int[3];
                this.speed[0] = 7;
                this.speed[1] = 18;
                this.speed[2] = 24;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 5;
                this.level[2] = 6;
                this.hitsImmaterial = true;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,6";
                break;
            case 210:
                this.type = 0;
                this.name = "Ven Blade";
                this.picstring = "ven_blade.gif";
                this.dpicstring = "dsword.gif";
                this.equipstring = "ven_blade-anim.gif";
                this.usedupstring = "ven_blade-dead.gif";
                this.throwpicstring = "sword";
                this.weight = 3.9F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Jab";
                this.function[0][1] = "f";
                this.function[1][0] = "Thrust";
                this.function[1][1] = "f";
                this.function[2][0] = "Venom";
                this.function[2][1] = "w";
                this.charges[2] = 20;
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 15;
                this.power[2] = 3;
                this.speed = new int[3];
                this.speed[0] = 8;
                this.speed[1] = 18;
                this.speed[2] = 24;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 5;
                this.level[2] = 1;
                this.poisonous = 6;
                break;
            case 211:
                this.type = 0;
                this.name = "Bolt Blade";
                this.picstring = "bolt_blade.gif";
                this.dpicstring = "dsword.gif";
                this.equipstring = "bolt_blade-anim.gif";
                this.usedupstring = "bolt_blade-dead.gif";
                this.throwpicstring = "sword";
                this.weight = 3.0F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Jab";
                this.function[0][1] = "f";
                this.function[1][0] = "Chop";
                this.function[1][1] = "f";
                this.function[2][0] = "Bolt";
                this.function[2][1] = "w";
                this.charges[2] = 20;
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 10;
                this.power[2] = 4;
                this.speed = new int[3];
                this.speed[0] = 8;
                this.speed[1] = 14;
                this.speed[2] = 22;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 3;
                this.level[2] = 1;
                break;
            case 212:
                this.type = 0;
                this.name = "Fury";
                this.picstring = "fury.gif";
                this.dpicstring = "dsword.gif";
                this.equipstring = "fury-anim.gif";
                this.usedupstring = "fury-dead.gif";
                this.throwpicstring = "sword";
                this.weight = 4.7F;
                this.size = 2;
                this.throwpow = 10;
                this.functions = 3;
                this.function = new String[3][2];
                this.function[0][0] = "Chop";
                this.function[0][1] = "f";
                this.function[1][0] = "Melee";
                this.function[1][1] = "f";
                this.function[2][0] = "Fireball";
                this.function[2][1] = "w";
                this.charges[2] = 20;
                this.power = new int[3];
                this.power[0] = 10;
                this.power[1] = 14;
                this.power[2] = 5;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 17;
                this.speed[2] = 24;
                this.level = new int[3];
                this.level[0] = 3;
                this.level[1] = 4;
                this.level[2] = 1;
                break;
            case 213:
                this.type = 0;
                this.name = "Ra Blade";
                this.picstring = "ra_blade.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.7F;
                this.size = 2;
                this.throwpow = 12;
                this.functions = 3;
                this.function = new String[3][2];
                this.function[0][0] = "Thrust";
                this.function[0][1] = "f";
                this.function[1][0] = "Bolt";
                this.function[1][1] = "w";
                this.function[2][0] = "Fireball";
                this.function[2][1] = "w";
                this.charges[1] = 20;
                this.charges[2] = 20;
                this.power = new int[3];
                this.power[0] = 16;
                this.power[1] = 4;
                this.power[2] = 4;
                this.speed = new int[3];
                this.speed[0] = 16;
                this.speed[1] = 22;
                this.speed[2] = 22;
                this.level = new int[3];
                this.level[0] = 4;
                this.level[1] = 1;
                this.level[2] = 1;
                break;
            case 214:
                this.type = 0;
                this.name = "Sar Sword";
                this.picstring = "sar_sword.gif";
                this.dpicstring = "dsar_sword.gif";
                this.throwpicstring = "sar_sword";
                this.weight = 5.1F;
                this.size = 2;
                this.throwpow = 20;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Thrust";
                this.function[1][1] = "f";
                this.function[2][0] = "Berzerk";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 9;
                this.power[1] = 17;
                this.power[2] = 21;
                this.speed = new int[3];
                this.speed[0] = 15;
                this.speed[1] = 21;
                this.speed[2] = 26;
                this.level = new int[3];
                this.level[0] = 2;
                this.level[1] = 5;
                this.level[2] = 9;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "strength,5";
                this.defense = 5;
                break;
            case 215:
                this.type = 0;
                this.name = "Stormbringer";
                this.picstring = "stormbringer.gif";
                this.dpicstring = "dstormbringer.gif";
                this.throwpicstring = "stormbringer";
                this.weight = 5.9F;
                this.size = 2;
                this.throwpow = 30;
                this.functions = 3;
                this.function = new String[3][2];
                this.function[0][0] = "Thrust";
                this.function[0][1] = "f";
                this.function[1][0] = "Cleave";
                this.function[1][1] = "f";
                this.function[2][0] = "Berzerk";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 18;
                this.power[1] = 21;
                this.power[2] = 25;
                this.speed = new int[3];
                this.speed[0] = 14;
                this.speed[1] = 19;
                this.speed[2] = 24;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 0;
                this.hitsImmaterial = true;
                this.haseffect = true;
                this.effect = new String[7];
                this.effect[0] = "health,50";
                this.effect[1] = "stamina,50";
                this.effect[2] = "mana,50";
                this.effect[3] = "strength,10";
                this.effect[4] = "intelligence,10";
                this.effect[5] = "flevel,1";
                this.effect[6] = "wlevel,1";
                this.defense = 12;
                this.magicresist = 12;
                break;
            case 216:
                this.type = 0;
                this.name = "Dragon Tounge";
                this.picstring = "dragon_tounge.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.3F;
                this.size = 2;
                this.throwpow = 12;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "Melee";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 7;
                this.power[1] = 7;
                this.power[2] = 14;
                this.speed = new int[3];
                this.speed[0] = 9;
                this.speed[1] = 14;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 4;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "vitality,2";
                break;
            case 217:
                this.type = 0;
                this.name = "Triashka";
                this.picstring = "triashka.gif";
                this.dpicstring = "dsword.gif";
                this.throwpicstring = "sword";
                this.weight = 3.3F;
                this.size = 2;
                this.throwpow = 12;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Chop";
                this.function[0][1] = "f";
                this.function[1][0] = "Melee";
                this.function[1][1] = "f";
                this.function[2][0] = "Thrust";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 10;
                this.power[1] = 14;
                this.power[2] = 16;
                this.speed = new int[3];
                this.speed[0] = 13;
                this.speed[1] = 15;
                this.speed[2] = 17;
                this.level = new int[3];
                this.level[0] = 3;
                this.level[1] = 4;
                this.level[2] = 5;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "health,5";
                this.effect[1] = "stamina,6";
                this.effect[2] = "mana,3";
                break;
            case 218:
                this.type = 0;
                this.name = "Darkwing Cutter";
                this.picstring = "darkwing_cutter.gif";
                this.dpicstring = "ddarkwing_cutter.gif";
                this.throwpicstring = "darkwing_cutter";
                this.weight = 3.3F;
                this.size = 2;
                this.throwpow = 12;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Chop";
                this.function[0][1] = "f";
                this.function[1][0] = "Melee";
                this.function[1][1] = "f";
                this.function[2][0] = "Disrupt";
                this.function[2][1] = "w";
                this.power = new int[3];
                this.power[0] = 10;
                this.power[1] = 14;
                this.power[2] = 18;
                this.speed = new int[3];
                this.speed[0] = 7;
                this.speed[1] = 15;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 3;
                this.level[1] = 4;
                this.level[2] = 7;
                this.hitsImmaterial = true;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,4";
                break;
            case 219:
                this.type = 0;
                this.name = "KU Sword";
                this.picstring = "ku_sword.gif";
                this.dpicstring = "ku_sword.gif";
                this.weight = 0.0F;
                this.weight = 3.2F;
                this.size = 2;
                this.functions = 1;
                this.function = new String[1][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.power = new int[1];
                this.power[0] = 6;
                this.speed = new int[1];
                this.speed[0] = 12;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 221:
                this.type = 0;
                this.name = "Axe";
                this.picstring = "axe.gif";
                this.dpicstring = "daxe.gif";
                this.throwpicstring = "axe";
                this.weight = 4.3F;
                this.size = 3;
                this.throwpow = 15;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Chop";
                this.function[1][1] = "f";
                this.function[2][0] = "Melee";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 12;
                this.power[2] = 14;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 14;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 3;
                this.level[2] = 5;
                break;
            case 222:
                this.type = 0;
                this.name = "Hardcleave";
                this.picstring = "hardcleave.gif";
                this.dpicstring = "daxe.gif";
                this.throwpicstring = "axe";
                this.weight = 5.7F;
                this.size = 3;
                this.throwpow = 20;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Chop";
                this.function[0][1] = "f";
                this.function[1][0] = "Melee";
                this.function[1][1] = "f";
                this.function[2][0] = "Cleave";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 12;
                this.power[1] = 14;
                this.power[2] = 18;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 16;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 3;
                this.level[1] = 4;
                this.level[2] = 6;
                break;
            case 223:
                this.type = 0;
                this.name = "Executioner";
                this.picstring = "executioner.gif";
                this.dpicstring = "daxe.gif";
                this.throwpicstring = "axe";
                this.weight = 6.5F;
                this.size = 3;
                this.throwpow = 25;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Chop";
                this.function[0][1] = "f";
                this.function[1][0] = "Cleave";
                this.function[1][1] = "f";
                this.function[2][0] = "Berzerk";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 14;
                this.power[1] = 17;
                this.power[2] = 21;
                this.speed = new int[3];
                this.speed[0] = 16;
                this.speed[1] = 20;
                this.speed[2] = 24;
                this.level = new int[3];
                this.level[0] = 3;
                this.level[1] = 5;
                this.level[2] = 8;
                break;
            case 226:
                this.type = 0;
                this.name = "Club";
                this.picstring = "club.gif";
                this.dpicstring = "dclub.gif";
                this.throwpicstring = "club";
                this.weight = 3.6F;
                this.size = 3;
                this.throwpow = 10;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Bash";
                this.function[0][1] = "f";
                this.function[1][0] = "Throw";
                this.function[1][1] = "n";
                this.power = new int[2];
                this.power[0] = 7;
                this.power[1] = 4;
                this.speed = new int[2];
                this.speed[0] = 12;
                this.speed[1] = 14;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                break;
            case 227:
                this.type = 0;
                this.name = "Stone Club";
                this.picstring = "stone_club.gif";
                this.dpicstring = "dstone_club.gif";
                this.throwpicstring = "stone_club";
                this.weight = 11.0F;
                this.size = 3;
                this.throwpow = 20;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Bash";
                this.function[0][1] = "f";
                this.function[1][0] = "Throw";
                this.function[1][1] = "n";
                this.power = new int[2];
                this.power[0] = 18;
                this.power[1] = 20;
                this.speed = new int[2];
                this.speed[0] = 20;
                this.speed[1] = 26;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                break;
            case 228:
                this.type = 0;
                this.name = "Mace";
                this.picstring = "mace.gif";
                this.dpicstring = "dmace.gif";
                this.weight = 3.1F;
                this.size = 3;
                this.throwpow = 12;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Bash";
                this.function[1][1] = "f";
                this.function[2][0] = "Stun";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 8;
                this.power[2] = 12;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 14;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 4;
                break;
            case 229:
                this.type = 0;
                this.name = "Mace of Order";
                this.picstring = "mace_order.gif";
                this.dpicstring = "dmace.gif";
                this.weight = 4.1F;
                this.size = 3;
                this.throwpow = 15;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Bash";
                this.function[1][1] = "f";
                this.function[2][0] = "Stun";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 10;
                this.power[2] = 14;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 14;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 5;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "strength,5";
                break;
            case 230:
                this.type = 0;
                this.name = "Morningstar";
                this.picstring = "morningstar.gif";
                this.dpicstring = "dmorningstar.gif";
                this.weight = 5.0F;
                this.size = 4;
                this.throwpow = 15;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Stun";
                this.function[1][1] = "f";
                this.function[2][0] = "Crush";
                this.function[2][1] = "f";
                this.power = new int[3];
                this.power[0] = 12;
                this.power[1] = 15;
                this.power[2] = 19;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 18;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 4;
                this.level[2] = 7;
                break;
            case 236:
                this.type = 0;
                this.name = "Wand";
                this.picstring = "wand.gif";
                this.dpicstring = "dwand.gif";
                this.weight = 0.1F;
                this.functions = 3;
                this.function = new String[3][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Shield";
                this.function[1][1] = "p";
                this.function[2][0] = "Heal";
                this.function[2][1] = "p";
                this.charges[1] = 12;
                this.charges[2] = 12;
                this.power = new int[3];
                this.power[0] = 1;
                this.power[1] = 5;
                this.power[2] = 20;
                this.speed = new int[3];
                this.speed[0] = 7;
                this.speed[1] = 16;
                this.speed[2] = 22;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,3";
                break;
            case 237:
                this.type = 0;
                this.name = "Teowand";
                this.picstring = "teowand.gif";
                this.dpicstring = "dwand.gif";
                this.weight = 0.2F;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Calm";
                this.function[0][1] = "p";
                this.function[1][0] = "Shield";
                this.function[1][1] = "p";
                this.function[2][0] = "SpellShield";
                this.function[2][1] = "p";
                this.charges[0] = 12;
                this.charges[1] = 24;
                this.charges[2] = 24;
                this.power = new int[3];
                this.power[0] = 2;
                this.power[1] = 8;
                this.power[2] = 8;
                this.speed = new int[3];
                this.speed[0] = 9;
                this.speed[1] = 18;
                this.speed[2] = 18;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,6";
                break;
            case 238:
                this.type = 0;
                this.name = "Staff";
                this.picstring = "staff.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.6F;
                this.size = 3;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Light";
                this.function[1][1] = "w";
                this.charges[1] = 80;
                this.power = new int[2];
                this.power[0] = 2;
                this.power[1] = 100;
                this.speed = new int[2];
                this.speed[0] = 8;
                this.speed[1] = 16;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,4";
                break;
            case 239:
                this.type = 0;
                this.name = "Staff of Claws";
                this.picstring = "staff_claws.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 0.9F;
                this.size = 3;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Slash";
                this.function[0][1] = "n";
                this.function[1][0] = "Frighten";
                this.function[1][1] = "p";
                this.charges[1] = 12;
                this.power = new int[2];
                this.power[0] = 10;
                this.power[1] = 2;
                this.speed = new int[2];
                this.speed[0] = 8;
                this.speed[1] = 12;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                this.poisonous = 3;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "dexterity,4";
                this.effect[1] = "mana,6";
                break;
            case 240:
                this.type = 0;
                this.name = "Yew Staff";
                this.picstring = "yew_staff.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 3.5F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Parry";
                this.function[0][1] = "n";
                this.function[1][0] = "Light";
                this.function[1][1] = "w";
                this.function[2][0] = "Dispell";
                this.function[2][1] = "w";
                this.charges[1] = 24;
                this.charges[2] = 10;
                this.power = new int[3];
                this.power[0] = 2;
                this.power[1] = 150;
                this.power[2] = 4;
                this.speed = new int[3];
                this.speed[0] = 14;
                this.speed[1] = 20;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,8";
                break;
            case 241:
                this.type = 0;
                this.name = "Staff of Manar";
                this.picstring = "staff_manar.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.9F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Dispell";
                this.function[1][1] = "w";
                this.function[2][0] = "SpellShield";
                this.function[2][1] = "p";
                this.charges[1] = 18;
                this.charges[2] = 14;
                this.power = new int[3];
                this.power[0] = 3;
                this.power[1] = 4;
                this.power[2] = 12;
                this.speed = new int[3];
                this.speed[0] = 10;
                this.speed[1] = 20;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 2;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "mana,15";
                this.effect[1] = "intelligence,5";
                break;
            case 242:
                this.type = 0;
                this.name = "Staff of Irra";
                this.picstring = "staff_irra.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.9F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Shield";
                this.function[1][1] = "p";
                this.function[2][0] = "SpellShield";
                this.function[2][1] = "p";
                this.charges[1] = 12;
                this.charges[2] = 12;
                this.power = new int[3];
                this.power[0] = 3;
                this.power[1] = 10;
                this.power[2] = 14;
                this.speed = new int[3];
                this.speed[0] = 10;
                this.speed[1] = 20;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "mana,15";
                this.effect[1] = "wisdom,5";
                break;
            case 243:
                this.type = 0;
                this.name = "Snake Staff";
                this.picstring = "snake_staff.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.1F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Heal";
                this.function[1][1] = "p";
                this.function[2][0] = "Anti-Ven";
                this.function[2][1] = "p";
                this.defense = 5;
                this.magicresist = 5;
                this.charges[1] = 12;
                this.charges[2] = 20;
                this.power = new int[3];
                this.power[0] = 4;
                this.power[1] = 30;
                this.power[2] = 13;
                this.speed = new int[3];
                this.speed[0] = 9;
                this.speed[1] = 20;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 2;
                this.level[2] = 4;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,12";
                break;
            case 244:
                this.type = 0;
                this.name = "Dragon Spit";
                this.picstring = "dragon_spit.gif";
                this.dpicstring = "ddragon_spit.gif";
                this.equipstring = "dragon_spit-on.gif";
                this.weight = 0.8F;
                this.size = 3;
                this.functions = 1;
                this.function = new String[2][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Fireball";
                this.function[1][1] = "w";
                this.charges[1] = 0;
                this.power = new int[2];
                this.power[0] = 12;
                this.power[1] = 6;
                this.speed = new int[2];
                this.speed[0] = 6;
                this.speed[1] = 6;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                this.magicresist = 8;
                break;
            case 245:
                this.type = 0;
                this.name = "Sceptre of Lyf";
                this.picstring = "sceptre_lyf.gif";
                this.dpicstring = "dsceptre.gif";
                this.weight = 1.8F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Shield";
                this.function[0][1] = "p";
                this.function[1][0] = "SpellShield";
                this.function[1][1] = "p";
                this.function[2][0] = "Heal";
                this.function[2][1] = "p";
                this.charges[0] = 20;
                this.charges[1] = 20;
                this.charges[2] = 20;
                this.power = new int[3];
                this.power[0] = 16;
                this.power[1] = 16;
                this.power[2] = 90;
                this.speed = new int[3];
                this.speed[0] = 16;
                this.speed[1] = 16;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 2;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[3];
                this.effect[0] = "mana,15";
                this.effect[1] = "health,20";
                this.effect[2] = "stamina,20";
                break;
            case 246:
                this.type = 0;
                this.name = "The Conduit";
                this.picstring = "conduit.gif";
                this.dpicstring = "dconduit.gif";
                this.weight = 3.3F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Bolt";
                this.function[1][1] = "w";
                this.function[2][0] = "Sight";
                this.function[2][1] = "p";
                this.charges[1] = 25;
                this.charges[2] = 30;
                this.power = new int[3];
                this.power[0] = 12;
                this.power[1] = 5;
                this.power[2] = 30;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 16;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 0;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,20";
                break;
            case 247:
                this.type = 0;
                this.name = "Serpent Staff";
                this.picstring = "serpent_staff.gif";
                this.dpicstring = "dserpent_staff.gif";
                this.weight = 3.3F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Venom";
                this.function[1][1] = "w";
                this.function[2][0] = "Ven Cloud";
                this.function[2][1] = "w";
                this.charges[1] = 25;
                this.charges[2] = 25;
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 5;
                this.power[2] = 5;
                this.speed = new int[3];
                this.speed[0] = 12;
                this.speed[1] = 16;
                this.speed[2] = 16;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 4;
                this.poisonous = 8;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "mana,20";
                break;
            case 248:
                this.type = 0;
                this.name = "The Firestaff";
                this.picstring = "firestaff.gif";
                this.dpicstring = "dfirestaff.gif";
                this.weight = 2.4F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Parry";
                this.function[1][1] = "n";
                this.function[2][0] = "SpellShield";
                this.function[2][1] = "p";
                this.charges[2] = -1;
                this.power = new int[3];
                this.power[0] = 6;
                this.power[1] = 5;
                this.power[2] = 20;
                this.speed = new int[3];
                this.speed[0] = 8;
                this.speed[1] = 14;
                this.speed[2] = 18;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 0;
                this.haseffect = true;
                this.effect = new String[4];
                this.effect[0] = "flevel,1";
                this.effect[1] = "nlevel,1";
                this.effect[2] = "plevel,1";
                this.effect[3] = "wlevel,1";
                break;
            case 249:
                this.type = 0;
                this.name = "The Firestaff+";
                this.picstring = "firestaff_gem.gif";
                this.dpicstring = "dfirestaff_gem.gif";
                this.weight = 3.6F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Invoke";
                this.function[0][1] = "w";
                this.function[1][0] = "Fuse";
                this.function[1][1] = "w";
                this.function[2][0] = "Fluxcage";
                this.function[2][1] = "p";
                this.charges[0] = -1;
                this.charges[1] = -1;
                this.charges[2] = -1;
                this.power = new int[3];
                this.power[0] = 0;
                this.power[1] = 0;
                this.power[2] = 0;
                this.speed = new int[3];
                this.speed[0] = 18;
                this.speed[1] = 4;
                this.speed[2] = 5;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 0;
                this.level[2] = 0;
                this.haseffect = true;
                this.effect = new String[4];
                this.effect[0] = "flevel,2";
                this.effect[1] = "nlevel,2";
                this.effect[2] = "plevel,2";
                this.effect[3] = "wlevel,2";
                break;
            case 250:
                this.type = 0;
                this.name = "Cross of Neta";
                this.picstring = "cross_neta.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.1F;
                this.size = 3;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Heal";
                this.function[1][1] = "p";
                this.function[2][0] = "Calm";
                this.function[2][1] = "p";
                this.charges[1] = 12;
                this.charges[2] = 24;
                this.power = new int[3];
                this.power[0] = 4;
                this.power[1] = 30;
                this.power[2] = 6;
                this.speed = new int[3];
                this.speed[0] = 9;
                this.speed[1] = 20;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 2;
                this.level[2] = 2;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "mana,15";
                this.effect[1] = "wisdom,8";
                break;
            case 251:
                this.type = 0;
                this.name = "Staff of Decay";
                this.picstring = "staff_decay.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.6F;
                this.size = 3;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Ruiner";
                this.function[1][1] = "w";
                this.charges[1] = 30;
                this.power = new int[2];
                this.power[0] = 3;
                this.power[1] = 6;
                this.speed = new int[2];
                this.speed[0] = 9;
                this.speed[1] = 25;
                this.level = new int[2];
                this.level[0] = 4;
                this.level[1] = 10;
                this.haseffect = true;
                this.effect = new String[4];
                this.effect[0] = "vitality,-10";
                this.effect[1] = "stamina,-10";
                this.effect[2] = "health,-10";
                this.effect[3] = "mana,30";
                break;
            case 252:
                this.type = 0;
                this.name = "Deth Staff";
                this.picstring = "deth_staff.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 2.6F;
                this.size = 3;
                this.hitsImmaterial = true;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Swing";
                this.function[0][1] = "f";
                this.function[1][0] = "Disrupt";
                this.function[1][1] = "w";
                this.function[2][0] = "Dispell";
                this.function[2][1] = "w";
                this.charges[2] = 30;
                this.power = new int[3];
                this.power[0] = 8;
                this.power[1] = 18;
                this.power[2] = 5;
                this.speed = new int[3];
                this.speed[0] = 9;
                this.speed[1] = 20;
                this.speed[2] = 20;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 6;
                this.level[2] = 1;
                this.haseffect = true;
                this.effect = new String[2];
                this.effect[0] = "intelligence,5";
                this.effect[1] = "mana,15";
                break;
            case 256:
                this.type = 0;
                this.name = "Sling";
                this.picstring = "sling.gif";
                this.dpicstring = "dstaff.gif";
                this.weight = 1.9F;
                this.size = 3;
                this.projtype = 2;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Shoot";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 9;
                this.speed = new int[1];
                this.speed[0] = 12;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 257:
                this.type = 0;
                this.name = "Bow";
                this.picstring = "bow.gif";
                this.dpicstring = "dbow.gif";
                this.weight = 1.5F;
                this.size = 4;
                this.projtype = 1;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Shoot";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 12;
                this.speed = new int[1];
                this.speed[0] = 16;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 258:
                this.type = 0;
                this.name = "Claw Bow";
                this.picstring = "claw_bow.gif";
                this.dpicstring = "dclaw_bow.gif";
                this.weight = 2.0F;
                this.size = 4;
                this.projtype = 1;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Shoot";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 17;
                this.speed = new int[1];
                this.speed[0] = 16;
                this.level = new int[1];
                this.level[0] = 4;
                break;
            case 259:
                this.type = 0;
                this.name = "Crossbow";
                this.picstring = "crossbow.gif";
                this.dpicstring = "dcrossbow.gif";
                this.weight = 2.8F;
                this.size = 4;
                this.projtype = 1;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Shoot";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 14;
                this.speed = new int[1];
                this.speed[0] = 20;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 260:
                this.type = 0;
                this.name = "Speedbow";
                this.picstring = "speedbow.gif";
                this.dpicstring = "dcrossbow.gif";
                this.weight = 3.0F;
                this.size = 4;
                this.projtype = 1;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Shoot";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 16;
                this.speed = new int[1];
                this.speed[0] = 12;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 261:
                this.type = 0;
                this.name = "ROS Bow";
                this.picstring = "ros_bow.gif";
                this.dpicstring = "ros_bow.gif";
                this.weight = 0.0F;
                this.size = 4;
                this.projtype = 1;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Magic Shot";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 0;
                this.speed = new int[1];
                this.speed[0] = 14;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 267:
                this.type = 0;
                this.name = "Dagger";
                this.picstring = "dagger.gif";
                this.dpicstring = "ddagger.gif";
                this.throwpicstring = "dagger";
                this.weight = 0.5F;
                this.projtype = 5;
                this.throwpow = 15;
                this.functions = 3;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Slash";
                this.function[0][1] = "n";
                this.function[1][0] = "Stab";
                this.function[1][1] = "f";
                this.function[2][0] = "Throw";
                this.function[2][1] = "n";
                this.power = new int[3];
                this.power[0] = 5;
                this.power[1] = 6;
                this.power[2] = 3;
                this.speed = new int[3];
                this.speed[0] = 9;
                this.speed[1] = 10;
                this.speed[2] = 10;
                this.level = new int[3];
                this.level[0] = 0;
                this.level[1] = 1;
                this.level[2] = 0;
                break;
            case 268:
                this.type = 0;
                this.name = "Poison Dart";
                this.picstring = "dart.gif";
                this.dpicstring = "ddart.gif";
                this.throwpicstring = "dart";
                this.weight = 0.3F;
                this.projtype = 3;
                this.throwpow = 12;
                this.poisonous = 3;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Throw";
                this.function[0][1] = "n";
                this.function[1][0] = "Stab";
                this.function[1][1] = "f";
                this.power = new int[2];
                this.power[0] = 2;
                this.power[1] = 3;
                this.speed = new int[2];
                this.speed[0] = 8;
                this.speed[1] = 8;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 1;
                break;
            case 269:
                this.type = 0;
                this.name = "Throwing Star";
                this.picstring = "throwing_star.gif";
                this.dpicstring = "dthrowing_star.gif";
                this.throwpicstring = "throwing_star";
                this.weight = 0.1F;
                this.projtype = 4;
                this.throwpow = 20;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Throw";
                this.function[0][1] = "n";
                this.power = new int[1];
                this.power[0] = 6;
                this.speed = new int[1];
                this.speed[0] = 6;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 270:
                this.type = 0;
                this.name = "Arrow";
                this.picstring = "arrow.gif";
                this.dpicstring = "darrow.gif";
                this.throwpicstring = "arrow";
                this.weight = 0.2F;
                this.throwpow = 12;
                this.projtype = 1;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Stab";
                this.function[0][1] = "f";
                this.function[1][0] = "Throw";
                this.function[1][1] = "n";
                this.power = new int[2];
                this.power[0] = 2;
                this.power[1] = 2;
                this.speed = new int[2];
                this.speed[0] = 6;
                this.speed[1] = 12;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                break;
            case 271:
                this.type = 0;
                this.name = "Slayer Arrow";
                this.picstring = "slayer.gif";
                this.dpicstring = "dslayer.gif";
                this.throwpicstring = "slayer";
                this.weight = 0.2F;
                this.throwpow = 17;
                this.projtype = 1;
                this.poisonous = 7;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Stab";
                this.function[0][1] = "f";
                this.function[1][0] = "Throw";
                this.function[1][1] = "n";
                this.power = new int[2];
                this.power[0] = 2;
                this.power[1] = 3;
                this.speed = new int[2];
                this.speed[0] = 6;
                this.speed[1] = 12;
                this.level = new int[2];
                this.level[0] = 1;
                this.level[1] = 0;
                break;
            case 276:
                this.type = 0;
                this.name = "Blue Box";
                this.picstring = "magic_box_blue.gif";
                this.dpicstring = "dbox_blue.gif";
                this.weight = 0.6F;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Freeze";
                this.function[0][1] = "p";
                this.charges[0] = 1;
                this.power = new int[1];
                this.power[0] = 125;
                this.speed = new int[1];
                this.speed[0] = 14;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 277:
                this.type = 0;
                this.name = "Green Box";
                this.picstring = "magic_box_green.gif";
                this.dpicstring = "dbox_green.gif";
                this.weight = 0.9F;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Freeze";
                this.function[0][1] = "p";
                this.charges[0] = 1;
                this.power = new int[1];
                this.power[0] = 225;
                this.speed = new int[1];
                this.speed[0] = 18;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 278:
                this.type = 0;
                this.name = "Eye of Time";
                this.picstring = "eye_time.gif";
                this.dpicstring = "dring.gif";
                this.usedupstring = "eye_time-dead.gif";
                this.weight = 0.1F;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Punch";
                this.function[0][1] = "f";
                this.function[1][0] = "Freeze Life";
                this.function[1][1] = "p";
                this.charges[1] = 10;
                this.power = new int[2];
                this.power[0] = 1;
                this.power[1] = 175;
                this.speed = new int[2];
                this.speed[0] = 5;
                this.speed[1] = 24;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                break;
            case 279:
                this.type = 0;
                this.name = "Horn of Fear";
                this.picstring = "horn_fear.gif";
                this.dpicstring = "dhorn.gif";
                this.weight = 0.8F;
                this.size = 1;
                this.functions = 1;
                this.function = new String[1][2];
                this.function[0][0] = "Blow Horn";
                this.function[0][1] = "p";
                this.charges[0] = -1;
                this.power = new int[1];
                this.power[0] = 6;
                this.speed = new int[1];
                this.speed[0] = 12;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 280:
                this.type = 0;
                this.name = "Storm Ring";
                this.picstring = "storm_ring.gif";
                this.dpicstring = "dring.gif";
                this.usedupstring = "storm_ring-dead.gif";
                this.weight = 0.1F;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Punch";
                this.function[0][1] = "f";
                this.function[1][0] = "Bolt";
                this.function[1][1] = "w";
                this.charges[1] = 12;
                this.power = new int[2];
                this.power[0] = 1;
                this.power[1] = 5;
                this.speed = new int[2];
                this.speed[0] = 5;
                this.speed[1] = 20;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 0;
                break;
            case 281:
                this.type = 0;
                this.name = "Flamitt";
                this.picstring = "flamitt.gif";
                this.dpicstring = "dflamitt.gif";
                this.equipstring = "flamitt-on.gif";
                this.usedupstring = "flamitt-dead.gif";
                this.weight = 1.2F;
                this.size = 1;
                this.functions = 2;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Punch";
                this.function[0][1] = "f";
                this.function[1][0] = "Fireball";
                this.function[1][1] = "w";
                this.charges[1] = 12;
                this.power = new int[2];
                this.power[0] = 3;
                this.power[1] = 5;
                this.speed = new int[2];
                this.speed[0] = 5;
                this.speed[1] = 16;
                this.level = new int[2];
                this.level[0] = 0;
                this.level[1] = 1;
                break;
            case 282:
                this.type = 1;
                this.name = "Fire Rune";
                this.picstring = "fire_rune.gif";
                this.dpicstring = "dfire_rune.gif";
                this.weight = 0.1F;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "strength,4";
                break;
            case 283:
                this.type = 1;
                this.name = "Water Rune";
                this.picstring = "water_rune.gif";
                this.dpicstring = "dwater_rune.gif";
                this.weight = 0.1F;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "vitality,4";
                break;
            case 284:
                this.type = 1;
                this.name = "Earth Rune";
                this.picstring = "earth_rune.gif";
                this.dpicstring = "dearth_rune.gif";
                this.weight = 0.1F;
                this.defense = 4;
                break;
            case 285:
                this.type = 1;
                this.name = "Wind Rune";
                this.picstring = "wind_rune.gif";
                this.dpicstring = "dwind_rune.gif";
                this.weight = 0.1F;
                this.haseffect = true;
                this.effect = new String[1];
                this.effect[0] = "dexterity,4";
                break;
            case 286:
                this.type = 0;
                this.name = "Blue Box";
                this.picstring = "magic_box_blue.gif";
                this.dpicstring = "dbox_blue.gif";
                this.weight = 0.6F;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Freeze Life";
                this.function[0][1] = "p";
                this.charges[0] = 1;
                this.power = new int[1];
                this.power[0] = 100;
                this.speed = new int[1];
                this.speed[0] = 14;
                this.level = new int[1];
                this.level[0] = 0;
                break;
            case 287:
                this.type = 0;
                this.name = "Green Box";
                this.picstring = "magic_box_green.gif";
                this.dpicstring = "dbox_green.gif";
                this.weight = 0.9F;
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Freeze Life";
                this.function[0][1] = "p";
                this.charges[0] = 1;
                this.power = new int[1];
                this.power[0] = 200;
                this.speed = new int[1];
                this.speed[0] = 18;
                this.level = new int[1];
                this.level[0] = 0;
        }

        this.pic = (Image)pics.get(this.picstring);
        if (this.pic == null) {
            this.pic = tk.createImage("Items" + File.separator + this.picstring);
            pics.put(this.picstring, this.pic);
            ImageTracker.addImage(this.pic, 4);
        }

        this.dpic = (Image)pics.get(this.dpicstring);
        if (this.dpic == null) {
            this.dpic = tk.createImage("Items" + File.separator + this.dpicstring);
            pics.put(this.dpicstring, this.dpic);
            ImageTracker.addImage(this.dpic, 4);
        }

        if (!this.equipstring.equals("")) {
            this.epic = (Image)pics.get(this.equipstring);
            if (this.epic == null) {
                this.epic = tk.createImage("Items" + File.separator + this.equipstring);
                pics.put(this.equipstring, this.epic);
                ImageTracker.addImage(this.epic, 4);
            }
        }

        if (!this.usedupstring.equals("")) {
            this.upic = (Image)pics.get(this.usedupstring);
            if (this.upic == null) {
                this.upic = tk.createImage("Items" + File.separator + this.usedupstring);
                pics.put(this.usedupstring, this.upic);
                ImageTracker.addImage(this.upic, 4);
            }
        }

        if (!this.throwpicstring.equals("")) {
            this.hasthrowpic = true;
            this.throwpic = new Image[4];
            this.throwpic[0] = (Image)pics.get(this.throwpicstring + "-away.gif");
            if (this.throwpic[0] == null) {
                this.throwpic[0] = tk.createImage("Items" + File.separator + this.throwpicstring + "-away.gif");
                ImageTracker.addImage(this.throwpic[0], 4);
            }

            this.throwpic[1] = (Image)pics.get(this.throwpicstring + "-toward.gif");
            if (this.throwpic[1] == null) {
                this.throwpic[1] = tk.createImage("Items" + File.separator + this.throwpicstring + "-toward.gif");
                ImageTracker.addImage(this.throwpic[1], 4);
            }

            this.throwpic[2] = (Image)pics.get(this.throwpicstring + "-left.gif");
            if (this.throwpic[2] == null) {
                this.throwpic[2] = tk.createImage("Items" + File.separator + this.throwpicstring + "-left.gif");
                ImageTracker.addImage(this.throwpic[2], 4);
            }

            this.throwpic[3] = (Image)pics.get(this.throwpicstring + "-right.gif");
            if (this.throwpic[3] == null) {
                this.throwpic[3] = tk.createImage("Items" + File.separator + this.throwpicstring + "-right.gif");
                ImageTracker.addImage(this.throwpic[3], 4);
            }
        }

    }

    public Item(int var1, int var2, int var3) {
        this.number = var1;
        this.potionpow = var2;
        this.potioncastpow = var3;
        this.ispotion = true;
        this.type = 0;
        this.throwpow = 1;
        this.weight = 0.3F;
        this.size = 1;
        this.xoffset = randGen.nextInt() % 5;
        this.yoffset = randGen.nextInt() % 5;
        this.functions = 1;
        this.function = new String[this.functions][2];
        this.function[0][0] = "Drink";
        this.function[0][1] = "-";
        this.speed = new int[this.functions];
        this.speed[0] = 5;
        this.level = new int[this.functions];
        this.level[0] = 0;
        this.power = new int[this.functions];
        this.power[0] = 0;
        switch(this.number) {
            case 10:
                this.name = "Health Potion";
                this.picstring = "potion_health.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 11:
                this.name = "Stamina Potion";
                this.picstring = "potion_stamina.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 12:
                this.name = "Mana Potion";
                this.picstring = "potion_mana.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 13:
                this.name = "Strength Potion";
                this.picstring = "potion_str.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 14:
                this.name = "Dexterity Potion";
                this.picstring = "potion_dex.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 15:
                this.name = "Vitality Potion";
                this.picstring = "potion_vit.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 16:
                this.name = "Intelligence Potion";
                this.picstring = "potion_int.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 17:
                this.name = "Wisdom Potion";
                this.picstring = "potion_wis.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 18:
                this.name = "Shield Potion";
                this.picstring = "potion_shield.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 19:
                this.name = "Resistance Potion";
                this.picstring = "potion_resist.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 20:
                this.name = "Anti-Venom";
                this.picstring = "potion_ven.gif";
                this.dpicstring = "dwater_flask.gif";
                break;
            case 21:
                this.name = "Ful Bomb";
                this.type = 0;
                this.picstring = "ful_bomb.gif";
                this.dpicstring = "dful_bomb.gif";
                this.isbomb = true;
                this.bombnum = this.potioncastpow + "46";
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Throw";
                this.function[0][1] = "n";
                this.speed = new int[this.functions];
                this.speed[0] = 12;
                this.level = new int[this.functions];
                this.level[0] = 0;
                break;
            case 22:
                this.name = "Ven Bomb";
                this.type = 0;
                this.picstring = "ven_bomb.gif";
                this.dpicstring = "dven_bomb.gif";
                this.isbomb = true;
                this.bombnum = this.potioncastpow + "61";
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Throw";
                this.function[0][1] = "n";
                this.speed = new int[this.functions];
                this.speed[0] = 12;
                this.level = new int[this.functions];
                this.level[0] = 0;
                break;
            case 23:
                this.name = "Bolt Bomb";
                this.type = 0;
                this.picstring = "bolt_bomb.gif";
                this.dpicstring = "dbolt_bomb.gif";
                this.isbomb = true;
                this.bombnum = this.potioncastpow + "365";
                this.functions = 1;
                this.function = new String[this.functions][2];
                this.function[0][0] = "Throw";
                this.function[0][1] = "n";
                this.speed = new int[this.functions];
                this.speed[0] = 12;
                this.level = new int[this.functions];
                this.level[0] = 0;
                break;
            case 24:
                this.name = "Anti-Silence";
                this.picstring = "potion_silence.gif";
                this.dpicstring = "dwater_flask.gif";
        }

        this.pic = (Image)pics.get(this.picstring);
        if (this.pic == null) {
            this.pic = tk.createImage("Items" + File.separator + this.picstring);
            pics.put(this.picstring, this.pic);
            ImageTracker.addImage(this.pic, 4);
        }

        this.dpic = (Image)pics.get(this.dpicstring);
        if (this.dpic == null) {
            this.dpic = tk.createImage("Items" + File.separator + this.dpicstring);
            pics.put(this.dpicstring, this.dpic);
            ImageTracker.addImage(this.dpic, 4);
        }

    }

    public Item(String[] var1) {
        this.number = 4;
        this.xoffset = randGen.nextInt() % 5;
        this.yoffset = randGen.nextInt() % 5;
        this.type = 8;
        this.name = "Scroll";
        this.size = 1;
        this.weight = 0.5F;
        this.throwpow = 1;
        this.scroll = var1;
        this.picstring = "scroll.gif";
        this.dpicstring = "dscroll.gif";
        this.equipstring = "scroll-open.gif";
        this.pic = (Image)pics.get(this.picstring);
        if (this.pic == null) {
            this.pic = tk.createImage("Items" + File.separator + this.picstring);
            pics.put(this.picstring, this.pic);
            ImageTracker.addImage(this.pic, 4);
        }

        this.dpic = (Image)pics.get(this.dpicstring);
        if (this.dpic == null) {
            this.dpic = tk.createImage("Items" + File.separator + this.dpicstring);
            pics.put(this.dpicstring, this.dpic);
            ImageTracker.addImage(this.dpic, 4);
        }

        this.epic = (Image)pics.get(this.equipstring);
        if (this.epic == null) {
            this.epic = tk.createImage("Items" + File.separator + this.equipstring);
            pics.put(this.equipstring, this.epic);
            ImageTracker.addImage(this.epic, 4);
        }

    }

    public Item(String var1, int var2) {
        this.number = var2;
        this.xoffset = randGen.nextInt() % 5;
        this.yoffset = randGen.nextInt() % 5;
        this.type = 8;
        this.name = var1 + " Bones";
        this.weight = 5.0F;
        this.size = 4;
        this.throwpow = 1;
        this.picstring = "bones.gif";
        this.dpicstring = "dbones.gif";
        this.pic = (Image)pics.get(this.picstring);
        if (this.pic == null) {
            this.pic = tk.createImage("Items" + File.separator + this.picstring);
            pics.put(this.picstring, this.pic);
            ImageTracker.addImage(this.pic, 4);
        }

        this.dpic = (Image)pics.get(this.dpicstring);
        if (this.dpic == null) {
            this.dpic = tk.createImage("Items" + File.separator + this.dpicstring);
            pics.put(this.dpicstring, this.dpic);
            ImageTracker.addImage(this.dpic, 4);
        }

    }

    public static void doFlaskBones() {
        Image var0;
        if (!pics.containsKey("flask.gif")) {
            var0 = tk.createImage("Items" + File.separator + "flask.gif");
            pics.put("flask.gif", var0);
            ImageTracker.addImage(var0, 4);
        }

        if (!pics.containsKey("bones.gif")) {
            var0 = tk.createImage("Items" + File.separator + "bones.gif");
            pics.put("bones.gif", var0);
            ImageTracker.addImage(var0, 4);
        }

    }

    public String toString() {
        return this.name;
    }

    public boolean equals(Object var1) {
        return var1 instanceof Item && this.number == ((Item)var1).number;
    }

    public static Item createCopy(Item var0) {
        Object var1;
        int var4;
        if (var0.number == 7 || var0.number > 30 && var0.number != 73 && var0.number < 300) {
            var1 = new Item(var0.number);
        } else if (var0.number > 9 && var0.number < 31) {
            var1 = new Item(var0.number, var0.potionpow, var0.potioncastpow);
        } else if (var0.number == 4) {
            String[] var2 = new String[]{new String(var0.scroll[0]), new String(var0.scroll[1]), new String(var0.scroll[2]), new String(var0.scroll[3]), new String(var0.scroll[4])};
            var1 = new Item(var2);
        } else if (var0.number == 8) {
            var1 = new Compass();
        } else if (var0.number == 9) {
            var1 = new Torch();
            ((Torch)var1).lightboost = ((Torch)var0).lightboost;
        } else if (var0.number == 73) {
            var1 = new Waterskin(((Waterskin)var0).drinks);
        } else if (var0.number == 5) {
            var1 = new Chest();

            for(var4 = 0; var4 < 12; ++var4) {
                if (((Chest)var0).itemAt(var4) != null) {
                    ((Chest)var1).putItem(var4, createCopy(((Chest)var0).itemAt(var4)));
                }
            }
        } else {
            var1 = new Item();
            ((Item)var1).number = var0.number;
        }

        ((Item)var1).name = new String(var0.name);
        ((Item)var1).type = var0.type;
        ((Item)var1).weight = var0.weight;
        ((Item)var1).size = var0.size;
        ((Item)var1).throwpow = var0.throwpow;
        ((Item)var1).picstring = new String(var0.picstring);
        ((Item)var1).pic = var0.pic;
        ((Item)var1).dpicstring = new String(var0.dpicstring);
        ((Item)var1).dpic = var0.dpic;
        if (!var0.equipstring.equals("")) {
            ((Item)var1).equipstring = new String(var0.equipstring);
            ((Item)var1).epic = var0.epic;
        } else {
            ((Item)var1).equipstring = "";
            ((Item)var1).epic = null;
        }

        if (!var0.usedupstring.equals("")) {
            ((Item)var1).usedupstring = new String(var0.usedupstring);
            ((Item)var1).upic = var0.upic;
        } else {
            ((Item)var1).usedupstring = "";
            ((Item)var1).upic = null;
        }

        ((Item)var1).hasthrowpic = var0.hasthrowpic;
        if (((Item)var1).hasthrowpic) {
            ((Item)var1).throwpicstring = new String(var0.throwpicstring);
            ((Item)var1).throwpic = new Image[4];
            ((Item)var1).throwpic[0] = var0.throwpic[0];
            ((Item)var1).throwpic[1] = var0.throwpic[1];
            ((Item)var1).throwpic[2] = var0.throwpic[2];
            ((Item)var1).throwpic[3] = var0.throwpic[3];
        }

        ((Item)var1).defense = var0.defense;
        ((Item)var1).magicresist = var0.magicresist;
        ((Item)var1).haseffect = var0.haseffect;
        if (((Item)var1).haseffect) {
            var4 = var0.effect.length;
            ((Item)var1).effect = new String[var4];

            for(int var3 = 0; var3 < var4; ++var3) {
                ((Item)var1).effect[var3] = new String(var0.effect[var3]);
            }
        }

        if (((Item)var1).type == 7) {
            ((Item)var1).foodvalue = var0.foodvalue;
        } else if (((Item)var1).type == 0) {
            ((Item)var1).hitsImmaterial = var0.hitsImmaterial;
            ((Item)var1).poisonous = var0.poisonous;
            ((Item)var1).projtype = var0.projtype;
            ((Item)var1).functions = var0.functions;
            ((Item)var1).function = new String[((Item)var1).functions][2];
            ((Item)var1).level = new int[((Item)var1).functions];
            ((Item)var1).power = new int[((Item)var1).functions];
            ((Item)var1).speed = new int[((Item)var1).functions];

            for(var4 = 0; var4 < ((Item)var1).functions; ++var4) {
                ((Item)var1).function[var4][0] = new String(var0.function[var4][0]);
                ((Item)var1).function[var4][1] = new String(var0.function[var4][1]);
                ((Item)var1).level[var4] = var0.level[var4];
                ((Item)var1).power[var4] = var0.power[var4];
                ((Item)var1).speed[var4] = var0.speed[var4];
                ((Item)var1).charges[var4] = var0.charges[var4];
            }
        }

        return (Item)var1;
    }

    public void equipEffect(Hero var1) {
        if (this.epic != null) {
            this.temppic = this.pic;
            this.pic = this.epic;
        }

        var1.defense += this.defense;
        var1.magicresist += this.magicresist;
        if (this.haseffect) {
            boolean var3 = false;

            for(int var4 = this.effect.length; var4 > 0; --var4) {
                String var2 = this.effect[var4 - 1].substring(0, this.effect[var4 - 1].indexOf(44));
                int var5 = Integer.parseInt(this.effect[var4 - 1].substring(this.effect[var4 - 1].indexOf(44) + 1));
                var2.toLowerCase();
                if (var2.equals("mana")) {
                    var1.maxmana += var5;
                } else if (var2.equals("health")) {
                    var1.maxhealth += var5;
                } else if (var2.equals("stamina")) {
                    var1.maxstamina += var5;
                } else if (var2.equals("strength")) {
                    var1.strength += var5;
                } else if (var2.equals("vitality")) {
                    var1.vitality += var5;
                } else if (var2.equals("dexterity")) {
                    var1.dexterity += var5;
                } else if (var2.equals("intelligence")) {
                    var1.intelligence += var5;
                } else if (var2.equals("wisdom")) {
                    var1.wisdom += var5;
                } else if (var2.equals("flevel")) {
                    if (var1.flevel + var5 < 15) {
                        var1.flevel += var5;
                        this.fleveladded = var5;
                    } else {
                        this.fleveladded = 15 - var1.flevel;
                        var1.flevel = 15;
                    }
                } else if (var2.equals("nlevel")) {
                    if (var1.nlevel + var5 < 15) {
                        var1.nlevel += var5;
                        this.nleveladded = var5;
                    } else {
                        this.nleveladded = 15 - var1.nlevel;
                        var1.nlevel = 15;
                    }
                } else if (var2.equals("wlevel")) {
                    if (var1.wlevel + var5 < 15) {
                        var1.wlevel += var5;
                        this.wleveladded = var5;
                    } else {
                        this.wleveladded = 15 - var1.wlevel;
                        var1.wlevel = 15;
                    }
                } else if (var2.equals("plevel")) {
                    if (var1.plevel + var5 < 15) {
                        var1.plevel += var5;
                        this.pleveladded = var5;
                    } else {
                        this.pleveladded = 15 - var1.plevel;
                        var1.plevel = 15;
                    }
                }
            }

            var1.setMaxLoad();
        }
    }

    public void unEquipEffect(Hero var1) {
        if (this.epic != null) {
            this.pic = this.temppic;
        }

        var1.defense -= this.defense;
        var1.magicresist -= this.magicresist;
        if (this.haseffect) {
            boolean var3 = false;

            for(int var4 = this.effect.length; var4 > 0; --var4) {
                String var2 = this.effect[var4 - 1].substring(0, this.effect[var4 - 1].indexOf(44));
                int var5 = Integer.parseInt(this.effect[var4 - 1].substring(this.effect[var4 - 1].indexOf(44) + 1));
                var2.toLowerCase();
                if (var2.equals("mana")) {
                    var1.maxmana -= var5;
                    if (var1.mana > var1.maxmana) {
                        var1.mana = var1.maxmana;
                    }
                } else if (var2.equals("health")) {
                    var1.maxhealth -= var5;
                    if (var1.health > var1.maxhealth) {
                        var1.health = var1.maxhealth;
                    }
                } else if (var2.equals("stamina")) {
                    var1.maxstamina -= var5;
                    if (var1.stamina > var1.maxstamina) {
                        var1.stamina = var1.maxstamina;
                    }
                } else if (var2.equals("strength")) {
                    var1.strength -= var5;
                } else if (var2.equals("vitality")) {
                    var1.vitality -= var5;
                } else if (var2.equals("dexterity")) {
                    var1.dexterity -= var5;
                } else if (var2.equals("intelligence")) {
                    var1.intelligence -= var5;
                } else if (var2.equals("wisdom")) {
                    var1.wisdom -= var5;
                } else if (var2.equals("flevel")) {
                    var1.flevel -= this.fleveladded;
                } else if (var2.equals("nlevel")) {
                    var1.nlevel -= this.nleveladded;
                } else if (var2.equals("wlevel")) {
                    var1.wlevel -= this.wleveladded;
                } else if (var2.equals("plevel")) {
                    var1.plevel -= this.pleveladded;
                }
            }

            var1.setMaxLoad();
        }
    }

    private void writeObject(ObjectOutputStream var1) throws IOException {
        try {
            var1.writeInt(this.type);
            var1.writeInt(this.size);
            var1.writeFloat(this.weight);
            var1.writeInt(this.number);
            var1.writeUTF(this.name);
            var1.writeUTF(this.picstring);
            var1.writeUTF(this.dpicstring);
            var1.writeUTF(this.equipstring);
            var1.writeUTF(this.usedupstring);
            if (this.epic != null && this.pic == this.epic) {
                var1.writeInt(1);
            } else if (this.upic != null && this.pic == this.upic && this.epic == this.upic) {
                var1.writeInt(2);
            } else {
                var1.writeInt(0);
            }

            var1.writeInt(this.throwpow);
            var1.writeInt(this.shotpow);
            if (this.type == 0) {
                var1.writeInt(this.functions);
                var1.writeObject(this.function);
                var1.writeObject(this.power);
                var1.writeObject(this.speed);
                var1.writeObject(this.level);
                var1.writeObject(this.charges);
                var1.writeBoolean(this.hitsImmaterial);
                var1.writeInt(this.poisonous);
                var1.writeInt(this.projtype);
            }

            if (this.type != 8 && this.type != 7) {
                var1.writeInt(this.defense);
                var1.writeInt(this.magicresist);
            } else if (this.type == 7 || this.number == 72 || this.number == 73) {
                var1.writeInt(this.foodvalue);
            }

            var1.writeBoolean(this.ispotion);
            if (this.ispotion) {
                var1.writeInt(this.potionpow);
                var1.writeInt(this.potioncastpow);
                var1.writeBoolean(this.isbomb);
                if (this.isbomb) {
                    var1.writeUTF(this.bombnum);
                }
            }

            var1.writeBoolean(this.hasthrowpic);
            if (this.hasthrowpic) {
                var1.writeUTF(this.throwpicstring);
            }

            if (this.number == 4) {
                var1.writeObject(this.scroll);
            } else if (this.number == 83) {
                var1.writeObject(this.bound);
            }

            var1.writeBoolean(this.haseffect);
            if (this.haseffect) {
                var1.writeObject(this.effect);
                var1.writeInt(this.fleveladded);
                var1.writeInt(this.nleveladded);
                var1.writeInt(this.wleveladded);
                var1.writeInt(this.pleveladded);
            }

            var1.writeInt(this.subsquare);
        } catch (Exception var3) {
            System.out.println("Error in Item write - " + this.name);
            var3.printStackTrace();
        }

    }

    private void readObject(ObjectInputStream var1) throws IOException {
        try {
            this.type = var1.readInt();
            this.size = var1.readInt();
            this.weight = var1.readFloat();
            this.number = var1.readInt();
            this.name = var1.readUTF();
            this.picstring = var1.readUTF();
            this.dpicstring = var1.readUTF();
            this.equipstring = var1.readUTF();
            this.usedupstring = var1.readUTF();
            int var2 = var1.readInt();
            this.throwpow = var1.readInt();
            this.shotpow = var1.readInt();
            if (this.type == 0) {
                this.functions = var1.readInt();
                this.function = (String[][])var1.readObject();
                this.power = (int[])var1.readObject();
                this.speed = (int[])var1.readObject();
                this.level = (int[])var1.readObject();
                this.charges = (int[])var1.readObject();
                this.hitsImmaterial = var1.readBoolean();
                this.poisonous = var1.readInt();
                this.projtype = var1.readInt();
            }

            if (this.type != 8 && this.type != 7) {
                this.defense = var1.readInt();
                this.magicresist = var1.readInt();
            } else if (this.type == 7 || this.number == 72 || this.number == 73) {
                this.foodvalue = var1.readInt();
            }

            this.ispotion = var1.readBoolean();
            if (this.ispotion) {
                this.potionpow = var1.readInt();
                this.potioncastpow = var1.readInt();
                this.isbomb = var1.readBoolean();
                if (this.isbomb) {
                    this.bombnum = var1.readUTF();
                }
            }

            this.pic = (Image)pics.get(this.picstring);
            if (this.pic == null) {
                this.pic = tk.createImage("Items" + File.separator + this.picstring);
                pics.put(this.picstring, this.pic);
                ImageTracker.addImage(this.pic, 4);
            }

            this.dpic = (Image)pics.get(this.dpicstring);
            if (this.dpic == null) {
                this.dpic = tk.createImage("Items" + File.separator + this.dpicstring);
                pics.put(this.dpicstring, this.dpic);
                ImageTracker.addImage(this.dpic, 4);
            }

            if (!this.equipstring.equals("")) {
                this.epic = (Image)pics.get(this.equipstring);
                if (this.epic == null) {
                    this.epic = tk.createImage("Items" + File.separator + this.equipstring);
                    pics.put(this.equipstring, this.epic);
                    ImageTracker.addImage(this.epic, 4);
                }
            }

            if (!this.usedupstring.equals("")) {
                this.upic = (Image)pics.get(this.usedupstring);
                if (this.upic == null) {
                    this.upic = tk.createImage("Items" + File.separator + this.usedupstring);
                    pics.put(this.usedupstring, this.upic);
                    ImageTracker.addImage(this.upic, 4);
                }
            }

            this.hasthrowpic = var1.readBoolean();
            if (this.hasthrowpic) {
                this.throwpicstring = var1.readUTF();
                this.throwpic = new Image[4];
                this.throwpic[0] = (Image)pics.get(this.throwpicstring + "-away.gif");
                if (this.throwpic[0] == null) {
                    this.throwpic[0] = tk.createImage("Items" + File.separator + this.throwpicstring + "-away.gif");
                    ImageTracker.addImage(this.throwpic[0], 4);
                }

                this.throwpic[1] = (Image)pics.get(this.throwpicstring + "-toward.gif");
                if (this.throwpic[1] == null) {
                    this.throwpic[1] = tk.createImage("Items" + File.separator + this.throwpicstring + "-toward.gif");
                    ImageTracker.addImage(this.throwpic[1], 4);
                }

                this.throwpic[2] = (Image)pics.get(this.throwpicstring + "-left.gif");
                if (this.throwpic[2] == null) {
                    this.throwpic[2] = tk.createImage("Items" + File.separator + this.throwpicstring + "-left.gif");
                    ImageTracker.addImage(this.throwpic[2], 4);
                }

                this.throwpic[3] = (Image)pics.get(this.throwpicstring + "-right.gif");
                if (this.throwpic[3] == null) {
                    this.throwpic[3] = tk.createImage("Items" + File.separator + this.throwpicstring + "-right.gif");
                    ImageTracker.addImage(this.throwpic[3], 4);
                }
            } else {
                this.throwpicstring = "";
            }

            if (this.number == 4) {
                this.scroll = (String[])var1.readObject();
            } else if (this.number == 83) {
                this.bound = (boolean[])var1.readObject();
            }

            this.haseffect = var1.readBoolean();
            if (this.haseffect) {
                this.effect = (String[])var1.readObject();
                this.fleveladded = var1.readInt();
                this.nleveladded = var1.readInt();
                this.wleveladded = var1.readInt();
                this.pleveladded = var1.readInt();
            }

            this.subsquare = var1.readInt();
            if (this.number != 215) {
                this.xoffset = randGen.nextInt() % 5;
                this.yoffset = randGen.nextInt() % 5;
            } else {
                this.xoffset = 0;
                this.yoffset = 0;
            }

            if (this.number > maxitemnum) {
                maxitemnum = this.number;
            }

            if (var2 == 1) {
                this.temppic = this.pic;
                this.pic = this.epic;
            } else if (var2 == 2) {
                this.temppic = this.upic;
                this.epic = this.upic;
                this.pic = this.upic;
            }
        } catch (Exception var3) {
            System.out.println("Error in Item read - " + this.name);
            var3.printStackTrace();
        }

    }
}
