package com.github.theresajayne.dmjava.entities;

public class HeroSheet {
    Hero hero;
    Image offscreen;
    Graphics2D offg;
    boolean mirror = false;
    boolean stats = false;
    boolean skipchestscroll = false;

    public HeroSheet() {
        this.setSize(448, 326);
        this.setBackground(Color.black);
        this.setDoubleBuffered(true);
    }

    public HeroSheet(dmnew.Hero var2) {
        this.hero = var2;
        this.setSize(448, 326);
        this.setBackground(Color.black);
        this.setDoubleBuffered(true);
    }

    public void setHero(dmnew.Hero var1) {
        this.hero = var1;
        this.repaint();
        this.hero.repaint();
    }

    public void setHero(dmnew.Hero var1, int var2) {
        this.hero = var1;
        this.mirror = true;
        this.repaint();
        this.hero.repaint();
        this.removeMouseListener(dmnew.this.sheetclick);
        this.addMouseListener(dmnew.this.new MirrorClick());
    }

    public void paint(Graphics var1) {
        if (dmnew.this.sleeping) {
            this.sleepsheet();
            var1.drawImage(this.offscreen, 0, 0, (ImageObserver)null);
        } else if (this.stats) {
            this.showStats();
            var1.drawImage(this.offscreen, 0, 0, (ImageObserver)null);
        } else if (dmnew.this.viewing) {
            this.showItem();
            var1.drawImage(this.offscreen, 0, 0, (ImageObserver)null);
        } else {
            this.offg.setFont(new Font("TimesRoman", 1, 14));
            this.offg.drawImage(dmnew.hsheet, 0, 0, (ImageObserver)null);
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString(this.hero.name + "   " + this.hero.lastname, 8, 18);
            this.offg.drawString("Health", 18, 288);
            this.offg.drawString(this.hero.health + " / " + this.hero.maxhealth, 128, 288);
            this.offg.drawString("Stamina", 18, 306);
            this.offg.drawString(this.hero.stamina + " / " + this.hero.maxstamina, 128, 306);
            this.offg.drawString("Mana", 18, 324);
            this.offg.drawString(this.hero.mana + " / " + this.hero.maxmana, 128, 324);
            if (dmnew.leader == this.hero.heronumber) {
                this.offg.setColor(Color.yellow);
            } else {
                this.offg.setColor(Color.white);
            }

            this.offg.drawString(this.hero.name + "   " + this.hero.lastname, 5, 15);
            if (dmnew.leader == this.hero.heronumber) {
                this.offg.setColor(Color.white);
            }

            this.offg.drawString("Health", 15, 285);
            this.offg.drawString("Stamina", 15, 303);
            this.offg.drawString("Mana", 15, 321);
            if (this.hero.health < this.hero.maxhealth / 3) {
                this.offg.setColor(Color.red);
            } else {
                this.offg.setColor(Color.white);
            }

            this.offg.drawString(this.hero.health + " / " + this.hero.maxhealth, 125, 285);
            if (this.hero.stamina < this.hero.maxstamina / 3) {
                this.offg.setColor(Color.red);
            } else {
                this.offg.setColor(Color.white);
            }

            this.offg.drawString(this.hero.stamina + " / " + this.hero.maxstamina, 125, 303);
            if (this.hero.mana < this.hero.maxmana / 3) {
                this.offg.setColor(Color.red);
            } else {
                this.offg.setColor(Color.white);
            }

            this.offg.drawString(this.hero.mana + " / " + this.hero.maxmana, 125, 321);
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString("Load       " + (float)((int)(this.hero.load * 10.0F + 0.5F)) / 10.0F + " / " + (float)((int)(this.hero.maxload * 10.0F + 0.5F)) / 10.0F, 263, 324);
            this.offg.drawString("Kg", 423, 324);
            if (this.hero.load > this.hero.maxload) {
                this.offg.setColor(Color.red);
            } else if (this.hero.load > this.hero.maxload * 3.0F / 4.0F) {
                this.offg.setColor(Color.yellow);
            } else {
                this.offg.setColor(Color.white);
            }

            this.offg.drawString("Load       " + (float)((int)(this.hero.load * 10.0F + 0.5F)) / 10.0F + " / " + (float)((int)(this.hero.maxload * 10.0F + 0.5F)) / 10.0F, 260, 321);
            this.offg.drawString("Kg", 420, 321);
            if (this.hero.ispoisoned) {
                this.offg.setColor(new Color(0, 150, 0));
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(106, 22, 40, 37);
                this.offg.drawImage(dmnew.poisonedpic, 224, 209, (ImageObserver)null);
            }

            if (this.hero.silenced) {
                this.offg.setFont(new Font("TimesRoman", 1, 24));
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("SILENCED", 264, 284);
                this.offg.setColor(Color.red);
                this.offg.drawString("SILENCED", 260, 280);
                this.offg.setFont(new Font("TimesRoman", 1, 14));
            }

            if (this.hero.strengthboost < 0 || this.hero.dexterityboost < 0 || this.hero.vitalityboost < 0 || this.hero.intelligenceboost < 0 || this.hero.wisdomboost < 0 || this.hero.defenseboost < 0 || this.hero.magicresistboost < 0) {
                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(18, 22, 40, 37);
            }

            int var2;
            int var3;
            int var4;
            if (!this.mirror && (this.hero.weapon == null || this.skipchestscroll || this.hero.weapon.number != 5 && this.hero.weapon.number != 4)) {
                var2 = (int)((float)this.hero.food / 1000.0F * 170.0F);
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.fillRect(245, 150, var2, 10);
                if (this.hero.food < 75) {
                    this.offg.setColor(Color.red);
                } else if (this.hero.food < 100) {
                    this.offg.setColor(Color.yellow);
                } else {
                    this.offg.setColor(new Color(200, 130, 10));
                }

                this.offg.fillRect(240, 145, var2, 10);
                var2 = (int)((float)this.hero.water / 1000.0F * 170.0F);
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.fillRect(245, 197, var2, 10);
                if (this.hero.water < 75) {
                    this.offg.setColor(Color.red);
                } else if (this.hero.water < 100) {
                    this.offg.setColor(Color.yellow);
                } else {
                    this.offg.setColor(Color.blue);
                }

                this.offg.fillRect(240, 192, var2, 10);
            } else if (!this.mirror && this.hero.weapon != null && this.hero.weapon.number == 5) {
                this.offg.drawImage(dmnew.openchest, 156, 104, (ImageObserver)null);

                for(var2 = 0; var2 < 12; ++var2) {
                    if (((Chest)this.hero.weapon).contents[var2] != null) {
                        if (var2 < 6) {
                            this.offg.drawImage(((Chest)this.hero.weapon).contents[var2].pic, 227 + var2 * 34, 207, this);
                        } else {
                            this.offg.drawImage(((Chest)this.hero.weapon).contents[var2].pic, 227 + (var2 - 6) * 34, 239, this);
                        }
                    }
                }
            } else if (!this.mirror && this.hero.weapon != null && this.hero.weapon.number == 4) {
                this.offg.drawImage(dmnew.scrollpic, 156, 104, (ImageObserver)null);
                this.offg.setFont(new Font("Courier", 1, 16));

                for(var2 = this.hero.weapon.scroll.length; this.hero.weapon.scroll[var2 - 1].equals(""); --var2) {
                    ;
                }

                var3 = 188 - var2 * 10;
                this.offg.setColor(new Color(30, 30, 30));

                for(var4 = 0; var4 < var2; ++var4) {
                    this.offg.drawString(this.hero.weapon.scroll[var4], 320 - this.hero.weapon.scroll[var4].length() * 5, var3 + var4 * 20);
                }
            } else {
                this.offg.drawImage(dmnew.ressreincpic, 206, 104, this);
            }

            this.offg.setColor(new Color(63, 63, 63));
            this.offg.drawImage(this.hero.weapon.pic, 124, 106, this);
            if (this.hero.hurtweapon) {
                if (this.hero.weapon == dmnew.fistfoot) {
                    this.offg.drawImage(dmnew.hurtweapon, 124, 106, this);
                }

                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(123, 105, 34, 34);
                this.offg.setColor(new Color(63, 63, 63));
            }

            if (this.hero.head != null) {
                this.offg.fillRect(68, 52, 32, 32);
                this.offg.drawImage(this.hero.head.pic, 68, 52, this);
            }

            if (this.hero.hurthead) {
                if (this.hero.head == null) {
                    this.offg.drawImage(dmnew.hurthead, 68, 52, this);
                }

                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(67, 51, 34, 34);
                this.offg.setColor(new Color(63, 63, 63));
            }

            if (this.hero.neck != null) {
                this.offg.fillRect(12, 66, 32, 32);
                this.offg.drawImage(this.hero.neck.pic, 12, 66, this);
            }

            if (this.hero.torso != null) {
                this.offg.fillRect(68, 92, 32, 32);
                this.offg.drawImage(this.hero.torso.pic, 68, 92, this);
            }

            if (this.hero.hurttorso) {
                if (this.hero.torso == null) {
                    this.offg.drawImage(dmnew.hurttorso, 68, 92, this);
                }

                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(67, 91, 34, 34);
                this.offg.setColor(new Color(63, 63, 63));
            }

            if (this.hero.hand != null) {
                this.offg.fillRect(12, 106, 32, 32);
                this.offg.drawImage(this.hero.hand.pic, 12, 106, this);
            }

            if (this.hero.hurthand) {
                if (this.hero.hand == null) {
                    this.offg.drawImage(dmnew.hurthand, 12, 106, this);
                }

                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(11, 105, 34, 34);
                this.offg.setColor(new Color(63, 63, 63));
            }

            if (this.hero.legs != null) {
                this.offg.fillRect(68, 132, 32, 32);
                this.offg.drawImage(this.hero.legs.pic, 68, 132, this);
            }

            if (this.hero.hurtlegs) {
                if (this.hero.legs == null) {
                    this.offg.drawImage(dmnew.hurtlegs, 68, 132, this);
                }

                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(67, 131, 34, 34);
                this.offg.setColor(new Color(63, 63, 63));
            }

            if (this.hero.feet != null) {
                this.offg.fillRect(68, 172, 32, 32);
                this.offg.drawImage(this.hero.feet.pic, 68, 172, this);
            }

            if (this.hero.hurtfeet) {
                if (this.hero.feet == null) {
                    this.offg.drawImage(dmnew.hurtfeet, 68, 172, this);
                }

                this.offg.setColor(Color.red);
                this.offg.setStroke(new BasicStroke(2.0F));
                this.offg.drawRect(67, 171, 34, 34);
                this.offg.setColor(new Color(63, 63, 63));
            }

            if (this.hero.pouch1 != null) {
                this.offg.drawImage(this.hero.pouch1.pic, 12, 167, this);
            }

            if (this.hero.pouch2 != null) {
                this.offg.drawImage(this.hero.pouch2.pic, 12, 201, this);
            }

            for(var2 = 0; var2 < 8; ++var2) {
                if (this.hero.pack[var2] != null) {
                    this.offg.drawImage(this.hero.pack[var2].pic, 166 + 34 * var2, 32, this);
                }
            }

            for(var3 = 0; var3 < 8; ++var3) {
                if (this.hero.pack[var3 + 8] != null) {
                    this.offg.drawImage(this.hero.pack[var3 + 8].pic, 166 + 34 * var3, 66, this);
                }
            }

            for(var4 = 0; var4 < 2; ++var4) {
                if (this.hero.quiver[var4] != null) {
                    this.offg.drawImage(this.hero.quiver[var4].pic, 124 + 34 * var4, 167, this);
                }
            }

            for(int var5 = 0; var5 < 2; ++var5) {
                if (this.hero.quiver[var5 + 2] != null) {
                    this.offg.drawImage(this.hero.quiver[var5 + 2].pic, 124 + 34 * var5, 201, this);
                }
            }

            for(int var6 = 0; var6 < 2; ++var6) {
                if (this.hero.quiver[var6 + 4] != null) {
                    this.offg.drawImage(this.hero.quiver[var6 + 4].pic, 124 + 34 * var6, 235, this);
                }
            }

            var1.drawImage(this.offscreen, 0, 0, (ImageObserver)null);
        }
    }

    public void showStats() {
        this.offg.setColor(new Color(60, 60, 60));
        this.offg.fillRect(206, 104, 232, 190);
        this.offg.setColor(new Color(95, 95, 95));
        this.offg.setStroke(new BasicStroke(2.0F));
        this.offg.drawRect(207, 105, 230, 190);
        this.offg.setFont(new Font("TimesRoman", 1, 14));
        int var1 = 120;
        if (dmnew.herosheet.hero.flevel > 0) {
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.flevel - 1] + " Fighter", 218, var1 + 3);
            this.offg.setColor(Color.white);
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.flevel - 1] + " Fighter", 215, var1);
            var1 += 15;
        }

        if (dmnew.herosheet.hero.nlevel > 0) {
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.nlevel - 1] + " Ninja", 218, var1 + 3);
            this.offg.setColor(Color.white);
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.nlevel - 1] + " Ninja", 215, var1);
            var1 += 15;
        }

        if (dmnew.herosheet.hero.wlevel > 0) {
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.wlevel - 1] + " Wizard", 218, var1 + 3);
            this.offg.setColor(Color.white);
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.wlevel - 1] + " Wizard", 215, var1);
            var1 += 15;
        }

        if (dmnew.herosheet.hero.plevel > 0) {
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.plevel - 1] + " Priest", 218, var1 + 3);
            this.offg.setColor(Color.white);
            this.offg.drawString(dmnew.LEVELNAMES[dmnew.herosheet.hero.plevel - 1] + " Priest", 215, var1);
        }

        this.offg.setColor(new Color(30, 30, 30));
        this.offg.drawString("Strength", 218, 200);
        this.offg.drawString("Dexterity", 218, 214);
        this.offg.drawString("Vitality", 218, 228);
        this.offg.drawString("Intelligence", 218, 242);
        this.offg.drawString("Wisdom", 218, 256);
        this.offg.drawString("Defense", 218, 275);
        this.offg.drawString("Resist Magic", 218, 290);
        this.offg.drawString("" + dmnew.herosheet.hero.strength, 303, 200);
        this.offg.drawString("" + dmnew.herosheet.hero.dexterity, 303, 214);
        this.offg.drawString("" + dmnew.herosheet.hero.vitality, 303, 228);
        this.offg.drawString("" + dmnew.herosheet.hero.intelligence, 303, 242);
        this.offg.drawString("" + dmnew.herosheet.hero.wisdom, 303, 256);
        this.offg.drawString("" + dmnew.herosheet.hero.defense, 303, 275);
        this.offg.drawString("" + dmnew.herosheet.hero.magicresist, 303, 290);
        this.offg.setColor(Color.white);
        this.offg.drawString("Strength", 215, 197);
        this.offg.drawString("Dexterity", 215, 211);
        this.offg.drawString("Vitality", 215, 225);
        this.offg.drawString("Intelligence", 215, 239);
        this.offg.drawString("Wisdom", 215, 253);
        this.offg.drawString("Defense", 215, 272);
        this.offg.drawString("Resist Magic", 215, 287);
        if (dmnew.herosheet.hero.strengthboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.strengthboost < 0) {
            this.offg.setColor(Color.red);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.strength, 300, 197);
        if (dmnew.herosheet.hero.dexterityboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.dexterityboost < 0) {
            this.offg.setColor(Color.red);
        } else {
            this.offg.setColor(Color.white);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.dexterity, 300, 211);
        if (dmnew.herosheet.hero.vitalityboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.vitalityboost < 0) {
            this.offg.setColor(Color.red);
        } else {
            this.offg.setColor(Color.white);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.vitality, 300, 225);
        if (dmnew.herosheet.hero.intelligenceboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.intelligenceboost < 0) {
            this.offg.setColor(Color.red);
        } else {
            this.offg.setColor(Color.white);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.intelligence, 300, 239);
        if (dmnew.herosheet.hero.wisdomboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.wisdomboost < 0) {
            this.offg.setColor(Color.red);
        } else {
            this.offg.setColor(Color.white);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.wisdom, 300, 253);
        if (dmnew.herosheet.hero.defenseboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.defenseboost < 0) {
            this.offg.setColor(Color.red);
        } else {
            this.offg.setColor(Color.white);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.defense, 300, 272);
        if (dmnew.herosheet.hero.magicresistboost > 0) {
            this.offg.setColor(Color.green);
        } else if (dmnew.herosheet.hero.magicresistboost < 0) {
            this.offg.setColor(Color.red);
        } else {
            this.offg.setColor(Color.white);
        }

        this.offg.drawString("" + dmnew.herosheet.hero.magicresist, 300, 287);
    }

    public void showItem() {
        int var1;
        int var2;
        if (dmnew.inhand.number == 4) {
            this.offg.drawImage(dmnew.scrollpic, 156, 104, (ImageObserver)null);
            this.offg.setFont(new Font("Courier", 1, 16));

            for(var1 = dmnew.inhand.scroll.length; dmnew.inhand.scroll[var1 - 1].equals(""); --var1) {
                ;
            }

            var2 = 188 - var1 * 10;
            this.offg.setColor(new Color(30, 30, 30));

            for(int var3 = 0; var3 < var1; ++var3) {
                this.offg.drawString(dmnew.inhand.scroll[var3], 320 - dmnew.inhand.scroll[var3].length() * 5, var2 + var3 * 20);
            }
        } else if (dmnew.inhand.number == 5) {
            this.offg.drawImage(dmnew.openchest, 156, 104, (ImageObserver)null);

            for(var1 = 0; var1 < 12; ++var1) {
                if (((Chest)dmnew.inhand).contents[var1] != null) {
                    if (var1 < 6) {
                        this.offg.drawImage(((Chest)dmnew.inhand).contents[var1].pic, 227 + var1 * 34, 207, this);
                    } else {
                        this.offg.drawImage(((Chest)dmnew.inhand).contents[var1].pic, 227 + (var1 - 6) * 34, 239, this);
                    }
                }
            }
        } else {
            this.offg.setColor(new Color(60, 60, 60));
            this.offg.fillRect(208, 106, 228, 142);
            this.offg.setColor(new Color(95, 95, 95));
            this.offg.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            this.offg.drawOval(231, 111, 50, 50);
            this.offg.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            this.offg.drawImage(dmnew.inhand.pic, 240, 120, (ImageObserver)null);
            this.offg.setFont(new Font("TimesRoman", 1, 14));
            this.offg.setColor(new Color(30, 30, 30));
            this.offg.drawString(dmnew.inhand.name, 302, 142);
            this.offg.drawString((float)((int)(dmnew.inhand.weight * 10.0F + 0.5F)) / 10.0F + " kg", 302, 158);
            this.offg.setColor(Color.white);
            this.offg.drawString(dmnew.inhand.name, 300, 140);
            this.offg.drawString((float)((int)(dmnew.inhand.weight * 10.0F + 0.5F)) / 10.0F + " kg", 300, 156);
            if (dmnew.inhand.number == 9) {
                if (((Torch)dmnew.inhand).lightboost > 36) {
                    return;
                }

                String var4;
                if (((Torch)dmnew.inhand).lightboost == 0) {
                    var4 = "(Burnt Out)";
                } else {
                    var4 = "(Almost Out)";
                }

                this.offg.setFont(new Font("TimesRoman", 1, 12));
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString(var4, 282, 202);
                this.offg.setColor(Color.white);
                this.offg.drawString(var4, 280, 200);
                return;
            }

            if (dmnew.inhand.number == 215) {
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("Stealer of Souls", 281, 206);
                this.offg.setColor(new Color(120, 120, 120));
                this.offg.drawString("Stealer of Souls", 280, 205);
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("Slayer of Friends", 281, 224);
                this.offg.setColor(new Color(120, 120, 120));
                this.offg.drawString("Slayer of Friends", 280, 223);
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("Destroyer of Balance", 281, 247);
                this.offg.setColor(new Color(120, 120, 120));
                this.offg.drawString("Destroyer of Balance", 280, 246);
                return;
            }

            if (dmnew.inhand.defense > 0) {
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("Defense: " + dmnew.inhand.defense, 301, 173);
                this.offg.setColor(new Color(250, 100, 100));
                this.offg.drawString("Defense: " + dmnew.inhand.defense, 300, 172);
            }

            if (dmnew.inhand.magicresist > 0) {
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("Resist Magic: " + dmnew.inhand.magicresist, 301, 186);
                this.offg.setColor(new Color(250, 100, 100));
                this.offg.drawString("Resist Magic: " + dmnew.inhand.magicresist, 300, 185);
            }

            var1 = 0;

            for(var2 = 0; var2 < dmnew.inhand.functions; ++var2) {
                if (dmnew.inhand.charges[var2] > 0 && (dmnew.herosheet.hero.wlevel > 4 && dmnew.inhand.function[var2][1].equals("w") || dmnew.herosheet.hero.plevel > 4 && dmnew.inhand.function[var2][1].equals("p"))) {
                    this.offg.setColor(new Color(30, 30, 30));
                    this.offg.drawString(dmnew.inhand.function[var2][0] + " charges: " + dmnew.inhand.charges[var2], 251, 206 + var1 * 14);
                    this.offg.setColor(new Color(120, 120, 255));
                    this.offg.drawString(dmnew.inhand.function[var2][0] + " charges: " + dmnew.inhand.charges[var2], 250, 205 + var1 * 14);
                    ++var1;
                }
            }

            if (dmnew.herosheet.hero.wlevel > 3 && dmnew.inhand.isbomb || dmnew.herosheet.hero.plevel > 3 && dmnew.inhand.ispotion && !dmnew.inhand.isbomb) {
                this.offg.setColor(Color.white);
                this.offg.drawString("(", 242, 178);
                this.offg.drawString(")", 267, 178);
                this.offg.drawImage(dmnew.this.spellsheet.spellsymbol[dmnew.inhand.potioncastpow - 1].getImage(), 251, 168, dmnew.herosheet);
            } else if (dmnew.inhand.number == 73) {
                this.offg.setColor(new Color(30, 30, 30));
                this.offg.drawString("Drinks left: " + ((Waterskin)dmnew.inhand).drinks, 271, 247);
                this.offg.setColor(new Color(100, 100, 255));
                this.offg.drawString("Drinks left: " + ((Waterskin)dmnew.inhand).drinks, 270, 246);
            }
        }

    }

    public void sleepsheet() {
        this.offg.setColor(Color.black);
        this.offg.fillRect(0, 0, dmnew.herosheet.getSize().width, dmnew.herosheet.getSize().height);
        this.offg.setFont(new Font("TimesRoman", 1, 14));
        this.offg.setColor(new Color(70, 70, 70));
        this.offg.drawString("Sleeping. Click mouse to wake up.", 143, 143);
        this.offg.setColor(Color.white);
        this.offg.drawString("Sleeping. Click mouse to wake up.", 140, 140);
    }
}
