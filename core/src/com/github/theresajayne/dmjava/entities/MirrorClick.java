package com.github.theresajayne.dmjava.entities;

class MirrorClick extends MouseAdapter {
    int x;
    int y;

    MirrorClick() {
    }

    public void mousePressed(MouseEvent var1) {
        this.x = var1.getX();
        this.y = var1.getY();
        if ((this.x <= 420 || this.x >= 440 || this.y >= 25 || this.y <= 5) && (this.x <= 205 || this.x >= 439 || this.y <= 223 || this.y >= 250) && !SwingUtilities.isRightMouseButton(var1)) {
            if (this.x > 22 && this.x < 57 && this.y < 58 && this.y > 24) {
                dmnew.herosheet.skipchestscroll = true;
                dmnew.herosheet.paint(dmnew.herosheet.offg);
                dmnew.herosheet.stats = true;
                dmnew.this.viewing = true;
                dmnew.herosheet.repaint();
            } else if (this.x > 205 && this.x < 320 && this.y > 103 && this.y < 220) {
                dmnew.hero[dmnew.numheroes] = dmnew.mirrorhero;
                dmnew.heroatsub[dmnew.mirrorhero.subsquare] = dmnew.numheroes;
                dmnew.this.formation.addNewHero();
                dmnew.message.setMessage(dmnew.hero[dmnew.numheroes].name + " ressurrected.", dmnew.numheroes);
                this.resetStuff();
            } else if (this.x > 323 && this.x < 439 && this.y > 103 && this.y < 220) {
                dmnew.hero[dmnew.numheroes] = dmnew.mirrorhero;
                dmnew.heroatsub[dmnew.mirrorhero.subsquare] = dmnew.numheroes;
                dmnew.this.formation.addNewHero();
                dmnew.hero[dmnew.numheroes].flevel = 0;
                dmnew.hero[dmnew.numheroes].nlevel = 0;
                dmnew.hero[dmnew.numheroes].plevel = 0;
                dmnew.hero[dmnew.numheroes].wlevel = 0;
                dmnew.herosheet.setVisible(false);
                new EnterName(dmnew.frame, dmnew.hero[dmnew.numheroes]);
                dmnew.herosheet.setVisible(true);
                dmnew.message.setMessage(dmnew.hero[dmnew.numheroes].name + " reincarnated.", dmnew.numheroes);
                this.resetStuff();
            }
        } else {
            dmnew.sheet = false;
            dmnew.herosheet.mirror = false;
            dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
            dmnew.dview.addMouseListener(dmnew.this.dclick);
            dmnew.herosheet.removeMouseListener(this);
            dmnew.herosheet.addMouseListener(dmnew.this.sheetclick);
            dmnew.this.hpanel.remove(dmnew.mirrorhero);
            dmnew.this.hpanel.repaint();
            dmnew.herosheet.offscreen.flush();
            dmnew.mirrorhero = null;
            if (dmnew.numheroes > 0) {
                for(int var2 = 0; var2 < dmnew.numheroes; ++var2) {
                    dmnew.hero[var2].addMouseListener(dmnew.this.hclick);
                }
            }

            dmnew.nomovement = false;
        }

    }

    private void resetStuff() {
        ((Mirror)dmnew.DungeonMap[dmnew.level][dmnew.herolookx][dmnew.herolooky]).wasUsed = true;
        if (dmnew.hero[dmnew.numheroes].neck != null && dmnew.hero[dmnew.numheroes].neck.number == 89) {
            dmnew.leveldark += 60;
        }

        dmnew.needredraw = true;
        dmnew.sheet = false;
        dmnew.this.centerlay.show(dmnew.this.centerpanel, "dview");
        if (dmnew.numheroes == 0) {
            dmnew.this.spellsheet = dmnew.this.new SpellSheet();
            dmnew.this.weaponsheet = dmnew.this.new WeaponSheet();
            dmnew.this.eastpanel.add(dmnew.this.spellsheet);
            dmnew.this.eastpanel.add(Box.createVerticalStrut(20));
            dmnew.this.eastpanel.add(dmnew.this.weaponsheet);
            dmnew.this.eastpanel.add(Box.createVerticalStrut(10));
            dmnew.hero[dmnew.numheroes].isleader = true;
        }

        ++dmnew.numheroes;
        dmnew.this.spellsheet.casterButton[dmnew.numheroes - 1].setEnabled(true);
        dmnew.this.spellsheet.setVisible(true);
        dmnew.this.weaponsheet.setVisible(true);
        dmnew.this.spellsheet.update();
        dmnew.this.weaponsheet.update();

        for(int var1 = 0; var1 < dmnew.numheroes; ++var1) {
            dmnew.hero[var1].addMouseListener(dmnew.this.hclick);
        }

        dmnew.dview.addMouseListener(dmnew.this.dclick);
        dmnew.herosheet.removeMouseListener(this);
        dmnew.herosheet.addMouseListener(dmnew.this.sheetclick);
        dmnew.herosheet.mirror = false;
        dmnew.this.spellsheet.repaint();
        dmnew.this.weaponsheet.repaint();
        dmnew.nomovement = false;
        dmnew.this.hupdate();
        dmnew.updateDark();
    }

    public void mouseReleased(MouseEvent var1) {
        if (dmnew.herosheet.stats) {
            dmnew.herosheet.skipchestscroll = false;
            dmnew.herosheet.stats = false;
            dmnew.this.viewing = false;
            dmnew.herosheet.paint(dmnew.herosheet.getGraphics());
        }

    }
}
