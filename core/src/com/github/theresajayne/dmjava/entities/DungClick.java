package com.github.theresajayne.dmjava.entities;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.github.theresajayne.dmjava.DMJava;

import java.awt.event.MouseAdapter;

class DungClick extends InputAdapter {

    private DMJava instance = DMJava.getInstance();

    DungClick() {
    }

    @Override
    public boolean touchDown(int screenX, int screeenY,int pointer, int button)
    {
        if(button== Input.Buttons.RIGHT && DMJava.getInstance().getNumHeroes() > 0)
        {

        }
    }
    public void mousePressed(MouseEvent var1) {
        if (SwingUtilities.isRightMouseButton(var1) && dmnew.numheroes > 0) {
            dmnew.herosheet.setHero(dmnew.hero[dmnew.leader]);
            dmnew.sheet = true;
            dmnew.this.centerlay.show(dmnew.this.centerpanel, "herosheet");
        } else {
            if (dmnew.clickqueue.isEmpty()) {
                dmnew.clickqueue.add(var1.getX() + "," + var1.getY());
            }

        }
    }

    public void processClick(int var1, int var2) {
        int var3 = dmnew.partyy;
        int var4 = dmnew.partyx;
        byte var5 = 1;
        if (dmnew.facing == 0) {
            --var3;
        } else if (dmnew.facing == 1) {
            --var4;
        } else if (dmnew.facing == 2) {
            ++var3;
        } else {
            ++var4;
        }

        Object var6;
        if (var4 >= 0 && var3 >= 0 && var4 < dmnew.mapwidth && var3 < dmnew.mapheight) {
            var6 = dmnew.DungeonMap[dmnew.level][var4][var3];
        } else {
            var6 = dmnew.outWall;
        }

        MapObject var7 = dmnew.DungeonMap[dmnew.level][dmnew.partyx][dmnew.partyy];
        if (dmnew.numheroes == 0) {
            if (var6 instanceof Mirror) {
                ((MapObject)var6).tryWallSwitch(var1, var2);
            }

        } else {
            if (dmnew.iteminhand) {
                if (var2 < 236) {
                    dmnew.iteminhand = ((MapObject)var6).tryWallSwitch(var1, var2, dmnew.inhand);
                    if (dmnew.doorkeyflag) {
                        dmnew.doorkeyflag = false;
                    } else if (!dmnew.iteminhand) {
                        if (dmnew.inhand.number != 71) {
                            dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                        } else {
                            dmnew.iteminhand = true;
                        }
                    } else if (var2 < 190 && var1 < 356 && var1 > 94 && (((MapObject)var6).canPassProjs || var6 instanceof Door || var6 instanceof Stairs)) {
                        dmnew.playSound("swing.wav", -1, -1);
                        dmnew.inhand.shotpow = dmnew.hero[dmnew.leader].strength / 10 + dmnew.randGen.nextInt(4);
                        byte var8 = 0;
                        if (var1 > 224) {
                            var8 = 1;
                        }

                        dmnew.hero[dmnew.leader].gainxp('n', 1);
                        dmnew.hero[dmnew.leader].vitalize(-dmnew.randGen.nextInt((int)dmnew.inhand.weight + 2));
                        dmnew.hero[dmnew.leader].wepThrow(dmnew.inhand, var8);
                        dmnew.iteminhand = false;
                        dmnew.hero[dmnew.leader].weaponcount = (int)dmnew.inhand.weight + 8;
                        if (dmnew.hero[dmnew.leader].dexterity < 40) {
                            ++dmnew.hero[dmnew.leader].weaponcount;
                            if (dmnew.hero[dmnew.leader].dexterity < 30) {
                                dmnew.hero[dmnew.leader].weaponcount += 2;
                                if (dmnew.hero[dmnew.leader].dexterity < 20) {
                                    dmnew.hero[dmnew.leader].weaponcount += 2;
                                    if (dmnew.hero[dmnew.leader].dexterity < 10) {
                                        dmnew.hero[dmnew.leader].weaponcount += 4;
                                    }
                                }
                            }
                        } else if (dmnew.hero[dmnew.leader].dexterity > 50) {
                            --dmnew.hero[dmnew.leader].weaponcount;
                            if (dmnew.hero[dmnew.leader].dexterity > 70) {
                                --dmnew.hero[dmnew.leader].weaponcount;
                                if (dmnew.hero[dmnew.leader].dexterity > 90) {
                                    --dmnew.hero[dmnew.leader].weaponcount;
                                }
                            }
                        }

                        if (dmnew.hero[dmnew.leader].weaponcount < 1) {
                            dmnew.hero[dmnew.leader].weaponcount = 1;
                        }

                        if (dmnew.hero[dmnew.leader].stamina < dmnew.hero[dmnew.leader].maxstamina / 5 || dmnew.hero[dmnew.leader].load > dmnew.hero[dmnew.leader].maxload) {
                            dmnew.hero[dmnew.leader].weaponcount += 4;
                        }

                        dmnew.hero[dmnew.leader].repaint();
                    }

                    dmnew.this.changeCursor();
                } else if (var2 > 235 && var1 > 64 && var1 < 384) {
                    if (var2 > 278) {
                        var6 = var7;
                        var5 = 0;
                    }

                    if (((MapObject)var6).canHoldItems) {
                        if (var1 < 224) {
                            dmnew.inhand.subsquare = (3 * var5 - dmnew.facing + 4) % 4;
                        } else {
                            dmnew.inhand.subsquare = (1 + var5 - dmnew.facing + 4) % 4;
                        }

                        dmnew.iteminhand = false;
                        dmnew.hero[dmnew.leader].load -= dmnew.inhand.weight;
                        dmnew.this.changeCursor();
                        ((MapObject)var6).tryFloorSwitch(2);
                        if (!((MapObject)var6).tryTeleport(dmnew.inhand)) {
                            ((MapObject)var6).addItem(dmnew.inhand);
                        }

                        dmnew.needredraw = true;
                    }
                }
            } else if (var2 > 235 && var1 > 64 && var1 < 384) {
                if (var2 > 278) {
                    var6 = var7;
                    var5 = 0;
                }

                if (((MapObject)var6).canHoldItems && ((MapObject)var6).hasItems) {
                    int var11;
                    if (var1 < 224) {
                        var11 = (3 * var5 - dmnew.facing + 4) % 4;
                    } else {
                        var11 = (1 + var5 - dmnew.facing + 4) % 4;
                    }

                    if (var5 != 0) {
                        dmnew.Monster var9 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + var4 + "," + var3 + "," + var11);
                        if (var9 == null) {
                            var9 = (dmnew.Monster)dmnew.dmmons.get(dmnew.level + "," + var4 + "," + var3 + "," + 5);
                        }

                        if (var9 != null && !var9.isflying) {
                            return;
                        }
                    }

                    dmnew.inhand = ((MapObject)var6).pickUpItem(var11);
                    if (dmnew.inhand != null) {
                        dmnew.needredraw = true;
                        dmnew.iteminhand = true;
                        dmnew.hero[dmnew.leader].load += dmnew.inhand.weight;
                        if (dmnew.inhand.number == 8 && !dmnew.this.compass.contains(dmnew.inhand)) {
                            dmnew.this.compass.add(dmnew.inhand);
                            ((Compass)dmnew.inhand).setPic(dmnew.facing);
                        } else if (dmnew.inhand.number == 5) {
                            for(int var12 = 0; var12 < 6; ++var12) {
                                Item var10 = ((Chest)dmnew.inhand).itemAt(var12);
                                if (var10 != null && var10.number == 8 && !dmnew.this.compass.contains(var10)) {
                                    dmnew.this.compass.add(var10);
                                    ((Compass)var10).setPic(dmnew.facing);
                                }
                            }
                        }

                        dmnew.this.changeCursor();
                        ((MapObject)var6).tryFloorSwitch(3);
                    }
                }
            } else {
                ((MapObject)var6).tryWallSwitch(var1, var2);
                if (dmnew.iteminhand && dmnew.inhand.number == 8 && !dmnew.this.compass.contains(dmnew.inhand)) {
                    dmnew.this.compass.add(dmnew.inhand);
                    ((Compass)dmnew.inhand).setPic(dmnew.facing);
                }

                dmnew.this.changeCursor();
            }

        }
    }
}

