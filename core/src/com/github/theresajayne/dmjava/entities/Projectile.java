package com.github.theresajayne.dmjava.entities;

import java.util.ArrayList;

class Projectile {
    Item it;
    Spell sp;
    Image pic;
    int pow;
    int dist;
    int direction;
    int level;
    int x;
    int y;
    int subsquare;
    int powcount;
    int powdrain;
    boolean justthrown;
    boolean needsfirstdraw;
    boolean isending;
    boolean notelnext;
    boolean hitsImmaterial;
    boolean passgrate;

    public Projectile(Item var2, int var3, int var4, int var5, int var6, int var7, int var8, boolean var9, boolean var10) {
        this.it = null;
        this.sp = null;
        this.justthrown = true;
        this.needsfirstdraw = true;
        this.isending = false;
        this.notelnext = false;
        this.hitsImmaterial = false;
        this.passgrate = false;
        this.it = var2;
        this.level = var3;
        this.x = var4;
        this.y = var5;
        this.dist = var6;
        this.direction = var7;
        this.subsquare = var8;
        this.justthrown = var9;
        this.notelnext = var10;
        this.pow = this.it.throwpow + this.it.shotpow;
        if (this.it.hasthrowpic) {
            byte var11 = 2;
            if (dmnew.facing == this.direction) {
                var11 = 0;
            } else if ((dmnew.facing - this.direction) % 2 == 0) {
                var11 = 1;
            } else if (dmnew.facing == (this.direction + 1) % 4) {
                var11 = 3;
            }

            this.pic = this.it.throwpic[var11];
        } else {
            this.pic = this.it.dpic;
        }

        if (this.it.hitsImmaterial) {
            this.hitsImmaterial = true;
        }

        if (this.it.size == 0 || this.it.size == 2) {
            this.passgrate = true;
        }

    }

    public Projectile(Spell var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, boolean var11, boolean var12) {
        this.it = null;
        this.sp = null;
        this.justthrown = true;
        this.needsfirstdraw = true;
        this.isending = false;
        this.notelnext = false;
        this.hitsImmaterial = false;
        this.passgrate = false;
        this.sp = var2;
        this.level = var3;
        this.x = var4;
        this.y = var5;
        this.dist = var6;
        this.direction = var7;
        this.subsquare = var8;
        this.powdrain = var9;
        this.powcount = var10;
        this.justthrown = var11;
        this.notelnext = var12;
        this.pow = this.sp.power;
        this.pic = this.sp.pic;
        if (this.sp.hitsImmaterial) {
            this.hitsImmaterial = true;
        }

        if (this.sp.number == 51 || this.sp.number == 52 || this.sp.number == 335 || this.sp.number == 31) {
            this.passgrate = true;
        }

    }

    public Projectile(Item var2, int var3, int var4, int var5) {
        this((Item)var2, dmnew.level, dmnew.partyx, dmnew.partyy, var3, var4, var5);
    }

    public Projectile(Item var2, int var3, int var4, int var5, int var6, int var7) {
        this((Item)var2, dmnew.level, var3, var4, var5, var6, var7);
    }

    public Projectile(Item var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        this.it = null;
        this.sp = null;
        this.justthrown = true;
        this.needsfirstdraw = true;
        this.isending = false;
        this.notelnext = false;
        this.hitsImmaterial = false;
        this.passgrate = false;
        this.it = var2;
        this.pow = this.it.throwpow + this.it.shotpow;
        this.dist = var6;
        this.direction = var7;
        this.subsquare = (var8 - dmnew.facing + 4) % 4;
        if (this.it.hasthrowpic) {
            byte var9 = 2;
            if (dmnew.facing == this.direction) {
                var9 = 0;
            } else if ((dmnew.facing - this.direction) % 2 == 0) {
                var9 = 1;
            } else if (dmnew.facing == (this.direction + 1) % 4) {
                var9 = 3;
            }

            this.pic = this.it.throwpic[var9];
        } else {
            this.pic = this.it.dpic;
        }

        this.level = var3;
        this.x = var4;
        this.y = var5;
        dmnew.dmprojs.add(this);
        ++dmnew.DungeonMap[this.level][var4][var5].numProjs;
        if (this.it.hitsImmaterial) {
            this.hitsImmaterial = true;
        }

        if (this.it.size == 0 || this.it.size == 2) {
            this.passgrate = true;
        }

    }

    public Projectile(Spell var2, int var3, int var4, int var5) {
        this((Spell)var2, dmnew.level, dmnew.partyx, dmnew.partyy, var3, var4, var5);
    }

    public Projectile(Spell var2, int var3, int var4, int var5, int var6, int var7) {
        this((Spell)var2, dmnew.level, var3, var4, var5, var6, var7);
    }

    public Projectile(Spell var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        this.it = null;
        this.sp = null;
        this.justthrown = true;
        this.needsfirstdraw = true;
        this.isending = false;
        this.notelnext = false;
        this.hitsImmaterial = false;
        this.passgrate = false;
        this.sp = var2;
        this.pic = this.sp.pic;
        this.pow = this.sp.power;
        this.dist = var6;
        this.direction = var7;
        this.level = var3;
        this.x = var4;
        this.y = var5;
        this.powcount = 0;
        this.powdrain = this.dist / this.sp.gain + 1;
        this.subsquare = (var8 - dmnew.facing + 4) % 4;
        dmnew.dmprojs.add(this);
        ++dmnew.DungeonMap[this.level][var4][var5].numProjs;
        if (this.sp.hitsImmaterial) {
            this.hitsImmaterial = true;
        }

        if (this.sp.number == 51 || this.sp.number == 52 || this.sp.number == 335 || this.sp.number == 31) {
            this.passgrate = true;
        }

    }

    public void update() {
        if (this.dist == 99) {
            this.dist = 100;
        } else if (this.sp != null && this.sp.type != 0) {
            ++this.powcount;
        }

        boolean var1 = true;
        byte var2;
        int var3;
        int var4;
        int var5;
        if (this.sp != null && this.sp.number == 642) {
            var5 = this.direction;
            ArrayList var6 = new ArrayList(4);

            for(int var7 = 0; var7 < 4; ++var7) {
                if (var7 != this.direction) {
                    var6.add("" + var7);
                }
            }

            do {
                do {
                    do {
                        do {
                            do {
                                var3 = this.x;
                                var4 = this.y;
                                this.direction = var5;
                                if (this.direction != 0 && this.direction != 2) {
                                    if (this.subsquare == 0) {
                                        var2 = 1;
                                    } else if (this.subsquare == 1) {
                                        var2 = 0;
                                    } else if (this.subsquare == 2) {
                                        var2 = 3;
                                    } else {
                                        var2 = 2;
                                    }

                                    if (this.direction == 3 && (var2 == 0 || var2 == 3)) {
                                        ++var3;
                                    } else if (this.direction == 1 && (var2 == 1 || var2 == 2)) {
                                        --var3;
                                    }

                                    if (dmnew.randGen.nextBoolean()) {
                                        if (var6.contains("0") && this.y > 0 && !(dmnew.DungeonMap[this.level][this.x][this.y - 1] instanceof Wall) && dmnew.DungeonMap[this.level][this.x][this.y - 1].mapchar != '2') {
                                            var5 = 0;
                                            var6.remove("0");
                                        } else if (var6.contains("2")) {
                                            var5 = 2;
                                            var6.remove("2");
                                        } else {
                                            var5 = (this.direction + 2) % 4;
                                            var6.remove(0);
                                        }
                                    } else if (var6.contains("2") && this.y < dmnew.mapheight - 1 && !(dmnew.DungeonMap[this.level][this.x][this.y + 1] instanceof Wall) && dmnew.DungeonMap[this.level][this.x][this.y + 1].mapchar != '2') {
                                        var5 = 2;
                                        var6.remove("2");
                                    } else if (var6.contains("0")) {
                                        var5 = 0;
                                        var6.remove("0");
                                    } else {
                                        var5 = (this.direction + 2) % 4;
                                        var6.remove(0);
                                    }
                                } else {
                                    if (this.subsquare == 0) {
                                        var2 = 3;
                                    } else if (this.subsquare == 3) {
                                        var2 = 0;
                                    } else if (this.subsquare == 1) {
                                        var2 = 2;
                                    } else {
                                        var2 = 1;
                                    }

                                    if (this.direction == 0 && (var2 == 3 || var2 == 2)) {
                                        --var4;
                                    } else if (this.direction == 2 && (var2 == 0 || var2 == 1)) {
                                        ++var4;
                                    }

                                    if (dmnew.randGen.nextBoolean()) {
                                        if (var6.contains("1") && this.x > 0 && !(dmnew.DungeonMap[this.level][this.x - 1][this.y] instanceof Wall) && dmnew.DungeonMap[this.level][this.x - 1][this.y].mapchar != '2') {
                                            var5 = 1;
                                            var6.remove("1");
                                        } else if (var6.contains("3")) {
                                            var5 = 3;
                                            var6.remove("3");
                                        } else {
                                            var5 = 2 - this.direction;
                                            var6.remove(0);
                                        }
                                    } else if (var6.contains("3") && this.x < dmnew.mapwidth - 1 && !(dmnew.DungeonMap[this.level][this.x + 1][this.y] instanceof Wall) && dmnew.DungeonMap[this.level][this.x + 1][this.y].mapchar != '2') {
                                        var5 = 3;
                                        var6.remove("3");
                                    } else if (var6.contains("1")) {
                                        var5 = 1;
                                        var6.remove("1");
                                    } else {
                                        var5 = 2 - this.direction;
                                        var6.remove(0);
                                    }
                                }
                            } while(var3 < 0 && this.direction == 1);
                        } while(var4 < 0 && this.direction == 0);
                    } while(var3 == dmnew.mapwidth && this.direction == 3);
                } while(var4 == dmnew.mapheight && this.direction == 2);
            } while(dmnew.DungeonMap[this.level][var3][var4] instanceof Wall && dmnew.DungeonMap[this.level][var3][var4].mapchar != '2' && !var6.isEmpty());
        } else {
            var3 = this.x;
            var4 = this.y;
            if (this.direction != 0 && this.direction != 2) {
                if (this.subsquare == 0) {
                    var2 = 1;
                } else if (this.subsquare == 1) {
                    var2 = 0;
                } else if (this.subsquare == 2) {
                    var2 = 3;
                } else {
                    var2 = 2;
                }

                if (this.direction == 3 && (var2 == 0 || var2 == 3)) {
                    ++var3;
                } else if (this.direction == 1 && (var2 == 1 || var2 == 2)) {
                    --var3;
                }
            } else {
                if (this.subsquare == 0) {
                    var2 = 3;
                } else if (this.subsquare == 3) {
                    var2 = 0;
                } else if (this.subsquare == 1) {
                    var2 = 2;
                } else {
                    var2 = 1;
                }

                if (this.direction == 0 && (var2 == 3 || var2 == 2)) {
                    --var4;
                } else if (this.direction == 2 && (var2 == 0 || var2 == 1)) {
                    ++var4;
                }
            }
        }

        if (this.dist != 0 && var3 >= 0 && var4 >= 0 && var3 != dmnew.mapwidth && var4 != dmnew.mapheight) {
            if (dmnew.DungeonMap[this.level][var3][var4].canPassProjs || dmnew.DungeonMap[this.level][var3][var4].mapchar == 'd' || dmnew.DungeonMap[this.level][var3][var4].mapchar == '>' && ((Stairs)dmnew.DungeonMap[this.level][var3][var4]).side == this.direction) {
                if (!this.justthrown && dmnew.DungeonMap[this.level][this.x][this.y].mapchar == '>') {
                    var1 = false;
                } else if (!this.justthrown && this.level == dmnew.level && this.x == dmnew.partyx && this.y == dmnew.partyy && !dmnew.alldead && dmnew.heroatsub[(this.subsquare + dmnew.facing) % 4] != -1) {
                    var1 = false;
                } else if (dmnew.DungeonMap[this.level][this.x][this.y].mapchar != 'd' || ((Door)dmnew.DungeonMap[this.level][this.x][this.y]).changecount <= 1 || this.passgrate && ((Door)dmnew.DungeonMap[this.level][this.x][this.y]).pictype == 1) {
                    if (!this.justthrown && dmnew.DungeonMap[this.level][this.x][this.y].hasMons) {
                        dmnew.Monster var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                        if (var9 == null) {
                            var9 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 5);
                        }

                        if (var9 != null && !var9.isdying && (this.hitsImmaterial || !var9.isImmaterial)) {
                            var1 = false;
                        }
                    }
                } else {
                    var1 = false;
                }
            } else {
                var1 = false;
            }
        } else {
            var1 = false;
        }

        if (var1) {
            --dmnew.DungeonMap[this.level][this.x][this.y].numProjs;
            --this.dist;
            if (this.powcount > this.powdrain) {
                this.powcount = 0;
                --this.sp.gain;
                this.sp.power = this.sp.powers[this.sp.gain - 1];
            }

            this.x = var3;
            this.y = var4;
            this.subsquare = var2;
            ++dmnew.DungeonMap[this.level][this.x][this.y].numProjs;
            int var8;
            if (this.level == dmnew.level) {
                var5 = dmnew.partyx - this.x;
                if (var5 < 0) {
                    var5 *= -1;
                }

                var8 = dmnew.partyy - this.y;
                if (var8 < 0) {
                    var8 *= -1;
                }

                if (var5 < 5 && var8 < 5) {
                    dmnew.needredraw = true;
                }
            }

            if (!this.notelnext) {
                dmnew.DungeonMap[this.level][this.x][this.y].tryTeleport(this);
                if (this.level == dmnew.level) {
                    var5 = dmnew.partyx - this.x;
                    if (var5 < 0) {
                        var5 *= -1;
                    }

                    var8 = dmnew.partyy - this.y;
                    if (var8 < 0) {
                        var8 *= -1;
                    }

                    if (var5 < 5 && var8 < 5) {
                        dmnew.needredraw = true;
                    }
                }
            } else {
                this.notelnext = false;
            }
        } else {
            this.projend();
        }

        this.justthrown = false;
    }

    public void projend() {
        int var5;
        if (this.it != null) {
            this.isending = true;
            boolean var1 = true;
            if (!this.justthrown && dmnew.DungeonMap[this.level][this.x][this.y].hasParty) {
                var5 = dmnew.heroatsub[(this.subsquare + dmnew.facing) % 4];
                if (var5 != -1) {
                    dmnew.playSound("oof.wav", -1, -1);
                    dmnew.hero[var5].damage(this.pow, 5);
                    if (this.it.poisonous > 0 && dmnew.randGen.nextBoolean()) {
                        dmnew.hero[var5].poison += this.it.poisonous;
                        dmnew.hero[var5].ispoisoned = true;
                    }
                }
            } else if (!this.justthrown && dmnew.DungeonMap[this.level][this.x][this.y].hasMons) {
                dmnew.Monster var2 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                if (var2 == null) {
                    var2 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 5);
                }

                if (var2 != null && (this.hitsImmaterial || !var2.isImmaterial) && (var2.hurtitem == 0 || this.it.number == 215 || this.it.number == var2.hurtitem)) {
                    var2.damage(this.pow, 5);
                    if (!var2.isImmaterial && this.it.poisonous > 0 && dmnew.randGen.nextBoolean()) {
                        var2.poisonpow += this.it.poisonous;
                        var2.ispoisoned = true;
                    }

                    if (!var2.isImmaterial && !var2.isdying && var2.defense < 80 && this.it.projtype > 0 && this.it.number != 266 && dmnew.randGen.nextInt(10) == 0) {
                        this.it.shotpow = 0;
                        var2.carrying.add(this.it);
                        var1 = false;
                    }
                }
            }

            if (var1) {
                if ((double)this.it.weight > 4.0D) {
                    dmnew.playSound("thunk.wav", this.x, this.y);
                } else if (this.it.type == 0) {
                    dmnew.playSound("dink.wav", this.x, this.y);
                }

                this.it.shotpow = 0;
                this.it.subsquare = this.subsquare;
                dmnew.DungeonMap[this.level][this.x][this.y].tryFloorSwitch(2);
                if (!dmnew.DungeonMap[this.level][this.x][this.y].tryTeleport(this.it)) {
                    dmnew.DungeonMap[this.level][this.x][this.y].addItem(this.it);
                }
            }

            --dmnew.DungeonMap[this.level][this.x][this.y].numProjs;
            if (this.level == dmnew.level) {
                var5 = dmnew.partyx - this.x;
                if (var5 < 0) {
                    var5 *= -1;
                }

                int var3 = dmnew.partyy - this.y;
                if (var3 < 0) {
                    var3 *= -1;
                }

                if (var5 < 5 && var3 < 5) {
                    dmnew.needredraw = true;
                }
            }
        } else {
            this.isending = true;
            if (this.level == dmnew.level) {
                if (this.sp.number != 44 && this.sp.number != 46 && this.sp.number != 335 && this.sp.number != 642 && this.sp.number != 365) {
                    dmnew.playSound("zap.wav", this.x, this.y);
                } else {
                    dmnew.playSound("fball.wav", this.x, this.y);
                }
            }

            int var7;
            if (!this.hitsImmaterial && dmnew.DungeonMap[this.level][this.x][this.y].hasParty) {
                if (!this.sp.ismultiple) {
                    if (!this.justthrown && this.sp.number != 6) {
                        var7 = dmnew.heroatsub[(this.subsquare + dmnew.facing) % 4];
                        if (var7 != -1) {
                            if (this.sp.number == 51) {
                                dmnew.hero[var7].poison += this.sp.gain * 2 + 1;
                                dmnew.hero[var7].ispoisoned = true;
                                dmnew.hero[var7].damage(this.pow, 1);
                            } else if (this.sp.number == 461) {
                                if (dmnew.hero[var7].strengthboost > -this.sp.power) {
                                    dmnew.hero[var7].strength -= dmnew.hero[var7].strengthboost;
                                    dmnew.hero[var7].strengthboost -= this.sp.power;
                                    if (dmnew.hero[var7].strengthboost < -this.sp.power) {
                                        dmnew.hero[var7].strengthboost = -this.sp.power;
                                    }

                                    if (dmnew.hero[var7].strength + dmnew.hero[var7].strengthboost <= 0) {
                                        dmnew.hero[var7].strengthboost = 1 - dmnew.hero[var7].strength;
                                    }

                                    dmnew.hero[var7].strength += dmnew.hero[var7].strengthboost;
                                    dmnew.hero[var7].setMaxLoad();
                                }

                                if (dmnew.hero[var7].vitalityboost > -this.sp.power) {
                                    dmnew.hero[var7].vitality -= dmnew.hero[var7].vitalityboost;
                                    dmnew.hero[var7].vitalityboost -= this.sp.power;
                                    if (dmnew.hero[var7].vitalityboost < -this.sp.power) {
                                        dmnew.hero[var7].vitalityboost = -this.sp.power;
                                    }

                                    if (dmnew.hero[var7].vitality + dmnew.hero[var7].vitalityboost <= 0) {
                                        dmnew.hero[var7].vitalityboost = 1 - dmnew.hero[var7].vitality;
                                    }

                                    dmnew.hero[var7].vitality += dmnew.hero[var7].vitalityboost;
                                }

                                dmnew.hero[var7].damage(1, 1);
                            } else if (this.sp.number == 363) {
                                if (dmnew.hero[var7].intelligenceboost > -this.sp.power) {
                                    dmnew.hero[var7].intelligence -= dmnew.hero[var7].intelligenceboost;
                                    dmnew.hero[var7].intelligenceboost -= this.sp.power;
                                    if (dmnew.hero[var7].intelligenceboost < -this.sp.power) {
                                        dmnew.hero[var7].intelligenceboost = -this.sp.power;
                                    }

                                    if (dmnew.hero[var7].intelligence + dmnew.hero[var7].intelligenceboost <= 0) {
                                        dmnew.hero[var7].intelligenceboost = 1 - dmnew.hero[var7].intelligence;
                                    }

                                    dmnew.hero[var7].intelligence += dmnew.hero[var7].intelligenceboost;
                                }

                                if (dmnew.hero[var7].wisdomboost > -this.sp.power) {
                                    dmnew.hero[var7].wisdom -= dmnew.hero[var7].wisdomboost;
                                    dmnew.hero[var7].wisdomboost -= this.sp.power;
                                    if (dmnew.hero[var7].wisdomboost < -this.sp.power) {
                                        dmnew.hero[var7].wisdomboost = -this.sp.power;
                                    }

                                    if (dmnew.hero[var7].wisdom + dmnew.hero[var7].wisdomboost <= 0) {
                                        dmnew.hero[var7].wisdomboost = 1 - dmnew.hero[var7].wisdom;
                                    }

                                    dmnew.hero[var7].wisdom += dmnew.hero[var7].wisdomboost;
                                }

                                dmnew.hero[var7].damage(1, 1);
                            } else if (this.sp.number == 664) {
                                if (dmnew.hero[var7].defenseboost > -this.sp.power) {
                                    dmnew.hero[var7].defense -= dmnew.hero[var7].defenseboost;
                                    dmnew.hero[var7].defenseboost -= this.sp.power;
                                    if (dmnew.hero[var7].defenseboost < -this.sp.power) {
                                        dmnew.hero[var7].defenseboost = -this.sp.power;
                                    }

                                    dmnew.hero[var7].defense += dmnew.hero[var7].defenseboost;
                                }

                                if (dmnew.hero[var7].magicresistboost > -this.sp.power) {
                                    dmnew.hero[var7].magicresist -= dmnew.hero[var7].magicresistboost;
                                    dmnew.hero[var7].magicresistboost -= this.sp.power;
                                    if (dmnew.hero[var7].magicresistboost < -this.sp.power) {
                                        dmnew.hero[var7].magicresistboost = -this.sp.power;
                                    }

                                    dmnew.hero[var7].magicresist += dmnew.hero[var7].magicresistboost;
                                }

                                dmnew.hero[var7].damage(1, 1);
                            } else {
                                dmnew.hero[var7].damage(this.pow, 1);
                            }
                        }
                    }
                } else {
                    if (this.sp.number == 362) {
                        dmnew.slowcount += this.sp.power * 3;
                        dmnew.message.setMessage("Party Slowed.", 4);
                    }

                    for(var7 = 0; var7 < dmnew.numheroes; ++var7) {
                        if (this.sp.number == 362) {
                            if (dmnew.hero[var7].dexterityboost > -this.sp.power) {
                                dmnew.hero[var7].dexterity -= dmnew.hero[var7].dexterityboost;
                                dmnew.hero[var7].dexterityboost -= this.sp.power;
                                if (dmnew.hero[var7].dexterityboost < -this.sp.power) {
                                    dmnew.hero[var7].dexterityboost = -this.sp.power;
                                }

                                if (dmnew.hero[var7].dexterity + dmnew.hero[var7].dexterityboost <= 0) {
                                    dmnew.hero[var7].dexterityboost = 1 - dmnew.hero[var7].dexterity;
                                }

                                dmnew.hero[var7].dexterity += dmnew.hero[var7].dexterityboost;
                            }

                            dmnew.hero[var7].damage(1, 1);
                        } else if (this.sp.number != 523) {
                            if (this.sp.number == 44 && dmnew.hero[var7].weapon.number == 244 && dmnew.hero[var7].weapon.functions == 1) {
                                dmnew.hero[var7].weapon.charges[1] = 1;
                                dmnew.hero[var7].weapon.power[1] = this.sp.gain;
                                dmnew.hero[var7].weapon.functions = 2;
                                dmnew.this.weaponsheet.update();
                                dmnew.hero[var7].repaint();
                            } else if (var7 == dmnew.heroatsub[(this.subsquare + dmnew.facing) % 4]) {
                                dmnew.hero[var7].damage(this.pow, 1);
                            } else {
                                dmnew.hero[var7].damage(this.pow * 2 / 3, 1);
                            }
                        } else {
                            if (dmnew.hero[var7].silencecount < this.sp.power) {
                                var5 = (dmnew.hero[var7].intelligence + dmnew.hero[var7].wisdom) / 2;
                                if (dmnew.randGen.nextInt(this.sp.gain * 20) + 30 > var5 || var5 >= 100 && dmnew.randGen.nextInt(5) == 0) {
                                    if (!dmnew.hero[var7].silenced) {
                                        dmnew.message.setMessage(dmnew.hero[var7].name + " is silenced!", var7);
                                    }

                                    dmnew.hero[var7].silenced = true;
                                    dmnew.hero[var7].silencecount += this.sp.power;
                                    if (dmnew.hero[var7].silencecount > this.sp.power) {
                                        dmnew.hero[var7].silencecount = this.sp.power;
                                    }
                                }
                            }

                            dmnew.hero[var7].damage(1, 1);
                        }
                    }
                }
            } else if (!dmnew.DungeonMap[this.level][this.x][this.y].hasMons) {
                if (this.sp.number == 6 && dmnew.DungeonMap[this.level][this.x][this.y] instanceof Door) {
                    Door var6 = (Door)dmnew.DungeonMap[this.level][this.x][this.y];
                    if (var6.opentype == 1) {
                        var6.activate();
                    }
                } else if ((this.sp.number == 44 || this.sp.number == 46) && dmnew.DungeonMap[this.level][this.x][this.y] instanceof Door) {
                    ((Door)dmnew.DungeonMap[this.level][this.x][this.y]).breakDoor(this.pow, false, false);
                }
            } else {
                dmnew.Monster var4;
                if (this.sp.ismultiple) {
                    var5 = 0;

                    while(var5 < 6) {
                        var4 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + var5);
                        if (var4 != null && this.hitsImmaterial == var4.isImmaterial) {
                            if (this.sp.number == 362) {
                                if (var4.speedboost > -this.sp.power) {
                                    var4.speed -= var4.speedboost;
                                    var4.speedboost -= this.sp.power;
                                    if (var4.speedboost < -this.sp.power) {
                                        var4.speedboost = -this.sp.power;
                                    }

                                    if (var4.speed + var4.speedboost <= 0) {
                                        var4.speedboost = 1 - var4.speed;
                                    }

                                    var4.speed += var4.speedboost;
                                }

                                var4.movespeed -= var4.movespeedboost;
                                var4.movespeedboost += this.sp.gain * 3;
                                if (var4.movespeedboost > this.sp.gain * 3) {
                                    var4.movespeedboost = this.sp.gain * 3;
                                }

                                var4.movespeed += var4.movespeedboost;
                            } else if (this.sp.number == 523) {
                                if (var4.number != 26 && var4.hasmagic && var4.silencecount < this.sp.power && (dmnew.randGen.nextInt(this.sp.gain * 20) + 30 > var4.manapower || var4.manapower >= 100 && dmnew.randGen.nextInt(5) == 0)) {
                                    var4.silenced = true;
                                    var4.silencecount += this.sp.power;
                                    if (var4.silencecount > this.sp.power) {
                                        var4.silencecount = this.sp.power;
                                    }
                                }
                            } else if (var5 != this.subsquare && var5 != 5) {
                                var4.damage(this.pow * 2 / 3, 1);
                            } else {
                                var4.damage(this.pow, 1);
                            }
                        }

                        if (var5 == 3) {
                            var5 = 5;
                        } else {
                            ++var5;
                        }
                    }
                } else if (!this.justthrown && this.sp.number != 6) {
                    var4 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + this.subsquare);
                    if (var4 == null) {
                        var4 = (dmnew.Monster)dmnew.dmmons.get(this.level + "," + this.x + "," + this.y + "," + 5);
                    }

                    if (var4 != null && (this.hitsImmaterial == var4.isImmaterial || this.sp.number == 0 && var4.number == 26)) {
                        if (this.sp.number == 51 && !var4.isImmaterial && var4.number != 26) {
                            var4.damage(this.pow, 1);
                            var4.poisonpow += this.sp.gain * 2 + 1;
                            var4.ispoisoned = true;
                        } else if (this.sp.number == 0 && var4.number == 26) {
                            boolean var8 = true;
                            if (dmnew.DungeonMap[this.level][var4.x][var4.y].mapchar == 'F' && ((FDecoration)dmnew.DungeonMap[this.level][var4.x][var4.y]).number == 2) {
                                if (dmnew.fluxcages.get(this.level + "," + var4.x + "," + var4.y) == null) {
                                    var8 = false;
                                } else if ((!(dmnew.DungeonMap[this.level][var4.x][var4.y - 1] instanceof Wall) || dmnew.DungeonMap[this.level][var4.x][var4.y - 1].mapchar == '2') && (dmnew.partyx != var4.x || dmnew.partyy != var4.y - 1) && dmnew.fluxcages.get(this.level + "," + var4.x + "," + (var4.y - 1)) == null) {
                                    var8 = false;
                                } else if ((!(dmnew.DungeonMap[this.level][var4.x][var4.y + 1] instanceof Wall) || dmnew.DungeonMap[this.level][var4.x][var4.y + 1].mapchar == '2') && (dmnew.partyx != var4.x || dmnew.partyy != var4.y + 1) && dmnew.fluxcages.get(this.level + "," + var4.x + "," + (var4.y + 1)) == null) {
                                    var8 = false;
                                } else if ((!(dmnew.DungeonMap[this.level][var4.x - 1][var4.y] instanceof Wall) || dmnew.DungeonMap[this.level][var4.x - 1][var4.y].mapchar == '2') && (dmnew.partyx != var4.x - 1 || dmnew.partyy != var4.y) && dmnew.fluxcages.get(this.level + "," + (var4.x - 1) + "," + var4.y) == null) {
                                    var8 = false;
                                } else if ((!(dmnew.DungeonMap[this.level][var4.x + 1][var4.y] instanceof Wall) || dmnew.DungeonMap[this.level][var4.x + 1][var4.y].mapchar == '2') && (dmnew.partyx != var4.x + 1 || dmnew.partyy != var4.y) && dmnew.fluxcages.get(this.level + "," + (var4.x + 1) + "," + var4.y) == null) {
                                    var8 = false;
                                }
                            } else {
                                var8 = false;
                            }

                            if (var8) {
                                var4.damage(var4.maxhealth, 6);
                            } else {
                                var4.teleport();
                            }
                        } else if (this.sp.number == 461) {
                            if (var4.powerboost > -this.sp.power) {
                                var4.power -= var4.powerboost;
                                var4.powerboost -= this.sp.power;
                                if (var4.powerboost < -this.sp.power) {
                                    var4.powerboost = -this.sp.power;
                                }

                                if (var4.power + var4.powerboost <= 0) {
                                    var4.powerboost = 1 - var4.power;
                                }

                                var4.power += var4.powerboost;
                            }
                        } else if (this.sp.number == 363) {
                            if (var4.manapowerboost > -this.sp.power) {
                                var4.manapower -= var4.manapowerboost;
                                var4.manapowerboost -= this.sp.power;
                                if (var4.manapowerboost < -this.sp.power) {
                                    var4.manapowerboost = -this.sp.power;
                                }

                                if (var4.manapower + var4.manapowerboost <= 0) {
                                    var4.manapowerboost = 1 - var4.manapower;
                                }

                                var4.manapower += var4.manapowerboost;
                            }
                        } else if (this.sp.number == 664) {
                            if (var4.defenseboost > -this.sp.power) {
                                var4.defense -= var4.defenseboost;
                                var4.defenseboost -= this.sp.power;
                                if (var4.defenseboost < -this.sp.power) {
                                    var4.defenseboost = -this.sp.power;
                                }

                                var4.defense += var4.defenseboost;
                            }

                            if (var4.magicresistboost > -this.sp.power) {
                                var4.magicresist -= var4.magicresistboost;
                                var4.magicresistboost -= this.sp.power;
                                if (var4.magicresistboost < -this.sp.power) {
                                    var4.magicresistboost = -this.sp.power;
                                }

                                var4.magicresist += var4.magicresistboost;
                            }
                        } else {
                            var4.damage(this.pow, 1);
                        }
                    }
                }
            }

            if (this.sp.number == 31 || this.sp.number == 61) {
                dmnew.this.new PoisonCloud(this.level, this.x, this.y, this.sp.gain);
            }

            if (this.level == dmnew.level) {
                var7 = dmnew.partyx - this.x;
                if (var7 < 0) {
                    var7 *= -1;
                }

                var5 = dmnew.partyy - this.y;
                if (var5 < 0) {
                    var5 *= -1;
                }

                if (var7 < 5 && var5 < 5) {
                    dmnew.needredraw = true;
                }
            }
        }

    }
}
