package com.github.theresajayne.dmjava.entities;

class SpellCasterClick implements ActionListener {
    SpellCasterClick() {
    }

    public void actionPerformed(ActionEvent var1) {
        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setText("");
        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setPreferredSize(new Dimension(16, 20));
        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setMaximumSize(new Dimension(16, 20));
        dmnew.this.spellready = Integer.parseInt(var1.getActionCommand());
        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setPreferredSize(new Dimension(95, 20));
        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setMaximumSize(new Dimension(95, 20));
        dmnew.this.spellsheet.casterButton[dmnew.this.spellready].setText(dmnew.hero[dmnew.this.spellready].name);
        dmnew.this.spellsheet.update();
    }
}

