package com.github.theresajayne.dmjava.entities;

class FluxCage {
    int level;
    int x;
    int y;
    int counter;
    boolean mirrored;

    public FluxCage(int var2, int var3, int var4) {
        dmnew.FluxCage var5 = (dmnew.FluxCage)dmnew.fluxcages.get(var2 + "," + var3 + "," + var4);
        if (var5 != null) {
            var5.counter = 0;
            var5.mirrored = !var5.mirrored;
        } else {
            this.level = var2;
            this.x = var3;
            this.y = var4;
            this.counter = 0;
            dmnew.fluxcages.put(var2 + "," + var3 + "," + var4, this);
            dmnew.fluxchanging = true;
        }
    }

    public boolean update() {
        if (dmnew.DungeonMap[this.level][this.x][this.y] instanceof Wall) {
            return false;
        } else {
            ++this.counter;
            boolean var1;
            if (this.counter > 100) {
                var1 = false;
            } else {
                var1 = true;
            }

            if (this.level == dmnew.level) {
                int var2 = this.x - dmnew.partyx;
                if (var2 < 0) {
                    var2 *= -1;
                }

                int var3 = this.y - dmnew.partyy;
                if (var3 < 0) {
                    var3 *= -1;
                }

                if (var2 < 4 && var3 < 4 && this.counter % 3 == 0) {
                    this.mirrored = !this.mirrored;
                    dmnew.needredraw = true;
                }
            }

            return var1;
        }
    }
}
