package com.github.theresajayne.dmjava.entities;

class WeaponSheet extends JPanel {
    JButton[] weaponFunction = new JButton[3];
    JToggleButton[] weaponButton = new JToggleButton[4];
    ButtonGroup userGroup = new ButtonGroup();
    Box uppan;
    JPanel dwnpan;
    JPanel hitpan;
    JLabel hitlabel;
    ImageIcon hiticon;
    ImageIcon hiticon2;
    ImageIcon hiticon3;
    ImageIcon missicon;
    dmnew.WeaponClick weaponclick = dmnew.this.new WeaponClick();
    dmnew.WeaponFunctClick weaponfunctclick = dmnew.this.new WeaponFunctClick();

    public WeaponSheet() {
        super(true);
        CompoundBorder var2 = BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder());
        this.setBorder(var2);
        this.setPreferredSize(new Dimension(155, 140));
        this.setMaximumSize(new Dimension(155, 140));
        this.setMinimumSize(new Dimension(155, 140));
        this.setLayout(new BoxLayout(this, 1));
        this.uppan = new Box(0);
        this.dwnpan = new JPanel();
        this.dwnpan.setPreferredSize(new Dimension(120, 62));
        this.dwnpan.setMaximumSize(new Dimension(120, 62));
        this.dwnpan.setMinimumSize(new Dimension(120, 62));
        this.dwnpan.setLayout(new GridLayout(3, 1));

        for(int var3 = 0; var3 < 4; ++var3) {
            this.weaponButton[var3] = new JToggleButton();
            this.weaponButton[var3].setPreferredSize(new Dimension(36, 36));
            this.weaponButton[var3].setMaximumSize(new Dimension(36, 36));
            this.weaponButton[var3].setMinimumSize(new Dimension(36, 36));
            this.weaponButton[var3].setActionCommand("" + var3);
            this.weaponButton[var3].addActionListener(this.weaponclick);
            this.userGroup.add(this.weaponButton[var3]);
            this.uppan.add(this.weaponButton[var3]);
        }

        this.weaponButton[dmnew.this.weaponready].setSelected(true);

        for(int var4 = 0; var4 < 3; ++var4) {
            this.weaponFunction[var4] = new JButton();
            this.weaponFunction[var4].setPreferredSize(new Dimension(120, 22));
            this.weaponFunction[var4].setMaximumSize(new Dimension(120, 22));
            this.weaponFunction[var4].setMinimumSize(new Dimension(120, 22));
            this.weaponFunction[var4].setActionCommand("" + var4);
            this.weaponFunction[var4].addActionListener(this.weaponfunctclick);
            this.dwnpan.add(this.weaponFunction[var4]);
        }

        this.hiticon = new ImageIcon("wephit.gif");
        this.hiticon2 = new ImageIcon("wephit2.gif");
        this.hiticon3 = new ImageIcon("wephit3.gif");
        this.missicon = new ImageIcon("wepmiss.gif");
        this.hitlabel = new JLabel(this.hiticon3);
        this.hitlabel.setHorizontalTextPosition(0);
        this.hitpan = new JPanel();
        this.hitpan.add(this.hitlabel);
        this.hitpan.setVisible(false);
        this.add(this.uppan);
        this.add(Box.createVerticalStrut(5));
        this.add(this.dwnpan);
        this.add(this.hitpan);
        this.setCursor(new Cursor(12));
    }

    public void update() {
        for(int var1 = 1; var1 < 4; ++var1) {
            if (dmnew.numheroes <= var1) {
                this.weaponButton[var1].setIcon((Icon)null);
                this.weaponButton[var1].setDisabledIcon((Icon)null);
                this.weaponButton[var1].setEnabled(false);
            }
        }

        for(int var2 = 0; var2 < dmnew.numheroes; ++var2) {
            ImageIcon var3 = (ImageIcon)this.weaponButton[var2].getIcon();
            if (var3 != null) {
                var3.setImageObserver((ImageObserver)null);
            }

            this.weaponButton[var2].setIcon(new ImageIcon(dmnew.hero[var2].weapon.pic));
            if (dmnew.hero[var2].weapon.type == 0 && !dmnew.hero[var2].isdead) {
                this.weaponButton[var2].setEnabled(true);
                if (dmnew.this.weaponready == var2) {
                    int var4;
                    for(var4 = 0; var4 < dmnew.hero[var2].weapon.functions; ++var4) {
                        boolean var5 = true;
                        switch(dmnew.hero[var2].weapon.function[var4][1].charAt(0)) {
                            case 'f':
                                if (dmnew.hero[var2].flevel < dmnew.hero[var2].weapon.level[var4]) {
                                    var5 = false;
                                }
                                break;
                            case 'n':
                                if (dmnew.hero[var2].nlevel < dmnew.hero[var2].weapon.level[var4]) {
                                    var5 = false;
                                }
                                break;
                            case 'p':
                                if (dmnew.hero[var2].plevel < dmnew.hero[var2].weapon.level[var4]) {
                                    var5 = false;
                                }
                                break;
                            case 'w':
                                if (dmnew.hero[var2].wlevel < dmnew.hero[var2].weapon.level[var4]) {
                                    var5 = false;
                                }
                        }

                        if (var5) {
                            this.weaponFunction[var4].setText(dmnew.hero[var2].weapon.function[var4][0]);
                            this.weaponFunction[var4].setVisible(true);
                        } else {
                            this.weaponFunction[var4].setText("");
                            this.weaponFunction[var4].setVisible(false);
                        }
                    }

                    for(int var6 = var4; var6 < 3; ++var6) {
                        this.weaponFunction[var6].setText("");
                        this.weaponFunction[var6].setVisible(false);
                    }

                    if (dmnew.hero[dmnew.this.weaponready].wepready) {
                        this.functionsEnable(true);
                    } else {
                        this.functionsEnable(false);
                    }
                }
            } else {
                this.weaponButton[var2].setDisabledIcon((Icon)null);
                this.weaponButton[var2].setEnabled(false);
                if (dmnew.this.weaponready == var2) {
                    this.weaponFunction[0].setVisible(false);
                    this.weaponFunction[1].setVisible(false);
                    this.weaponFunction[2].setVisible(false);
                }
            }
        }

    }

    public void functionsEnable(boolean var1) {
        for(int var2 = 0; var2 < dmnew.hero[dmnew.this.weaponready].weapon.functions; ++var2) {
            this.weaponFunction[var2].setEnabled(var1);
        }

        if (this.hitpan.isVisible() && dmnew.hero[dmnew.this.weaponready].hitcounter <= 0) {
            this.hitpan.setVisible(false);
            this.dwnpan.setVisible(true);
        } else if (!this.hitpan.isVisible() && dmnew.hero[dmnew.this.weaponready].hitcounter > 0) {
            this.dwnpan.setVisible(false);
            this.hitpan.setVisible(true);
        }

        ((BoxLayout)this.uppan.getLayout()).invalidateLayout(this.uppan);
        this.uppan.validate();
        this.repaint();
    }
}

