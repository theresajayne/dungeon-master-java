package com.github.theresajayne.dmjava.entities;

import java.awt.image.RGBImageFilter;

class DarknessFilter extends RGBImageFilter {
    private float factor = 0.7F;

    public DarknessFilter() {
        super.canFilterIndexColorModel = true;
    }

    public void setDarks(int var1) {
        this.factor = 0.7F;

        for(int var2 = 1; var2 < var1; ++var2) {
            this.factor *= 0.7F;
        }

    }

    public final int filterRGB(int var1, int var2, int var3) {
        return (var3 >> 24 & 255) << 24 | (int)((float)(var3 >> 16 & 255) * this.factor) << 16 | (int)((float)(var3 >> 8 & 255) * this.factor) << 8 | (int)((float)(var3 & 255) * this.factor);
    }
}