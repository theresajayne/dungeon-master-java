package com.github.theresajayne.dmjava;

import com.badlogic.gdx.Game;
import com.github.theresajayne.dmjava.Screens.StartScreen;
import com.github.theresajayne.dmjava.entities.HeroSheet;

public class DMJava extends Game {

	private int numHeroes;
	private static DMJava instance;
	private HeroSheet heroSheet  = new HeroSheet();

	public static DMJava getInstance()
	{
		return instance;
	}

	@Override
	public void create () {
		DMJava.instance = this;
		setScreen(new StartScreen(this));

	}

	public int getNumHeroes() {
		return numHeroes;
	}

	public void setNumHeroes(int numHeroes) {
		this.numHeroes = numHeroes;
	}
}
