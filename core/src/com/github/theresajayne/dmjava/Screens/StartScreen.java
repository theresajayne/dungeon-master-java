package com.github.theresajayne.dmjava.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import javafx.scene.input.MouseButton;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class StartScreen extends BaseScreen {

    private Texture title;
    private Texture entrance;
    private Texture greenGem;

    Image titleImage;
    Image entranceImage;
    Image greenGemImage;

    private float counter =0;

    public StartScreen(final Game game)
    {
        this.game = game;
        WORLD_WIDTH = Gdx.graphics.getWidth();
        WORLD_HEIGHT = Gdx.graphics.getHeight();
    }

    @Override
    public void show()
    {
        Music music = Gdx.audio.newMusic(Gdx.files.internal("music/OpeningTheme.mp3"));
        stage = new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT));
        entrance = new Texture(Gdx.files.internal("dm3.jpg"));
        entranceImage = new Image(entrance);
        title = new Texture(Gdx.files.internal("title.gif"));
        titleImage = new Image(title);
        titleImage.setFillParent(true);
        stage.addActor(titleImage);
        titleImage.addAction(sequence(Actions.alpha(0),Actions.delay(5),Actions.fadeIn(2.5f, Interpolation.pow2In),Actions.alpha(1),Actions.delay(5),Actions.fadeOut(2.5f,Interpolation.pow2Out)));
        entranceImage.setFillParent(true);
        stage.addActor(entranceImage);
        entranceImage.addAction(sequence(Actions.alpha(0),Actions.delay(20),Actions.fadeIn(5, Interpolation.pow2In)));
        music.setVolume(0.5f);
        music.play();
        greenGem = new Texture(Gdx.files.internal("greengem.png"));
        greenGemImage = new Image(greenGem);
        greenGemImage.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x,float y)
            {
                System.out.println("Clicked");
                music.stop();
                game.setScreen(new MainScreen());
            }
        });
        greenGemImage.setScale(Scaling.fillX.ordinal(),Scaling.fillY.ordinal());
        greenGemImage.setPosition((float)7.65*(stage.getWidth()/10),(float)6.9*(stage.getHeight()/10));
        greenGemImage.addAction(sequence(Actions.alpha(0),Actions.delay(20),Actions.fadeIn(5, Interpolation.pow2In)));
        stage.addActor(greenGemImage);
        Gdx.input.setInputProcessor(stage);
        //stage.setDebugAll(true);
    }

    @Override
    public void render(float delta)
    {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0,0);
        stage.act();
        stage.draw();
    }

}
