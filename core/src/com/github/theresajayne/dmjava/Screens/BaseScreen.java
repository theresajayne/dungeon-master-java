package com.github.theresajayne.dmjava.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class BaseScreen extends ScreenAdapter {

    protected int WORLD_WIDTH = Gdx.graphics.getWidth();
    protected int WORLD_HEIGHT = Gdx.graphics.getHeight();
    protected Game game;
    protected Stage stage;

    public abstract void show();

    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    public void render(float delta)
    {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0,0);
        stage.act();
        stage.draw();
    }

}
