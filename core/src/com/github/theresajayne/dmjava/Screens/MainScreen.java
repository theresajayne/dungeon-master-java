package com.github.theresajayne.dmjava.Screens;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class MainScreen extends BaseScreen {
    @Override
    public void show() {
        stage = new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT));
    }
}
